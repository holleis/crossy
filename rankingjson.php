<?php

/*
 * Retrieves JSON data from local DB (updated daily from Ophardt)
 */

//ignore_user_abort(true);
//set_time_limit(5000);

$servername = "localhost";
$username = "d022d814";
$password = "azwm6Jrz3fZnDJNt";
$dbname = "d022d814";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$conn->set_charset("utf8");

//$cat = $_GET["catname"];

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Headers: Content-Type");
header("Content-Type: application/json; charset=utf-8'");
// Disable caching
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies
header("Content-Disposition: attachment; filename=ranking_" . date('Ymd') . ".json");

// for test only:
//$cats = ["DCV Ü40 Herren"];
$cats = ["DCV Damen", "DCV Damen Doppel", "DCV Open", "DCV Mixed Doppel", "DCV Open Doppel", "DCV U12 Männlich", "DCV U12 Weiblich", "DCV U14 Männlich", "DCV U14 Weiblich", "DCV U16 Männlich", "DCV U16 Weiblich", "DCV U18 Männlich", "DCV U18 Weiblich", "DCV Ü35 Damen", "DCV Ü40 Damen", "DCV Ü50 Damen", "DCV Ü40 Herren", "DCV Ü50 Herren", 
		 "ICO Female", "ICO Female Doubles", "ICO Mixed Doubles", "ICO O35 Female", "ICO O40 Female", "ICO O40 Male", "ICO O50 Female", "ICO O50 Male", "ICO Open Division", "ICO Open Doubles", "ICO U12 Female", "ICO U12 Male", "ICO U14 Female", "ICO U14 Male", "ICO U18 Female", "ICO U18 Female Doubles", "ICO U18 Male", "ICO U18 Male Doubles"];

print("[");
$firstcat = true;
foreach ($cats as $cat) {
	// because of Umlaute etc. in category name
//	$cat = utf8_encode($cat);

	if (!$firstcat) {
		print(",");
	}
	$firstcat = false;
	$lastupdated = "1900-01-01";

	// for import / query of past states:
	// 				WHERE category = \"$cat\"                      AND updated < \"2018-03-01\"

	// returns only latest entry per name
	$sql = "SELECT R.rank, R.name, R.points, R.updated FROM `Ranking` R
	   JOIN (SELECT name, max(updated) as updated
				FROM `Ranking`
				WHERE category = \"$cat\"
				GROUP BY name) R2 ON R2.name = R.name
									AND R2.updated = R.updated
	WHERE R.`category` = \"$cat\"
	ORDER BY `R`.`points` DESC";

	//$sql = "SELECT rank, name, points FROM `Ranking` WHERE `category` = \"$cat\" ORDER BY points DESC";
	//print("sql=$sql<br/>");

	$result = $conn->query($sql);
	//print("rows=" . $result->num_rows . "<br>");

	if ($result === FALSE) {
		echo "Error: " . $sql . "<br>" . $conn->error . '<br/>';
	} else {
		foreach ($result as $row) {
			if ($row['updated'] > $lastupdated) {
				$lastupdated = $row['updated'];
			}
		}

		print("{\"category\":\"$cat\", \"lastupdate\":\"" . $lastupdated . "\", \"players\":[");
			$first = true;
		foreach ($result as $row) {
			unset($row['updated']);
			if (!$first) {
				print(",");
			}
			$first = false;
			print json_encode($row);
		}
		print("]}");
	}
}
print("]");


$conn->close();
