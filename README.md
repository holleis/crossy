### set players' rank to 0 if rank === 1 and points === 0
db.rankings.updateMany({rank: 1, points: 0}, {$set: {rank: 0}})

## change club of a player
db.players.updateOne({last_name: 'SCHMIDT', first_name: 'Christian'}, {$set: {'clubs.0': ObjectId('5ab417efa2a306f03507a15b')}})


### add category (e.g. DCV Ü40 Open Doppel)

## server
/home/crossy2/mongo_inst/bin/mongo localhost:21122/Crossy
db.categories.insertOne({"federation":"DCV","name":"DCV Ü40 Open Doppel","type":"doubles","gender":0,"minimum_age":40,"maximum_age":999})
# and add to loaddb_categories.sh



### Rankings

- Rankings are manually updated because there is no mechanism to freeze / roll back to a seeding deadline (role=Paul, My Home -> "Update Rankings")



### Local development

## Server
- start MongoDB
cd ~/Documents/Projects/crossy/src/server/
node ../../node_modules/nodemon/bin/nodemon.js server.js

## App
cd Documents/Projects/crossy/
ng serve --port 9876 --ssl --ssl-key "src/server/sslcert/privkey.pem" --ssl-cert "src/server/sslcert/fullchain.pem"


# Deploy

ng build --base-href "https://crossy.paul-holleis.de/" --configuration=uber-prod-de
rsync -avz dist/de/ crossy2@klemola.uberspace.de:/var/www/virtual/crossy2/html/


# Errors

ERROR in ../node_modules/@types/node/globals.d.ts:139:11 - error TS2430: Interface 'NodeRequire' incorrectly extends interface 'Require'.
  Types of property 'extensions' are incompatible.
    Type 'NodeExtensions' is not assignable to type 'RequireExtensions'.
      Index signature is missing in type 'NodeExtensions'.

139 interface NodeRequire extends NodeJS.Require {}
              ~~~~~~~~~~~
../node_modules/@types/node/globals.d.ts:141:11 - error TS2430: Interface 'NodeModule' incorrectly extends interface 'Module'.
  Types of property 'require' are incompatible.
    Type 'NodeRequireFunction' is missing the following properties from type 'Require': resolve, cache, extensions, main

141 interface NodeModule extends NodeJS.Module {}
              ~~~~~~~~~~

=> install @types/node version 12.x.x
