<?php
 header('Content-Type: text/plain');
 header('Access-Control-Allow-Origin: *');

#include <curl/curl.h>

ignore_user_abort(true);
set_time_limit(5000);
	
$ctx = stream_context_create(array('http'=>
    array(
        'timeout' => 1200,  //1200 Seconds is 20 Minutes
    )
));


$last_name = $_GET["last_name"];
$first_name = $_GET["first_name"];


//print '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';

$BASE = 'https://crossminton.ophardt.online';

$loginpage = $BASE . '/login_check';
$postdata = '_username=paul&_password=speeden&_remember_me=on&_target_path=/en/home';

//initial request with login data


$redirect = '';

function HandleHeaderLine( $curl, $header_line ) {
	global $redirect;
	
    echo "<br/>Header: ".$header_line; // or do whatever
	
	print("<br/>&nbsp;" . substr($header_line, 0, strlen("Location: ")));
	if (substr($header_line, 0, strlen("Location: ")) === "Location: ") {
		$redirect = trim(substr($header_line, strlen("Location: ")));
		print(" --- found redirect: @" . $redirect . "@");
	}
	
    return strlen($header_line);
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_NOBODY, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

curl_setopt($ch, CURLOPT_URL, $loginpage);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($ch, CURLOPT_ENCODING, "");

curl_setopt($ch, CURLOPT_USERAGENT,
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
curl_setopt($ch, CURLOPT_REFERER, 'https://crossminton.ophardt.online/en/login');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//curl_setopt($ch, CURLOPT_COOKIESESSION, true);
curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
curl_setopt($ch, CURLOPT_COOKIEJAR, './tmp/cookie4.txt');  //could be empty, but cause problems on some hosts
curl_setopt($ch, CURLOPT_COOKIEFILE, './tmp/cookie4.txt');  //could be empty, but cause problems on some hosts

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

$answer = curl_exec($ch);
if (curl_error($ch)) {
    echo "error: ";
	echo curl_error($ch);
}

// print("<p>login maybe successful</p>");


// other requests preserve the session

// change role to Verein manager
$filename = $BASE . '/en/role/2576/select/2832';
curl_setopt($ch, CURLOPT_POST, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, "");
//curl_setopt($ch, CURLOPT_HEADERFUNCTION, "HandleHeaderLine");
curl_setopt($ch, CURLOPT_URL, $filename);

$answer = curl_exec($ch);
if (curl_error($ch)) {
    echo "error: ";
    echo curl_error($ch);
}



$filename = $BASE . '/en/athletes/lookup?search='. $last_name . '&searchfirst=' . $first_name;
// print('now getting ' . $filename);
curl_setopt($ch, CURLOPT_URL, $filename);

$answer = curl_exec($ch);
if (curl_error($ch)) {
    echo "error: ";
    echo curl_error($ch);
}

// echo "<html><head></head><body>";
// print("<h1>Found:</h1><xmp>");
// print_r($answer);
// print("</xmp>");
// echo "</body>";

// parse result from
//
//    <td>ESCUDERO GONZALEZ Hugo</td>
//    <td width="60">MEX</td>
//    <td width="40">LS</td>
//    <td width="250">TSV Poing Speed Fires</td>
//    <td width="100">Dec 5, 1973</td>
//    <td width="40">M</td>
//

$lines = explode('<td width="', $answer);
// print_r($lines);
// print('len = ' . count($lines));
for ($r = 1; $r < count($lines) -1; $r++) {
	$line = $lines[$r];
	//print($line);
	// 100">Dec 5, 1973</td>
	if (substr($line, 0, 5) === '100">') {
		$result = substr($line, 5, strpos($line, '</td>') -5);
		print('{"bd":"' . $result . '"}');
		break;
	}
}

curl_close($ch);
