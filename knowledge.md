# Update
`npm-upgrade`
`npm install`

https://github.com/npm/npm/issues/17444: if ENOENT: remove package-lock.json
for specific package versions, e.g.
`npm install typescript@'>=2.4.2 <2.5.0'`

## from Angular 14 to 15
- update Angular core
`ng update @angular/core@15 @angular/cli@15`
git commit, git push

- update material
`ng update @angular/material@15`
git commit, git push

- move @keyframes declaration to styles.scss https://angular.io/guide/update-to-version-15#v15-bc-03

- use new material stuff
`ng generate @angular/material:mdc-migration`
https://rc.material.angular.io/guide/mdc-migration

- remove all component decorators called "moduleId: module.id,"

- check all // TODO(mdc-migration) entries
  
- !!! *use no or a default browserslist config; https://stackoverflow.com/a/75434738/13171715* !!!


# Run

## Windows

### MongoDB database
`c:\Progs\Develop\mongodb\bin\mongod.exe --port 21121 -dbpath "D:\My Documents\private\Projects\mondo_data"`

### server serving the API to the MongoDB
`d: && cd "d:\My Documents\private\Projects\crossy\crossy\src\server\"`
`npx nodemon server.js`

or (with debugging enabled, see https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27)
`npx nodemon --inspect server.js`
running on port 65127

### main App
`d: && cd "d:\My Documents\private\Projects\crossy\crossy"`
`ng serve --port 8080 --ssl 1 --ssl-key "./src/server/sslcert/privkey.pem" --ssl-cert "./src/server/sslcert/fullchain.pem"`

debug with devtools how to save changes to filesystem: https://developers.google.com/web/tools/chrome-devtools/workspaces/


## OSX

### MongoDB database
To have launchd start mongodb now and restart at login:
`brew services start mongodb`

Or, if you don't want/need a background service you can just run:
`mongod --config /usr/local/etc/mongod.conf --port 21121`

### server serving the API to the MongoDB
`npx nodemon server.js`
or (with debugging enabled, see https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27)
`npx nodemon --inspect server.js`
running on port 65127

### main App
`d: && cd "d:\My Documents\private\Projects\crossy\crossy"`
`ng serve --port 8080 --ssl 1 --ssl-key "./src/server/sslcert/privkey.pem" --ssl-cert "./src/server/sslcert/fullchain.pem"`



## UNIX SERVER

### MongoDB database
`./mongodb_inst/bin/mongod --dbpath /home/dcv/mongo_data --port 21121`

### server serving the API to the MongoDB
`nodemon server.js`

### main App
`ng serve --port 8080 --ssl 1 --ssl-key "./src/server/sslcert/privkey.pem" --ssl-cert "./src/server/sslcert/fullchain.pem"`

### UPDATE ON UBERSPACE SERVER
`cd /home/crossy2/crossy-server`
`./deploy`

check with
`tail -f /home/dcv/service/crossyServer/log/main/current`
if it was overwritten, change globals.ts
`nano src/app/globals.ts`

### update web app on server
`cd /crossy`
`./deploy`

old way:
`node_modules/.bin/ng build --base-href "https://crossy.paul-holleis.de/"`

`node_modules/.bin/ng build --base-href "https://crossy.paul-holleis.de/" --configuration=uber-dev`
or prod
`node_modules/.bin/ng build --base-href "https://crossy.paul-holleis.de/" --configuration=uber-prod`
`cp -r dist/* /var/www/virtual/crossy2/html`

OR local build and transfer
`ng build --configuration=production --base-href "https://crossy.paul-holleis.de/"`
in crossy directory
`scp -r dist/* crossy2@klemola.uberspace.de:/var/www/virtual/crossy2/html`


server process daemon:
`NODE_ENV=uber exec /package/host/localhost/nodejs-9.2.0/bin/node /home/dcv/crossy/crossy/src/server/server.js 2>&1`




Offline
`npm run serve-prod-ngsw`


# transfer mongoDB
`~/mongodb_inst/bin/mongodump --port 21121 --db=Crossy --gzip --archive=crossydb.zip`
`c:\Progs\Develop\mongodb\bin\mongodump --port 21121 --db=Crossy --gzip --archive=crossydb.zip`
`~/mongodb_inst/bin/mongorestore --port 21121 --gzip --archive=crossydb.zip`
OR replacing all collections with --drop
`~/mongodb_inst/bin/mongorestore --port 21121 --gzip --drop --archive=crossydb.zip`

## WINDOWS
`c:\Progs\Develop\mongodb\bin\mongorestore.exe --port 21121 --gzip --drop --archive=crossydb_20180410.zip`

subscribe / unsubscribe, cancel HTTP request, HTTPClient
https://stackoverflow.com/questions/38008334/angular-rxjs-when-should-i-unsubscribe-from-subscription/41177163#41177163


https://stackoverflow.com/questions/34671715/angular2-http-get-map-subscribe-and-observable-pattern-basic-understan/34672550

http://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#findOne
https://mongodb.github.io/node-mongodb-native/api-generated/mongoclient.html
https://mongodb.github.io/node-mongodb-native/api-generated/db.html

https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications
https://scotch.io/tutorials/mean-app-with-angular-2-and-the-angular-cli


https://coryrylan.com/blog/angular-ng-for-syntax
https://stackoverflow.com/questions/36388270/angular-2-how-to-apply-limit-to-ngfor
https://stackoverflow.com/questions/43006550/how-to-use-ngif-else-in-angular-4

https://stackoverflow.com/questions/30944763/typescript-looping-trough-class-type-properties

https://stackoverflow.com/questions/14212527/how-to-set-default-value-to-the-inputtype-date

https://github.com/angular/angular-cli/wiki

https://scotch.io/tutorials/mean-app-with-angular-2-and-the-angular-cli

https://stackoverflow.com/questions/9122966/mongodb-rename-database-field-within-array

JOIN for MongoDB:
https://www.w3schools.com/nodejs/nodejs_mongodb_join.asp

MongoDB API
https://docs.mongodb.com/manual/reference/method/db.collection.renameCollection/

Express.js und HTTPS/SSL
https://medium.com/@yash.kulshrestha/using-lets-encrypt-with-express-e069c7abe625

Observer, Subject, pass values between components
https://stackoverflow.com/questions/34376854/delegation-eventemitter-or-observable-in-angular

Dynamically set CSS classes
The key is a CSS class name (e.g. disabled) and the value is an expression from our class (e.g. isSubmitting) that returns a boolean value (i.e. either truthy or falsy).
[ngClass]="{ 'disabled': isSubmitting,

*ngIf with else
<div *ngIf="isValid; else templateName">
    If isValid is true
</div>

<ng-template #templateName>
    If isValid is false
</ng-template>

*ngIf with local variable
<div *ngIf="condition as value; else elseBlock">{{value}}</div>
<ng-template #elseBlock>...</ng-template>

Hide spinner in input fields
https://stackoverflow.com/questions/3790935/can-i-hide-the-html5-number-input-s-spin-box
https://stackoverflow.com/questions/23372903/hide-spinner-in-input-number-firefox-29

- wait until all requests are finished
  https://stackoverflow.com/questions/37172093/q-all-for-angular2-observables
    const obsvbls = [];
    for (let i = 0; i < categories.length; i++) {
        obsvbls.push(this.rankingService.getRanking(categories[i].name));
    }
    Observable.forkJoin(obsvbls).subscribe(values => {
        for (let i = 0; i < values.length; i++) {
            this.rankings.push(values[i]['result']);
        }
        logger.debug('all rankings retrieved', this.rankings);
    });

- Retrieve latest entries
  https://stackoverflow.com/questions/29772575/aggregate-mongodb-by-latest-timestamp

- populate sub documents (array)
  https://stackoverflow.com/questions/12770682/cant-get-mongoose-js-subdocument-array-to-populate

- create an Observable out of static data
  https://stackoverflow.com/questions/35219713/how-to-create-an-observable-from-static-data-similar-to-http-one-in-angular

- check for undefined
  typeof myvar === "undefined"

- generate component / service / etc. in current directory
  ng g component COMPONENTNAME --flat 1 --module=app.module
  ng generate component coffee --flat 1 --module=../app.module.ts

- listen to keystrokes (e.g. for filtering)
  https://blog.thoughtram.io/angular/2016/01/06/taking-advantage-of-observables-in-angular2.html

- mat-select for large lists with autocomplete
  https://stackblitz.com/edit/angular-v1b716?file=app%2Fautocomplete-overview-example.html



# Fix Errors:
- Pipe <port> is already in use
  lsof -i tcp:3000
  kill -9 PID

- "Uncaught reflect-metadata shim is required when using class decorators"
  https://stackoverflow.com/questions/30911861/angular2-jspm-io-reflect-metadata-shim-is-required-when-using-class-decorato

- Can't bind to 'formGroup' since it isn't a known property of 'form'.
  https://stackoverflow.com/questions/39152071/cant-bind-to-formgroup-since-it-isnt-a-known-property-of-form

- localtunnel invalid headers
  https://github.com/angular/angular-cli/issues/6349
  https://github.com/angular/angular-cli/issues/6070

- does not work in IE
  https://stackoverflow.com/questions/46036097/angular-4-error-in-ie11

- mongodbimport (use --jsonArray):
  https://stackoverflow.com/questions/30380751/importing-json-from-file-into-mongodb-using-mongoimport

- mongoJS: Topology was destroyed
  ensure that db.close() is called within the callback
  use mongoist instead of mongojs

- Topology was destroyed
  in the server api.js, tried a
    const documents = await db.CurrentRankings.find();
  and suddenly it worked again ...... :-(

- test using wget
  wget --no-check-certificate --post-data="{\"user\": {\"username\": \"sami\", \"password\":\"pass\", \"first_name\":\"Samuel\",\"last_name\":\"Holleis\"}}" --header="Content-Type: application/json;charset=utf-8" https://localhost:62187/users/register

- routes not protected with JWT
  check order of app.use(expressJwt({... and the app.get or app.use('/api... calls
  https://github.com/auth0/express-jwt/issues/74

- module not found, deep imports /src/... no longer supported
  https://github.com/angular/angular/issues/15542

  - ng serve uses a lot of CPU (=> install fsevents on OSX)
    https://github.com/angular/angular-cli/issues/2748

- 'require' not found; 'exports' does not exist
 according to
 https://github.com/angular/angular-cli/issues/2221 and
 https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/node/index.d.ts
 adapted typings.d.ts to contain
 interface NodeRequireFunction {
 	(id: string): any;
 }

 interface NodeExtensions {
     '.js': (m: NodeModule, filename: string) => any;
     '.json': (m: NodeModule, filename: string) => any;
     '.node': (m: NodeModule, filename: string) => any;
     [ext: string]: (m: NodeModule, filename: string) => any;
 }

 interface NodeRequire extends NodeRequireFunction {
 	resolve(id: string): string;
 	cache: any;
 	extensions: NodeExtensions;
 	main: NodeModule | undefined;
 }

 declare var require: NodeRequire;

 interface NodeModule {
 	exports: any;
 	require: NodeRequireFunction;
 	id: string;
 	filename: string;
 	loaded: boolean;
 	parent: NodeModule | null;
 	children: NodeModule[];
 }

 declare var module: NodeModule;

- when using .populate in mongoose
 MissingSchemaError: Schema hasn't been registered for model "xxx"
 Answer about multiple connections in:
 https://stackoverflow.com/questions/26818071/mongoose-schema-hasnt-been-registered-for-model

- UnauthorizedError: No authorization token was found
  probably misspelled API endpoint or forgotten parameter: cannot find it, uses another protected one

- strangely 2 different objects are the same in the Mongo DB
  in roleapi.js: declared mIds const; otherwise, 2 separate calls used the same (first) instance ?!?
  const mIds = new Array(ko.matches.length);

- strangely else case in *ngIf doesn't work
  name every *ngIf="...; else xyz" xyz differently for the <ng-templae #xyz>

- *ngIf in <ng-template [ngIf]="">

- syntax error in ng generate: validate angular.json (e.g. https://jsonlint.com/)

- update one field of all documents with lowercase letters:
  db.users.find().forEach(function(u) {db.users.updateMany({email: u.email}, {$set: {email: u.email.toLowerCase()}})})

- endless loop with ngFor in mat-option
  options if ngFor must be backed by a variable, cannot be a this.properties.map(...)

- ngfor with ng-template: <ng-template ngFor let-item [ngForOf]="items" let-i="index" [ngForTrackBy]="trackByFn">



### Setup

npm install @angular/material @angular/cdk
npm install hammerjs

npm install mongoose

npm install lodash.pick
npm install lodash.omit

no:
npm install moment
npm i @angular/material-moment-adapter

for better client-side logging
npm install ngx-logger

Offline support (https://coryrylan.com/blog/fast-offline-angular-apps-with-service-workers)
npm install --save-dev sw-precache


# Install

## OSX: https://docs.npmjs.com/getting-started/installing-node
brew install node
brew install mongodb

## (sudo npm install -g @angular/cli)
sudo npm install --save-dev @angular/cli@latest
EACCESS error: https://docs.npmjs.com/getting-started/fixing-npm-permissions
(I had to manually create some directories, e.g. sudo mkdir /usr/local/lib/node_modules/angular-cli/node_modules/node-sass/.node-gyp/
also try npm install to fix)

## OSX
ssl certificates for localhost: https://stackoverflow.com/questions/8169999/how-can-i-create-a-self-signed-cert-for-localhost
SAN: https://www.endpoint.com/blog/2014/10/30/openssl-csr-with-alternative-names-one
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout privkey.pem -out fullchain.pem -config ./openssl.cnf

in one line:
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout privkey.pem -out fullchain.pem \
    -subj "/C=DE/ST=Bavaria/O=Me/CN=localhost" \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /etc/ssl/openssl.cnf \
              <(printf "[SAN]\nsubjectAltName=DNS.1:localhost,DNS.2:www.localhost"))

# In Keychain Access, double-click on this new localhost cert. Expand the arrow next to "Trust" and choose to "Always trust".
# copy as fullchain.pem and privkey.pem to src/app/server/sslcert directory

# Windows: use Git Bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout privatekey.key -out certificate.crt
# copy as fullchain.pem and privkey.pem to src/app/server/sslcert directory
# import as Trusted Root Certification Authorities using Windows Manage Computer Certificates (Action -> All Tasks -> Import)

# for the server serving the API to the MongoDB
npm install --save-dev nodemon
# small helper for Express HTTPS https://helmetjs.github.io/
npm install helmet --save
# for file uploads
npm install express-fileupload
# start with npx nodemon server.js

# in angular project directory
node_modules/.bin/ng build
# ng build ERROR ERROR in Error: No NgModule metadata found for 'AppModule'.
# first try a
node_modules/.bin/ng build --prod
# ng build --prod ERROR Error: TypeError: Cannot read property 'flags' of undefined
npm install typescript@latest
# potentially use --base-href
# https://coursetro.com/posts/code/64/How-to-Deploy-an-Angular-App-(Angular-4)
node_modules/.bin/ng build --base-href "https://www.crossy.paul-holleis.de/"
cp dist/* /home/dcv/html/


# mongoDB
mkdir $HOME/data
mkdir $HOME/data/db
test -d ~/service || uberspace-setup-svscan
## NO: uberspace-setup-service startMongoDB mongod --dbpath /home/dcv/data/db
## https://wiki.uberspace.de/database:mongodb
## but this is too old
## install according to https://docs.mongodb.com/manual/tutorial/install-mongodb-on-linux/
## using https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-4.0.2.tgz (no SSL!)
uberspace-setup-mongodb
# Hostname: localhost
# Portnum#: 21121
# Username: dcv_mongoadmin
# Password: Ahtie5Iesh
# To connect on the shell, you can use:
# mongo admin --port 21121 -u dcv_mongoadmin -p
/home/dcv/mongodb_inst/bin/mongod --port 21121 --dbpath /home/dcv/mongo_data

#MongoDB import
\progs\Develop\mongodb\bin\mongo.exe Crossy --port 27017 --host localhost
\progs\Develop\mongodb\bin\mongoimport.exe --jsonArray /d Crossy /c ranking players.json
# MongoDB restore (after mongodump)
c:\Progs\Develop\mongodb\bin\mongorestore.exe /port:21121 "D:\My Documents\private\Projects\crossy\dump"

# OSX etc.
mongodump -d Crossy --port 21121 --gzip --archive=mongodb.zip
mongorestore --port 21121 --gzip --archive=mongodb.zip


# from Angular 4 to 5
# https://angular-update-guide.firebaseapp.com/
# if error e.g. npm ERR! notarget No matching version found for @angular/common@'5.0.0'
npm install typescript@2.4.2 --save-exact
npm install @angular/common@latest @angular/compiler@latest @angular/compiler-cli@latest @angular/core@latest @angular/forms@latest @angular/http@latest @angular/platform-browser@latest @angular/platform-browser-dynamic@latest @angular/platform-server@latest @angular/router@latest @angular/animations@latest
npm install rxjs@^5.5.2
npm install typescript@latest

# MongoJS driver (don't use mongodb node.js driver)
https://github.com/mafintosh/mongojs


# uberspace domain
uberspace-add-domain -d x.paul-holleis.de -w
# add A / AAAA entry in DNS for x.paul-holleis.de (kontent.de, DNS Plus)


### Clean deploy
rm -f /var/www/virtual/dcv/crossy.paul-holleis.de/index.html /var/www/virtual/dcv/crossy.paul-holleis.de/main.* /var/www/virtual/dcv/crossy.paul-holleis.de/polyfills.* /var/www/virtual/dcv/crossy.paul-holleis.de/runtime.* /var/www/virtual/dcv/crossy.paul-holleis.de/scripts.* /var/www/virtual/dcv/crossy.paul-holleis.de/styles.* /var/www/virtual/dcv/crossy.paul-holleis.de/vendor.js*
scp dist/index.html dist/main.*.js dist/polyfills.*.js dist/runtime.*.js dist/scripts.*.js dist/styles.*.css dcv@grus.uberspace.de:/var/www/virtual/dcv/crossy.paul-holleis.de/
cp -r dist/* /var/www/virtual/dcv/crossy.paul-holleis.de/




### IMPORT DATA FROM OPHARDT
# see separate file ophardt_import.txt in DropBox


##### TEST
# POST
curl -H "Content-Type: application/json" -k --data '{"title":"Poinger Jubiläums Cup", "start_date":"2018-02-18 08:00", "nation":"GER", "federation":"DCV", "points":100}' https://localhost:62187/api/events
curl -H "Content-Type: application/json" -k --data '{"title": "4th Speedminton ICO DCV Poing Open", "start_date": "Sat Sep 29 2018 00:00:00 GMT+0200 (CEST)", "end_date": "Sun Sep 30 2018 00:00:00 GMT+0200 (CEST)", "location": "Poing", "type": "tournament"}' https://localhost:62187/api/tournaments
# PUT
curl -X PUT -H "Content-Type: application/json" -k --data '{"name":"oldesthouse","bla":"blu"}' https://localhost:62187/api/events/5a81b5704ade316330d2aa2e
# GET
curl -H "Content-Type: application/json" -k https://localhost:62187/api/events



##### regexp
# replace 2 .insert with one update
db.certifications.insert\({"owner": null,"label": ("[^"]*").*\ndb.certifications.insert\({"owner": null,"label": ("[^"]*"),("abbrev": "([^"]*)")?.*
db.certifications.update({label: $1}, {$set: {label: $2, abbrev: "$4"}})

# replace varietal definition with 2 synonyms with 3 entries with each other as synonyms
(.*)variety: '([^']*)'(.*)synonyms: \['([^']*)', '([^']*)'\](.*)
$1variety: '$2'$3synonyms: ['$4', '$5']$6\n$1variety: '$4'$3synonyms: ['$2', '$5']$6\n$1variety: '$5'$3synonyms: ['$2', '$4']$6


##### MunichCup

# Correct dates
db.tournamentdata.find({tournament: ObjectId("5bdef8cc4d27575aec5621a1")}, {groups: 1, "groups.name": 1})
db.tournamentdata.update({tournament: ObjectId("5bdef8cc4d27575aec5621a1")}, {$set: {"groups.1.name": "1;17.12.2018;13.01.2019"}})



### Add new year for DM Quali Ranking:

- Update year in app.component.html

        `<a mat-list-item routerLink="/dmquali" [queryParams]="{year: 2022}" routerLinkActive="active" i18n="menu, DM Qualification ranking link@@DM Qualification">DM Qualification</a>`

- Add to link list in list.component.html

        `<a mat-list-item routerLink="/dmquali" [queryParams]="{year: 2022}" routerLinkActive="active">
        <ng-container i18n="menu, DM Qualification ranking link@@DM Qualification">DM Qualification</ng-container> 2022
        </a> &mdash;`

- Add to DB:

        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Damen", "cat_type" : "singles", "gender" : 1, "minimum_age" : 0, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Open", "cat_type" : "singles", "gender" : 0, "minimum_age" : 0, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Ü40 Damen", "cat_type" : "singles", "gender" : 1, "minimum_age" : 40, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Ü50 Damen", "cat_type" : "singles", "gender" : 1, "minimum_age" : 50, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Ü60 Damen", "cat_type" : "singles", "gender" : 1, "minimum_age" : 60, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Ü40 Herren", "cat_type" : "singles", "gender" : 2, "minimum_age" : 40, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Ü50 Herren", "cat_type" : "singles", "gender" : 2, "minimum_age" : 50, "maximum_age" : 999, "active" : true})
        db.categories.insertOne({"federation" : "DCV", "name" : "DM Quali 2024 Ü60 Herren", "cat_type" : "singles", "gender" : 2, "minimum_age" : 60, "maximum_age" : 999, "active" : true})



### query to get all GER players in rankings updated >=2023 that have isDCV false

[
  {
    $lookup:
      /**
       * from: The target collection.
       * localField: The local join field.
       * foreignField: The target join field.
       * as: The name for the results.
       * pipeline: Optional pipeline to run on the foreign collection.
       * let: Optional variables to use in the pipeline field stages.
       */
      {
        from: "players",
        localField: "player",
        foreignField: "_id",
        as: "player",
      },
  },
  {
    $addFields:
      /**
       * newField: The new field name.
       * expression: The new field expression.
       */
      {
        player: {
          $first: "$player",
        },
      },
  },
  {
    $match:
      /**
       * query: The query in MQL.
       */
      {
        "player.nation": "GER",
        updated: {
          $gt: "2023",
        },
        "player.isDCV": {
          $ne: true,
        },
      },
  },
  {
    $lookup:
      /**
       * from: The target collection.
       * localField: The local join field.
       * foreignField: The target join field.
       * as: The name for the results.
       * pipeline: Optional pipeline to run on the foreign collection.
       * let: Optional variables to use in the pipeline field stages.
       */
      {
        from: "clubs",
        localField: "player.clubs",
        foreignField: "_id",
        as: "clubs",
      },
  },
  {
    $project:
      /**
       * specifications: The fields to
       *   include or exclude.
       */
      {
        tournament: 1,
        updated: 1,
        club: {
          $first: "$clubs",
        },
        player: 1,
      },
  },
  {
    $project:
      /**
       * specifications: The fields to
       *   include or exclude.
       */
      {
        tournament: 1,
        updated: 1,
        club: "$club.name",
        player: {
          $concat: [
            "$player.first_name",
            " ",
            "$player.last_name",
          ],
        },
      },
  },
]






# use ICO Ranking for draw

- download and store ICO ranking from TS

- tournament.component.ts
  - uses tournamentsService.getEnrollments
    - calls openapi.js - getPlayerRanking
  - uses this.rankingService.getPlayerRanking for playersToUpdatePoints
    - uses openapi getPlayerRanking to look up latest entry in Ranking DB collection
  - setData executes getPlayerRanking for playersToUpdatePoints
- bracket.component.ts
  - makeDraw
  - sortByRank
  - assignToGroups, createGroupMatches