<?php
 header('Content-Type: application/json');
 header('Access-Control-Allow-Origin: *');
/*
 * Retrieves (HTML) data from Ophardt
 */

ignore_user_abort(true);
set_time_limit(5000);
	
$ctx = stream_context_create(array('http'=>
    array(
        'timeout' => 1200,  //1200 Seconds is 20 Minutes
    )
));

$errors = array();

// ICO Ophardt information

// replace with Ophart link
//$filename = "https://my-tonino.com/cross/ophardt/ophardt.calendar.html";
$filename = "https://crossminton.ophardt.online/en/calendar?date-from=2018-01-01&date-to=&title=&city=&nation=";

// get data from Ophardt
$html = file_get_contents($filename, false, $ctx);

print('{"events": [');


// parse data from HTML
if ($html) {
	$tables = explode('<table', $html, 2);
	$table = $tables[1];

	$rows = explode('<tr', $table);
	// remove header row and everything before the first <tr
	array_shift($rows);
	array_shift($rows);

	$counter = 0;

	foreach ($rows as $row) {
		$info = explode('<td', $row);

		// active
		// invitation
		// inscriptions
		// open for
		// Date
		// Nation
		// location
		// Title National
		// Age
		// Gender

		$defaultValue = '""';
		
		$matches = array();
		$invitation = $defaultValue;
		if (preg_match('/<a href="([^"]+)"/', $info[2], $matches) == 1) {
			$invitation = $matches[1];
		}
		$matches = array();
		$inscriptions = $defaultValue;
		if (preg_match('/<a href="([^"]+)"/', $info[3], $matches) == 1) {
			$inscriptions = $matches[1];
			if (substr($inscriptions, 0, 4) !== 'http') {
				$inscriptions = 'https://crossminton.ophardt.online' . $inscriptions;
			}
		}
		$links = $invitation;
		if ($inscriptions !== $defaultValue) {
			if ($invitation === $defaultValue) {
				$links = $inscriptions;
			} else {
				$links .= ', ' . $inscriptions;
			}
		}
		
		$matches = array();
		$startdate = "";
		$enddate = $defaultValue;
		// title="Jan 27, 2018 - Jan 28, 2018"
		if (preg_match('/title="([^"]+)"/', $info[5], $matches) == 1) {
			$date = $matches[1];
			if (strpos($date, ' - ') === FALSE) {
				$startdate = $date;
				$enddate = $startdate;
			} else {
				$pos = strpos($date, ' - ');
				$startdate = substr($date, 0, $pos);
				$enddate = substr($date, $pos+3);
			}
			// // parse date and convert into seconds from epoch
			// if (($startdate = strtotime($startdate)) === false) {
			// 	$startdate = $defaultValue;
			// }
			// if (($enddate = strtotime($enddate)) === false) {
			// 	$enddate = $defaultValue;
			// }

			// parse date (UTC) and format according to crossy
			$startdate = DateTime::createFromFormat('!M j, Y', $startdate, new DateTimeZone('UTC'));
			//$startdate = $startdate->format('d.m.Y');
			$startdate = $startdate->format(DateTime::ATOM); // ==ISO

			$enddate = DateTime::createFromFormat('!M j, Y', $enddate, new DateTimeZone('UTC'));
			//$enddate = $enddate->format('d.m.Y');
			$enddate = $enddate->format(DateTime::ATOM);
		}
		$matches = array();
		$federation = $defaultValue;
		if (preg_match('/title="([^"]+)"/', $info[6], $matches) == 1) {
			$federation = html_entity_decode($matches[1], ENT_NOQUOTES | ENT_HTML5, "UTF-8");
		}
		$nation = $defaultValue;
		if (preg_match('/">([^<]+)</', $info[6], $matches) == 1) {
			$nation = html_entity_decode($matches[1], ENT_NOQUOTES | ENT_HTML5, "UTF-8");
			$nation = substr($nation, 0, 3);
		}
		$matches = array();
		$location = $defaultValue;
		if (preg_match('/title="([^"]+)"/', $info[7], $matches) == 1) {
			$location = html_entity_decode($matches[1], ENT_NOQUOTES | ENT_HTML5, "UTF-8");
			//print(strlen($matches[1]) . ": $matches[1] /// $location /// ");
		}
		$matches = array();
		$points = $defaultValue;
		if (preg_match('/title="([^"]+)"/', $info[8], $matches) == 1) {
			$points = html_entity_decode($matches[1], ENT_NOQUOTES | ENT_HTML5, "UTF-8");
		}
		$matches = array();
		$title = $defaultValue;
		if (preg_match('/title="([^"]+)"/', $info[9], $matches) == 1) {
			$title = html_entity_decode($matches[1], ENT_NOQUOTES | ENT_HTML5, "UTF-8");
		}
		$matches = array();
		$ageclasses = $defaultValue;
		if (preg_match('/">\s*([^<]+)</', $info[11], $matches) == 1) {
			$ageclasses = html_entity_decode($matches[1], ENT_NOQUOTES | ENT_HTML5, "UTF-8");
			$ageclasses = trim($ageclasses);
			$cls = explode(" ", $ageclasses);
			$ageclasses = '[';
			foreach ($cls as $cl) {
				$ageclasses .= "\"$cl\","; 
			}
			if (count($cls) > 0) {
				$ageclasses = substr($ageclasses, 0, -1);
			}
			$ageclasses .= ']';
		}
		
		$jsonEvent = "{\"event\": {\"title\": \"$title\", \"start_date\": \"$startdate\", \"end_date\": \"$enddate\", \"federation\": \"$federation\", \"nation\": \"$nation\", \"location\": \"$location\", \"type\": \"tournament\", \"points\": $points, \"ageclasses\": $ageclasses, \"links\": \"$links\"}}";
		print("$jsonEvent, ");
		$counter++;
	}


} else { // if ($html)
	$errors[] = "Nothing retrieved from Ophardt";
}

print('{}]');
print(', "errors":[');
$errcnt = 0;
foreach ($errors as $error) {
	print("{\"error\": $error}, ");
	$errcnt++;
}
print '{}]}';
