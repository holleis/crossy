export const Globals = Object.freeze({
    BASE_API_URL: 'https://localhost:62187',
    WEBSOCKET_URL: 'ws://127.0.0.1:8764',
    SUB_API_URL: '/api/v1',
    SCRIPTS_BASE_URL: 'https://crossy.paul-holleis.de/de/',
    IMPORT_CALENDAR_PHP_URL: 'retrieveCalendar.php',
    GET_BIRTHDAY_PHP_URL: 'getbirthday.php?',
    SCORESHEET_PHP_URL: 'ss.php?'
 });
