import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MunichcupComponent } from './munichcup.component';

describe('MunichcupComponent', () => {
  let component: MunichcupComponent;
  let fixture: ComponentFixture<MunichcupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MunichcupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MunichcupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
