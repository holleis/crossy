import { Match } from '../../match';
import { PlayerRef } from '../../playerref';

export class Round {
    start?: string;
    end?: string;

    // sorted according to scores
    sortedPlayers: PlayerRef[] = [];
    matches: Match[] = [];

    constructor (start: string, end: string) {
        this.start = start;
        this.end = end;
    }
}
