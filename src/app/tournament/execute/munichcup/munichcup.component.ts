import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EnrollmentInfo } from '../../cat-info';
import { Round } from './round';
import { TournamentsService } from '../../tournaments.service';
import { Tournament } from '../../tournament';
import { MatchdbService } from '../matchdb.service';
import { Group } from '../../group';
import { Match } from '../../match';
import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../../../util/alert/alert.service';
import { PlayerRef } from '../../playerref';
import { UserService } from '../../../auth/_services/user.service';
import { Role } from '../../../user/role';
import { MatDialog } from '@angular/material/dialog';
import { YesNoDialogComponent } from '../../../util/yesno-dialog.component';
import { SocketService, SockEvent, UpdateEvent } from '../../../util/websockets/socket.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
    selector: 'app-munichcup',
    templateUrl: './munichcup.component.html',
    styleUrls: ['./munichcup.component.css'],
})
export class MunichcupComponent implements OnInit {

    @Input() assignmentMode = 'MunichCup';
    @Input() catType = 'singles'; // or doubles
    @Input() tournamentId: string;
    tournament: Tournament;
    @Input() categoryId: string;
    @Input() newDraw: boolean;
    // this is the master array; all other sortedPlayers reference this; don't change the order!
    @Input() allPlayers: EnrollmentInfo['players'] = [];
    @Output() allPlayersChange = new EventEmitter<EnrollmentInfo['players']>();

    rounds: Round[] = [];
    newFrom: moment.Moment;
    newTo: moment.Moment;

    playedCurrentRound: string[] = [];

    // stores who already had a freilos (references the allPlayers array!)
    // TODO doesn't allow removing a round!
    hadFreilos: boolean[] = [];

    isdraw: boolean[][] = [];

    ioConnection: any;

    scorelistener = new Subject<{roundNr: number, match: Match}>();

    myPlayerId;
    myRoles;

    subject = 'MunichCup - Nächste Runde';
    emailToAll = 'Liebe MunichCup-Spieler,\n\n\
    \
    die Auslosung ist beendet, der neue Spieltag beginnt am {{START}} und endet am {{END}}.\n\n\
    Der Link zu der Auslosung unter dem Ihr auch Eure Ergebnisse eintragen könnt ist:\n\n\
    \
    https://crossy.paul-holleis.de/ko;id={{TOURNAMENTID}}\n\n\
    \
    Versucht bitte so bald wie möglich mit Eurem Gegner einen Termin und Ort zu finden.\n\n\
    \
    Bitte sagt doch AUCH über WhatsApp kurz Bescheid, wie Euer Match ausgegangen ist, dann wissen alle gleich, was so los ist!\n\n\
    \
    Euer Charly';

    constructor(
        private tournamentsService: TournamentsService,
        private matchdbService: MatchdbService,
        private logger: NGXLogger,
        private alertService: AlertService,
        private userService: UserService,
        public dialog: MatDialog,
        private socketService: SocketService
    ) {
        const debounce = this.scorelistener.pipe(
            debounceTime(3000),
            distinctUntilChanged(),
            switchMap((newscore: {roundNr: number, match: Match}) => {
                this.logger.trace('score update:', newscore);
                this.onScoreChange(newscore.match);
                return [];
            })
        );
        debounce.subscribe();
    }

    ngOnInit() {
        const user = this.userService.getCurrentUser();
        if (user) {
            this.myPlayerId = user.player_id;
            this.myRoles = user.roles;
        }

        if (this.newDraw) {
            // all start with 0 points

            for (let p = 0; p < this.allPlayers.length; p++) {
                this.allPlayers[p].points = 0;
                this.hadFreilos[p] = false;
            }

            // create first round
            // assume today's date as start, will be corrected later
            const start = moment().format('DD.MM.YYYY');
            const end = moment().add(2, 'weeks').format('DD.MM.YYYY');
            const round = new Round(start, end);
            this.rounds.push(round);

            // a round stores PlayerRef[] to the allPlayers array of real Players
            const roundPlayers = [];
            for (let p = 0; p < this.allPlayers.length; p++) {
                const pRef = new PlayerRef();
                pRef.playerListIdx = p;
                roundPlayers[p] = pRef;
            }
            round.sortedPlayers = roundPlayers;
            this.allPlayersChange.emit(this.allPlayers);

            // get real start date from tournament

            try {
                this.tournamentsService.getTournament(this.tournamentId).subscribe(
                    response => {
                        if (response.success === true) {
                            if (response.result && response.result.start_date) {
                                this.tournament = response.result;
                                this.rounds[0].start = response.result.start_date;
                                const endDate = moment(response.result.start_date, 'DD.MM.YYYY').add(13, 'days');
                                this.rounds[0].end = endDate.format('DD.MM.YYYY');
                                this.newFrom = endDate.clone().add(1, 'days');
                                this.newTo = endDate.clone().add(13, 'days');

                                this.setupRound(0);

                                this.storeToDB();
                                this.updateEmailTemplate();
                            }
                        } else {
                            this.logger.error('could not get tournament', this.tournamentId, response.error);
                        }
                    },
                    error => {
                        this.logger.error('could not get tournament', this.tournamentId, error);
                    }
                );
            } catch (err) {
                this.logger.error('could not get tournament', this.tournamentId, err);
            }

        } else {
            this.readStateFromDB();

            // tslint:disable-next-line: no-bitwise
            if ((this.myRoles & Role.ROLE_DCV) === 0 && (this.myRoles & Role.ROLE_PAUL) === 0) {
                // get tournament to have the responsibles (see getReadOnly())

                try {
                    this.tournamentsService.getTournament(this.tournamentId).subscribe(
                        response => {
                            if (response.success === true) {
                                if (response.result && response.result.responsibles) {
                                    this.tournament = response.result;
                                }
                            } else {
                                this.logger.error('could not get tournament', this.tournamentId, response.error);
                            }
                        },
                        error => {
                            this.logger.error('could not get tournament', this.tournamentId, error);
                        }
                    );
                } catch (err) {
                    this.logger.error('could not get tournament', this.tournamentId, err);
                }
            }
        }

        this.initIoConnection();
    }

    private initIoConnection(): void {
        if (this.ioConnection = this.socketService.initSocket()) {
            // first init, register listeners
            this.socketService.onMessage(
                (msg: UpdateEvent) => {
                    if (msg.tournamentId === this.tournamentId &&
                        this.categoryId === this.categoryId && msg.type === SockEvent.MATCHUPDATE) {

                        const message = msg.message;
                        this.logger.debug('message received', message);
                        try {
                            const match = JSON.parse(message) as Match;
                            if (match.score[0].p1 === -1 && match.score[0].p2 === -1) {
                                match.score[0].p1 = 0;
                                match.score[0].p2 = 0;
                                this.isdraw[match.matchNr.level][match.matchNr.num] = true;
                            } else {
                                this.isdraw[match.matchNr.level][match.matchNr.num] = false;
                            }
                            const round = this.rounds[match.matchNr.level];
                            round.matches[match.matchNr.num].score = match.score;
                            this.updatePlayerPoints();
                        } catch (err) {
                            this.logger.error('could not parse websocket message for matchupdate', message);
                        }
                    }
                }
            );

            this.socketService.onEvent(SockEvent.OPEN, () =>
                this.logger.debug('munichcup connected')
            );

            this.socketService.onEvent(SockEvent.CLOSE, () =>
                this.logger.debug('munichcup disconnected')
            );
        }
    }

    readStateFromDB() {
        this.matchdbService.getTournamentData(this.tournamentId, this.categoryId).subscribe(
            response => {
                if (response.success === true && response.result) {
                    this.allPlayers = response.result.players;
                    const groups = response.result.groups;

                    // restore data from groups

                    for (let g = 0; g < groups.length; g++) {
                        this.isdraw[g] = [];
                        const group = groups[g];
                        const nrStartEnd = group.name; // 0;01.01.2018;15.01.2018
                        const nrStartEndSplit = nrStartEnd.split(';');
                        const round = new Round(nrStartEndSplit[1], nrStartEndSplit[2]);

                        const endDate = moment(nrStartEndSplit[2], 'DD.MM.YYYY');
                        this.newFrom = endDate.clone().add(1, 'days');
                        this.newTo = endDate.clone().add(13, 'days');

                        round.sortedPlayers = group.players;
                        round.matches = group.matches;
                        // restore isdraw state (encoded by -1 score)
                        for (let m = 0; m < round.matches.length; m++) {
                            const mm = round.matches[m];
                            if (mm.score[0].p1 === -1 && mm.score[0].p2 === -1) {
                                this.isdraw[g][m] = true;
                                mm.score[0].p1 = 0;
                                mm.score[0].p2 = 0;
                            }
                        }

                        // read freilos array from winners array

                        for (let f = 0; f < this.allPlayers.length; f++) {
                            if (group.winners[f] && group.winners[f].bye === true) {
                                this.hadFreilos[f] = true;
                            } else {
                                this.hadFreilos[f] = false;
                            }
                        }
                        this.rounds[g] = round;
                    }
                    this.updatePlayerPoints();
                    this.updateEmailTemplate();
                    this.allPlayersChange.emit(this.allPlayers);

                } else if (!response.result) {
                    this.logger.trace('no stored data found for', this.tournamentId);

                } else {
                    this.logger.error('Could not retrieve tournament data:', response.error);
                    this.alertService.error($localize`Could not retrieve tournament data`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve tournament data:', error);
                this.alertService.error($localize`Could not retrieve tournament data`, error);
            }
        );
    }

    storeToDB() {
        // model the rounds as groups and save to DB
        const rounds: Group[] = [];
        for (let r = 0; r < this.rounds.length; r++) {
            const theRound = this.rounds[r];
            // encode the start and end of the round in the group name ...
            const roundModel = new Group(theRound.sortedPlayers.length, r.toString() + ';' + theRound.start + ';' + theRound.end);
            roundModel.players = theRound.sortedPlayers;
            roundModel.matches = JSON.parse(JSON.stringify(theRound.matches));

            // encode isdraw as -1 score
            if (!this.isdraw[r]) {
                this.isdraw[r] = [];
            }
            for (let m = 0; m < roundModel.matches.length; m++) {
                if (this.isdraw[r][m]) {
                    const mm = roundModel.matches[m];
                    mm.score[0].p1 = -1;
                    mm.score[0].p2 = -1;
                } else {
                    this.isdraw[r][m] = false;
                }
            }

            // use winners array to store freilos array

            for (let f = 0; f < this.hadFreilos.length; f++) {
                if (this.hadFreilos[f]) {
                    const byeRef = new PlayerRef();
                    byeRef.bye = true;
                    roundModel.winners[f] = byeRef;
                }
            }
            rounds[r] = roundModel;
        }

        this.matchdbService.storeTournamentData(this.tournamentId, this.categoryId, this.allPlayers, rounds, undefined, undefined, this.assignmentMode).subscribe(
            response2 => {
                if (response2.success === true) {
                    this.logger.debug('stored ok');

                } else {
                    this.logger.error('Could not store tournament data:', response2.error);
                    this.alertService.error($localize`Could not store tournament data`, response2.error);
                }
            },
            error => {
                this.logger.error('Could not store tournament data:', error);
                this.alertService.error($localize`Could not store tournament data`, error);
            }
        );
    }

    updateEmailTemplate() {
        if (this.rounds.length > 0) {
            this.emailToAll = this.emailToAll
            .replace('{{START}}', this.rounds[this.rounds.length - 1].start)
            .replace('{{END}}', this.rounds[this.rounds.length - 1].end)
            .replace('{{TOURNAMENTID}}', this.tournamentId);
        }
    }

    onScoreChange(match: Match) {
        this.logger.debug('onScoreChange', match);

        // store in DB
        this.matchdbService.updateMatch(this.tournamentId, this.categoryId, match).subscribe(
            response => {
                if (response.success !== true) {
                    this.logger.error('Failed to store score. Will try again on next change', response.error);
                    this.alertService.error($localize`Failed to store score. Will try again on next change`, response.error);
                } else {
                    this.logger.debug('successfully updated score');
                    // this.socketService.send(SockEvent.MATCHUPDATE, JSON.stringify(match));
                    this.socketService.send(SockEvent.MATCHUPDATE, this.tournamentId, this.categoryId, JSON.stringify(match));
                }
            },
            error => {
                this.logger.error('Failed to store score. Will try again on next change:', error);
                this.alertService.error($localize`Failed to store score. Will try again on next change`, error);
            }
        );

        this.updatePlayerPoints();
    }

    isdrawChanged(isdraw: boolean, match: Match) {
        const matchCopy = JSON.parse(JSON.stringify(match));
        if (isdraw) {
            // encode as -1 score
            matchCopy.score[0].p1 = -1;
            matchCopy.score[0].p2 = -1;
        } else {
            matchCopy.score[0].p1 = 0;
            matchCopy.score[0].p2 = 0;
        }
        this.onScoreChange(matchCopy);
    }

    updatePlayerPoints() {
        // update player points
        // first reset all to 0

        for (let p = 0; p < this.allPlayers.length; p++) {
            this.allPlayers[p].points = 0;
        }
        this.playedCurrentRound = [];
        // count all points

        for (let r = 0; r < this.rounds.length; r++) {
            // need to go through all rounds
            const round = this.rounds[r];
            for (let m = 0; m < round.matches.length; m++) {
                // need to check all matches
                const theMatch = round.matches[m];
                const wins = {p1: 0, p2: 0};
                for (let s = 0; s < theMatch.score.length; s++) {
                    // count all set points
                    Match.checkWinnerSetWins(theMatch.score[s], wins);
                }
                // add to players

                if (theMatch.player1.bye !== true) {
                    this.allPlayers[theMatch.player1.playerListIdx].points += wins.p1;
                }
                if (theMatch.player2.bye !== true) {
                    this.allPlayers[theMatch.player2.playerListIdx].points += wins.p2;
                }

                if (r === this.rounds.length - 1) {
                    // current round

                    if (wins.p1 >= 3 || wins.p2 >= 3 || this.isdraw[r][m]) {
                        // match done
                        this.playedCurrentRound.push(this.allPlayers[theMatch.player1.playerListIdx]._id);
                        this.playedCurrentRound.push(this.allPlayers[theMatch.player2.playerListIdx]._id);
                    }
                }
            }
        }
    }

    hasPlayed(player) {
        return this.playedCurrentRound.indexOf(player._id) > -1;
    }

    sortPlayerRefs(playerRefs: PlayerRef[]) {
        playerRefs.sort((pRef1: PlayerRef, pRef2: PlayerRef) => {
            if (typeof pRef1.playerListIdx === 'undefined' || typeof pRef2.playerListIdx === 'undefined') {
                this.logger.error('playerListIdx should not be undefined while sorting');
                return 0;
            }
            const p1 = this.allPlayers[pRef1.playerListIdx];
            const p2 = this.allPlayers[pRef2.playerListIdx];
            if (p1.points === p2.points) {
                if (p1.first_name < p2.first_name) {
                    return -1;
                } else if (p1.first_name > p2.first_name) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return p2.points - p1.points;
            }
        });
    }

    getRanking(): {first_name: string, last_name: string, points: number, rank: number, _id: string}[] {
        const allPlayersCopy = this.allPlayers.slice();
        allPlayersCopy.sort((p1, p2) => {
            if (p1.points === p2.points) {
                if (p1.first_name < p2.first_name) {
                    return -1;
                } else if (p1.first_name > p2.first_name) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return p2.points - p1.points;
            }
        });
        const ranking = [];
        let rank = 1;
        let jumped = 1;
        let lastpoints = -1;
        for (let p = 0; p < allPlayersCopy.length; p++) {
            if (p > 0) {
                if (allPlayersCopy[p].points !== lastpoints) {
                    rank += jumped;
                    jumped = 1;
                } else {
                    jumped++;
                }
            }
            ranking.push({
                first_name: allPlayersCopy[p].first_name,
                last_name: allPlayersCopy[p].last_name,
                points: allPlayersCopy[p].points,
                _id: allPlayersCopy[p]._id,
                rank: rank
            });
            lastpoints = allPlayersCopy[p].points;
        }
        return ranking;
    }

    isException(_players: PlayerRef[]): boolean {
        return false;
    }

    setupRound(roundNr: number) {
        const roundPlayers = this.rounds[roundNr].sortedPlayers;
        if (roundPlayers.length > 0) {
            // randomize those with same points (in the first round, those are all of them)
            let lastPoints = this.allPlayers[roundPlayers[0].playerListIdx].points;
            let lastSamePointsIndex = 0;
            let numRowsToSort;

            for (let p = 1; p < roundPlayers.length; p++) {
                const curPoints = this.allPlayers[roundPlayers[p].playerListIdx].points;
                if (curPoints !== lastPoints) {
                    // now randomize rows from lastSamePointsRow till previous row (if more than one line)
                    numRowsToSort = p - 1 - lastSamePointsIndex + 1;
                    if (numRowsToSort > 1) {
                        do {
                            this.shuffleArray(roundPlayers, lastSamePointsIndex, p - 1);
                        } while (this.isException(roundPlayers));
                    }
                    lastPoints = curPoints;
                    lastSamePointsIndex = p;
                }
            }
            // check whether last rows have same points
            numRowsToSort = roundPlayers.length - lastSamePointsIndex;
            if (numRowsToSort > 1) {
                this.shuffleArray(roundPlayers, lastSamePointsIndex, roundPlayers.length - 1);
            }

            let freilosNr = -1;
            // make match pairs according to points

            if (roundPlayers.length % 2 === 1) {
                // pick one to have a Freilos

                // count how many players did not yet have a freilos
                let cnt = 0;
                let lastFreilosIdx = -1;
                for (let f = 0; f < this.hadFreilos.length; f++) {
                    if (this.hadFreilos[f] !== true) {
                        cnt++;
                        lastFreilosIdx = f;
                    }
                }
                if (cnt === 1) {
                    // only one left, pick this one
                    freilosNr = lastFreilosIdx;
                } else if (cnt === 0) {
                    // everybody had a freilos, start over

                    for (let f = 0; f < this.hadFreilos.length; f++) {
                        this.hadFreilos[f] = false;
                    }
                    freilosNr = Math.floor(Math.random() * roundPlayers.length);
                } else {
                    do {
                        freilosNr = Math.floor(Math.random() * roundPlayers.length);
                    } while (this.hadFreilos[freilosNr]);
                }
                this.hadFreilos[freilosNr] = true;

                const p1Ref = new PlayerRef();
                p1Ref.playerListIdx = freilosNr;
                const p2Ref = new PlayerRef();
                p2Ref.bye = true;
                const lastMatchNr = Math.floor(roundPlayers.length / 2);
                const match = new Match(p1Ref, p2Ref, {level: roundNr.toString(), num: lastMatchNr, ko_type: 'M'});
                match.score[0] = {p1: 16, p2: 0};
                match.score[1] = {p1: 16, p2: 0};
                match.score[2] = {p1: 16, p2: 0};
                match.score[3] = {p1: 0, p2: 0};
                match.score[4] = {p1: 0, p2: 0};
                this.rounds[roundNr].matches[lastMatchNr] = match;
            }

            for (let p = 0; p < roundPlayers.length; p += 2) {
                if (roundPlayers[p].playerListIdx === freilosNr) {
                    p++;
                    if (p >= roundPlayers.length) {
                        // last player is Freilos
                        break;
                    }
                }
                const p1Ref = new PlayerRef();
                p1Ref.playerListIdx = roundPlayers[p].playerListIdx;
                const p2Ref = new PlayerRef();
                if (roundPlayers[p + 1].playerListIdx === freilosNr) {
                    p++;
                }
                p2Ref.playerListIdx = roundPlayers[p + 1].playerListIdx;
                const match = new Match(p1Ref, p2Ref, {level: roundNr.toString(), num: Math.floor(p / 2), ko_type: 'M'});
                match.score = [{p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}];
                this.rounds[roundNr].matches[Math.floor(p / 2)] = match;
            }
        }
    }

    addRound() {
        if (this.newFrom && this.newTo && (this.newTo > this.newFrom)) {
            if (this.playedCurrentRound.length !== this.allPlayers.length) {
                const dialogRef = this.dialog.open(YesNoDialogComponent, {
                    data: { text: $localize`Are you sure? There are UNFINISHED matches!!` }
                });

                dialogRef.afterClosed().subscribe(result => {
                    if (result === true) {
                        this.doAddRound();
                    }
                });
            } else {
                this.doAddRound();
            }
        }
    }

    doAddRound() {
        const round = new Round(this.newFrom.format('DD.MM.YYYY'), this.newTo.format('DD.MM.YYYY'));
        this.rounds.push(round);
        this.isdraw.push([]);

        const roundPlayers = [];
        for (let p = 0; p < this.allPlayers.length; p++) {
            const pRef = new PlayerRef();
            pRef.playerListIdx = p;
            roundPlayers[p] = pRef;
        }
        this.sortPlayerRefs(roundPlayers);
        round.sortedPlayers = roundPlayers;

        this.setupRound(this.rounds.length - 1);

        this.newFrom = this.newTo.clone().add(1, 'days');
        this.newTo = this.newTo.clone().add(13, 'days');

        this.updatePlayerPoints();
        this.storeToDB();
        this.allPlayersChange.emit(this.allPlayers);
    }

    // unsupported since it would mess up the Freilos

    removeRound(rIdx: number) {
        if (rIdx >= 0 && rIdx < this.rounds.length) {
            this.rounds.splice(rIdx, 1);
            this.storeToDB();
            this.updatePlayerPoints();
        }
    }

    // helper to randomize assignment, adapted from
    // http://stackoverflow.com/quesions/2450954/how-to-randomize-shuffle-a-javascript-array

    private shuffleArray(array: Array<any>, from: number, to: number) {
        for (let i = to; i > from; i--) {
            const j = from + Math.floor(Math.random() * (i - from));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    calculateSum(match: Match): {p1: number, p2: number} {
        const wins = {p1: 0, p2: 0};
        Match.checkWinnerSetWins(match.score[0], wins);
        Match.checkWinnerSetWins(match.score[1], wins);
        if (match.score[2]) {
            Match.checkWinnerSetWins(match.score[2], wins);
        }
        if (match.score[3]) {
            Match.checkWinnerSetWins(match.score[3], wins);
        }
        if (match.score[4]) {
            Match.checkWinnerSetWins(match.score[4], wins);
        }
        return wins;
    }

    isFinished(match: Match, c, m): boolean {
        return this.isdraw[c][m] || Match.checkWinner(match) !== 0;
    }

    canSendEmail(): boolean {
        // DCV is allowed

        // tslint:disable-next-line: no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            return true;
        }
        // organiser is allowed

        if (this.myPlayerId && this.tournament && this.tournament.responsibles && this.tournament.responsibles.indexOf(this.myPlayerId) >= 0) {
            return true;
        }
        return false;
    }

    sendEmailToAll() {
        this.logger.debug('sending email to all:', this.emailToAll);
        for (let p = 0; p < this.allPlayers.length; p++) {
            const player = this.allPlayers[p];
            try {
                this.userService.getByPlayerId(player._id).subscribe(
                    response => {
                        if (response.success === true) {
                            if (response.result && response.result.email) {
                                this.sendEmail(this.emailToAll, response.result.email, this.subject);
                            }
                        } else {
                            this.logger.error('could not get email for player', player.first_name, player.last_name, response.error);
                            this.alertService.error($localize`could not get email for player ${player.first_name}:firstName: ${player.last_name}:lastName:`, response.error);
                        }
                    },
                    error => {
                        this.logger.error('could not get email for player', player.first_name, player.last_name, error);
                        this.alertService.error($localize`could not get email for player ${player.first_name}:firstName: ${player.last_name}:lastName:`, error);
                    }
                );
            } catch (err) {
                this.logger.error('could not get email for player', player.first_name, player.last_name, err);
                this.alertService.error($localize`could not get email for player ${player.first_name}:firstName: ${player.last_name}:lastName:`, err);
            }
        }
    }

    sendEmail(content, email, subject) {
        this.userService.sendEmail(content, email, subject).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('successfully sent email to', response.result);
                    this.alertService.success($localize`Successfully sent email`);
                } else {
                    this.logger.error('could not send email to', email, response.error);
                    this.alertService.error($localize`could not send email to ${email}:email:`, response.error);
                }
            },
            error => {
                this.logger.error('could not send email to', email, error);
                this.alertService.error($localize`could not send email to ${email}:email:`, error);
            }
        );
    }

    getReadOnly(match: Match) {
        // DCV is allowed
        // tslint:disable-next-line:no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            return false;
        }
        // BYE cannot be changed

        if (match.player2.bye === true) {
            return true;
        }
        // organiser is allowed

        if (this.myPlayerId && this.tournament && this.tournament.responsibles && this.tournament.responsibles.indexOf(this.myPlayerId) >= 0) {
            return false;
        }
        // allowed if user is one of the players

        return this.allPlayers[match.player1.playerListIdx]._id !== this.myPlayerId &&
        this.allPlayers[match.player2.playerListIdx]._id !== this.myPlayerId;
    }

    cannotAddRounds(): boolean {
        // have to allow creating the next round before all games are
        // played since some might be 0:0
        // if (this.playedCurrentRound.length != this.allPlayers.length) {
        //     return true;
        // }

        // DCV is allowed

        // tslint:disable-next-line: no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            return false;
        }
        // organiser is allowed

        if (this.myPlayerId && this.tournament && this.tournament.responsibles && this.tournament.responsibles.indexOf(this.myPlayerId) >= 0) {
            return false;
        }
        return true;
    }

    allZeroes(match: Match): boolean {
        for (let s = 0; s < match.score.length; s++) {
            if (match.score[s].p1 !== 0 || match.score[s].p2 !== 0) {
                return false;
            }
        }
        return true;
    }
}
