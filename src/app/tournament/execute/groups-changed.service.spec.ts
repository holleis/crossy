import { TestBed, inject } from '@angular/core/testing';

import { GroupsChangedService } from './groups-changed.service';

describe('GroupsChangedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupsChangedService]
    });
  });

  it('should be created', inject([GroupsChangedService], (service: GroupsChangedService) => {
    expect(service).toBeTruthy();
  }));
});
