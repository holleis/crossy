import { FilesystemService } from './../../util/filesystem.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../globals';
import { NGXLogger } from 'ngx-logger';
import { Match } from '../match';
import { Ko } from '../ko';
import { Group } from '../group';
import { Category } from '../category';
import { EnrollmentInfo } from '../cat-info';
import { Tournament } from '../tournament';
import { AlertService } from '../../util/alert/alert.service';


@Injectable()
export class MatchdbService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger,
        private alertService: AlertService,
        private filesystemService: FilesystemService,
    ) { }

    updateMatch(tournamentId: string, categoryId: string, match: Match) {
        this.logger.debug('updateMatch', tournamentId, categoryId, match);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/state/' + tournamentId + '/' + categoryId;
        return this.http.post<{'success': boolean, 'result': string, 'error': string}>(url, match);
    }

    updateMatchWithSubscribe(tournamentId: string, categoryId: string, match: Match,
        players: EnrollmentInfo['players'], groups: Group[], ko: Ko, ko_b: Ko, callback?: (match: Match) => void) {
        this.logger.debug('updateMatchWithSubscribe', tournamentId, categoryId, match);

        // first see if there is an appropriate version in local storage
        const local = localStorage.getItem(tournamentId + '/' + categoryId);
        if (local) {
            // update the local copy and try to save the whole thing
            if (this.tryStoreLocally('', tournamentId, categoryId, players, groups, ko, ko_b)) {
                this.pushLocalResults(callback, match);
            } else {
                // couldn't check local storage, at least try to push the current update
                this.storeTournamentData(tournamentId, categoryId, players, groups, ko, ko_b, '').subscribe(
                    response => {
                        if (response.success === true) {
                            this.logger.debug('stored ok');
                            localStorage.removeItem(tournamentId + '/' + categoryId);
                            this.filesystemService.deleteFile(tournamentId, categoryId);
                            this.alertService.success($localize`Matches successfully stored`);
                            if (callback) {
                                callback(match);
                            }
                            this.pushLocalResults();
                        } else {
                            this.alertService.warn($localize`Still offline! Stored locally.`);
                        }
                    },
                    _error => {
                        this.alertService.warn($localize`Still offline! Stored locally.`);
                    }
                );
            }
        } else {
            const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/state/' + tournamentId + '/' + categoryId;
            return this.http.post<{'success': boolean, 'result': string, 'error': string}>(url, match).subscribe(
                response => {
                    if (response.success === true) {
                        this.logger.debug('successfully updated score');
                        if (callback) {
                            callback(match);
                        }
                        this.pushLocalResults();
                    } else {
                        this.tryStoreLocally(response.error, tournamentId, categoryId, players, groups, ko, ko_b);
                    }
                },
                error => {
                    this.tryStoreLocally(error, tournamentId, categoryId, players, groups, ko, ko_b);
                }
            );
        }
    }

    private tryStoreLocally(error, tournamentId: string, categoryId: string, players: EnrollmentInfo['players'], groups: Group[], ko: Ko, ko_b: Ko): boolean {
        try {
            this.storeTournamentDataLocally(tournamentId, categoryId, players, groups, ko, ko_b);
            if (error !== '') {
                this.logger.error('Stored OFFLINE. Could not store match scores to cloud', error);
                this.alertService.error($localize`Stored OFFLINE. Could not store match scores to cloud`, error);
            }
            return true;
        } catch (err) {
            this.logger.error('ALARM: Could not store match score', err.message + ' // ' + error);
            this.alertService.error($localize`ALARM: Could not store match score`, err.message + ' // ' + error);
            return false;
        }
    }

    pushLocalResults(callback?: (match: Match) => void, match?: Match) {
        if (localStorage.length > 1) {
            // store to DB all potentially locally stored categories
            for (let i = 0; i < localStorage.length; i++) {
                const key = localStorage.key(i);
                if (key !== 'currentUser' && key.indexOf('/') !== -1) {
                    try {
                        const data = localStorage.getItem(key);
                        const keysplit = key.split('/');
                        const tournamentId = keysplit[0];
                        const categoryId = keysplit[1];
                        const jsondata = JSON.parse(data);
                        // TODO use forkJoin
                        this.storeTournamentData(tournamentId, categoryId, jsondata.players, jsondata.groups,
                            jsondata.ko, jsondata.ko_b, '').subscribe(
                            response => {
                                if (response.success === true) {
                                    this.logger.debug('stored ok', tournamentId, categoryId);
                                    localStorage.removeItem(tournamentId + '/' + categoryId);
                                    this.filesystemService.deleteFile(tournamentId, categoryId);
                                    this.alertService.success($localize`Matches successfully stored`);
                                    if (callback && match) {
                                        callback(match);
                                    }
                                } else {
                                    this.alertService.warn($localize`Still offline! Stored locally.`);
                                }
                            },
                            _error => {
                                this.alertService.warn($localize`Still offline! Stored locally.`);
                            }
                        );
                    } catch (err) {
                        this.logger.error('Error loading data from local storage:', key, err);
                        this.alertService.warn('Error loading data from local storage: ' + key, err);
                    }
                }
            }
        }
    }

    getMatch(tournamentId: string, categoryId: string, matchNr: {level: string, num: number, ko_type: string}) {
        this.logger.debug('getMatch', tournamentId, categoryId, matchNr);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/state/' +
            tournamentId + '/' + categoryId + '/' + matchNr.level + '/' + matchNr.num + '/' + matchNr.ko_type;
        return this.http.get<{'success': boolean, 'result': Match[], 'error': string}>(url);
    }

    getMatches(tournamentId: string) {
        this.logger.debug('getMatches', tournamentId);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/matches/' + tournamentId;
        return this.http.get<{'success': boolean, 'result': {
            category: string,
            categoryId: string,
            matches: Match[],
            players: {last_name: string, first_name: string, partner: {last_name: string, first_name: string}}[],
            groups: any,
            ko: any,
        }[], 'error': string}>(url);
    }

    drawExists(tournamentId: string) {
        this.logger.debug('drawExists', tournamentId);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/drawExists/' + tournamentId;
        return this.http.get<{'success': boolean, 'result': boolean, 'error': string}>(url);
    }

    getTournamentData(tournamentId: string, categoryId: string) {
        this.logger.debug('getTournamentData', tournamentId, categoryId);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/stateall/' + tournamentId + '/' + categoryId;
        return this.http.get<{'success': boolean, 'result': {
            category: Category,
            assignmentMode: string,
            players: EnrollmentInfo['players'],
            groups: Group[],
            ko: Ko,
            ko_b: Ko,
            tournament: Tournament,
            results: {rank: number, player: any, partner: any, points: number}[],
            results_b: {rank: number, player: any, partner: any, points: number}[],
            resultsFromOtherCats: {rank: number, player: any, partner: any, points: number},
            resultsRanking: {rank: number, player: any, partner: any, points: number}[],
            resultsRankingFromOtherCats: {rank: number, player: any, partner: any, points: number}[][]
        }, 'error': string}>(url);
    }

    removeTournamentData(tournamentId: string) {
        this.logger.debug('removeTournamentData', tournamentId);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/removeall/' + tournamentId;
        return this.http.post<{'success': boolean, 'result': string, 'error': string}>(url, {});
    }

    removeTournamentDataSingleCat(tournamentId: string, categoryId: string) {
        this.logger.debug('removeTournamentDataSingleCat', tournamentId);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/removeallsinglecat/' + tournamentId + '/' + categoryId;
        return this.http.post<{'success': boolean, 'result': string, 'error': string}>(url, {});
    }

    updateResults(tournamentId: string, categoryId: string,
            results: {rank: number, player: any, partner: any, points: number}[],
            results_b: {rank: number, player: any, partner: any, points: number}[],
            resultsRanking: {rank: number, player: any, partner: any, points: number}[]) {
        const data = {results: results, results_b: results_b, resultsRanking: resultsRanking};
        this.logger.trace('updateResults', tournamentId, categoryId, data);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/updateResults/' + tournamentId + '/' + categoryId;
        return this.http.post<{'success': boolean, 'result': string, 'error': string}>(url, data);
    }

    storeTournamentData(tournamentId: string, categoryId: string, players: EnrollmentInfo['players'], groups: Group[], ko: Ko, ko_b: Ko, assignmentMode: string) {
        const data = {players: players, groups: groups, ko: ko, ko_b: ko_b, assignmentMode: assignmentMode};
        this.logger.trace('storeTournamentData', tournamentId, categoryId, data);
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/stateall/' + tournamentId + '/' + categoryId;
        return this.http.post<{'success': boolean, 'result': string, 'error': string}>(url, data);
    }

    storeTournamentDataLocally(tournamentId: string, categoryId: string, players: EnrollmentInfo['players'], groups: Group[], ko: Ko, ko_b: Ko) {
        const tournamentData = {players: players, groups: groups, ko: ko, ko_b: ko_b};
        this.logger.trace('storeTournamentDataLocally', tournamentId, categoryId, tournamentData);
        localStorage.setItem(tournamentId + '/' + categoryId, JSON.stringify(tournamentData));

        this.filesystemService.useFileStorage(tournamentId, categoryId, tournamentData);
    }



}
