import { Component, OnInit, Input } from '@angular/core';
import { Group } from '../group';
import { Ko } from '../ko';
import { NGXLogger } from 'ngx-logger';
import { EnrollmentInfo } from '../cat-info';

@Component({
    selector: 'app-ko',
    templateUrl: './ko.component.html',
    styleUrls: ['./ko.component.css']
})
export class KoComponent implements OnInit {

    @Input() ko: Ko;
    @Input() otherko: Ko;
    @Input() groups: Group[];
    @Input() players: EnrollmentInfo['players'];
    @Input() ko_type: string;
    @Input() tournamentId: string;
    @Input() categoryId: string;
    @Input() catType: string;
    @Input() canMakeChanges = false;
    levelHeaders: string[] = [];

    constructor(
        private logger: NGXLogger,
    ) {}

    ngOnInit() {
        if (!this.ko) {
            this.logger.trace('error: ko not set on KoComponent');
            return;
        }

        // TODO translate level headers
        let l = 0;
        const thesize = this.ko.matches.length - 1; // this.ko.size;
        while (l <= thesize - 3) {
            this.levelHeaders[l] = $localize`Round of ` + 2 ** (thesize - l + 1);
            l++;
        }
        if (thesize >= 2) {
            this.levelHeaders[l++] = $localize`Quarter-Finals`;
        }
        if (thesize >= 1) {
            this.levelHeaders[l++] = $localize`Semi-Finals`;
        }
        this.levelHeaders[l++] = $localize`Final`;

        this.logger.trace(this.ko);
    }

}
