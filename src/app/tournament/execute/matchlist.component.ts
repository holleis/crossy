import { Component, OnInit, Input } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../../util/alert/alert.service';
import { ActivatedRoute } from '@angular/router';
import { PlayerRef } from '../playerref';
import { MatchdbService } from './matchdb.service';
import { Match } from '../match';
import { TournamentsService } from '../tournaments.service';

@Component({
    selector: 'app-matchlist',
    templateUrl: './matchlist.component.html',
    styleUrls: ['./matchlist.component.css'],
})
export class MatchlistComponent implements OnInit {

    constructor(
        private alertService: AlertService,
        private logger: NGXLogger,
        private route: ActivatedRoute,
        private matchdbService: MatchdbService,
        private tournamentsService: TournamentsService,
    ) {}

    @Input() tournamentId: string;
    tournamentName: string;
    // matches: Match[] = [];
    // groupMatches: Match[] = [];
    // koAMatches: Match[] = [];
    // koBMatches: Match[] = [];
    data: {category: string, players: {last_name: string, first_name: string, partner: {last_name: string, first_name: string}}[], groupMatches: Match[], koAMatches: Match[], koBMatches: Match[], groups: any, ko: any}[] = [];
    maxLevelA: number[] = [];
    maxLevelB: number[] = [];
    PlayerRef = PlayerRef;

    Match = Match;

    ngOnInit() {
        this.load();
    }

    load() {
        if (!this.tournamentId) {
            this.tournamentId = this.route.snapshot.params.id;
        }

        if (this.tournamentId) {
            this.matchdbService.getMatches(this.tournamentId).subscribe(
                response => {
                    if (response.success === true && response.result) {
                        this.data = [];
                        for (let r = 0; r < response.result.length; r++) {
                            const res = response.result[r];
                            const catName = res.category;
                            const matches = res.matches;
                            const players = res.players;
                            this.logger.debug('found', matches.length, 'matches in', this.tournamentId, 'for category', catName);
                            this.logger.debug('found', players.length, 'players in', this.tournamentId);
                            const groupMatches = matches.filter(m => m.matchNr.ko_type === 'G');
                            const koAMatches = matches.filter(m => m.matchNr.ko_type === 'A');
                            koAMatches.sort((m1, m2) => -parseInt(m1.matchNr.level, 10) * 1000 + m1.matchNr.num + parseInt(m2.matchNr.level, 10) * 1000 - m2.matchNr.num);
                            const koBMatches = matches.filter(m => m.matchNr.ko_type === 'B');
                            koBMatches.sort((m1, m2) => -parseInt(m1.matchNr.level, 10) * 1000 + m1.matchNr.num + parseInt(m2.matchNr.level, 10) * 1000 - m2.matchNr.num);
                            this.data.push({category: catName, players: players, groupMatches: groupMatches, koAMatches: koAMatches, koBMatches: koBMatches, groups: res.groups, ko: res.ko});

                            let maxl = 0;
                            for (let m = 0; m < koAMatches.length; m++) {
                                const lev = parseInt(koAMatches[m].matchNr.level, 10);
                                if (lev > maxl) {
                                    maxl = lev;
                                }
                            }
                            this.maxLevelA[r] = maxl;
                            maxl = 0;
                            for (let m = 0; m < koBMatches.length; m++) {
                                const lev = parseInt(koBMatches[m].matchNr.level, 10);
                                if (lev > maxl) {
                                    maxl = lev;
                                }
                            }
                            this.maxLevelB[r] = maxl;
                        }

                    } else {
                        this.logger.error('Could not retrieve the matches of the tournament', response.error);
                        this.alertService.error($localize`Could not retrieve the matches of the tournament`, response.error);
                        return;
                    }
                },
                error => {
                    this.logger.error('Could not retrieve the matches of the tournament', error);
                    this.alertService.error($localize`Could not retrieve the matches of the tournament`, error);
                    return;
                }
            );

            // get tournament name and potentially set canMakeChanges for tournament responsibles
            this.tournamentsService.getTournament(this.tournamentId).subscribe(
                response => {
                    this.logger.trace('retrieve tournament for canMakeChanges (responsibles)', this.tournamentId, response);
                    if (response.success === true) {
                        this.tournamentName = response.result.title;
                    } else {
                        this.logger.error('Could not retrieve tournament:', response.error);
                    }
                },
                error => {
                    this.logger.error('Could not retrieve tournament:', error);
                }
            );
        }
    }

    update() {
        this.load();
    }

    getGroupName(match: Match): string {
        const grNr = parseInt(match.matchNr.level, 10);
        return String.fromCharCode(grNr + 65);
    }

    getPlayers(idx: number, match: Match): string {
        let p1names;
        let p2names;
        const p1Idx = PlayerRef.getPlayerListIdx(this.logger, match.player1, this.data[idx].ko, this.data[idx].groups);
        const p1 = this.data[idx].players[p1Idx];
        if (!p1) {
            p1names = '______________';
        } else {
            p1names = p1.last_name + ' ' + p1.first_name;
            if (typeof p1.partner !== 'undefined' && typeof p1.partner.last_name !== 'undefined') {
                p1names += '+' + p1.partner.last_name + ' ' + p1.partner.first_name;
            }
        }
        const p2Idx = PlayerRef.getPlayerListIdx(this.logger, match.player2, this.data[idx].ko, this.data[idx].groups);
        const p2 = this.data[idx].players[p2Idx];
        if (!p2) {
            p2names = '______________';
        } else {
            p2names = p2.last_name + ' ' + p2.first_name;
            if (typeof p2.partner !== 'undefined' && typeof p2.partner.last_name !== 'undefined') {
                p2names += '+' + p2.partner.last_name + ' ' + p2.partner.first_name;
            }
        }

        return p1names + ' : ' + p2names;
    }

    getScores(match: Match) {
        if (!match.score[0] || (!match.score[0].p1 && !match.score[0].p2)) {
            return '';
        }
        let str = `${match.score[0].p1}:${match.score[0].p2}   ${match.score[1].p1}:${match.score[1].p2}`;
        if (match.score[2] && (match.score[2].p1 || match.score[2].p2)) {
            str += `   ${match.score[2].p1}:${match.score[2].p2}`;
        }

        return str;
    }

    getKoLevelName(level: string): string {
        const lev = parseInt(level, 10);
        if (lev === 0) {
            return 'Final';
        } else if (lev === 1) {
            return 'Semi-Finals';
        } else if (lev === 2) {
            return 'Quarter-Finals';
        } else {
            return 'Round of ' + 2 ** (lev + 1);
        }
        // const thesize = isB ? this.maxLevelB[cIdx] : this.maxLevelA[cIdx];
        // const lev = parseInt(level);
        // if (lev === thesize) {
        //     return 'Final';
        // } else if (lev === thesize-1) {
        //     return 'Semi-Finals';
        // } else if (lev === thesize-2) {
        //     return 'Quarter-Finals';
        // } else {
        //     return 'Round of ' + 2 ** (thesize - lev + 1);
        // }
    }
}
