import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EnrollmentInfo } from '../cat-info';

@Component({
  selector: 'app-choose-partner',
  templateUrl: './pickplayer.component.html',
})
export class PickPlayerComponent implements OnInit {

    constructor (
        public dialogRef: MatDialogRef<PickPlayerComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {
            players: EnrollmentInfo['players'],
            selectedPlayers: EnrollmentInfo['players'];
        },
    ) { }

    ngOnInit() {
    }
}
