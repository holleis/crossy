import { FilesystemService } from './../../util/filesystem.service';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AlertService } from '../../util/alert/alert.service';
import { Group } from '../group';
import { Ko } from '../ko';
import { Match } from '../match';
import { PlayerRef } from '../playerref';
import { NGXLogger } from 'ngx-logger';

import { Subscription } from 'rxjs';
import { GroupsChangedService } from './groups-changed.service';
import { ActivatedRoute } from '@angular/router';
import { EnrollmentInfo } from '../cat-info';

import { SockEvent, SocketService } from '../../util/websockets/socket.service';

import { MatchdbService } from './matchdb.service';
import { Tournament } from '../tournament';
import { Category } from '../category';

// import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { generateCsv, mkConfig as mkConfigCsv, download as downloadCsv } from 'export-to-csv';
import { Role } from '../../user/role';
import { UserService } from '../../auth/_services/user.service';
import { TournamentsService, ResultInfo, Entry } from '../tournaments.service';
import { Title } from '@angular/platform-browser';
import { ReloadService } from './reload-service.service';
// import { MessageDialogComponent } from '../../util/message-dialog.component';

@Component({
    selector: 'app-bracket',
    templateUrl: './bracket.component.html',
    styleUrls: ['./bracket.component.css'],
})
export class BracketComponent implements OnInit, OnDestroy {
    // data from tournament
    isDouble = false;
    doBRound = true;

    @Input() doGroupPhase = true;
    @Input() assignmentMode = 'DCV';
    @Input() catType = 'singles'; // or doubles
    @Input() tournamentId: string;
    @Input() categoryId: string;
    @Input() categoryIdx: number;
    @Input() newDraw: boolean;
    // stores all players if 'players' is halfed for doubles
    @Input() allPlayers: EnrollmentInfo['players'] = [];
    @Input() canMakeChanges = false;
    // isPaul = false;

    groups: Group[] = [];
    ko: Ko;
    ko_b: Ko;
    koNumber = -1;
    koNumber_b = -1;

    playerOrder: number[];
    playerOrder_b: number[];

    BYE_PLAYER: PlayerRef;

    players: EnrollmentInfo['players'] = [];
    resultsRanking: Entry[] = [];
    resultsRankingFromOtherCats = {};
    exportForDB = [];
    results: Entry[] = [];
    results_b: Entry[] = [];
    // TODO not necessary (dont need to export the DM Quali points as CSV):
    resultsFromOtherCats = {};

    maxPoints = 0.0;

    // mapping from groups to KO first level; used to create this.ko
    koAssignment: {group, num}[] = [];
    koAssignment_b: {group, num}[] = [];

    rankingBestSeconds: {group: number, groupName: string, player: PlayerRef, points: number,
        games: number, sets: number, setslost: number, relpoints: number, pointswon: number,
        pointslost: number, ranking: number}[] = [];
    nrBestSecondsForA = 0;

    // TODO make this not configurable as changing it doesn't make sense
    // 2 best for each group for <16 players; otherwise only 1 best
    numberOfBestPlayersToUse = 2;

    groupChangeSubscription: Subscription;
    reloadSubscription: Subscription;

    separateTabDisplay = false;
    tournament: Tournament;
    category: Category;

    selectedTab = 1;

    csvOptions;

    // socketIO connection for live updates
    ioConnection: any;

    constructor(
        private alertService: AlertService,
        private groupsChangedService: GroupsChangedService,
        private logger: NGXLogger,
        private route: ActivatedRoute,
        private userService: UserService,
        private matchdbService: MatchdbService,
        private tournamentsService: TournamentsService,
        private titleService: Title,
        private socketService: SocketService,
        private reloadService: ReloadService,
        private filesystemService: FilesystemService,
    ) {
        this.BYE_PLAYER = new PlayerRef();
        this.BYE_PLAYER.bye = true;
        this.csvOptions = {
            fieldSeparator: ';', quoteStrings: '', useBom: false
        };
    }

    ngOnDestroy() {
        if (this.groupChangeSubscription) {
            this.groupChangeSubscription.unsubscribe();
        }
        if (this.reloadSubscription) {
            this.reloadSubscription.unsubscribe();
        }
    }

    ngOnInit() {
        if (typeof this.categoryIdx === 'undefined') {
            // used only to calculate tabIndex
            this.categoryIdx = 0;
        }

        const ts = new Date();
        this.reloadSubscription = this.reloadService.reloadStatus.subscribe(
            _priority => {
                // avoid direct reload while subscribing
                if (new Date().getTime() - ts.getTime() > 1000) {
                    this.readStateFromDB();
                }
            }
        );

        if (this.route.snapshot.params.id && this.route.snapshot.params.catId) {

            if (this.route.snapshot.params.newDraw === 'true') {
                if ((typeof this.allPlayers !== 'undefined') && this.allPlayers.length > 0) {
                    this.isDouble = this.catType !== 'singles';
                    this.doBRound = this.assignmentMode === 'DCV' && this.doGroupPhase && this.allPlayers.length >= (this.isDouble ? 32 : 16);
                    this.makeDraw();
                }
            } else {
                this.separateTabDisplay = true;
                // get tournament and category ID from URL
                this.tournamentId = this.route.snapshot.params.id;
                this.categoryId = this.route.snapshot.params.catId;

                this.initIoConnection();
                this.readStateFromDB();
            }

        } else {
            this.isDouble = this.catType !== 'singles';
            this.doBRound = this.assignmentMode === 'DCV' && this.doGroupPhase && this.allPlayers.length >= (this.isDouble ? 32 : 16);

            if (this.newDraw) {
                this.makeDraw();
            } else {
                this.readStateFromDB();
            }
        }

        // see if user is allowed to enter scores
        const user = this.userService.getCurrentUser();
        // tslint:disable-next-line: no-bitwise
        if ((user?.roles & Role.ROLE_PAUL) > 0 || (user?.roles & Role.ROLE_DCV) > 0) {
            this.canMakeChanges = true;
            // // tslint:disable-next-line: no-bitwise
            // if ((user.roles & Role.ROLE_PAUL) > 0) {
            //     this.isPaul = true;
            // }
        }

    }

    public setTitle(tournamentName: string, catName: string) {
        if (!tournamentName && !catName) {
            this.titleService.setTitle('Crossy');
        }

        if (this.separateTabDisplay || !tournamentName) {
            this.titleService.setTitle(catName);
        } else if (catName) {
            this.titleService.setTitle(tournamentName);
        }
    }

    private initIoConnection(): void {
        if (this.ioConnection = this.socketService.initSocket()) {
            // first init, register listeners
            this.socketService.onMessage(
                (message: { type: string, tournamentId: string, categoryId: string, message: string }) => {
                    const msg = JSON.parse(message['data']);
                    if (msg.tournamentId === this.tournamentId && this.categoryId === msg.categoryId) {
                        this.updateWithMessage(msg);
                    }
                }
            );

            this.socketService.onEvent(SockEvent.OPEN, () =>
                this.logger.debug('bracket connected')
            );

            this.socketService.onEvent(SockEvent.CLOSE, () =>
                this.logger.debug('bracket disconnected')
            );
        }
    }

    updateWithMessage(msg: {type: string, tournamentId: string, categoryId: string, message: string}) {
        if (msg.tournamentId === this.tournamentId && msg.categoryId === this.categoryId) {
            const message = msg.message;
            if (msg.type === SockEvent.GROUPMATCHUPDATE) {
                this.logger.debug('group match update message received', message);
                try {
                    const match = JSON.parse(message) as Match;
                    // legacy treatment: initially, group matches stored groupNames 'A', 'B' etc. as level
                    let groupNr = parseInt(match.matchNr.level, 10);
                    if (isNaN(groupNr)) {
                        groupNr = match.matchNr.level.charCodeAt(0) - 65;
                    }
                    this.groups[groupNr].matches[match.matchNr.num].score = match.score;
                    this.updateAfterGroupChange({ groupNr, categoryId: this.categoryId });
                } catch (err) {
                    this.logger.error('could not parse websocket message for matchupdate', message, err);
                }
            } else if (msg.type === SockEvent.KOMATCHUPDATE) {
                this.logger.debug('KO match update message received', message);
                try {
                    let match = JSON.parse(message) as Match;
                    const newscore = match.score;
                    if (match.matchNr.ko_type === 'A') {
                        match = this.ko.matches[parseInt(match.matchNr.level, 10)][match.matchNr.num];
                    } else {
                        // B_KO
                        match = this.ko_b.matches[parseInt(match.matchNr.level, 10)][match.matchNr.num];
                    }
                    match.score = newscore;

                    this.updateMatch(match);
                } catch (err) {
                    this.logger.error('could not parse websocket message for matchupdate', message, err);
                }
            }
        }
    }

    updateMatch(match: Match) {
        const winner = Match.checkWinner(match);
        if (winner === -1) {
            if (!match.winner) {
                match.winner = match.player1;
            } else {
                // don't do winner = player1 since we want to keep the object reference the same
                PlayerRef.update(this.logger, match.winner, match.player1, match.matchNr.ko_type === 'A' ? this.ko : this.ko_b, this.groups);
            }
        } else if (winner === 1) {
            if (!match.winner) {
                match.winner = match.player2;
            } else {
                PlayerRef.update(this.logger, match.winner, match.player2, match.matchNr.ko_type === 'A' ? this.ko : this.ko_b, this.groups);
            }
        } else {
            if (!match.winner) {
                match.winner = new PlayerRef();
            } else {
                PlayerRef.update(this.logger, match.winner, new PlayerRef());
            }
        }
    }

    readStateFromDB() {
        this.matchdbService.getTournamentData(this.tournamentId, this.categoryId).subscribe(
            response => {
                if (response.success === true && response.result) {
                    this.players = response.result.players;
                    this.groups = response.result.groups;
                    this.doGroupPhase = this.groups.length > 0;
                    this.catType = response.result.category.cat_type;
                    this.isDouble = this.catType !== 'singles';
                    this.assignmentMode = response.result.assignmentMode;
                    this.doBRound = this.assignmentMode === 'DCV' && this.doGroupPhase && this.players.length >= (this.isDouble ? 32 : 16);
                    this.results = response.result.results;
                    this.results_b = response.result.results_b;
                    this.resultsFromOtherCats = response.result.resultsFromOtherCats;
                    this.resultsRanking = response.result.resultsRanking;
                    this.resultsRankingFromOtherCats = response.result.resultsRankingFromOtherCats;
                    if (typeof this.resultsFromOtherCats === 'undefined') {
                        this.resultsFromOtherCats = {};
                    }
                    if (typeof this.resultsRankingFromOtherCats === 'undefined') {
                        this.resultsRankingFromOtherCats = {};
                    }

                    this.tournament = response.result.tournament;
                    this.category = response.result.category;

                    this.setTitle(this.tournament.title, this.category.name);

                    // correct that all matches now have different PlayerRef objects than the group.players
                    for (let g = 0; g < this.groups.length; g++) {
                        const group = this.groups[g];
                        const plyrs = new Array(group.players.length);
                        for (let p = 0; p < group.players.length; p++) {
                            const groupPlayer = group.players[p];
                            plyrs[groupPlayer.playerListIdx] = groupPlayer;
                        }
                        for (let m = 0; m < group.matches.length; m++) {
                            const match = group.matches[m];
                            // get the player with the same playerListIdx
                            match.player1 = plyrs[match.player1.playerListIdx];
                            match.player2 = plyrs[match.player2.playerListIdx];
                        }
                    }

                    this.ko = response.result.ko;
                    this.playerOrder = this.ko.playerOrder;
                    // tslint:disable-next-line: no-bitwise
                    this.koNumber = 1 << this.ko.matches.length;

                    if (this.doBRound) {
                        this.ko_b = response.result.ko_b;
                        this.playerOrder_b = this.ko_b.playerOrder;
                        // tslint:disable-next-line: no-bitwise
                        this.koNumber_b = 1 << this.ko_b.matches.length;

                        this.nrBestSecondsForA = Math.pow(2, this.ko.matches.length) - this.groups.length;
                    }

                    if (this.doGroupPhase) {
                        // whenever something changed in the groups, need to update winner orders
                        this.groupChangeSubscription = this.groupsChangedService.groupChangeStatus.subscribe(
                            info => this.updateAfterGroupChange(info)
                        );

                        setTimeout(() => {
                            this.selectedTab = 0;
                        });
                    }

                    // check if there is data in the file system (e.g. closed / crashed browser)
                    this.filesystemService.checkLocalFilesystem(this.tournamentId, this.categoryId, (tournamentData) => {
                        if (tournamentData) {
                            // TODO maybe ask the user before using this data?
                            this.players = tournamentData.players;
                            this.groups = tournamentData.groups;
                            this.ko = tournamentData.ko;
                            if (this.doBRound) {
                                this.ko_b = tournamentData.ko_b;
                            }
                            // try to send to server
                            this.matchdbService.storeTournamentData(this.tournamentId, this.categoryId,
                                this.players, this.groups, this.ko, this.ko_b, this.assignmentMode).subscribe(
                                response2 => {
                                    if (response2.success === true) {
                                        this.logger.debug('stored ok');
                                        this.filesystemService.deleteFile(this.tournamentId, this.categoryId);
                                        // TODO also delete localStorage entry

                                        // if (this.isDouble) {
                                        //     // TODO this is only done to retrieve the double partner info
                                        //  TODO this would create an endless loop if the file is not deleted
                                        //     this.readStateFromDB();
                                        // }
                                    } else {
                                        this.logger.error('Error: Could not update server with local data.', response2.error);
                                        this.alertService.error($localize`Error: Could not update server with local data.`, response2.error);
                                    }
                                },
                                error => {
                                    this.logger.error('Error: Could not update server with local data.', error);
                                    this.alertService.error($localize`Error: Could not update server with local data.`, error);
                                }
                            );
                        }
                    });

                } else if (!response.result) {
                    this.logger.trace('no stored data found for', this.tournamentId);

                } else {
                    this.logger.error('Could not retrieve tournament data:', response.error);
                    this.alertService.error($localize`Could not retrieve tournament data`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve tournament data:', error);
                this.alertService.error($localize`Could not retrieve tournament data`, error);
            }
        );

        // calculate overall state
        setTimeout(() => {
            this.groupsChangedService.groupsChange(-1, this.categoryId);
        }, 3000);
    }

    makeDraw() {
        // set all players with -1 points to 0 points (
        for (let p = 0; p < this.allPlayers.length; p++) {
            if (this.allPlayers[p].points === -1 || this.allPlayers[p].points === -2) {
                this.allPlayers[p].points = 0;
            }
        }

        // players is a copy of allPlayers if they are the same (i.e. not double)
        this.players = this.combinePlayersForDouble(this.allPlayers);
        if (this.players.length === 0) {
            // must be an error such as some doubles partner not found
            return;
        }
        this.logger.debug('making the draw for the tournament');

        this.startAssignment();

        this.createKOs();

        if (this.ko) {
            this.ko.playerOrder = this.playerOrder;
        }
        if (this.ko_b && this.playerOrder_b && this.playerOrder_b.length > 0) {
            this.ko_b.playerOrder = this.playerOrder_b;
        }
        this.matchdbService.storeTournamentData(this.tournamentId, this.categoryId,
            this.players, this.groups, this.ko, this.ko_b, this.assignmentMode).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('stored ok');

                    if (this.isDouble) {
                        // TODO this is only done to retrieve the double partner info
                        this.readStateFromDB();
                    }
                } else {
                    this.logger.error('ALARM: Could not store draw. You HAVE to be connected to the internet for the draw!', response.error);
                    this.alertService.error($localize`ALARM: Could not store draw. You HAVE to be connected to the internet for the draw!`, response.error);
                }
            },
            error => {
                this.logger.error('ALARM: Could not store draw. You HAVE to be connected to the internet for the draw!', error);
                this.alertService.error($localize`ALARM: Could not store draw. You HAVE to be connected to the internet for the draw!`, error);
            }
        );
    }

    createKOs() {
        if (this.koNumber > 0) {
            // TODO put into a function for A and B creation
            this.ko = new Ko(this.koNumber);
            const matches: Match[][] = [];
            const firstLevel = Math.log2(this.koNumber) - 1;

            // first level: place players according to determined assignment into KO matches
            // TODO check what if less players assigned than spaces
            const koSeed: Match[] = [];
            for (let i = 0; i < this.koNumber; i += 2) {
                let p1 = this.BYE_PLAYER;
                let p2 = this.BYE_PLAYER;
                if (this.koAssignment[i]) {
                    if (this.koAssignment[i].group === -1) {
                        // no group phase, use direct index into players list
                        if (this.koAssignment[i].num < this.players.length) {
                            p1 = new PlayerRef();
                            p1.playerListIdx = this.koAssignment[i].num;
                        }
                    } else {
                        p1 = new PlayerRef();
                        p1.groupIdx = {groupNr: this.koAssignment[i].group, winnerNr: this.koAssignment[i].num};
                    }
                }
                if (this.koAssignment[i + 1]) {
                    if (this.koAssignment[i + 1].group === -1) {
                        // no group phase, use direct index into players list
                        if (this.koAssignment[i + 1].num < this.players.length) {
                            p2 = new PlayerRef();
                            p2.playerListIdx = this.koAssignment[i + 1].num;
                        }
                    } else {
                        p2 = new PlayerRef();
                        p2.groupIdx = {groupNr: this.koAssignment[i + 1].group, winnerNr: this.koAssignment[i + 1].num};
                    }
                }
                koSeed.push(new Match(p1, p2, {level: firstLevel.toString(), num: i / 2, ko_type: 'A'}));
            }
            matches[firstLevel] = koSeed;

            // fill other levels
            // index of last level is size-1; already have firstLevel: -1 again
            let levelNr = this.ko.size - 1 - 1;
            while (levelNr >= 0) {
                const levelMatches: Match[] = [];
                for (let l = 0; l < 2 ** levelNr; l++) {
                    const p1Ref = new PlayerRef();
                    p1Ref.koIdx = {level: (levelNr + 1).toString(), num: l * 2, ko_type: 'A'};
                    const p2Ref = new PlayerRef();
                    p2Ref.koIdx = {level: (levelNr + 1).toString(), num: l * 2 + 1, ko_type: 'A'};
                    levelMatches.push(new Match(p1Ref, p2Ref, {level: levelNr.toString(), num: l, ko_type: 'A'}));
                }
                matches[levelNr] = levelMatches;
                levelNr -= 1;
            }
            this.ko.setMatches(matches);

            // check for BYE players in first round of KO
            for (let i = 0; i < matches[firstLevel].length; i++) {
                const m = matches[firstLevel][i];
                const winner = Match.checkWinner(m);
                if (winner === -1) {
                    // don't do winner = player1 since we want to keep the object reference the same
                    PlayerRef.update(this.logger, m.winner, m.player1, this.ko, this.groups);
                } else if (winner === 1) {
                    PlayerRef.update(this.logger, m.winner, m.player2, this.ko, this.groups);
                }
            }

            // B_KO
            if (this.koNumber_b > 0) {
                this.ko_b = new Ko(this.koNumber_b);
                const firstLevel_b = Math.log2(this.koNumber_b) - 1;

                // first level: place players according to determined assignment into KO matches
                // TODO check what if less players assigned than spaces
                const koSeed_b: Match[] = [];
                for (let i = 0; i < this.koNumber_b; i += 2) {
                    let p1 = this.BYE_PLAYER;
                    let p2 = this.BYE_PLAYER;
                    if (this.koAssignment_b[i]) {
                        p1 = new PlayerRef();
                        p1.groupIdx = {groupNr: this.koAssignment_b[i].group, winnerNr: this.koAssignment_b[i].num};
                    }
                    if (this.koAssignment_b[i + 1]) {
                        p2 = new PlayerRef();
                        p2.groupIdx = {groupNr: this.koAssignment_b[i + 1].group, winnerNr: this.koAssignment_b[i + 1].num};
                    }
                    koSeed_b.push(new Match(p1, p2, {level: firstLevel_b.toString(), num: i / 2, ko_type: 'B'}));
                }

                // fill other levels
                const matches_b = [];
                matches_b[firstLevel_b] = koSeed_b;
                // index of last level is size-1; already have firstLevel: -1 again
                let levelNr_b = this.ko_b.size - 1 - 1;
                while (levelNr_b >= 0) {
                    const levelMatches = [];
                    for (let l = 0; l < 2 ** levelNr_b; l++) {
                        const p1Ref = new PlayerRef();
                        p1Ref.koIdx = {level: (levelNr_b + 1).toString(), num: l * 2, ko_type: 'B'};
                        const p2Ref = new PlayerRef();
                        p2Ref.koIdx = {level: (levelNr_b + 1).toString(), num: l * 2 + 1, ko_type: 'B'};
                        levelMatches.push(new Match(p1Ref, p2Ref, {level: levelNr_b.toString(), num: l, ko_type: 'B'}));
                    }
                    matches_b[levelNr_b] = levelMatches;
                    levelNr_b -= 1;
                }
                this.ko_b.setMatches(matches_b);

                // check for BYE players in first round of KO
                for (let i = 0; i < matches_b[firstLevel_b].length; i++) {
                    const m = matches_b[firstLevel_b][i];
                    const winner = Match.checkWinner(m);
                    if (winner === -1) {
                        // don't do winner = player1 since we want to keep the object reference the same
                        PlayerRef.update(this.logger, m.winner, m.player1, this.ko_b, this.groups);
                    } else if (winner === 1) {
                        PlayerRef.update(this.logger, m.winner, m.player2, this.ko_b, this.groups);
                    }
                }
            }

        } else {
            this.logger.trace('error: cannot create KO, koNumber:', this.koNumber);
        }

        // // TODO remove test
        // for (let g = 0; g < this.groups.length; g++) {
        //     this.groups[g].matches[0].score = [{p1: 16, p2: 1}, {p1: 16, p2: 2}, {p1: 0, p2: 0}];
        //     this.groups[g].matches[1].score = [{p1: 16, p2: 3}, {p1: 16, p2: 4}, {p1: 0, p2: 0}];
        //     this.groups[g].matches[2].score = [{p1: 16, p2: 5}, {p1: 16, p2: 6}, {p1: 0, p2: 0}];
        // }
        this.groupsChangedService.groupsChange(-1, this.categoryId);
    }

    combinePlayersForDouble(players: EnrollmentInfo['players']): EnrollmentInfo['players'] {
        if (!this.isDouble) {
            return players;
        }

        const dontuse: boolean[] = [];
        const retPlayers: EnrollmentInfo['players'] = [];

        for (let p = 0; p < players.length; p++) {
            const player = players[p];
            if (!player.partner) {
                this.logger.debug('Partner missing for player', player._id, player.first_name, player.last_name);
                this.alertService.error($localize`Partner missing for player ${player.first_name}:firstName: ${player.last_name}:lastName:`);
                return [];
            }
            if (dontuse[p] !== true) {
                retPlayers.push(player);
                let foundPartner = false;
                for (let pp = p + 1; pp < players.length; pp ++) {
                    if ((player.partner._id || player.partner).toString() === (players[pp]._id || players[pp]).toString()) {
                        dontuse[pp] = true;
                        foundPartner = true;
                        break;
                    }
                }
                if (!foundPartner) {
                    this.logger.debug('Partner missing for player', player._id, player.first_name, player.last_name);
                    this.alertService.error($localize`Partner missing for player ${player.first_name}:firstName: ${player.last_name}:lastName:`);
                    return [];
                }
            }
        }
        return retPlayers;
    }

    /**
    * helper to randomize assignment
    * http://stackoverflow.com/quesions/2450954/how-to-randomize-shuffle-a-javascript-array
    */
    shuffleArray(array: any[]): any[] {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            // this.logger.trace("   swapping " + array[i] + " with " + array[j]);
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    /**
    * Randomly assign the player numbers to the group numbers
    *
    * @param {number} groupNrFrom first group to which to assign players to
    * @param {number} groupNrTo last group to which to assign players to
    * @param {number} playerNrFrom first number of player which shall be assigned
    * @param {number} playerNrTo last number of player which shall be assigned
    * @param {boolean} if randomize is false the order in which the players are numbered will be used
    */
    assignPlayers(groupNrFrom: number, groupNrTo: number, playerNrFrom: number, playerNrTo: number, randomize: boolean) {
        this.logger.trace('assignPlayers(' + groupNrFrom + ', ' + groupNrTo + ', ' +
                     playerNrFrom + ', ' + playerNrTo + ', ' + randomize + ')');

        let playersIdx = [];
        for (let i = playerNrFrom; i <= playerNrTo; i++) {
            playersIdx.push(i);
        }

        if (randomize) {
            playersIdx = this.shuffleArray(playersIdx);
        }

        let playerNr = 0;
        for (let groupNr = groupNrFrom; groupNr <= groupNrTo; groupNr++) {
            this.logger.trace('  accessing player ' + this.players[playersIdx[playerNr]] +
                           ' at ' + (playersIdx[playerNr]) + ' playerNr=' + playerNr);
            const player = new PlayerRef();
            player.playerListIdx = playersIdx[playerNr];
            this.logger.trace('  assigning player ' + playersIdx[playerNr] +
                            ' (' + this.players[playersIdx[playerNr]] + ') to group ' + groupNr);
            if (this.isDouble) {
                this.logger.trace('     with partner ' + this.players[playersIdx[playerNr]].partner);
            }
            this.groups[groupNr].players.push(player);
            playerNr++;
        }
    }


    /**
    * Returns array specifying the number of players for each group
    * This implements the DCV mode (since April 2018)
    *
    * @param {number} nrPlayers the number of players to assign
    * @return array of group size
    */
    calculateGroups_DCV(nrPlayers: number): number[] {
        this.logger.trace('calculateGroups_DCV(' + nrPlayers + ')');
        let groupSizes = [];
        if (nrPlayers === 4 || nrPlayers === 5) {
            groupSizes = [nrPlayers];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
        } else if (nrPlayers === 6) {
            groupSizes = [3, 3];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
        } else if (nrPlayers === 7) {
            groupSizes = [3, 4];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
        } else if (nrPlayers === 11) {
            groupSizes = [3, 4, 4];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
        } else if (nrPlayers < 16) {
            groupSizes = [];
            const nrGroups = Math.floor(nrPlayers / 4);
            const overPlayers = nrPlayers % 4;
            for (let groupNr = 0; groupNr < nrGroups; groupNr++) {
                if (nrGroups - groupNr <= overPlayers) {
                    groupSizes[groupNr] = 5;
                } else {
                    groupSizes[groupNr] = 4;
                }
            }
            this.logger.trace('from ' + nrPlayers + ' players calculated ' + groupSizes.length + ' groups: ' + groupSizes);
        } else {
            // A+B mode, mostly groups of 3
            const nrGroups = Math.floor(nrPlayers / 3);
            const overPlayers = nrPlayers % 3;
            for (let groupNr = 0; groupNr < nrGroups; groupNr++) {
                if (nrGroups - groupNr <= overPlayers) {
                    groupSizes[groupNr] = 4;
                } else {
                    groupSizes[groupNr] = 3;
                }
            }
            this.logger.trace('A/B, from ' + nrPlayers + ' players calculated ' + groupSizes.length + ' groups: ' + groupSizes);
        }

        return groupSizes;
    }

    /**
    * Returns array specifying the number of players for each group
    * This implements the ICO mode
    *
    * @param {number} nrPlayers the number of players to assign
    * @return array of group size
    */
    calculateGroups_ICO(nrPlayers: number): number[] {
        this.logger.trace('calculateGroups_ICO(' + nrPlayers + ')');
        let groupSizes = [];
        if (nrPlayers === 6) {
            groupSizes = [3, 3];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
          } else if (nrPlayers === 7) {
            groupSizes = [3, 4];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
          } else if (nrPlayers === 11) {
            groupSizes = [3, 4, 4];
            this.logger.trace('from ' + nrPlayers + ' players calculated groups ' + groupSizes);
          } else {
            const nrGroups = Math.floor(nrPlayers / 4);
            const overPlayers = nrPlayers % 4;
            for (let groupNr = 0; groupNr < nrGroups; groupNr++) {
              if (nrGroups - groupNr <= overPlayers) {
                groupSizes[groupNr] = 5;
              } else {
                groupSizes[groupNr] = 4;
              }
            }
            this.logger.trace('from ' + nrPlayers + ' players calculated ' + groupSizes.length + ' groups: ' + groupSizes);
        }

        return groupSizes;
    }

    /**
     * Sorts this.players according to their points
     */
    sortByRank() {
        if (this.players.length <= 1) {
            // nothing to do
            return;
        }

        if (this.isDouble) {
            // assign points of both double players to the first player
            for (let p = 0; p < this.players.length; p++) {
                const p1 = this.players[p];
                const p1PartnerId = p1.partner;
                let p1PartnerPoints = -1;
                for (let pp = 0; pp < this.allPlayers.length; pp++) {
                    const player = this.allPlayers[pp];
                    if (player._id === (p1PartnerId._id || p1PartnerId).toString()) {
                        if (typeof player.points !== 'undefined') {
                            p1PartnerPoints = player.points;
                        }
                        break;
                    }
                }
                p1.points += p1PartnerPoints;
            }
        }

        this.players.sort(function (p1: EnrollmentInfo['players'][0], p2: EnrollmentInfo['players'][0]) {
            // numeric sort on points DESC
            return p2.points - p1.points;
        });

        // randomize players / doubles with same number of points
        // this.logger.trace("sortPerRank: non-groupPhase, randomizing entries with same points");
        // grab first row
        let lastPoints = this.players[0].points;
        let lastSamePointsRow = 0;

        for (let playerNr = 1; playerNr < this.players.length; playerNr++) {
            // start from 1 to check against playerNr-1
            const curPoints = this.players[playerNr].points;
            if (curPoints !== lastPoints) {
                if ((playerNr - 1) - (lastSamePointsRow - 1) > 1) {
                    // now sort rows from lastSamePointsRow till previous row (if more than one line)
                    this.randomizeRange(this.players, lastSamePointsRow, playerNr - 1);
                }
                lastPoints = curPoints;
                lastSamePointsRow = playerNr;
            }
        }
        if ((this.players.length - 1) - (lastSamePointsRow - 1) > 1) {
            // check whether last rows have same points
            this.randomizeRange(this.players, lastSamePointsRow, this.players.length - 1);
        }
        this.logger.trace('sortPerRank: done');
    }

    /**
    * Randomly shuffles all items in the given range (in place).
    */
    randomizeRange(randData: any[], from: number, to: number) {
        this.logger.trace('randomizing part from ' + from + ' to ' + to + ' of ' + randData);
        randData.splice(from, to - from + 1, ...this.shuffleArray(randData.slice(from, to + 1)));
        this.logger.trace('randomized: ' + randData);
    }

    /**
     * Randomly assigns the 2 values in nums to arr at index 'at' and 'max-1-at'
     */
    fillin(arr: any[], max: number, at: number, nums: any[]) {
        nums = this.shuffleArray(nums);
        arr[at] = nums[0];
        arr[max - 1 - at] = nums[1];
    }


    /**
    * Specifies the order in which the 2 best of each group are assigned to the KO round
    * This implements the current (up to at least 2018) official mode of the ICO
    * @param {number} nrPlayers the total number of players
    * @param playerOrder array that will hold the order
    *         1st of group A, 2nd group A, 1st group B, 2nd group B, ...
    *         or 1st, 2nd, 3rd ... of ranking if without group phase
    * @return the number of players (maybe some BYEs) in the first round of KO
    */
    calculateKO_ICO(nrPlayers: number, playerOrder: number[], doGroupPhase: boolean = this.doGroupPhase): number {
        // Ditschi: "to clarify to follow tournament mode it should be the way: 1st&2nd: 1or8, 3rd&4th: 5or4, 5th&6th: 3or6, 7th&8th:2or7"
        // for playerOrder read: on number 1 can be 1st/2nd (same for number 8), ...

        if (!doGroupPhase) {
            // TODO adapt
            if (nrPlayers <= 4) {
                const num = 4;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;
            } else if (nrPlayers <= 8) {
                // playerOrder.push(shuffleArray([1, 8]), shuffleArray([5, 4]), shuffleArray([6, 3]), shuffleArray([2, 7]));
                const num = 8;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [7, 8]);
                this.fillin(playerOrder, num, 2, [6, 5]);
                this.fillin(playerOrder, num, 3, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;
            } else if (nrPlayers <= 16) {
                const num = 16;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [15, 16]);
                this.fillin(playerOrder, num, 2, [8, 7]);
                this.fillin(playerOrder, num, 3, [11, 12]);
                this.fillin(playerOrder, num, 4, [5, 6]);
                this.fillin(playerOrder, num, 5, [10, 9]);
                this.fillin(playerOrder, num, 6, [14, 13]);
                this.fillin(playerOrder, num, 7, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;

            } else if (nrPlayers <= 32) {
                const num = 32;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [31, 32]);
                this.fillin(playerOrder, num, 2, [18, 17]);
                this.fillin(playerOrder, num, 3, [16, 15]);
                this.fillin(playerOrder, num, 4, [9, 10]);
                this.fillin(playerOrder, num, 5, [23, 24]);
                this.fillin(playerOrder, num, 6, [26, 25]);
                this.fillin(playerOrder, num, 7, [8, 7]);
                this.fillin(playerOrder, num, 8, [5, 6]);
                this.fillin(playerOrder, num, 9, [27, 28]);
                this.fillin(playerOrder, num, 10, [22, 21]);
                this.fillin(playerOrder, num, 11, [12, 11]);
                this.fillin(playerOrder, num, 12, [13, 14]);
                this.fillin(playerOrder, num, 13, [19, 20]);
                this.fillin(playerOrder, num, 14, [30, 29]);
                this.fillin(playerOrder, num, 15, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;

            } else if (nrPlayers <= 64) {
                const num = 64;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [64, 63]);
                this.fillin(playerOrder, num, 2, [32, 34]);
                this.fillin(playerOrder, num, 3, [33, 31]);
                this.fillin(playerOrder, num, 4, [16, 18]);
                this.fillin(playerOrder, num, 5, [49, 47]);
                this.fillin(playerOrder, num, 6, [17, 50]);
                this.fillin(playerOrder, num, 7, [48, 15]);
                this.fillin(playerOrder, num, 8, [8, 10]);
                this.fillin(playerOrder, num, 9, [57, 55]);
                this.fillin(playerOrder, num, 10, [25, 23]);
                this.fillin(playerOrder, num, 11, [40, 42]);
                this.fillin(playerOrder, num, 12, [9, 39]);
                this.fillin(playerOrder, num, 13, [56, 26]);
                this.fillin(playerOrder, num, 14, [24, 58]);
                this.fillin(playerOrder, num, 15, [41, 7]);
                this.fillin(playerOrder, num, 16, [4, 11]);
                this.fillin(playerOrder, num, 17, [61, 54]);
                this.fillin(playerOrder, num, 18, [29, 43]);
                this.fillin(playerOrder, num, 19, [36, 22]);
                this.fillin(playerOrder, num, 20, [13, 38]);
                this.fillin(playerOrder, num, 21, [52, 27]);
                this.fillin(playerOrder, num, 22, [20, 59]);
                this.fillin(playerOrder, num, 23, [45, 6]);
                this.fillin(playerOrder, num, 24, [5, 51]);
                this.fillin(playerOrder, num, 25, [60, 14]);
                this.fillin(playerOrder, num, 26, [28, 46]);
                this.fillin(playerOrder, num, 27, [37, 19]);
                this.fillin(playerOrder, num, 28, [12, 35]);
                this.fillin(playerOrder, num, 29, [53, 30]);
                this.fillin(playerOrder, num, 30, [21, 62]);
                this.fillin(playerOrder, num, 31, [44, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;
            }

            return 128;
        }

        if (nrPlayers < 6) {
            // only 1 group of <=5
            return 0;
        } else if (nrPlayers < 11) {
            // 2 groups, 4 players in KO
            playerOrder.push(1, 4, 3, 2);
            return 4;
        } else if (nrPlayers < 20) {
            if (nrPlayers < 16) {
                // 3 groups, 6 players in KO
                playerOrder.push(1, 8, 5, 6, 4, 3);
            } else {
                // 4 groups, 8 players in KO
                playerOrder.push(1, 8, 5, 4, 6, 3, 2, 7);
            }
            return 8;
        } else if (nrPlayers < 36) {
            if (nrPlayers < 24) {
                // 5 groups, 10 players in KO
                playerOrder.push(1, 16, 9, 8, 5, 12, 4, 3, 13, 14);
            } else if (nrPlayers < 28) {
                // 6 groups, 12 players in KO
                playerOrder.push(1, 16, 9, 8, 5, 12, 13, 3, 4, 14, 11, 6);
            } else if (nrPlayers < 32) {
                // 7 groups, 14 players in KO
                playerOrder.push(1, 16, 9, 8, 5, 12, 14, 11, 3, 4, 13, 10, 6, 7);
            } else {
                // 8 groups, 16 players in KO
                playerOrder.push(1, 16, 9, 8, 5, 12, 14, 3, 11, 6, 4, 13, 10, 7, 2, 15);
            }
            return 16;
        } else if (nrPlayers < 68) {
            if (nrPlayers < 40) {
                // 9 groups, 18 players in KO
                playerOrder.push(1, 32, 17, 16, 24, 9, 25, 8, 21, 20, 12, 5, 28, 13, 30, 4, 29, 3);
            } else if (nrPlayers < 44) {
                // 10 groups, 20 players in KO
                playerOrder.push(1, 32, 17, 16, 24, 9, 25, 8, 21, 12, 28, 5, 14, 20, 13, 19, 3, 29, 4, 30);
            } else if (nrPlayers < 48) {
                // 11 groups, 22 players in KO
                playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 20, 4, 12, 29, 22, 13, 11, 19, 30, 14, 3);
            } else if (nrPlayers < 52) {
                // 12 groups, 24 players in KO
                playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 20, 13, 4, 29, 30, 3, 14, 19, 22, 11, 6, 27);
            } else if (nrPlayers < 56) {
                // 13 groups, 26 players in KO
                playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 4, 3, 29, 30, 14, 11, 19, 22, 6, 7, 27, 26);
            } else if (nrPlayers < 60) {
                // 14 groups, 28 players in KO
                playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 29, 4, 3, 30, 19, 14, 11, 22, 27, 6, 7, 26, 23, 10);
            } else if (nrPlayers < 64) {
                // 15 groups, 30 players in KO
                playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20,
                    29, 30, 4, 3, 19, 22, 14, 11, 27, 26, 6, 7, 23, 18, 10, 15);
            } else {
                // 16 groups, 32 players in KO
                playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 29,
                4, 30, 3, 14, 19, 22, 11, 6, 27, 26, 7, 10, 23, 18, 15, 2, 31);
            }
            return 32;
        }
    }

    // TODO make sure that No-group-phase is not allowed for DCV
    /**
    * Specifies the order in which the 1-2 best of each group are assigned to the A-KO round
    * This implements the current (from April 2018) official mode of the DCV
    * @param {number} nrPlayers the total number of players
    * @param playerOrder array that will hold the order
    *         1st of group A, 2nd group A, 1st group B, 2nd group B, ...
    * @return the number of players (maybe some BYEs) in the first round of KO
    */
    calculateKO_DCV_A(nrPlayers: number, playerOrder: number[]): -1 | 0 | 4 | 8 | 16 | 32 | 64 | 128 {
        if (!this.doGroupPhase) {
            // TODO adapt
            if (nrPlayers <= 4) {
                const num = 4;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;
            } else if (nrPlayers <= 8) {
                // playerOrder.push(shuffleArray([1, 8]), shuffleArray([5, 4]), shuffleArray([6, 3]), shuffleArray([2, 7]));
                const num = 8;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [7, 8]);
                this.fillin(playerOrder, num, 2, [6, 5]);
                this.fillin(playerOrder, num, 3, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;
            } else if (nrPlayers <= 16) {
                const num = 16;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [15, 16]);
                this.fillin(playerOrder, num, 2, [8, 7]);
                this.fillin(playerOrder, num, 3, [11, 12]);
                this.fillin(playerOrder, num, 4, [5, 6]);
                this.fillin(playerOrder, num, 5, [10, 9]);
                this.fillin(playerOrder, num, 6, [14, 13]);
                this.fillin(playerOrder, num, 7, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;

            } else if (nrPlayers <= 32) {
                const num = 32;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [31, 32]);
                this.fillin(playerOrder, num, 2, [18, 17]);
                this.fillin(playerOrder, num, 3, [16, 15]);
                this.fillin(playerOrder, num, 4, [9, 10]);
                this.fillin(playerOrder, num, 5, [23, 24]);
                this.fillin(playerOrder, num, 6, [26, 25]);
                this.fillin(playerOrder, num, 7, [8, 7]);
                this.fillin(playerOrder, num, 8, [5, 6]);
                this.fillin(playerOrder, num, 9, [27, 28]);
                this.fillin(playerOrder, num, 10, [22, 21]);
                this.fillin(playerOrder, num, 11, [12, 11]);
                this.fillin(playerOrder, num, 12, [13, 14]);
                this.fillin(playerOrder, num, 13, [19, 20]);
                this.fillin(playerOrder, num, 14, [30, 29]);
                this.fillin(playerOrder, num, 15, [4, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;

            } else if (nrPlayers <= 64) {
                const num = 64;
                this.fillin(playerOrder, num, 0, [1, 2]);
                this.fillin(playerOrder, num, 1, [64, 63]);
                this.fillin(playerOrder, num, 2, [32, 34]);
                this.fillin(playerOrder, num, 3, [33, 31]);
                this.fillin(playerOrder, num, 4, [16, 18]);
                this.fillin(playerOrder, num, 5, [49, 47]);
                this.fillin(playerOrder, num, 6, [17, 50]);
                this.fillin(playerOrder, num, 7, [48, 15]);
                this.fillin(playerOrder, num, 8, [8, 10]);
                this.fillin(playerOrder, num, 9, [57, 55]);
                this.fillin(playerOrder, num, 10, [25, 23]);
                this.fillin(playerOrder, num, 11, [40, 42]);
                this.fillin(playerOrder, num, 12, [9, 39]);
                this.fillin(playerOrder, num, 13, [56, 26]);
                this.fillin(playerOrder, num, 14, [24, 58]);
                this.fillin(playerOrder, num, 15, [41, 7]);
                this.fillin(playerOrder, num, 16, [4, 11]);
                this.fillin(playerOrder, num, 17, [61, 54]);
                this.fillin(playerOrder, num, 18, [29, 43]);
                this.fillin(playerOrder, num, 19, [36, 22]);
                this.fillin(playerOrder, num, 20, [13, 38]);
                this.fillin(playerOrder, num, 21, [52, 27]);
                this.fillin(playerOrder, num, 22, [20, 59]);
                this.fillin(playerOrder, num, 23, [45, 6]);
                this.fillin(playerOrder, num, 24, [5, 51]);
                this.fillin(playerOrder, num, 25, [60, 14]);
                this.fillin(playerOrder, num, 26, [28, 46]);
                this.fillin(playerOrder, num, 27, [37, 19]);
                this.fillin(playerOrder, num, 28, [12, 35]);
                this.fillin(playerOrder, num, 29, [53, 30]);
                this.fillin(playerOrder, num, 30, [21, 62]);
                this.fillin(playerOrder, num, 31, [44, 3]);
                this.logger.trace('playerOrder: ' + playerOrder);
                return num;
            }

            return 128;
        }

        if (nrPlayers <= 5) {
            // only 1 group of <=5
            return 0;
        } else if (nrPlayers <= 10) {
            // 2 groups, 4 players in KO
            playerOrder.push(1, 4, 3, 2);
            return 4;
        } else if (nrPlayers <= 15) {
            // 3 groups, 6 players in KO
            playerOrder.push(1, 8, 5, 6, 4, 3);
            return 8;
        } else if (nrPlayers <= 23) {
            playerOrder.push(1, 8, 5, 4, 3, 6, 7, 2);
            return 8;

        } else if (nrPlayers <= 26) {
            // 16 groups, 16 players in KO
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 10, 7, 6, 11, 14, 3, 2, 15);
            return 16;
        } else if (nrPlayers <= 48) {
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 3, 14, 11, 6, 7, 10, 15, 2);
            return 16;

        } else {
            this.logger.debug('error: only <= 48 players in one category possible; current number:', nrPlayers);
            this.alertService.error($localize`Error: at most 48 players in one category possible; current number:${nrPlayers.toString()}:nrOf:`);
            return -1;
        }
    }

    /**
    * Specifies the order in which the 1-2 'worst' of each group are assigned to the B-KO round
    * This implements the current (from April 2018) official mode of the DCV
    * @param {number} totalNrPlayers the total number of players (A+B)!
    * @param playerOrder array that will hold the order
    *         2nd of group X1, 2nd group X2, ..., 3rd group Y1, 3nd group Y2, ...
    * @return the number of players (maybe some BYEs) in the first round of KO
    */
    calculateKO_DCV_B(totalNrPlayers: number, playerOrder: number[]): number {
        if (totalNrPlayers <= 15) {
            this.logger.error('error: should not have B-KO with less than 16 payers; current number:', totalNrPlayers);
            this.alertService.error($localize`Error: should not have B-KO with less than 16 payers; current number: ${totalNrPlayers.toString()}:nrOf:`);
            return -1;
        } else if (totalNrPlayers === 16) {
            playerOrder.push(1, 8, 5, 4, 3, 6, 7, 2);
            return 8;
        } else if (totalNrPlayers === 17) {
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 3);
            return 16;
        } else if (totalNrPlayers <= 20) {
            playerOrder.push(1, 16, 9, 8, 6, 11, 4, 13, 12, 5, 14, 3);
            return 16;
        } else if (totalNrPlayers <= 23) {
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 3, 14, 11, 6, 7, 10, 15);
            return 16;
        } else if (totalNrPlayers === 24) {
            playerOrder.push(1, 8, 5, 4, 3, 6, 7, 2);
            return 8;
        } else if (totalNrPlayers <= 26) {
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 11, 6);
            return 16;
        } else if (totalNrPlayers <= 29) {
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 3, 14, 11, 7);
            return 16;
        } else if (totalNrPlayers <= 32) {
            playerOrder.push(1, 16, 9, 8, 5, 12, 13, 4, 3, 14, 11, 6, 10, 7, 15, 2);
            return 16;
        } else if (totalNrPlayers <= 35) {
            playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 29, 4, 3, 30, 19);
            return 32;
        } else if (totalNrPlayers <= 38) {
            playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 29, 4, 3, 30, 19, 14, 22, 11);
            return 32;
        } else if (totalNrPlayers <= 41) {
            playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 29, 4, 3, 30, 19, 14, 11, 22, 27, 6, 7);
            return 32;
        } else if (totalNrPlayers <= 44) {
            playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20, 29, 4, 3, 30, 19, 14, 11, 22, 27, 6, 7, 26, 23, 10);
            return 32;
        } else if (totalNrPlayers <= 47) {
            playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20,
                29, 4, 3, 30, 19, 14, 11, 22, 27, 6, 7, 26, 23, 10, 15, 18, 31);
            return 32;
        } else if (totalNrPlayers <= 48) {
            playerOrder.push(1, 32, 17, 16, 9, 24, 25, 8, 5, 28, 21, 12, 13, 20,
                29, 4, 31, 2, 15, 18, 23, 10, 7, 26, 27, 6, 11, 22, 19, 14, 3, 30);
            return 32;
        } else {
            this.logger.debug('error: only <= 48 players in one category possible; current number:', totalNrPlayers);
            this.alertService.error($localize`Error: at most 48 players in one category possible; current number:${totalNrPlayers.toString()}:nrOf:`);
        }
    }

    /**
     * Displays a % status as alert
     */
    updateStatus(status: number) {
        this.alertService.info('Status: ' + (status * 100) + '%');
    }

    // assign participants to groups according to DCV / ICO guideline
    // e.g. 1 or 2; 3 or 4; 5-8; 9-16; 17-...
    // (it's always [1,2], [3,4] (except 3 groups), then 5 till number of groups max 8;
    //  then fill each layer; last layer from the right (last group first))
    assignToGroups(groupSizes: number[]) {
        if (groupSizes.length === 1) {
            for (let playerNr = 0; playerNr < this.players.length; playerNr++) {
                // just assign players according to their order (rank) one by one
                this.assignPlayers(0, 0, playerNr, playerNr, false);
            }

        } else {
            // here we have at least 2 groups

            // first layer is different
            // rank 1 or 2 in group 1 or 2
            this.assignPlayers(0, 1, 0, 1, true);

            if (groupSizes.length === 3) {
                // fixed rank 3 in group 3
                this.assignPlayers(2, 2, 2, 2, false);
                // done
            } else if (groupSizes.length > 3) {
                // have at least 4 groupSizes; rank 3 or 4 in group 3 or 4
                this.assignPlayers(2, 3, 2, 3, true);

                if (groupSizes.length > 4) {
                    this.assignPlayers(4, Math.min(7, groupSizes.length - 1), 4, Math.min(7, groupSizes.length - 1), true);

                    if (groupSizes.length > 8) {
                        this.assignPlayers(8, Math.min(15, groupSizes.length - 1), 8, Math.min(15, groupSizes.length - 1), true);

                        if (groupSizes.length > 16) {
                            // not supported
                            this.logger.debug('More than 16 groups not supported!');
                            this.alertService.error($localize`More than 16 groups not supported!`);
                        }
                    }
                }
            }
            // all layers but first and last just fill the whole layer randomly
            const lastLayer = Math.round(this.players.length / groupSizes.length + 0.5);
            for (let layerNr = 2; layerNr < lastLayer; layerNr++) {
                const startPlayer = (layerNr - 1) * groupSizes.length;
                this.assignPlayers(0, groupSizes.length - 1, startPlayer, startPlayer + groupSizes.length - 1, true);
            }

            // last layer, start filling with last group
            const playersLeft = this.players.length % groupSizes.length;
            if (playersLeft > 0) {
                this.assignPlayers(groupSizes.length - playersLeft, groupSizes.length - 1,
                    this.players.length - playersLeft, this.players.length - 1, true);
            }
        }
    }

    /**
     * Generate the according number of Matches for each group
     * Each Match contains the 2 PlayerRefs from the group
     */
    createGroupMatches() {
        const gameOrder = [[], [], [],
            // for group of 3: 1-3, 2-3, 1-2
            [0, 2, 1, 2, 0, 1],
            [0, 3, 1, 2, 0, 2, 1, 3, 2, 3, 0, 1],
            [2, 4, 1, 3, 0, 4, 1, 2, 0, 3, 1, 4, 0, 2, 3, 4, 0, 1, 2, 3]
        ];

        for (let grNr = 0; grNr < this.groups.length; grNr++) {
            const group = this.groups[grNr];
            const go = gameOrder[group.groupSize];
            for (let g = 0; g < go.length - 1; g += 2) {
                group.matches.push(new Match(group.players[go[g]], group.players[go[g + 1]],
//                    {level: String.fromCharCode(grNr + 65), num: g / 2, ko_type: 'G'}));
                    {level: grNr.toString(), num: g / 2, ko_type: 'G'}));
            }
        }
    }

    private updateBestSeconds() {
        const pointsOfSeconds: {ptcalc: {points: number, games: number, sets: number, setslost: number, relpoints: number, pointswon: number, pointslost: number, ranking: number}, gNr: number}[] = [];
        for (let gNr = 0; gNr < this.groups.length; gNr++) {
            const pOS = this.calculatePoints(this.groups[gNr], this.groups[gNr].winners[1]);
            pointsOfSeconds.push({ptcalc: pOS, gNr: gNr});
        }
        pointsOfSeconds.sort(function(pos1, pos2) {
            return pos2.ptcalc.points - pos1.ptcalc.points;
        });
        for (let p = 0; p < pointsOfSeconds.length; p++) {
            this.rankingBestSeconds[p] = {
                group: pointsOfSeconds[p].gNr,
                groupName: String.fromCharCode(pointsOfSeconds[p].gNr + 65),
                player: this.groups[pointsOfSeconds[p].gNr].winners[1],
                points: pointsOfSeconds[p].ptcalc.points,
                games: pointsOfSeconds[p].ptcalc.games,
                sets: pointsOfSeconds[p].ptcalc.sets,
                setslost: pointsOfSeconds[p].ptcalc.setslost,
                relpoints: pointsOfSeconds[p].ptcalc.relpoints,
                pointswon: pointsOfSeconds[p].ptcalc.pointswon,
                pointslost: pointsOfSeconds[p].ptcalc.pointslost,
                ranking: pointsOfSeconds[p].ptcalc.ranking
            };
        }

        this.updateKO();
    }

    // same calculation as in group.ts but too different since only for the
    // given player (and seeding position not relative to other player) (?)
    private calculatePoints(group: Group, player: PlayerRef): {points: number, games: number, sets: number, setslost: number, relpoints: number, pointswon: number, pointslost: number, ranking: number} {
        // can't use second.calcPoints because those are used
        // to calculate position within group

        const data: {points: number, games: number, sets: number, setslost: number, relpoints: number, pointswon: number, pointslost: number, ranking: number} = {
                points: 0, games: 0, sets: 0, setslost: 0, relpoints: 0, pointswon: 0, pointslost: 0, ranking: 0};
        // 7th: seed ranking position
        data.ranking = player.playerListIdx + 1;

        // weigh each match
        for (let i = 0; i < group.matches.length; i++) {
            const match = group.matches[i];

            if ((match.player1.playerListIdx !== player.playerListIdx &&
                match.player2.playerListIdx !== player.playerListIdx) ||
                (group.winners.length > 3 &&
                    (match.player1.playerListIdx === group.winners[3].playerListIdx ||
                    match.player2.playerListIdx === group.winners[3].playerListIdx))) {
                // player not part of this match, ignore
                continue;
            }

            let winner = Match.checkWinner(match);
            // 1st: match wins
            if (winner === -1) {
                // player as player1 won
                if (match.player1.playerListIdx === player.playerListIdx) {
                    data.games += 1;
                }
            } else if (winner === 1) {
                // player as player2 won
                if (match.player2.playerListIdx === player.playerListIdx) {
                    data.games += 1;
                }
            }

            // weigh each set
            for (let s = 0; s < match.score.length; s++) {
                const set = match.score[s];

                winner = Match.checkWinnerSet(set);
                // 2nd: set wins
                // 3rd: set losses
                if (winner === -1) {
                    if (match.player1.playerListIdx === player.playerListIdx) {
                        data.sets += 1;
                    } else {
                        data.setslost += 1;
                    }
                } else if (winner === 1) {
                    if (match.player2.playerListIdx === player.playerListIdx) {
                        data.sets += 1;
                    } else {
                        data.setslost += 1;
                    }
                }
                if (match.player1.playerListIdx === player.playerListIdx) {
                    data.pointswon += set.p1;
                    data.pointslost += set.p2;
                } else {
                    data.pointswon += set.p2;
                    data.pointslost += set.p1;
                }
            }
        }

        // 4th: relative points
        data.relpoints += data.pointswon - data.pointslost;
        // 5th: point wins
        // 6th: point losses

        // add all points
        data.points += data.games * Group.gamePts;
        data.points += data.sets * Group.setsWonPts;
        data.points += data.sets * Group.setsLostPts;
        data.points += (data.pointswon - data.pointslost) * Group.ptsRelPts;
        data.points += data.pointswon * Group.ptsWonPts;
        data.points += data.pointslost * Group.ptsLostPts;
        data.points += data.ranking * Group.rankingPts;

        return data;
    }

    updateAfterGroupChange(info: { groupNr: number, categoryId: string }) {
        if (this.categoryId !== info.categoryId) {
            return;
        }
        const gNr = info.groupNr;
        if (gNr >= 0 && gNr < this.groups.length) {
            Group.updateWinners(this.logger, this.groups[gNr]);
        } else {
            for (let groupNr = 0; groupNr < this.groups.length; groupNr++) {
                Group.updateWinners(this.logger, this.groups[groupNr]);
            }
        }
        if (this.players.length >= 16 && this.assignmentMode === 'DCV') {
            // TODO this repeats a lot of calculations already done in updateWinners
            this.updateBestSeconds();
        }
        if (this.ko && this.ko.matches && this.ko.matches.length >= 1) {
            // check for BYE players in first round of KO
            const firstLevel = this.ko.matches.length - 1;
            for (let i = 0; i < this.ko.matches[firstLevel].length; i++) {
                const m = this.ko.matches[firstLevel][i];
                const winner = Match.checkWinner(m);
                if (winner === -1) {
                    PlayerRef.update(this.logger, m.winner, m.player1, this.ko, this.groups);
                } else if (winner === 1) {
                    PlayerRef.update(this.logger, m.winner, m.player2, this.ko, this.groups);
                }
            }
        }
    }


    // after a best 2nd changed, need to update
    // update matches in matches[firstLevel] that have a player that was changed
    changeAssignment(ko: Ko, theKoNumber: number, i: number, newAssignment: {groupNr: number, winnerNr: number}) {
        const firstLevel = Math.log2(theKoNumber) - 1;
        let match;
        if (i % 2 === 0) {
            match = ko.matches[firstLevel][i / 2];
            match.player1.groupIdx = newAssignment;
        } else {
            match = ko.matches[firstLevel][(i - 1) / 2];
            match.player2.groupIdx = newAssignment;
        }
        // there might be already some matches played with the replaced played - we ignore this case here!
        // but there might be a player with a Freilos changed. So we need to calculate the winner
        this.updateMatch(match);
    }


    updateKO() {
        if (typeof this.playerOrder === 'undefined' || this.playerOrder.length === 0) {
            this.logger.debug('ignoring call to updateKO since playerOrder is undefined / empty');
            return;
        }
        this.logger.trace('updating KO phase after best 2nd change for ' + this.players.length + ' players/pairs');

        let cnt = 0;
        for (let i = this.groups.length; i < this.koNumber; i++) {
            // fill rest of this.koNumber-this.groups.length players from ranking of group 2nd
            const gNr = this.rankingBestSeconds[cnt++].group;
            this.changeAssignment(this.ko, this.koNumber, this.playerOrder[i] - 1, {groupNr: gNr, winnerNr: 1});
            this.logger.trace('  put 2nd of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder[i]);
        }

        // update B KO round
        if (this.doBRound) {
            this.logger.trace('updating B-KO');

            // number of 2nd in groups to use for KO-B
            // TODO this probably won't work >48 players
            const remaining2nd = (2 * this.groups.length - this.koNumber) % 8;

            cnt = 0;
            this.logger.trace('updating B-KO phase after change in best 2nd');

            // first assign 2nd in groups according to assignment rules
            for (; cnt < remaining2nd; cnt++) {
                if (cnt >= this.playerOrder_b.length) { break; }
                const gNr = this.rankingBestSeconds[this.koNumber - this.groups.length + cnt].group;
                this.logger.trace('  put #2 of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder_b[cnt]);
                this.changeAssignment(this.ko_b, this.koNumber_b, this.playerOrder_b[cnt] - 1, {groupNr: gNr, winnerNr: 1});
            }
            // then assign all 3rd in group, from group A to higher
            for (let gNr = 0; gNr < this.groups.length; gNr++) {
                if (cnt >= this.playerOrder_b.length) { break; }
                this.logger.trace('  put #3 of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder_b[cnt]);
                this.changeAssignment(this.ko_b, this.koNumber_b, this.playerOrder_b[cnt] - 1, {groupNr: gNr, winnerNr: 2});
                cnt++;
            }
            // and last, put remaining 4th in group, from last group to lower
            for (let gNr = this.groups.length - 1; gNr >= 0 && this.groups[gNr].groupSize > 3; gNr--) {
                if (cnt >= this.playerOrder_b.length) { break; }
                this.logger.trace('  put #4 of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder_b[cnt]);
                this.changeAssignment(this.ko_b, this.koNumber_b, this.playerOrder_b[cnt] - 1, {groupNr: gNr, winnerNr: 3});
                cnt++;
            }
            if (cnt > this.playerOrder_b.length) {
                this.logger.warn('error: had more players (', cnt, ') to assign than koAssignment gives');
            } else {
                this.logger.trace('assigned', cnt, 'players to KO-B');
            }
        }
    }


    /**
    * Main entry point
    */
    startAssignment() {
        if (this.assignmentMode === 'DCV') {
            if (this.players.length > 48) {
                this.logger.debug('More than 48 players / double teams (found ' + this.players.length + ') not supported yet!');
                this.alertService.error($localize`More than 48 players / double teams (found ${this.players.length}:nrOf:) not supported yet!`);
                this.updateStatus(1);
                return;
            }
        } else {
            if (this.players.length > 64) {
                this.logger.debug('More than 64 players / double teams (found ' + this.players.length + ') not supported yet!');
                this.alertService.error($localize`More than 64 players / double teams (found ${this.players.length}:nrOf:) not supported yet!`);
                this.updateStatus(1);
                return;
            }
        }

        if (this.doBRound) {
            // use A/B doBRound, groups of 3 (or 4), only best (and some best 2nd) get to A-KO, rest 2nd, 3rd, or 4th go to B-KO
            this.numberOfBestPlayersToUse = 1;
        }

        this.logger.trace('Assigning ' + this.players.length + ' players / double teams');

        // sort according to rank / points
        this.sortByRank();
        this.updateStatus(0.2);

        let groupSizes;
        if (this.doGroupPhase) {
            // specify number of groups and number of players per group, see DCV / ICO guidelines
            if (this.assignmentMode === 'DCV') {
                groupSizes = this.calculateGroups_DCV(this.players.length);
            } else {
                groupSizes = this.calculateGroups_ICO(this.players.length);
            }
            if (groupSizes.length <= 0) {
                this.logger.debug('Error, 0 groups generated. Maybe you have selected Double=True for a single category?');
                this.alertService.error($localize`Error, 0 groups generated. Maybe you have selected Double=True for a single category?`);
                return;
            }

            for (let groupNr = 0; groupNr < groupSizes.length; groupNr++) {
                // initialize groups
                this.groups[groupNr] = new Group(groupSizes[groupNr], String.fromCharCode(groupNr + 65));
            }
            this.updateStatus(0.5);

            // assign players from players list to the groups
            this.assignToGroups(groupSizes);
            this.updateStatus(0.6);

            // create the corresponding number of Match-es for all groups
            this.createGroupMatches();

            // calculate the initial ranking within groups and the 2nd bests
            this.updateAfterGroupChange({ groupNr: -1, categoryId: this.categoryId });
        }

        this.updateStatus(0.7);

        // create final rounds (KO, maybe A and B)
        this.playerOrder = [];
        if (this.assignmentMode === 'DCV') {
            this.koNumber = this.calculateKO_DCV_A(this.players.length, this.playerOrder);
        } else {
            this.koNumber = this.calculateKO_ICO(this.players.length, this.playerOrder);
        }
        this.logger.trace('playerOrder: ' + this.playerOrder);

        if (this.koNumber > 64) {
            // not supported
            this.alertService.error($localize`More than 64 players / double teams in final round not supported yet!`);
            return;
        }

        if (this.koNumber > 0) {
            this.updateStatus(0.8);

            if (this.doGroupPhase) {
                if (this.players.length >= 16 && this.assignmentMode === 'DCV') {
                    // reference best of each group into kophase
                    this.logger.trace('KO phase for ' + this.players.length + ' players/pairs, ' + this.playerOrder);

                    for (let gNr = 0; gNr < this.groups.length; gNr++) {
                        // '-1' since playOrders are 1-based
                        this.koAssignment[this.playerOrder[gNr] - 1] = {group: gNr, num: 0};
                        this.logger.trace('  put 1st of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder[gNr]);
                    }

                    let cnt = 0;
                    this.nrBestSecondsForA = this.koNumber - this.groups.length;
                    for (let i = this.groups.length; i < this.koNumber; i++) {
                        // fill rest of this.koNumber-this.groups.length players from ranking of group 2nd
                        const gNr = this.rankingBestSeconds[cnt++].group;
                        this.koAssignment[this.playerOrder[i] - 1] = {group: gNr, num: 1};
                        this.logger.trace('  put 2nd of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder[i]);
                    }

                } else {
                    // reference numberOfBestPlayersToUse winners from group phase into kophase
                    let cnt = 0;
                    this.logger.trace('KO phase for ' + this.players.length + ' players/pairs, ' + this.playerOrder);
                    for (let best = 0; best < this.numberOfBestPlayersToUse; best++) {
                        for (let gNr = 0; gNr < this.groups.length; gNr++) {
                            this.logger.trace('  put', best === 0 ? '1st' : '2nd', 'of group',
                                String.fromCharCode(gNr + 65), 'to', this.playerOrder[cnt]);
                            // '-1' since playOrders are 1-based
                            this.koAssignment[this.playerOrder[cnt] - 1] = {group: gNr, num: best};
                            cnt++;
                        }
                    }
                }

            } else {
                // no group phase
                this.logger.trace('KO, no group phase ' + this.koNumber + ' players/pairs, ' + this.playerOrder);
                for (let slotNr = 0; slotNr < this.koNumber; slotNr++) {
                    this.logger.trace('  put player', slotNr, 'to', this.playerOrder[slotNr]);
                    // '-1' since playOrders are 1-based
                    this.koAssignment[this.playerOrder[slotNr] - 1] = {group: -1, num: slotNr};
                }
            }

            // create B KO round
            if (this.doBRound) {
                this.logger.trace('calculating B-KO');
                const nrPlayers_B = this.players.length - this.koNumber;

                if (!this.doGroupPhase) {
                    // no group phase
                    this.logger.debug('no group phase ' + this.koNumber + ' players/pairs, not supperted!');
                    // makes no sense
                    this.alertService.error($localize`Having a B-KO round without group phase not supported!`);
                    return;
                }

                this.playerOrder_b = [];
                if (!groupSizes) {
                    // may need to calculate here if doGroupPhase was false
                    groupSizes = this.calculateGroups_DCV(this.players.length);
                }
                if (groupSizes.length <= 0) {
                    this.logger.debug('Error, 0 groupSizes generated. Maybe you have selected Double=True for a single category?');
                    this.alertService.error($localize`Error, 0 groupSizes generated. Maybe you have selected Double=True for a single category?`);
                    return;
                }

                // calculated from ALL players
                this.koNumber_b = this.calculateKO_DCV_B(this.players.length, this.playerOrder_b);
                if (this.koNumber_b > 64) {
                    // not supported
                    this.logger.debug('More than 64 players / double teams in final round not supported yet!');
                    this.alertService.error($localize`More than 64 players / double teams in final round not supported yet!`);
                    return;
                }

                // number of 2nd in groups to use for KO-B
                // TODO this probably won't work >48 players
                const remaining2nd = (2 * this.groups.length - this.koNumber) % 8;

                let cnt = 0;
                this.logger.trace('B-KO phase for ' + nrPlayers_B + ' players/pairs, ' + this.playerOrder_b);

                // first assign 2nd in groups according to assignment rules
                for (; cnt < remaining2nd; cnt++) {
                    if (cnt >= this.playerOrder_b.length) { break; }
                    const gNr = this.rankingBestSeconds[this.koNumber - this.groups.length + cnt].group;
                    this.koAssignment_b[this.playerOrder_b[cnt] - 1] = {group: gNr, num: 1};
                }
                // then assign all 3rd in group, from group A to higher
                for (let gNr = 0; gNr < this.groups.length; gNr++) {
                    if (cnt >= this.playerOrder_b.length) { break; }
                    this.logger.trace('  put #3 of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder_b[cnt]);
                    this.koAssignment_b[this.playerOrder_b[cnt] - 1] = {group: gNr, num: 2};
                    cnt++;
                }
                // and last, put remaining 4th in group, from last group to lower
                for (let gNr = this.groups.length - 1; gNr >= 0 && this.groups[gNr].groupSize > 3; gNr--) {
                    if (cnt >= this.playerOrder_b.length) { break; }
                    this.logger.trace('  put #4 of group', String.fromCharCode(gNr + 65), 'to', this.playerOrder_b[cnt]);
                    this.koAssignment_b[this.playerOrder_b[cnt] - 1] = {group: gNr, num: 3};
                    cnt++;
                }
                if (cnt > this.playerOrder_b.length) {
                    this.logger.warn('error: had more players (', cnt, ') to assign than koAssignment gives');
                } else {
                    this.logger.trace('assigned', cnt, 'players to KO-B');
                }

                this.updateStatus(0.85);
            }
        }

        this.updateStatus(1);
    }

    downloadResult() {
        // TODO implement
    }

    calculateResults() {
        const infos: ResultInfo = this.getResultInfoObject();
        this.tournamentsService.calculateResults(this.tournamentId, infos, (infos: ResultInfo) => this.storeResultInfoObject(infos));
    }

    // called from tournament.component
    addPointsToRanking() {
        const infos: ResultInfo = this.getResultInfoObject();
        this.tournamentsService.addPointsToRanking(infos, false);
    }

    // called from tournament.component
    addPointsToDMQualiRanking() {
        const infos: ResultInfo = this.getResultInfoObject();
        this.tournamentsService.addPointsToRanking(infos, true);
    }

    exportCSV() {
        const infos: ResultInfo = this.getResultInfoObject();
        this.tournamentsService.calculateResults(this.tournamentId, infos, (infos: ResultInfo) => {
            this.storeResultInfoObject(infos);

            // 1;;KAATZ;Sönke;Winner;
            const csv = [];

            if (this.resultsRanking.length > 0) {
                for (let r = 0; r < this.resultsRanking.length; r++) {
                    const res = this.resultsRanking[r];
                    csv.push({rank: res.rank, id: [], last_name: res.player.last_name, first_name: res.player.first_name, comment: res.points});
                    if (res.partner) {
                        csv.push({rank: res.rank, id: [], last_name: res.partner.last_name, first_name: res.partner.first_name, comment: res.points});
                    }
                }
            }

            const filename = this.category && this.category.name ? (this.category.name) : 'results';
            // tslint:disable-next-line: no-unused-expression
            // new Angular5Csv(csv, filename, this.csvOptions);
            this.csvOptions.title = filename;
            this.csvOptions.filename = filename;
            const csvConfig = mkConfigCsv({ useKeysAsHeaders: true });
            const csvData = generateCsv(csvConfig)(csv);
            downloadCsv(csvConfig)(csvData)
        });
    }

    // exportCSV_b() {
    //     if (this.results_b.length > 0) {
    //         var options = {
    //             fieldSeparator: ';', quoteStrings: '"'
    //         };
    //
    //         const csv_b = [];
    //         for (let r = 0; r < this.results_b.length; r++) {
    //             const res = this.results_b[r];
    //             csv_b.push({rank: res.rank, empty: [], last_name: res.player.last_name, first_name: res.player.first_name, comment: res.points + ', B-Runde'});
    //             if (res.partner) {
    //                 csv_b.push({rank: res.rank, empty: [], last_name: res.partner.last_name, first_name: res.partner.first_name, comment: res.points + ', B-Runde'});
    //             }
    //         }
    //
    //         const filename_b = this.category && this.category.name ? (this.category.name + '_b') : 'results_b';
    //         new Angular5Csv(csv_b, filename_b, options);
    //     }
    // }

    exportCSV_others(res: string) {
        if (this.resultsRankingFromOtherCats[res].length > 0) {
            const csv = [];
            for (let r = 0; r < this.resultsRankingFromOtherCats[res].length; r++) {
                const result = this.resultsRankingFromOtherCats[res][r];
                csv.push({rank: result.rank, id: [], last_name: result.player.last_name, first_name: result.player.first_name, comment: result.points + (result.isBRound ? ', B-Runde' : '')});
                if (result.partner) {
                    csv.push({rank: result.rank, id: [], last_name: result.partner.last_name, first_name: result.partner.first_name, comment: result.points + (result.isBRound ? ', B-Runde' : '')});
                }
            }

            const filename = res;
            // tslint:disable-next-line: no-unused-expression
            // new Angular5Csv(csv, filename, this.csvOptions);
            this.csvOptions.title = filename;
            this.csvOptions.filename = filename;
            const csvConfig = mkConfigCsv({ useKeysAsHeaders: true });
            const csvData = generateCsv(csvConfig)(csv);
            downloadCsv(csvConfig)(csvData)
        }
    }

    getOwnPropertyNames(something) {
        return Object.getOwnPropertyNames(something);
    }

    getResultInfoObject(): ResultInfo {
        return { tournament: this.tournament, results: this.results, category: this.category, maxPoints: this.maxPoints, isDouble: this.isDouble, groups: this.groups, results_b: this.results_b, ko: this.ko, ko_b: this.ko_b, players: this.players, resultsRanking: this.resultsRanking, doBRound: this.doBRound, resultsFromOtherCats: this.resultsFromOtherCats, resultsRankingFromOtherCats: this.resultsRankingFromOtherCats };
    }
    storeResultInfoObject(infos: ResultInfo): void {
        this.tournament = infos.tournament;
        this.results = infos.results;
        this.category = infos.category;
        this.maxPoints = infos.maxPoints;
        this.isDouble = infos.isDouble;
        this.groups = infos.groups;
        this.results_b = infos.results_b;
        this.ko = infos.ko;
        this.ko_b = infos.ko_b;
        this.players = infos.players;
        this.resultsRanking = infos.resultsRanking;
        this.doBRound = infos.doBRound;
        this.resultsFromOtherCats = infos.resultsFromOtherCats;
        this.resultsRankingFromOtherCats = infos.resultsRankingFromOtherCats;
    }

    removePoints(isDCVquali: boolean) {
        const sendResults = [];
        const sendResults_b = [];
        const sendResultsRanking = [];

        // TODO these two cases do exactly the same?!
        if (isDCVquali) {
            for (const catName of Object.getOwnPropertyNames(this.resultsFromOtherCats)) {
                if (this.resultsFromOtherCats[catName].length > 0) {
                    for (let r = 0; r < this.resultsFromOtherCats[catName].length; r++) {
                        this.resultsFromOtherCats[catName][r].points = 0;
                    }
                }
            }
            for (let r = 0; r < this.results.length; r++) {
                this.results[r].points = 0;
                const partner = this.results[r].partner ? this.results[r].partner._id : undefined;
                sendResults.push({rank: this.results[r].rank, player: this.results[r].player._id, partner: partner, points: this.results[r].points});
            }
            for (let r = 0; r < this.results_b.length; r++) {
                this.results_b[r].points = 0;
                const partner = this.results_b[r].partner ? this.results[r].partner._id : undefined;
                sendResults_b.push({rank: this.results_b[r].rank, player: this.results_b[r].player._id, partner: partner, points: this.results_b[r].points});
            }
            // keep other
            for (let r = 0; r < this.resultsRanking.length; r++) {
                this.resultsRanking[r].points = 0;
                const partner = this.resultsRanking[r].partner ? this.resultsRanking[r].partner._id : undefined;
                sendResultsRanking.push({rank: this.resultsRanking[r].rank, player: this.resultsRanking[r].player._id, partner: partner, points: this.resultsRanking[r].points});
            }

        } else {
            for (const catName of Object.getOwnPropertyNames(this.resultsRankingFromOtherCats)) {
                if (this.resultsRankingFromOtherCats[catName].length > 0) {
                    for (let r = 0; r < this.resultsRankingFromOtherCats[catName].length; r++) {
                        this.resultsRankingFromOtherCats[catName][r].points = 0;
                    }
                }
            }
            for (let r = 0; r < this.resultsRanking.length; r++) {
                this.resultsRanking[r].points = 0;
                const partner = this.resultsRanking[r].partner ? this.resultsRanking[r].partner._id : undefined;
                sendResultsRanking.push({rank: this.resultsRanking[r].rank, player: this.resultsRanking[r].player._id, partner: partner, points: this.resultsRanking[r].points});
            }

            // keep other
            for (let r = 0; r < this.results.length; r++) {
                this.results[r].points = 0;
                const partner = this.results[r].partner ? this.results[r].partner._id : undefined;
                sendResults.push({rank: this.results[r].rank, player: this.results[r].player._id, partner: partner, points: this.results[r].points});
            }
            for (let r = 0; r < this.results_b.length; r++) {
                this.results_b[r].points = 0;
                const partner = this.results_b[r].partner ? this.results[r].partner._id : undefined;
                sendResults_b.push({rank: this.results_b[r].rank, player: this.results_b[r].player._id, partner: partner, points: this.results_b[r].points});
            }
        }

        this.matchdbService.updateResults(this.tournamentId, this.categoryId, sendResults, sendResults_b, sendResultsRanking).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('results stored ok');
                } else {
                    this.logger.error('Could not update results in the cloud!', response.error);
                    this.alertService.error($localize`Could not update results in the cloud!`, response.error);
                }
            },
            error => {
                this.logger.error('Could not update results in the cloud!', error);
                this.alertService.error($localize`Could not update results in the cloud!`, error);
            }
        );
    }
}
