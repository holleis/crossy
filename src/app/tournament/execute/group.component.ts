import { Component, OnInit, Input, OnDestroy, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Group } from '../group';
import { Match } from '../match';
import { Ko } from '../ko';

import { GroupsChangedService } from './groups-changed.service';
import { Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { SocketService, SockEvent } from '../../util/websockets/socket.service';

import { NGXLogger } from 'ngx-logger';
import { MatchdbService } from './matchdb.service';
import { EnrollmentInfo } from '../cat-info';

@Component({
    selector: 'app-group',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.css'],
})
export class GroupComponent implements OnInit, OnDestroy {

    @Input() players: EnrollmentInfo['players'];
    @Input() groups: Group[];
    @Input() tournamentId: string;
    @Input() categoryId: string;
    @Input() categoryIdx: number;
    @Input() catType: string;

    // only for storing locally through common API
    @Input() ko: Ko;
    @Input() ko_b: Ko;

    // groupTable[group][i][j].p1
    groupTable: Array<Array<Array<{p1: number, p2: number}>>> = [];
    subscription: Subscription;

    scorelistener = new Subject<{groupNr: number, matchNr: number, setNr: number, playerNr: number, value: number}>();

    @Input() canMakeChanges = false;

    @ViewChildren('pointInput') inputs: QueryList<any>;

    constructor(
        private logger: NGXLogger,
        private groupsChangedService: GroupsChangedService,
        private matchdbService: MatchdbService,
        private socketService: SocketService
    ) {
        const debounce = this.scorelistener.pipe(
            debounceTime(1500),
            distinctUntilChanged(),
            switchMap((newscore: {groupNr: number, matchNr: number, setNr: number, playerNr: number, value: number}) => {
                this.logger.trace('score update:', newscore);
                this.onScoreChange(newscore.groupNr, newscore.matchNr);
                return [];
            })
        );
        debounce.subscribe();
    }

    ngOnInit() {
        this.initGroupTable();
        for (let g = 0; g < this.groups.length; g++) {
            this.calculateGroupTable(g);
        }
        this.logger.trace('init group component; groupTable', this.groupTable);

        // whenever something changed in the groups, need to update
        this.subscription = this.groupsChangedService.groupChangeStatus.subscribe(
            info => {
                if (this.categoryId !== info.categoryId) {
                    return;
                }
                if (info.groupNr >= 0 && info.groupNr < this.groups.length) {
                    this.calculateGroupTable(info.groupNr);
                } else {
                    for (let g = 0; g < this.groups.length; g++) {
                        this.calculateGroupTable(g);
                    }
                }
            }
        );
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onKey(key: any, groupNr: number, matchNr: number, inputNr: number): boolean {
        // calculate the number of inputs from previous groups
        let preMatches = 0;
        if (key.code === 'Enter' || key.code === 'Tab') {
            for (let g = 0; g < this.groups.length && g < groupNr; g++) {
                preMatches += this.groups[g].matches.length;
            }
            preMatches *= 6;
        }
        // undo the automatic tab movement
        if (key.code === 'Tab') {
            inputNr -= 1;
            if (inputNr < 0) {
                inputNr = 5;
                matchNr -= 1;
            }
            let nextElem = preMatches + matchNr * 6 + inputNr;
            if (nextElem < 0) {
                return false;
            }
            let elem = (this.inputs.get(nextElem) as ElementRef)?.nativeElement;
            if (!(elem?.classList?.value?.indexOf('dontshow') < 0)) {
                inputNr -= 1;
                nextElem = preMatches + matchNr * 6 + inputNr;
                if (nextElem < 0) {
                    return false;
                }
            }
        }
        // move focus to other player or next set or next match if any
        if (key.code === 'Enter' || key.code === 'Tab') {
            let nextMatch = inputNr === 5 ? matchNr + 1 : matchNr;
            let nextInput = inputNr === 5 ? 0 : (inputNr >= 3 ? inputNr - 2 : inputNr + 3);
            let nextElem = preMatches + nextMatch * 6 + nextInput;
            if (nextElem >= this.inputs.length) {
                return false;
            }
            let elem = (this.inputs.get(nextElem) as ElementRef)?.nativeElement;
            if (elem?.classList?.value?.indexOf('dontshow') < 0) {
                elem.focus();
                elem.select();
            } else {
                nextMatch = matchNr + 1;
                nextInput = 0;
                nextElem = preMatches + nextMatch * 6 + nextInput;
                // avoid jumping to the next group
                if (nextElem >= preMatches + this.groups[groupNr].matches.length * 6 || nextElem >= this.inputs.length) {
                    return false;
                }
                elem = (this.inputs.get(nextElem) as ElementRef)?.nativeElement;
                if (elem?.classList?.value?.indexOf('dontshow') < 0) {
                    elem.focus();
                    elem.select();
                }
            }
            return false;
        }
    }

    initGroupTable() {
        for (let g = 0; g < this.groups.length; g++) {
            Group.calculateStats(this.logger, this.groups[g]);

            const len = this.groups[g].players.length;
            this.groupTable[g] = new Array(len);
            for (let i = 0; i < len; i++) {
                this.groupTable[g][i] = new Array(len);
                for (let j = 0; j < len; j++) {
                    this.groupTable[g][i][j] = {p1 : 0, p2 : 0};
                }
            }
        }
    }

    matchSavedSuccessfully(match: Match) {
        match.saved = true;
    }

    onScoreChange(groupNr: number, matchNr: number) {
        this.logger.trace('onScoreChange', groupNr);

        this.groups[groupNr].matches[matchNr].saved = false;

        this.groupsChangedService.groupsChange(groupNr, this.categoryId);

        // store in DB
        this.matchdbService.updateMatchWithSubscribe(this.tournamentId, this.categoryId, this.groups[groupNr].matches[matchNr],
            this.players, this.groups, this.ko, this.ko_b, this.matchSavedSuccessfully);

        this.socketService.send(SockEvent.GROUPMATCHUPDATE, this.tournamentId, this.categoryId, JSON.stringify(this.groups[groupNr].matches[matchNr]));
    }

    isFinished(match: Match): boolean {
        return Match.checkWinner(match) !== 0;
    }

    calculateSum(match: Match): {p1: number, p2: number} {
        const wins = {p1: 0, p2: 0};
        Match.checkWinnerSetWins(match.score[0], wins);
        Match.checkWinnerSetWins(match.score[1], wins);
        if (match.score[2]) {
            Match.checkWinnerSetWins(match.score[2], wins);
        }
        return wins;
    }

    calculateGroupTable(groupNr: number): void {
        if (this.groups && this.groups.length > 0 && (!this.groupTable || this.groupTable.length === 0)) {
            this.initGroupTable();
        }

        this.logger.trace('calculateGroupTable', this.groups);
        const group = this.groups[groupNr];
        this.logger.trace('calculateGroupTable', group);
        // this.groupTable = this.groupTable.slice();
        for (let i = 0; i < group.players.length; i++) {
            const player1 = group.players[i];
            for (let j = 0; j < this.groupTable[groupNr][i].length; j++) {
                const player2 = group.players[j];
                if (i !== j) {
                    for (let m = 0; m < group.matches.length; m++) {
                        if (group.matches[m].player1.playerListIdx === player1.playerListIdx &&
                                group.matches[m].player2.playerListIdx === player2.playerListIdx) {
                            this.groupTable[groupNr][i][j] = this.calculateSum(group.matches[m]);
                        } else if (group.matches[m].player2.playerListIdx === player1.playerListIdx &&
                                group.matches[m].player1.playerListIdx === player2.playerListIdx) {
                            const wins = this.calculateSum(group.matches[m]);
                            this.groupTable[groupNr][i][j] = {p1 : wins.p2, p2 : wins.p1};
                        }
                    }
                } else {
                    this.groupTable[groupNr][i][j] = {p1 : 0, p2 : 0};
                    this.logger.trace('calculateGroupTable 00');
                }
            }
        }
        Group.updateWinners(this.logger, group);
        Group.calculateStats(this.logger, this.groups[groupNr]);
    }
}
