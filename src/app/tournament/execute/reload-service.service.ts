import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReloadService {

  constructor() { }

  private statusSubject = new BehaviorSubject<number>(-1);
  reloadStatus = this.statusSubject.asObservable();

  reload(priority: number) {
      setTimeout(() => {
          this.statusSubject.next(priority);
      });
  }
}
