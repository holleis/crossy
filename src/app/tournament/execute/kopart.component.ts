import { Component, OnInit, Input } from '@angular/core';
import { Match } from '../match';
import { Group } from '../group';
import { Ko } from '../ko';
import { NGXLogger } from 'ngx-logger';
import { EnrollmentInfo } from '../cat-info';

@Component({
    selector: 'app-kopart',
    templateUrl: './kopart.component.html',
    styleUrls: ['./kopart.component.css']
})
export class KopartComponent implements OnInit {

    @Input() ko: Ko;
    @Input() otherko: Ko;
    @Input() ko_type: string;
    @Input() groups: Group[];
    @Input() level: number;
    @Input() players: EnrollmentInfo['players'];
    @Input() tournamentId: string;
    @Input() categoryId: string;
    @Input() catType: string;
    @Input() canMakeChanges = false;

    matches: Match[] = [];

    constructor(
        private logger: NGXLogger,
    ) {}

    ngOnInit() {
        if (!this.ko) {
            this.logger.trace('error: ko not set on KoComponent');
            return;
        }

        // this.logger.trace(this.ko);
        // this.logger.trace('level', this.level);
        // extract from ko all matches for this level
        for (let num = 0; num < this.ko.matches[this.level].length; num++) {
            this.matches[num] = this.ko.matches[this.level][num];
            this.matches[num].matchNr = {level: this.level.toString(), num: num, ko_type: this.ko_type};
            // this.logger.trace('match', this.matches[num]);
        }
    }

}
