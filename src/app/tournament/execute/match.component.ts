import { Component, OnInit, Input } from '@angular/core';
import { PlayerRef } from '../playerref';
import { Match } from '../match';
import { Group } from '../group';
import { Ko } from '../ko';
import { NGXLogger } from 'ngx-logger';
import { MatchdbService } from './matchdb.service';
import { SocketService, SockEvent } from '../../util/websockets/socket.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { EnrollmentInfo } from '../cat-info';

@Component({
    selector: 'app-match',
    templateUrl: './match.component.html',
    styleUrls: ['./match.component.css'],
})
export class MatchComponent implements OnInit {

    @Input() groups: Group[];
    @Input() ko: Ko;
    @Input() ko_type: string;
    @Input() otherko: Ko;
    @Input() match: Match;
    @Input() players: EnrollmentInfo['players'];
    @Input() tournamentId: string;
    @Input() categoryId: string;
    @Input() catType: string;
    @Input() canMakeChanges = false;

    // for computing the tabindex
    @Input() level: number;
    // match index in the KO level
    @Input() idx: number;

    matchcontainerStyle = {};
    matchStyle = {};

    paddings = [20, 50, 115, 245, 505, 0];
    heights = [45, 80, 145, 275, 535, 1055];

    first = '1st';
    second = '2nd';

    scorelistener = new Subject<{set: number, playerNr: number, value: number}>();

    constructor(
        private logger: NGXLogger,
        private matchdbService: MatchdbService,
        private socketService: SocketService
    ) {
        const debounce = this.scorelistener.pipe(
            debounceTime(3000),
            distinctUntilChanged(),
            switchMap((newscore: {set: number, playerNr: number, value: number}) => {
                this.logger.trace('score update:', newscore);
                this.match.saved = false;
                this.onScoreChange(newscore.set, newscore.playerNr, newscore.value);
                return [];
            })
        );
        debounce.subscribe();
    }

    ngOnInit() {
        this.first = $localize`1st`;
        this.second = $localize`2nd`;

        // formatting for KO
        if (this.match && this.match.matchNr && parseInt(this.match.matchNr.level, 10) >= 0) {
            const idx = this.ko.size - parseInt(this.match.matchNr.level, 10);
            this.matchcontainerStyle = {'padding-top.px': this.paddings[idx], 'padding-bottom.px': this.paddings[idx]};
            this.matchStyle = {'height.px': this.heights[idx]};
        }
        if (this.match?.score && !this.match.score[2]) {
            this.match.score[2] = { p1: 0, p2: 0 };
        }

        // this.logger.trace('match', this.match.matchNr, this.match, this.match.player1, this.match.player1.koIdx);
        // if (this.match.player1.koIdx) {
        //     this.logger.trace(this.ko.matches[this.match.player1.koIdx.level][this.match.player1.koIdx.num],
        //          this.ko.matches[this.match.player1.koIdx.level][this.match.player1.koIdx.num].winner);
        // }
        // if (this.match.player1.groupIdx) {
        //     this.logger.trace(this.groups[this.match.player1.groupIdx.groupNr], this.match.player1.groupIdx.winnerNr);
        // }
    }

    isWinner(playerNr: 1 | 2): boolean {
        return Match.checkWinner(this.match) === playerNr * 2 - 3;
    }

    isFinished(match: Match): boolean {
        return Match.checkWinner(match) !== 0;
    }

    onScoreChange(set: number, playerNr: number, value: number) {
        this.logger.debug('onScoreChange', set, playerNr, value);
        const winner = Match.checkWinner(this.match);
        if (winner === -1) {
            if (!this.match.winner) {
                this.match.winner = this.match.player1;
            } else {
                // don't do winner = player1 since we want to keep the object reference the same
                PlayerRef.update(this.logger, this.match.winner, this.match.player1, this.ko, this.groups);
            }
        } else if (winner === 1) {
            if (!this.match.winner) {
                this.match.winner = this.match.player2;
            } else {
                PlayerRef.update(this.logger, this.match.winner, this.match.player2, this.ko, this.groups);
            }
        } else {
            if (!this.match.winner) {
                this.match.winner = new PlayerRef();
            } else {
                PlayerRef.update(this.logger, this.match.winner, new PlayerRef());
            }
        }

        // store in DB
        this.matchdbService.updateMatchWithSubscribe(this.tournamentId, this.categoryId, this.match,
            this.players, this.groups, this.ko_type === 'A' ? this.ko : this.otherko, this.ko_type === 'A' ? this.otherko : this.ko,
            this.matchSavedSuccessfully);

        this.socketService.send(SockEvent.KOMATCHUPDATE, this.tournamentId, this.categoryId, JSON.stringify(this.match));
    }

    matchSavedSuccessfully(match: Match) {
        match.saved = true;
    }

    getLetter(groupNr: number): string {
        return String.fromCharCode(groupNr + 65);
    }
}
