import { Group } from '../group';
import { Ko } from '../ko';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
// import { AlertService } from '../../util/alert/alert.service';
import { ActivatedRoute } from '@angular/router';
import { PlayerRef } from '../playerref';
import { MatchdbService } from './matchdb.service';
import { Match } from '../match';
import { TournamentsService } from '../tournaments.service';
import moment from 'moment';
import { EnrollmentInfo } from '../cat-info';
import { Category } from '../category';
import { Player } from '../../player/player';
import unionwith from 'lodash-es/unionWith';
import { forkJoin, of } from 'rxjs';
import { Tournament } from '../tournament';
import { catchError } from 'rxjs/operators';
import { Utils } from '../../util/utils';
// import * as ACCESSdb from '../../../assets/accessdb.js';
// declare var ActiveXObject: any;

@Component({
    selector: 'app-tsmatchlist',
    templateUrl: './tsmatchlist.component.html',
    styleUrls: ['./tsmatchlist.component.css'],
})
export class TSMatchlistComponent implements OnInit, OnDestroy {

    constructor(
        // private alertService: AlertService,
        private logger: NGXLogger,
        private route: ActivatedRoute,
        private matchdbService: MatchdbService,
        private tournamentsService: TournamentsService,
        private utils: Utils,
    ) {}

    JSON = JSON;

    INCID = true;

    data = '';
    htmlData = '';
    tableData = { Event: '', player: '', stage: '', stageentry: '', Draw: '', Link: '', Entry: '', PlayerMatch: '' };

    @Input() tournamentId: string;
    tournamentName: string;
    categories: Category[];
    tDate: string;
    groups: Group[][] = [];
    ko: Ko[] = [];
    // ko_b: Ko;
    koNumber: number[] = [];
    // koNumber_b = -1;

    // playerOrder: number[];
    // playerOrder_b: number[];

    BYE_PLAYER: PlayerRef;

    // players: EnrollmentInfo['players'] = [];
    players: Player[] = [];
    doGroupPhase: boolean[] = [];
    catType: string[] = [];
    isDouble: boolean[] = [];
    entries: {event: number, player1: number, player2: number | ''}[][] = [];
    // entriesStartIdx: number[] = [];
    drawId = 0;
    entryId = 0;
    // resultsRanking: { rank: number, player: any, partner: any, points: number }[] = [];
    // resultsRankingFromOtherCats = {};
    // exportForDB = [];
    // results: { rank: number, player: any, partner: any, points: number }[] = [];
    // results_b: { rank: number, player: any, partner: any, points: number }[] = [];
    // // TODO not necessary (dont need to export the DM Quali points as CSV):
    // resultsFromOtherCats = {};

    // maxPoints = 0.0;

    // mapping from groups to KO first level; used to create this.ko
    // koAssignment: { group, num }[] = [];
    // koAssignment_b: { group, num }[] = [];
    // matches: Match[] = [];
    // groupMatches: Match[] = [];
    // koAMatches: Match[] = [];
    // koBMatches: Match[] = [];
    // data: {category: string, players: {last_name: string, first_name: string, partner: {last_name: string, first_name: string}}[], groupMatches: Match[], koAMatches: Match[], koBMatches: Match[], groups: any, ko: any}[] = [];
    maxLevelA: number[] = [];
    maxLevelB: number[] = [];
    PlayerRef = PlayerRef;

    Match = Match;

    ts_players: string[] = [];
    ts_event: string[] = [];
    ts_stage: string[] = [];
    ts_stageentry: string[] = [];
    ts_draw: string[] = [];
    ts_groupLinks: string[] = [];
    ts_entry: string[] = [];
    ts_playermatch: string[] = [];

    ENTRYOFFSET = 1;
    PLAYEROFFSET = 1;
    DRAWOFFSET = 1;
    LINKOFFSET = 1;
    STAGEOFFSET = 1;
    STAGEENTRYOFFSET = 1;
    EVENTOFFSET = 1;
    PLAYERMATCHOFFSET = 1;
    playersPerCat = [];


    ngOnInit() {
        // this.entriesStartIdx[0] = 1;
        this.load();
    }

    ngOnDestroy(): void {
        if (this['kill']) {
            this['kill'];
        }
    }

    findPlayer(player_id: string, players: Player[]): number {
        for (let p = 0; p < players.length; p++) {
            const player = players[p];
            if (player._id.toString() === player_id) {
                return p;
            }
        }
        return -1;
    }

    getGroupStartIdx(cIdx: number): number {
        let cnt = 1;
        for (let c = 0; c < cIdx; c++) {
            cnt += this.groups[c] ? this.groups[c].length : 1;
        }
        return cnt;
    }

    getEntryStartIdx(cIdx: number): number {
        let cnt = 1 + this.ENTRYOFFSET;
        for (let c = 0; c < cIdx; c++) {
            cnt += this.entries[c] ? this.entries[c].length : 1;
        }
        return cnt;
    }

    getMatchStartIdx(cIdx: number): number {
        // TODO more exact (?)
        return cIdx * 1000;
    }

    scoreToStr(score: { p1: number, p2: number }[], inverse = false): string {
        let str = '';
        for (let s = 0; s < score.length; s++) {
            const set = score[s];
            str += !inverse ? set.p1 + ',' + set.p2 : set.p2 + ',' + set.p1;
            if (s < score.length - 1) {
                str += ',';
            }
        }
        return str;
    }

    load() {
        if (!this.tournamentId) {
            this.tournamentId = this.route.snapshot.params.id;
        }

        if (this.tournamentId) {
            // this.matchdbService.getMatches(this.tournamentId).subscribe(
            //     response => {
            //         if (response.success === true && response.result) {
            //             this.data = [];
            //             for (let r = 0; r < response.result.length; r++) {
            //                 const res = response.result[r];
            //                 const catName = res.category;
            //                 const matches = res.matches;
            //                 const players = res.players;
            //                 this.logger.debug('found', matches.length, 'matches in', this.tournamentId, 'for category', catName);
            //                 this.logger.debug('found', players.length, 'players in', this.tournamentId);
            //                 const groupMatches = matches.filter(m => m.matchNr.ko_type === 'G');
            //                 const koAMatches = matches.filter(m => m.matchNr.ko_type === 'A');
            //                 koAMatches.sort((m1, m2) => -parseInt(m1.matchNr.level, 10) * 1000 + m1.matchNr.num + parseInt(m2.matchNr.level, 10) * 1000 - m2.matchNr.num);
            //                 const koBMatches = matches.filter(m => m.matchNr.ko_type === 'B');
            //                 koBMatches.sort((m1, m2) => -parseInt(m1.matchNr.level, 10) * 1000 + m1.matchNr.num + parseInt(m2.matchNr.level, 10) * 1000 - m2.matchNr.num);
            //                 this.data.push({category: catName, players: players, groupMatches: groupMatches, koAMatches: koAMatches, koBMatches: koBMatches, groups: res.groups, ko: res.ko});

            //                 let maxl = 0;
            //                 for (let m = 0; m < koAMatches.length; m++) {
            //                     const lev = parseInt(koAMatches[m].matchNr.level, 10);
            //                     if (lev > maxl) {
            //                         maxl = lev;
            //                     }
            //                 }
            //                 this.maxLevelA[r] = maxl;
            //                 maxl = 0;
            //                 for (let m = 0; m < koBMatches.length; m++) {
            //                     const lev = parseInt(koBMatches[m].matchNr.level, 10);
            //                     if (lev > maxl) {
            //                         maxl = lev;
            //                     }
            //                 }
            //                 this.maxLevelB[r] = maxl;
            //             }

            //         } else {
            //             this.logger.error('Could not retrieve the matches of the tournament', response.error);
            //             this.alertService.error($localize`Could not retrieve the matches of the tournament`, response.error);
            //             return;
            //         }
            //     },
            //     error => {
            //         this.logger.error('Could not retrieve the matches of the tournament', error);
            //         this.alertService.error($localize`Could not retrieve the matches of the tournament`, error);
            //         return;
            //     }
            // );

            this.tournamentsService.getTournament(this.tournamentId).subscribe(
                response => {
                    this.logger.trace('retrieve tournament for canMakeChanges (responsibles)', this.tournamentId, response);
                    if (response.success === true) {
                        this.tournamentName = response.result.title;
                        this.categories = response.result.categories;
// // temp
// this.categories = this.categories.slice(0, 1);
                        for (let c = 0; c < this.categories.length; c++) {
                            const cat = this.categories[c];
                            cat['tsgender'] = this.getTSGender(cat.gender, cat.maximum_age);
                        }
                        this.tDate = moment(response.result.start_date_epoch).format('YYYY-MM-DD');

                        const obs = [];
                        for (let c = 0; c < this.categories.length; c++) {
                            const categoryId = (this.categories[c]._id || this.categories[c]).toString();
                            obs.push(this.matchdbService.getTournamentData(this.tournamentId, categoryId).pipe(
                                catchError(err => of(err.status)),
                            ));
                        }

                        forkJoin(obs).subscribe(
                            (response: {
                                'success': boolean, 'result': {
                                    category: Category,
                                    assignmentMode: string,
                                    players: EnrollmentInfo['players'],
                                    groups: Group[],
                                    ko: Ko,
                                    ko_b: Ko,
                                    tournament: Tournament,
                                    results: { rank: number, player: any, partner: any, points: number }[],
                                    results_b: { rank: number, player: any, partner: any, points: number }[],
                                    resultsFromOtherCats: { rank: number, player: any, partner: any, points: number },
                                    resultsRanking: { rank: number, player: any, partner: any, points: number }[],
                                    resultsRankingFromOtherCats: { rank: number, player: any, partner: any, points: number }[][]
                                }, 'error': string
                            }[]) => {
                                for (let c = 0; c < response.length; c++) {
// // temp
// if (c > 0) break;
                                    if (response[c].success === true && response[c].result) {
                                        for (let p = 0; p < response[c].result.players.length; p++) {
                                            const player = response[c].result.players[p];
                                            if (player.gender !== 1 && player.gender !== 2) {
                                                if (response[c].result.category.gender === 2 || (response[c].result.category.gender === 3 && player.partner && player.partner.gender === 1)) {
                                                    response[c].result.players[p].gender = 2;
                                                } else if (response[c].result.category.gender === 1 || (response[c].result.category.gender === 3 && player.partner && player.partner.gender === 2)) {
                                                    response[c].result.players[p].gender = 1;
                                                } else {
                                                    response[c].result.players[p].gender = undefined;
                                                }
                                            }
                                            const partner = response[c].result.players[p].partner;
                                            if (partner && partner.gender !== 1 && partner.gender !== 2) {
                                                if (response[c].result.category.gender === 2 || (response[c].result.category.gender === 3 && player.gender === 1)) {
                                                    response[c].result.players[p].partner.gender = 2;
                                                } else if (response[c].result.category.gender === 1 || (response[c].result.category.gender === 3 && player.gender === 2)) {
                                                    response[c].result.players[p].partner.gender = 1;
                                                } else {
                                                    response[c].result.players[p].partner.gender = undefined;
                                                }
                                            }
                                        }
                                        this.players = unionwith(this.players, response[c].result.players as any as Player[], this.compareObjectsFn);
                                        // add all partners
                                        this.players = unionwith(this.players, response[c].result.players.map((p) => p.partner).filter((p) => p) as any as Player[], this.compareObjectsFn);

                                        this.entries[c] = [];
                                        this.playersPerCat[c] = response[c].result.players.slice();
                                        for (let p = 0; p < response[c].result.players.length; p++) {
                                            const player = response[c].result.players[p];
                                            const p1 = this.findPlayer(player._id, this.players);
                                            const p2 = player.partner ? this.findPlayer((player.partner['_id'] || player.partner).toString(), this.players) : '';
                                            this.entries[c].push({ event: c, player1: p1, player2: p2 });
                                        }

                                        this.groups[c] = response[c].result.groups;
                                        this.doGroupPhase[c] = this.groups.length > 0;
                                        this.catType[c] = response[c].result.category.cat_type;
                                        this.isDouble[c] = this.catType[c] !== 'singles';
                                        this.ko[c] = response[c].result.ko;
                                        // this.assignmentMode = response[c].result.assignmentMode;
                                        // this.doBRound = this.assignmentMode === 'DCV' && this.doGroupPhase && this.players.length >= (this.isDouble ? 32 : 16);
                                        // this.results = response[c].result.results;
                                        // this.results_b = response[c].result.results_b;
                                        // this.resultsFromOtherCats = response[c].result.resultsFromOtherCats;
                                        // this.resultsRanking = response[c].result.resultsRanking;
                                        // this.resultsRankingFromOtherCats = response[c].result.resultsRankingFromOtherCats;
                                        // if (typeof this.resultsFromOtherCats === 'undefined') {
                                        //     this.resultsFromOtherCats = {};
                                        // }
                                        // if (typeof this.resultsRankingFromOtherCats === 'undefined') {
                                        //     this.resultsRankingFromOtherCats = {};
                                        // }

                                        // correct that all matches now have different PlayerRef objects than the group.players
                                        for (let g = 0; g < this.groups[c].length; g++) {
                                            const group = this.groups[c][g];
                                            const plyrs = new Array(group.players.length);
                                            for (let p = 0; p < group.players.length; p++) {
                                                const groupPlayer = group.players[p];
                                                plyrs[groupPlayer.playerListIdx] = groupPlayer;
                                            }
                                            for (let m = 0; m < group.matches.length; m++) {
                                                const match = group.matches[m];
                                                // get the player with the same playerListIdx
                                                match.player1 = plyrs[match.player1.playerListIdx];
                                                match.player2 = plyrs[match.player2.playerListIdx];
                                            }
                                        }

                                        // this.playerOrder[c] = this.ko.playerOrder;
                                        // // tslint:disable-next-line: no-bitwise
                                        // this.koNumber[c] = 1 << this.ko.matches.length;

                                        // if (this.doBRound) {
                                        //     this.ko_b = response[c].result.ko_b;
                                        //     this.playerOrder_b = this.ko_b.playerOrder;
                                        //     // tslint:disable-next-line: no-bitwise
                                        //     this.koNumber_b = 1 << this.ko_b.matches.length;

                                        //     this.nrBestSecondsForA = Math.pow(2, this.ko.matches.length) - this.groups.length;
                                        // }

                                        // if (this.doGroupPhase) {
                                        //     // whenever something changed in the groups, need to update winner orders
                                        //     this.groupChangeSubscription = this.groupsChangedService.groupChangeStatus.subscribe(
                                        //         groupNr => this.updateAfterGroupChange(groupNr)
                                        //     );

                                        //     setTimeout(() => {
                                        //         this.selectedTab = 0;
                                        //     });
                                        // }



                                    } else if (!response[c].result) {
                                        this.logger.trace('no stored data found for', this.tournamentId);

                                    } else {
                                        this.logger.error('Could not retrieve tournament data:', response[c].error);
                                        // this.alertService.error($localize`Could not retrieve tournament data`, response[c].error);
                                    }
                                }

                                // {{i}}, {{player.last_name[0]}}{{player.last_name.substr(1) | lowercase}}, {{player.first_name}}, , , , , , , , , , , , , , , {{3 - player.gender}}, {{player.date_of_birth !== '01.01.2018' ? player.date_of_birth : ''}}, {{palyer.tsid || player.opid}}<br />
                                for (let p = 0; p < this.players.length; p++) {
                                    const player = this.players[p];
                                    const gender = player.gender ? 3 - player.gender : 0;
                                    const playerDOB = player.date_of_birth?.toString();
                                    const dateOfBirthStr = `${playerDOB && playerDOB !== '01.01.2018' && playerDOB.indexOf('Jan 01 2018') < 0 ? ('\'' + (playerDOB.length === 10 ? moment(playerDOB, 'DD.MM.YYYY') : moment(playerDOB).format('YYYY-MM-DD')) + '\'') : 0}`;
                                    this.ts_players.push(`${this.INCID ? (p + this.PLAYEROFFSET) + ',' : ''}'${player.last_name[0]}${player.last_name.substring(1).toLowerCase()}','${player.first_name}', '', NULL, NULL, 0, 0, '', '', '', '', '', '', '', '', '', ${gender},${dateOfBirthStr},'${player.tsid || player.opid || ''}', True`);
                                }

                                // {{cIdx}},{{getTSCatname(cat.name)}},,{{cat.tsgender}},{{cat.cat_type === 'singles' ? 1 : 2}},,,{{cat.mininum_age}},{{cat.maxinum_age > 99 ? 0 : cat.maxinum_age}},0,False,True,,0,32,,32,8,0,,,0,,,,,,,0,,,0,0,0,0,0,0,,,,0,0,,,,,,,,,,,<br />
                                for (let c = 0; c < this.categories.length; c++) {
                                    const cat = this.categories[c];
                                    this.ts_event.push(`${this.INCID ? (c+this.EVENTOFFSET) + ',' : ''}'${this.getTSCatname(cat.name)}','',${cat['tsgender']},${cat.cat_type === 'singles' ? 1 : 2},NULL,NULL,${cat.minimum_age},${cat.maximum_age >= 99 ? 0 : cat.maximum_age}, 0, False, True, NULL, 0, 32, NULL, 32, 8`);
                                }

                                // <span *ngFor="let cat of categories; let cIdx=index">
                                //     {{cIdx*4 + 0}},Main Draw,{{cIdx}},1,1,0,,<br />
                                //     {{cIdx*4 + 1}},Reserve,{{cIdx}},9998,9998,0,,<br />
                                //     {{cIdx*4 + 2}},Exclude,{{cIdx}},9999,9999,0,,<br />
                                //     {{cIdx*4 + 3}},Playoff,{{cIdx}},1,8,0,,<br />
                                // 
                                // id, name, event (Event), displayorder (1 oder 2; 9998 for Reserve, 9999 for Exclude), stagetype (Main Draw 1, PlayOff 8, Reserve 9998, Exclude 9999), ...
                                for (let c = 0; c < this.categories.length; c++) {
                                    // const cat = this.categories[c];
                                    this.ts_stage.push(`${this.INCID ? (c * 4 + this.STAGEOFFSET + 0) + ',' : ''}'Main Draw',${c + this.EVENTOFFSET},2,1,0,0,0`);
                                    this.ts_stage.push(`${this.INCID ? (c * 4 + this.STAGEOFFSET + 1) + ',' : ''}'Reserve',${c + this.EVENTOFFSET},9998,9998,0,0,0`);
                                    this.ts_stage.push(`${this.INCID ? (c * 4 + this.STAGEOFFSET + 2) + ',' : ''}'Exclude',${c + this.EVENTOFFSET},9999,9999,0,0,0`);
                                    this.ts_stage.push(`${this.INCID ? (c * 4 + this.STAGEOFFSET + 3) + ',' : ''}'Playoff',${c + this.EVENTOFFSET},1,8,0,0,0`);
                                }

                                // <span *ngIf="groups[cIdx] && groups[cIdx].length">
                                //     <span *ngFor="let group of groups[cIdx]; let g=index">
                                //         {{getGroupStartIdx(cIdx) + g}},Group {{g}},{{cIdx}},{{cIdx*4 + 0}},2,{{group.groupSize}},False,,0,,,False,0,260,1,0,,,,True,False,,,,0,0,,,,,,,,,False,,,True,,,0<br />
                                //     
                                // 
                                // <span *ngIf="ko[cIdx]">
                                //     {{500 + cIdx}},{{getTSCatname(cat.name)}},{{cIdx}},{{cIdx*4 + 3}},1,{{ko[cIdx].size}},False,,0,,,False,0,260,1,0,,,,True,False,,,,0,0,,,,,,,,,False,,,True,,,0<br />
                                // 
                                // id, name(Group n or Event name), event(Event), stage(stage), drawtype(KO: 1, group: 2), drawsize(group size or 2 ^ n for KO), playoff(False), ...
                                const eventToDraw: number[] = [];
                                const catGroupWinnerToLink: number[][][] = [];
                                let cnt = this.DRAWOFFSET;
                                let cntLink = this.LINKOFFSET;
                                for (let c = 0; c < this.categories.length; c++) { // event
                                    const cat = this.categories[c];
                                    catGroupWinnerToLink[c] = [];
                                    if (this.groups[c] && this.groups[c].length) {
                                        eventToDraw[c] = cnt;
                                        for (let g = 0; g < this.groups[c].length; g++) {
                                            const group = this.groups[c][g];
                                            catGroupWinnerToLink[c][g] = [];
                                            this.ts_draw.push(`${this.INCID ? cnt + ',' : ''}'Group ${g + 1}',${(c + this.EVENTOFFSET)},${c * 4 + this.STAGEOFFSET + 0},2,${group.groupSize}, False, NULL, 0, NULL, NULL, True, 0, 260, 1, 0`);
                                            for (let i = 0; i < group.groupSize; i++) {
                                                this.ts_groupLinks.push(`${this.INCID ? cntLink + ',' : ''}${cnt},${i + 1},${i + 1},'Group ${g + 1} #${i + 1}'`);
                                                catGroupWinnerToLink[c][g][i] = cntLink;
                                                cntLink += 1;
                                            }
                                            cnt += 1;
                                        }
                                    }
                                    if (this.ko[c] && this.ko[c].size) {
                                        this.ts_draw.push(`${this.INCID ? cnt + ',' : ''}'${this.getTSCatname(cat.name)}',${c + this.EVENTOFFSET},${c * 4 + this.STAGEOFFSET + 3},1,${Math.pow(2, this.ko[c].size)}, False, NULL, 0, NULL, NULL, False, 0, 260, 1, 0`);
                                        eventToDraw[c + 10000] = cnt;
                                        cnt += 1;
                                    }
                                }

                                // <span *ngFor="let cat of categories; let cIdx=index">
                                //     <span *ngFor="let entry of entries[cIdx]; let e=index">
                                //         {{getEntryStartIdx(cIdx) + e}},{{cIdx}},{{entry.player1}},{{entry.player2}},,,0,False,False,,,,0,1,True,0,0,0,0,0,0,False,0,False,0,0,0,,,0,0,0,,False,False,00:00:00,,False,0,,0<br />
                                //     
                                // 
                                // id, event(Event), player 1(Player), player 2(0 or ''; Player if double),
                                cnt = this.ENTRYOFFSET;
                                const dbEntries = [];
                                for (let c = 0; c < this.categories.length; c++) {
                                    // const cat = this.categories[c];
                                    if (this.entries[c] && this.entries[c].length) {
                                        for (let e = 0; e < this.entries[c].length; e++) {
                                            const entry = this.entries[c][e];
                                            this.ts_entry.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${entry.player1 + this.PLAYEROFFSET},${entry.player2 || entry.player2 === 0 ? entry.player2 + this.PLAYEROFFSET : 'NULL'}, NULL, NULL, 0, False, False, NULL, NULL, NULL, 0, 1, True`);
                                            // TODO check the + this.PLAYEROFFSET
                                            dbEntries[entry.player1 + (entry.player2 || entry.player2 === 0 ? ('-' + entry.player2) : '')] = cnt;// - this.ENTRYOFFSET + this.PLAYEROFFSET;
                                            cnt += 1;
                                        }
                                    }
                                }

                                // <!-- id, event, draw, planning, entry, winner, link, plandate, court, location, van1, van2, wn, vn, walkover, team1set1, tea2set1, team1set2, ... -->
                                // <!-- 345,16,20,1000,29,0,,00:00:00,,,0,0,,,False,False,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,,,,,,,False,,,False,-1,-1,-1,-1,-1,-1,-1,,00:00:00,,False,0,,False,0,0,0,0,0 -->
                                // <!-- <span *ngFor="let cat of categories; let cIdx=index">
                                //     <span *ngFor="let group of groups[cIdx]; let g=index">
                                //         {{getMatchStartIdx(cIdx)}},{{cIdx}},{{getGroupStartIdx(cIdx) + g}},{{(g+1)*1000}},<br />
                                //     
                                //  -->
                                cnt = this.PLAYERMATCHOFFSET;
                                let se_id = 0;
                                for (let c = 0; c < this.categories.length; c++) { // event: c
                                    // const cat = this.categories[c];
                                    if (this.groups[c]?.length) {
                                        let drawIdx = eventToDraw[c];
                                        for (let g = 0; g < this.groups[c].length; g++) {
                                            const group = this.groups[c][g];
                                            const playerToPlanning: number[] = [];
                                            for (let i = 0; i < group.groupSize; i++) {
                                                const playeridx = PlayerRef.getPlayerListIdx(this.logger, group.players[i], this.ko[c], this.groups[c]);
                                                const player = this.playersPerCat[c][playeridx]
                                                const play = this.findPlayer(player._id, this.players);
                                                let playId = play + '';
                                                if (player.partner) {
                                                    const play2 = this.findPlayer(player.partner._id, this.players);
                                                    playId = play + '-' + play2;
                                                }
                                                // const playerid = this.entries[c][playeridx].player1; // + this.PLAYEROFFSET;
                                                // const entryid = playerid + this.ENTRYOFFSET;
                                                const entryId = dbEntries[playId];
                                                // const player = this.findPlayer(playeridx, this.players);

                                                // each player is given a "planning" ID n * 1000; winner 0, van1/van2/wn/vn all 0
                                                // id, entry, stage
                                                this.ts_stageentry.push(`${this.INCID ? (se_id + this.STAGEENTRYOFFSET) + ',' : ''}${entryId},${c * 4 + this.STAGEOFFSET + 0}`);
                                                se_id += 1;

                                                const planning = (i + 1) * 1000;
                                                playerToPlanning[playeridx] = planning;
                                                this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx + g},${planning},${entryId},0,NULL,0,NULL,NULL,0,0,0,0,False,False,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);
                                                cnt += 1;
                                            }

                                            for (let m = 0; m < group.matches.length; m++) {
                                                const match = group.matches[m];
                                                const player1idx = PlayerRef.getPlayerListIdx(this.logger, match.player1, this.ko[c], this.groups[c]);
                                                const p1planning = playerToPlanning[player1idx];
                                                const player2idx = PlayerRef.getPlayerListIdx(this.logger, match.player2, this.ko[c], this.groups[c]);
                                                const p2planning = playerToPlanning[player2idx];
                                                const chkwn = Match.checkWinner(match);
                                                const winner = chkwn === -1 ? 1 : chkwn === 1 ? 2 : 0;
                                                const scores = this.scoreToStr(match.score);
                                                const scoresInv = this.scoreToStr(match.score, true);
                                                // for each match 2 entries with new planning ID van1 + (van2 / 1000)
                                                // van1 versus van2 (each planning IDs), with winner either 1 or 2 and scores in team1set1, team2set1, ...
                                                this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx + g},${p1planning + p2planning / 1000},NULL,${winner},NULL,0,NULL,NULL,${p1planning},${p2planning},NULL,NULL,False,False,${scores},0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);
                                                // this one has reversehomeaway set to True
                                                this.ts_playermatch.push(`${this.INCID ? (cnt + 1) + ',' : ''}${c + this.EVENTOFFSET},${drawIdx + g},${p2planning + p1planning / 1000},NULL,${winner ? (3 - winner) : 0},NULL,0,NULL,NULL,${p2planning},${p1planning},NULL,NULL,False,False,${scoresInv},0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,True,0,0,0,0,0`);
                                                cnt += 2;
                                            }
                                        }
                                    }

                                    // KO
                                    if (this.ko[c]) {
                                        //   - each player
                                        //     - given a "planning" ID <level+1>*1000 + n with level 0 (final), 1 semi-final ...; n = 1,2,...
                                        //     - winner 0
                                        //     - "wn" the match planning ID into which the player will go
                                        // <!-- id, event, draw, planning, entry, winner, link, plandate, court, location, van1, van2, wn, vn, walkover, retired, team1set1, tea2set1, team1set2, ... -->
                                        const ko = this.ko[c];
                                        // const drawIdx = eventToDraw[c] + (this.groups[c] && this.groups[c].length ? this.groups[c] : []).length;
                                        const drawIdx = eventToDraw[c + 10000];
                                        const playerToPlanning: number[] = [];
                                        const playerToEntry: number[] = [];
                                        // level===0 is final; level===ko.size-1 is 1st round of KO
                                        // get all players from the 1st round matches
                                        if (ko.size) {
                                            const firstRoundLevel = ko.size - 1;
                                            let planning = (firstRoundLevel + 2) * 1000 + 1;
                                            const byes: number[][] = [];
                                            for (let m = 0; m < ko.matches[firstRoundLevel].length; m++) {
                                                const match = ko.matches[firstRoundLevel][m];
                                                const p1 = match.player1;
                                                const nextMatchPlanning = (firstRoundLevel + 1) * 1000 + m + 1;
                                                if (!PlayerRef.isBye(p1)) {
                                                    const playeridx = PlayerRef.getPlayerListIdx(this.logger, p1, this.ko[c], this.groups[c]);
                                                    const player = this.playersPerCat[c][playeridx]
                                                    const play = this.findPlayer(player._id, this.players);
                                                    let playId = play + '';
                                                    if (player.partner) {
                                                        const play2 = this.findPlayer(player.partner._id, this.players);
                                                        playId = play + '-' + play2;
                                                    }
                                                    const entryId = dbEntries[playId];

                                                    // // id, entry, stage
                                                    // this.ts_stageentry.push(`${this.INCID ? (se_id + this.STAGEENTRYOFFSET) + ',' : ''}${entryId},${c * 4 + this.STAGEOFFSET + 4}`);
                                                    // se_id += 1;

                                                    playerToPlanning[playeridx] = planning;
                                                    playerToEntry[playeridx] = entryId;
                                                    let link: number | 'NULL' = 'NULL';
                                                    if (p1.groupIdx) {
                                                        const fromGroup = p1.groupIdx.groupNr;
                                                        const groupWinnerNr = p1.groupIdx.winnerNr;
                                                        link = catGroupWinnerToLink[c][fromGroup][groupWinnerNr];
                                                    }
                                                    // don't fill in entryId if KO after group phase
                                                    // otherwise a player will already be assigned to this; use link instead
                                                    this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx},${planning},${this.groups[c]?.length ? '' : entryId},0,${link},0,NULL,NULL,0,0,${nextMatchPlanning},0,False,False,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);

                                                    // id, entry, stage
                                                    this.ts_stageentry.push(`${this.INCID ? (se_id + this.STAGEENTRYOFFSET) + ',' : ''}${entryId},${c * 4 + this.STAGEOFFSET + 3}`);
                                                    se_id += 1;
                                                } else {
                                                    if (!byes[firstRoundLevel]) { byes[firstRoundLevel] = []; } byes[firstRoundLevel].push(planning);
                                                    this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx},${planning},NULL,0,NULL,0,NULL,NULL,0,0,${nextMatchPlanning},0,False,False,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);
                                                }
                                                planning += 1;
                                                cnt += 1;
                                                const p2 = match.player2;
                                                if (!PlayerRef.isBye(p2)) {
                                                    const playeridx = PlayerRef.getPlayerListIdx(this.logger, p2, this.ko[c], this.groups[c]);
                                                    const player = this.playersPerCat[c][playeridx]
                                                    const play = this.findPlayer(player._id, this.players);
                                                    let playId = play + '';
                                                    if (player.partner) {
                                                        const play2 = this.findPlayer(player.partner._id, this.players);
                                                        playId = play + '-' + play2;
                                                    }
                                                    const entryId = dbEntries[playId];
                                                    playerToPlanning[playeridx] = planning;
                                                    playerToEntry[playeridx] = entryId;
                                                    let link: number | 'NULL' = 'NULL';
                                                    if (p2.groupIdx) {
                                                        const fromGroup = p2.groupIdx.groupNr;
                                                        const groupWinnerNr = p2.groupIdx.winnerNr;
                                                        link = catGroupWinnerToLink[c][fromGroup][groupWinnerNr];
                                                    }
                                                    this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx},${planning},${this.groups[c]?.length ? '' : entryId},0,${link},0,NULL,NULL,0,0,${nextMatchPlanning},NULL,False,False,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);

                                                    // id, entry, stage
                                                    this.ts_stageentry.push(`${this.INCID ? (se_id + this.STAGEENTRYOFFSET) + ',' : ''}${entryId},${c * 4 + this.STAGEOFFSET + 3}`);
                                                    se_id += 1;
                                                } else {
                                                    if (!byes[firstRoundLevel]) { byes[firstRoundLevel] = []; } byes[firstRoundLevel].push(planning);
                                                    this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx},${planning},NULL,0,NULL,0,NULL,NULL,0,0,${nextMatchPlanning},0,False,False,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);
                                                }
                                                planning += 1;
                                                cnt += 1;
                                            }

                                            // - for each match 1 entry with planning ID <level>*1000 + n, and
                                            //     - van1: planning ID of player 1; van2: planning ID of player 2
                                            //     - winner either 1 or 2 and scores in team1set1, team2set1, ...
                                            //     - entry: the winner
                                            //     - "wn": the planning ID of the match to which the winner will go; empty for final
                                            for (let level = ko.size - 1; level >= 0; level--) {
                                                planning = (level + 1) * 1000 + 1;
                                                for (let m = 0; m < ko.matches[level].length; m++) {
                                                    const match = ko.matches[level][m];
                                                    const nextMatchPlanning = level === 0 ? 'NULL' : level * 1000 + Math.floor(m / 2) + 1;
                                                    const p1 = match.player1;
                                                    let plan1: number;
                                                    let playeridx1: number;
                                                    if (!PlayerRef.isBye(p1)) {
                                                        playeridx1 = PlayerRef.getPlayerListIdx(this.logger, p1, this.ko[c], this.groups[c]);
                                                        plan1 = playerToPlanning[playeridx1];
                                                    } else {
                                                        plan1 = byes[level].shift();
                                                    }
                                                    const p2 = match.player2;
                                                    let plan2: number;
                                                    let playeridx2: number;
                                                    if (!PlayerRef.isBye(p2)) {
                                                        playeridx2 = PlayerRef.getPlayerListIdx(this.logger, p2, this.ko[c], this.groups[c]);
                                                        plan2 = playerToPlanning[playeridx2];
                                                    } else {
                                                        plan2 = byes[level].shift();
                                                    }
                                                    // 3001 => 4001 / 4002; 3002 => 4003 / 4004
                                                    if (!plan2) { plan2 = (level + 2) * 1000 + (planning % 1000 * 2); }
                                                    if (!plan1) { plan1 = plan2 - 1; }

                                                    const chkwn = Match.checkWinner(match);
                                                    const winner = chkwn === -1 ? 1 : chkwn === 1 ? 2 : 0;
                                                    const scores = this.scoreToStr(match.score);
                                                    if (typeof playeridx1 !== 'undefined' && winner === 1) {
                                                        playerToPlanning[playeridx1] = planning;
                                                    } else if (typeof playeridx2 !== 'undefined' && winner === 2) {
                                                        playerToPlanning[playeridx2] = planning;
                                                    }
                                                    // <!-- id, event, draw, planning, entry, winner, link, plandate, court, location, van1, van2, wn, vn, walkover, retired, team1set1, tea2set1, team1set2, ... -->
                                                    this.ts_playermatch.push(`${this.INCID ? cnt + ',' : ''}${c + this.EVENTOFFSET},${drawIdx},${planning},${!winner ? 'NULL' : winner === 1 ? playerToEntry[playeridx1] : playerToEntry[playeridx2]},${winner},NULL,0,NULL,NULL,${plan1 ?? 'NULL'},${plan2 ?? 'NULL'},${nextMatchPlanning},NULL,False,False,${scores},0,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,False,NULL,NULL,False,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,False,NULL,NULL,False,0,0,0,0,0`);
                                                    planning += 1;
                                                    cnt += 1;
                                                }
                                            }
                                        }
                                    }
                                }
                                this.generateDataString();

                            },
                            error => {
                                this.logger.warn('Could not retrieve tournament data:', error);
                                // this.alertService.error($localize`Could not retrieve tournament data`, error);
                            }
                        );
                    } else {
                        this.logger.error('Could not retrieve tournament:', response.error);
                    }
                },
                error => {
                    this.logger.error('Could not retrieve tournament:', error);
                }
            );
        }
    }

    // <- open=0, female=1, male=2, mixed=3
    // -> men 1, women 2, open 6, mixed 3, boys 4, girls 5
    getTSGender(gender: number, maxAge): number {
        switch (gender) {
            case 0:
                return 6;
            case 1:
                return maxAge <= 18 ? 5 : 2;
            case 2:
                return maxAge <= 18 ? 4 : 1;
            case 3:
                return 3;
        
            default:
                break;
        }
    }

    getTSCatname(catName: string): string {
        switch (catName) {
            case 'DCV Damen':
                return 'Women';
            case 'DCV Open':
                return 'Open Division';
            case 'DCV Ü35 Damen':
                return 'O35 women';
            case 'DCV Ü40 Herren':
                return 'O40 mens';
            case 'DCV Ü50 Herren':
                return 'O50 mens';
            case 'DCV U18 Männlich':
                return 'U18 Boys';
            case 'DCV U16 Männlich':
                return 'U16 Boys';
            case 'DCV U14 Männlich':
                return 'U14 Boys';
            case 'DCV U12 Männlich':
                return 'U12 Boys';
            case 'DCV U18 Weiblich':
                return 'U18 Girls';
            case 'DCV U16 Weiblich':
                return 'U16 Girls';
            case 'DCV U14 Weiblich':
                return 'U14 Girls';
            case 'DCV U12 Weiblich':
                return 'U12 Girls';
            case 'DCV Damen Doppel':
                return 'Womens doubles';
            case 'DCV Open Doppel':
                return 'Open doubles';
            case 'DCV Mixed Doppel':
                return 'Mixed doubles';
            case 'DCV Ü40 Damen':
                return 'O40 women';
            case 'DCV Ü50 Damen':
                return 'O50 women';
            case 'ICO Women':
                return 'Women';
            case 'ICO Open':
                return 'Open Division';
            case 'ICO O35 Women':
                return 'O35 women';
            case 'ICO O40 Women':
                return 'O40 women';
            case 'ICO O50 Women':
                return 'O50 women';
            case 'ICO O40 Men':
                return 'O40 mens';
            case 'ICO O50 Men':
                return 'O50 mens';
            case 'ICO U18 Male':
                return 'U18 Boys';
            case 'ICO U16 Male':
                return 'U16 Boys';
            case 'ICO U14 Male':
                return 'U14 Boys';
            case 'ICO U12 Male':
                return 'U12 Boys';
            case 'ICO U18 Female':
                return 'U18 Girls';
            case 'ICO U16 Female':
                return 'U16 Girls';
            case 'ICO U14 Female':
                return 'U14 Girls';
            case 'ICO U12 Female':
                return 'U12 Girls';
            case 'ICO Female Doubles':
                return 'Womens doubles';
            case 'ICO Open Doubles':
                return 'Open doubles';
            case 'ICO Mixed Doubles':
                return 'Mixed doubles';
            case 'ICO U18 Male Doubles': case 'ICO U18 Open Doubles':
                return 'U18 Boys doubles';
            case 'ICO U18 Female Doubles':
                return 'U18 Girls doubles';
            case 'DCV Ü40 Open Doppel':
                return 'O40 mens doubles';
            case 'ICO O60 Women':
                return 'O60 women';
            case 'ICO O70 Women':
                return 'O70 women';
            case 'ICO O60 Men':
                return 'O60 mens';
            case 'ICO O70 Men':
                return 'O70 mens';
            case 'ICO O40 Female Doubles':
                return 'O40 womens doubles';
            case 'ICO O50 Female Doubles':
                return 'O50 womens doubles';
            case 'ICO O60 Female Doubles':
                return 'O60 womens doubles';
            case 'ICO O70 Female Doubles':
                return 'O70 womens doubles';
            case 'ICO O40 Male Doubles':
                return 'O40 mens doubles';
            case 'ICO O50 Male Doubles':
                return 'O50 mens doubles';
            case 'ICO O60 Male Doubles':
                return 'O60 mens doubles';
            case 'ICO O70 Male Doubles':
                return 'O70 mens doubles';
            case 'ICO O40 Mixed Doubles':
                return 'O40 mixed doubles';
            case 'ICO O50 Mixed Doubles':
                return 'O50 mixed doubles';
            case 'ICO O60 Mixed Doubles':
                return 'O60 mixed doubles';
            case 'ICO O70 Mixed Doubles':
                return 'O70 mixed doubles';
            default:
                return catName;
        }

        // U12 Boys
        // U12 Girls
        // U14 Boys
        // U14 Girls
        // U18 Boys
        // U18 Girls
        // U18 Boys doubles
        // U18 Girls doubles

        // Women
        // Open Division
        // Womens doubles
        // Mixed doubles
        // Open doubles
        // O40 mens
        // O40 women
        // O40 mens doubles
        // O40 mixed doubles
        // O50 mens
        // O50 women
        // O50 mens doubles
        // O50 womens doubles
        // O60 mens
    }

    update() {
        this.load();
    }

    generateDataString() {
        this.data = '';
        this.data += 'Settings\n';
        this.data += 'ID, name, `value`\n';
        this.data += this.tournamentName + ',,,\n';

        this.data += 'ScoringFormat\n';
        this.data += 'id, name, isdefault, numsets, settype, lastsettype, score\n';
        this.data += '1, \'Standard\', False, 3, 999, 999, 16\n';
        this.data += '2, \'5x11\', False, 3, 301, 301, 21\n';
        this.data += '3, \'5x11 (15)\', False, 3, 304, 304, 21\n';
        this.data += '4, \'5x11 (13)\', False, 3, 305, 305, 21\n';
        this.data += '5, \'5x11 (21)\', False, 3, 306, 306, 21\n';
        this.data += '6, \'Crossminton\', True, 3, 999, 999, 16\n';

        this.data += 'TournamentDay\n';
        this.data += 'id, tournamentday, availability\n';
        this.data += `1, '${this.tDate}', True\n`;

        this.data += 'TournamentTime\n';
        this.data += 'id, tournamenttime, tournamentday, indexnr, location, courts\n';
        for (let i = 1; i < 11; i++) {
            this.data += `${i}, 0, '${this.tDate}',${i}, 1, 8\n`;
        }
        this.data += 'Location\n';
        this.data += 'id, name\n';
        this.data += "1, 'Main Location'\n";

        this.data += 'Club\n';
        this.data += 'id, name\n';
        this.data += `1, 'no club specified'\n`;

        this.data += 'Player\n';
        this.tableData.player = '';
        this.tableData.player += 'id, name, firstname, middlename, club, country, payed, discount, address, postalcode, city, state, phone, office, fax, mobile, email, gender, dob, memberid, validated\n';
        for (let i = 0; i < this.ts_players.length; i++) {
            this.tableData.player += this.ts_players[i] + '\n';
        }
        this.data += this.tableData.player;

        this.data += 'Event\n';
        this.tableData.Event = '';
        this.tableData.Event += 'id, name, abbreviation, gender, eventtype, minlevel, `level`, min_age, max_age, fee, separateseeding, allowonlineentry, drawcomposition, qualcomposition, maindrawsize, mainmaxentries, qualdrawsize, qualendsize\n';
        for (let i = 0; i < this.ts_event.length; i++) {
            this.tableData.Event += this.ts_event[i] + '\n';
        }
        this.data += this.tableData.Event;

        this.data += 'stage\n';
        this.tableData.stage = '';
        this.tableData.stage += 'ID, name, event, displayorder, stagetype, drawgroup, scoringformat, scoringformatcons\n';
        for (let i = 0; i < this.ts_stage.length; i++) {
            this.tableData.stage += this.ts_stage[i] + '\n';
        }
        this.data += this.tableData.stage;

        this.data += 'stageentry\n';
        this.tableData.stageentry = '';
        this.tableData.stageentry += 'ID, entry, stage\n';
        for (let i = 0; i < this.ts_stageentry.length; i++) {
            this.tableData.stageentry += this.ts_stageentry[i] + '\n';
        }
        this.data += this.tableData.stageentry;

        this.data += 'Draw\n';
        this.tableData.Draw = '';
        this.tableData.Draw += 'id, name, event, stage, drawtype, drawsize, playoff, playoffsize, consolation, consolationsize, consolationplayoffsize, qualification, drawgroup, drawcolumns, position, header\n';
        for (let i = 0; i < this.ts_draw.length; i++) {
            this.tableData.Draw += this.ts_draw[i] + '\n';
        }
        this.data += this.tableData.Draw;

        this.data += 'Link\n';
        this.tableData.Link = '';
        this.tableData.Link += 'id, src_draw, src_pos, intsrc_pos, name\n';
        for (let i = 0; i < this.ts_groupLinks.length; i++) {
            this.tableData.Link += this.ts_groupLinks[i] + '\n';
        }
        this.data += this.tableData.Link;

        this.data += 'Entry\n';
        this.tableData.Entry = '';
        this.tableData.Entry += 'id, event, player1, player2, seed1, seed2, status, partnerwanted, exclude, qseed1, qseed2, qstatus, entrytype, entrylist, entered\n';
        for (let i = 0; i < this.ts_entry.length; i++) {
            this.tableData.Entry += this.ts_entry[i] + '\n';
        }
        this.data += this.tableData.Entry;

        this.data += 'Court\n';
        this.data += 'id, name, location, playermatch, courttype, courtsurface, sortorder\n';
        for (let i = 1; i < 9; i++) {
            this.data += `${i},${i},1,NULL,NULL,0,${i}\n`;
        }

        this.data += 'PlayerMatch\n';
        const heading = '`id`, `event`,`draw`,`planning`,`entry`,`winner`,`link`,`plandate`,`court`,`location`,`van1`,`van2`,`wn`,`vn`,`walkover`,`retired`,`team1set1`,`team2set1`,`team1set2`,`team2set2`,`team1set3`,`team2set3`,`team1set4`,`team2set4`,`team1set5`,`team2set5`,`team1set6`,`team2set6`,`team1set7`,`team2set7`,`status`,`matchorder`,`duration`,`ranking`,`matchno`,`scorestatus`,`starttime`,`highlight`,`note`,`notetimestamp`,`scoresheetprinted`,`official1`,`official2`,`forwardloser`,`set1tiebreak`,`set2tiebreak`,`set3tiebreak`,`set4tiebreak`,`set5tiebreak`,`set6tiebreak`,`set7tiebreak`,`shuttles`,`prevplaytime`,`endtime`,`showbye`,`scoringformat`,`scoringformatnew`,`reversehomeaway`,`stage`,`matchnr`,`roundnr`,`teamlocation`,`livescore`\n';
        this.data += heading;
        this.tableData.PlayerMatch = heading.replaceAll('`', '');
        for (let i = 0; i < this.ts_playermatch.length; i++) {
            this.tableData.PlayerMatch += this.ts_playermatch[i] + '\n';
        }
        this.data += this.tableData.PlayerMatch;

        this.htmlData = this.data.replace(/\n/g, '<br/>');
    }

    exportCSV(specificTable?: string) {
        if (!specificTable) {
            this.utils.saveToFileSystem(this.data, 'text', this.tournamentName.replace(/[^a-zA-Z0-9]/g, '') + '.txt');
        } else {
            const tableData = this.tableData[specificTable];
            this.utils.saveToFileSystem(tableData, 'text', specificTable + '.txt');
        }
    }

    // getGroupName(match: Match): string {
    //     const grNr = parseInt(match.matchNr.level, 10);
    //     return String.fromCharCode(grNr + 65);
    // }

    // getPlayers(idx: number, match: Match): string {
    //     let p1names;
    //     let p2names;
    //     const p1Idx = PlayerRef.getPlayerListIdx(this.logger, match.player1, this.data[idx].ko, this.data[idx].groups);
    //     const p1 = this.data[idx].players[p1Idx];
    //     if (!p1) {
    //         p1names = '______________';
    //     } else {
    //         p1names = p1.last_name + ' ' + p1.first_name;
    //         if (typeof p1.partner !== 'undefined' && typeof p1.partner.last_name !== 'undefined') {
    //             p1names += '+' + p1.partner.last_name + ' ' + p1.partner.first_name;
    //         }
    //     }
    //     const p2Idx = PlayerRef.getPlayerListIdx(this.logger, match.player2, this.data[idx].ko, this.data[idx].groups);
    //     const p2 = this.data[idx].players[p2Idx];
    //     if (!p2) {
    //         p2names = '______________';
    //     } else {
    //         p2names = p2.last_name + ' ' + p2.first_name;
    //         if (typeof p2.partner !== 'undefined' && typeof p2.partner.last_name !== 'undefined') {
    //             p2names += '+' + p2.partner.last_name + ' ' + p2.partner.first_name;
    //         }
    //     }

    //     return p1names + ' : ' + p2names;
    // }

    // getScores(match: Match) {
    //     if (!match.score[0] || (!match.score[0].p1 && !match.score[0].p2)) {
    //         return '';
    //     }
    //     let str = `${match.score[0].p1}:${match.score[0].p2}   ${match.score[1].p1}:${match.score[1].p2}`;
    //     if (match.score[2] && (match.score[2].p1 || match.score[2].p2)) {
    //         str += `   ${match.score[2].p1}:${match.score[2].p2}`;
    //     }

    //     return str;
    // }

    // getKoLevelName(level: string): string {
    //     const lev = parseInt(level, 10);
    //     if (lev === 0) {
    //         return 'Final';
    //     } else if (lev === 1) {
    //         return 'Semi-Finals';
    //     } else if (lev === 2) {
    //         return 'Quarter-Finals';
    //     } else {
    //         return 'Round of ' + 2 ** (lev + 1);
    //     }
    //     // const thesize = isB ? this.maxLevelB[cIdx] : this.maxLevelA[cIdx];
    //     // const lev = parseInt(level);
    //     // if (lev === thesize) {
    //     //     return 'Final';
    //     // } else if (lev === thesize-1) {
    //     //     return 'Semi-Finals';
    //     // } else if (lev === thesize-2) {
    //     //     return 'Quarter-Finals';
    //     // } else {
    //     //     return 'Round of ' + 2 ** (thesize - lev + 1);
    //     // }
    // }

    compareObjectsFn(o1: { _id: string, partner?: any }, o2: { _id: string, partner?: any }): boolean {
        return o1 && o2 ? (o1._id && o2._id ? o1._id === o2._id
            : (o1.toString() === o2._id.toString() || o1._id.toString() === o2.toString() || o1 === o2))
            : o1 === o2;
    }
    // compareObjectsFn(o1: { _id: string, partner?: any }, o2: { _id: string, partner?: any }): boolean {
    //     if (o1 && o2) {
    //         if (o1._id && o2._id) {
    //             if (o1._id === o2._id) {
    //                 if (!o1.partner && !o2.partner || o1.partner === o2.partner || ((o1.partner ? o1.partner._id : o1.partner) || '').toString() === ((o2.partner ? o2.partner._id : o2.partner) || '').toString()) {
    //                     return true;
    //                 }
    //             }
    //             return false;
    //         }
    //     }
    //     return o1 as any === o2._id || o1._id === o2 as any || o1 === o2;
    // }
}
