import { forkJoin as observableForkJoin, Subject, takeUntil } from 'rxjs';
import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, ViewChildren, QueryList, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ResultInfo, TournamentsService } from '../tournaments.service';
import { Tournament } from '../tournament';
import { AlertService } from '../../util/alert/alert.service';
import { EnrollmentInfo } from '../cat-info';
import { BracketComponent } from './bracket.component';
import { Role } from '../../user/role';
import { UserService } from '../../auth/_services/user.service';
import { RankingService } from '../../ranking/ranking.service';

import { Globals } from '../../globals';
import { MatchdbService } from './matchdb.service';

import { SocketService, SockEvent, UpdateEvent } from '../../util/websockets/socket.service';
import { MatDialog } from '@angular/material/dialog';
import { DrawOptionsDialogComponent } from '../../util/drawoptions-dialog.component';
import { ReloadService } from './reload-service.service';

import { saveAs } from 'file-saver';

@Component({
    selector: 'app-tournament',
    templateUrl: './tournament.component.html',
    styleUrls: ['./tournament.component.css'],
})
export class TournamentComponent implements OnInit, OnDestroy {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private logger: NGXLogger,
        private tournamentsService: TournamentsService,
        private alertService: AlertService,
        public dialog: MatDialog,
        private userService: UserService,
        private rankingService: RankingService,
        private matchdbService: MatchdbService,
        private socketService: SocketService,
        public changeDetectorRef: ChangeDetectorRef,
        public media: MediaMatcher,
        private reloadService: ReloadService,
    ) {
        this.scoresheetURL = Globals.SCRIPTS_BASE_URL + Globals.SCORESHEET_PHP_URL;
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        // this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        // this.mobileQuery.addListener(this._mobileQueryListener);
        try {
            // Chrome & Firefox
            this.mobileQuery.addEventListener('change', () => changeDetectorRef.detectChanges());
        } catch (e1) {
            try {
                // Safari
                this.mobileQuery.addListener(changeDetectorRef.detectChanges);
            } catch (e2) {
                // ignore
                this.logger.error('error: could not assign darkColorSchemeCahnge listener');
            }
        }
        this.router.events
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((e: any) => {
                // If it is a NavigationEnd event re-initalise the component
                if (e instanceof NavigationEnd) {
                    if (this.route.snapshot.params.catId && this.route.snapshot.params.newDraw === 'true') {
                        this.initNewDraw();
                    } else if (this.route.snapshot.params.reload === 'true') {
                        this.ngOnInit();
                    }
                }
            });
    }

    // players per category
    players: EnrollmentInfo['players'][] = [];
    categoryTypes: string[] = [];
    categoryNames: string[] = [];
    categoryIds: string[] = [];
    tournamentName: string;
    tournamentId: string;
    assignmentMode = 'DCV';
    newDraw = false;
    showNewDraw = -1;
    canMakeChanges = false;
    isPaul = false;
    isDCV = false;
    MINPLAYERSPERCATEGORY = 4;
    // will only be set if user.roles '<' DCV
    tournament: Tournament;
    enrollments: EnrollmentInfo[];
    doGroupPhase: boolean[] = [];

    playersToUpdatePoints = [];

    scoresheetURL: string;

    // Observables collected for reassignments
    obsbls = [];

    // socketIO connection for live updates
    ioConnection: any;

    @ViewChildren('bracket') brackets: QueryList<BracketComponent>;
    private ngUnsubscribe = new Subject();

    mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;
    disableReloadButton = false;

    navigationSubscription;


    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);

        this.ngUnsubscribe.next('');
        this.ngUnsubscribe.complete();
    }

    ngOnInit() {
        this.showNewDraw = -1;

        // get tournament ID from URL
        this.tournamentId = this.route.snapshot.params.id;

        if (!this.tournamentId) {
            this.alertService.error($localize`Need to know the tournament ID. Please ask the organiser.`);
            return;
        }

        // see if user is allowed to enter scores
        const user = this.userService.getCurrentUser();
        // tslint:disable-next-line: no-bitwise
        if (user && ((user.roles & Role.ROLE_PAUL) > 0 || (user.roles & Role.ROLE_DCV) > 0)) {
            this.canMakeChanges = true;
            // tslint:disable-next-line: no-bitwise
            if (user && (user.roles & Role.ROLE_PAUL) > 0) {
                this.isPaul = true;
            }
            if (user && (user.roles & Role.ROLE_DCV) > 0) {
                this.isDCV = true;
            }
        }

        // get tournament name and potentially set canMakeChanges for tournament responsibles
        this.tournamentsService.getTournament(this.tournamentId).subscribe({
            next: response => {
                this.logger.trace('retrieve tournament for canMakeChanges (responsibles)', this.tournamentId, response);
                if (response.success === true) {
                    this.tournament = response.result;
                    this.tournamentName = this.tournament.title;
                    if (user && response?.result.responsibles) {
                        if (response.result.responsibles.indexOf(user.player_id) >= 0) {
                            this.canMakeChanges = true;
                        }
                    } else {
                        this.logger.trace('no responsibles for tournament');
                    }
                } else {
                    this.logger.error('Could not retrieve tournament:', response.error);
                }
            },
            error: error => {
                this.logger.error('Could not retrieve tournament:', error);
            }
        });

        this.newDraw = this.tournamentsService.needNewDraw(this.tournamentId);

        if (this.newDraw) {
            // get enrollments
            // ask for min. players per category
            // reassign players from categories with too few players
            // update points of reassigned players
            // ask for additional draw options
            // set properties for BracketComponent

            this.tournamentsService.getEnrollments(this.tournamentId).subscribe({
                next: response => {
                    this.logger.trace('retrieved enrollments', response);
                    if (response.success === true) {
                        this.enrollments = response.result;
                        if (this.enrollments.length > 0) {
                            this.tournamentName = this.enrollments[0].tournamentName;
                        }

                        // sort enrollments according to minAge (adults, DESC) and maxAge (kids, ASC)
                        // this is important for the reassign procedure below
                        this.enrollments.sort((e1: EnrollmentInfo, e2: EnrollmentInfo) => {
                            if (e1.minimum_age === e2.minimum_age) {
                                return e1.maximum_age - e2.maximum_age;
                            } else {
                                return e2.minimum_age - e1.minimum_age;
                            }
                        });

                        // if (this.enrollments.length > 0 && this.enrollments[0].tournament_type !== 'MunichCup' && this.enrollments[0].tournament_type !== 'MunichCup (Gruppen)') {
                        //     // TODO: specific arrangements not according to the normal rules must be asked / implemented here
                        //     // temp solution: let choose min. number of players per category
                        //     const dialogRef = this.dialog.open(DrawOptionsDialogComponent, {
                        //         data: {options: 0,
                        //             minPlayersPerCategory: this.minPlayersPerCategory
                        //         }
                        //     });
                        //     dialogRef.afterClosed().subscribe(result => {
                        //         if (result) {
                        //             this.minPlayersPerCategory = result.minPlayersPerCategory;
                        this.setupNewDraw();
                        //         } else {
                        //             // cancelled
                        //             this.router.navigate(['/mytournaments']);
                        //             return;
                        //         }
                        //     });
                        // } else {
                        //     // MunichCup
                        //     this.setupNewDraw();
                        // }

                    } else {
                        this.logger.error('Could not retrieve enrollment for tournament:', response.error);
                        this.alertService.error($localize`Could not retrieve enrollment for tournament`, response.error);
                    }
                },
                error: error => {
                    this.logger.error('Could not retrieve enrollment:', error);
                    this.alertService.error($localize`Could not retrieve enrollment`, error);
                }
            });

        } else { // if (this.newDraw) {}
            // get final categories for tournament
            // set properties for BracketComponent and let it load all data
            this.tournamentsService.getDrawnCategories(this.tournamentId).subscribe({
                next: response => {
                    this.logger.trace('retrieved drawn categories', response);
                    if (response.success === true) {
                        this.assignmentMode = response.result.assignmentMode;
                        if (!this.assignmentMode) {
                            this.assignmentMode = 'DCV';
                        }
                        this.categoryIds = response.result.catIds;
                        this.categoryNames = response.result.catNames;

                    } else {
                        this.logger.error('Could not retrieve drawn categories', response.error);
                        this.alertService.error($localize`Could not retrieve drawn categories`, response.error);
                    }
                },
                error: error => {
                    this.logger.error('Could not retrieve drawn categories', error);
                    this.alertService.error($localize`Could not retrieve drawn categories`, error);
                }
        });
        }

        this.initIoConnection();
    }

    initNewDraw() {
        this.handleSingleCatNewDraw();
        this.initIoConnection();
    }

    handleSingleCatNewDraw() {
        const catId = this.route.snapshot.params.catId;
        if (!this.tournamentId) {
            this.tournamentId = this.route.snapshot.params.id;
        }
        this.tournamentsService.getEnrollments(this.tournamentId).subscribe({
            next: response => {
                this.logger.trace('retrieved enrollments', response);
                if (response.success === true) {
                    this.enrollments = response.result;
                    if (this.enrollments.length > 0) {
                        this.tournamentName = this.enrollments[0].tournamentName;
                    }

                    // sort enrollments according to minAge (adults, DESC) and maxAge (kids, ASC)
                    // this is important for the reassign procedure below
                    this.enrollments.sort((e1: EnrollmentInfo, e2: EnrollmentInfo) => {
                        if (e1.minimum_age === e2.minimum_age) {
                            return e1.maximum_age - e2.maximum_age;
                        } else {
                            return e2.minimum_age - e1.minimum_age;
                        }
                    });

                    // // TODO: specific arrangements not according to the normal rules must be asked / implemented here
                    // // temp solution: let choose min. number of players per category
                    // const dialogRef = this.dialog.open(DrawOptionsDialogComponent, {
                    //     data: {options: 0,
                    //         minPlayersPerCategory: this.minPlayersPerCategory
                    //     }
                    // });
                    // dialogRef.afterClosed().subscribe(result => {
                    //     if (result) {
                    //         this.minPlayersPerCategory = result.minPlayersPerCategory;
                    this.setupNewDrawSingleCat(catId);
                    //     } else {
                    //         // cancelled
                    //         this.router.navigate(['/mytournaments']);
                    //         return;
                    //     }
                    // });

                } else {
                    this.logger.error('Could not retrieve enrollment', response.error);
                    this.alertService.error($localize`Could not retrieve enrollment`, response.error);
                }
            },
            error: error => {
                this.logger.error('Could not retrieve enrollment', error);
                this.alertService.error($localize`Could not retrieve enrollment`, error);
            }
    });
    }

    private setupNewDrawSingleCat(catId: string) {
        // this.logger.debug('looking at', enrollmentInfo.catName, 'with', enrollmentInfo.players.length, 'players');

        // need to look at all categories bc of reassignments
        this.tournamentsService.resetReassignments(this.tournamentId).subscribe({
            next: response => {
            if (response.success === true) {

                this.obsbls = [];
                for (let cNr = 0; cNr < this.enrollments.length; cNr++) {
                    const eInfo = this.enrollments[cNr];
                    this.logger.debug('looking at', eInfo.catName, 'with', eInfo.players.length, 'players');

                    if (this.needsReassignment(eInfo)) {
                        if (eInfo.maximum_age <= 18) {
                            if (!this.reassignJunior(cNr)) {
                                this.logger.error('Could not find enough players for this category', this.enrollments[cNr].catName);
                                this.alertService.error($localize`Could not find enough players for category ${this.enrollments[cNr].catName.toString()}:category:`);
                                return;
                            }
                        } else {
                            if (!this.reassign(cNr)) {
                                this.logger.error('Could not find enough players for this category', this.enrollments[cNr].catName);
                                this.alertService.error($localize`Could not find enough players for category ${this.enrollments[cNr].catName.toString()}:category:`);
                                return;
                            }
                        }
                    }
                }
                this.executeObsbls();

                // remove categories without players
                this.enrollments = this.enrollments.filter(e => e.players.length > 0);

                // update points of all that had to be moved to another category
                const obsbl = [];
                const playerInfos = [];
                for (let p = 0; p < this.playersToUpdatePoints.length; p++) {
                    const playerId = this.playersToUpdatePoints[p].playerInfo._id;
                    const theCatId = this.playersToUpdatePoints[p].catId;
                    const playerInfo = this.playersToUpdatePoints[p].playerInfo;

                    obsbl.push(this.rankingService.getPlayerRanking(playerId, theCatId));
                    playerInfos.push({ playerInfo: playerInfo, catId: theCatId });
                }
                // order for viewing
                this.enrollments.sort((e1: EnrollmentInfo, e2: EnrollmentInfo) => {
                    if (e1.maximum_age === e2.maximum_age) {
                        return e1.minimum_age - e2.minimum_age;
                    } else {
                        return e2.maximum_age - e1.maximum_age;
                    }
                });

                for (let cNr = 0; cNr < this.enrollments.length; cNr++) {
                    this.doGroupPhase[cNr] = this.enrollments[cNr].cat_type === 'singles';
                }

                let enrollmentInfo;
                let catNr;
                for (let e = 0; e < this.enrollments.length; e++) {
                    if (catId === this.enrollments[e]._id.toString()) {
                        enrollmentInfo = this.enrollments[e];
                        catNr = e;
                        break;
                    }
                }

                // ask whether this category should have group phase
                const dialogRef2 = this.dialog.open(DrawOptionsDialogComponent, {
                    data: {
                        options: 1,
                        cats: [enrollmentInfo.catName],
                        doGroupPhase: [this.doGroupPhase[catNr]],
                        assignmentMode: (enrollmentInfo.tournament_type)
                    }
                });
                dialogRef2.afterClosed().subscribe(result => {
                    if (result) {
                        this.doGroupPhase[catNr] = result.doGroupPhase;
                        this.matchdbService.removeTournamentDataSingleCat(this.tournamentId, catId).subscribe(
                            response2 => {
                                if (response2.success === true) {
                                    this.setData(obsbl, playerInfos, result.assignmentMode, catId);

                                } else {
                                    this.logger.error('Could not remove tournament data', response2.error);
                                    this.alertService.error($localize`Could not remove tournament data`, response2.error);
                                }
                            },
                            error => {
                                this.logger.error('Could not remove tournament data', error);
                                this.alertService.error($localize`Could not remove tournament data`, error);
                            }
                        );

                    } else {
                        // cancelled
                        this.router.navigate(['/mytournaments']);
                        return;
                    }
                });
            } else {
                this.logger.error('Could not remove tournament data', response.error);
                this.alertService.error($localize`Could not remove tournament data`, response.error);
                return;
            }
        },
            error: error => {
                this.logger.error('Could not remove tournament data', error);
                this.alertService.error($localize`Could not remove tournament data`, error);
                return;
            }
        });
    }

    private setupNewDraw() {
        this.tournamentsService.resetReassignments(this.tournamentId).subscribe({
            next: response => {
                if (response.success === true) {

                    this.obsbls = [];
                    for (let catNr = 0; catNr < this.enrollments.length; catNr++) {
                        const enrollmentInfo = this.enrollments[catNr];
                        this.logger.debug('looking at', enrollmentInfo.catName, 'with', enrollmentInfo.players.length, 'players');

                        if (this.needsReassignment(enrollmentInfo)) {
                            if (enrollmentInfo.maximum_age <= 18) {
                                if (!this.reassignJunior(catNr)) {
                                    this.logger.error('Could not find enough players for this category', this.enrollments[catNr].catName);
                                    this.logger.error('Could not find enough players for this category', this.enrollments[catNr].catName);
                                    return;
                                }
                            } else {
                                if (!this.reassign(catNr)) {
                                    this.logger.error('Could not find enough players for this category', this.enrollments[catNr].catName);
                                    this.logger.error('Could not find enough players for this category', this.enrollments[catNr].catName);
                                    return;
                                }
                            }
                        }
                    }
                    this.executeObsbls();

                    // remove categories without players
                    this.enrollments = this.enrollments.filter(e => e.players.length > 0);

                    // update points of all that had to be moved to another category
                    const obsbl = [];
                    const playerInfos = [];
                    for (let p = 0; p < this.playersToUpdatePoints.length; p++) {
                        const playerId = this.playersToUpdatePoints[p].playerInfo._id;
                        const catId = this.playersToUpdatePoints[p].catId;
                        const playerInfo = this.playersToUpdatePoints[p].playerInfo;

                        obsbl.push(this.rankingService.getPlayerRanking(playerId, catId));
                        playerInfos.push({ playerInfo: playerInfo, catId: catId });
                    }
                    // order for viewing
                    this.enrollments.sort((e1: EnrollmentInfo, e2: EnrollmentInfo) => {
                        if (e1.maximum_age === e2.maximum_age) {
                            return e1.minimum_age - e2.minimum_age;
                        } else {
                            return e2.maximum_age - e1.maximum_age;
                        }
                    });

                    for (let catNr = 0; catNr < this.enrollments.length; catNr++) {
                        this.doGroupPhase[catNr] = this.enrollments[catNr].cat_type === 'singles';
                    }

                    // ask which categories should have group phase
                    const dialogRef2 = this.dialog.open(DrawOptionsDialogComponent, {
                        data: {
                            options: 1,
                            cats: this.enrollments.map(e => e.catName),
                            doGroupPhase: this.doGroupPhase,
                            assignmentMode: (this.enrollments.length > 0 ? this.enrollments[0].tournament_type : 'DCV')
                        }
                    });
                    dialogRef2.afterClosed().subscribe(result => {
                        if (result) {
                            this.doGroupPhase = result.doGroupPhase;
                            this.matchdbService.removeTournamentData(this.tournamentId).subscribe({
                                next: response2 => {
                                    if (response2.success === true) {
                                        this.setData(obsbl, playerInfos, result.assignmentMode);

                                    } else {
                                        this.logger.error('Could not remove tournament data', response2.error);
                                        this.alertService.error($localize`Could not remove tournament data`, response2.error);
                                    }
                                },
                                error: error => {
                                    this.logger.error('Could not remove tournament data', error);
                                    this.alertService.error($localize`Could not remove tournament data`, error);
                                }
                        });

                        } else {
                            // cancelled
                            this.router.navigate(['/mytournaments']);
                            return;
                        }
                    });
                } else {
                    this.logger.error('Could not remove tournament data', response.error);
                    this.alertService.error($localize`Could not remove tournament data`, response.error);
                    return;
                }
            },
            error: error => {
                this.logger.error('Could not remove tournament data', error);
                this.alertService.error($localize`Could not remove tournament data`, error);
                return;
            }
        });
    }

    // private setDataSingleCat(catNr, enrollmentInfo, assMode) {
    //     // set all the data to be passed to the next steps (BracketComponent)
    //     this.players[catNr] = enrollmentInfo.players;
    //     this.logger.debug('players:', enrollmentInfo.catName, enrollmentInfo.players.length);
    //     this.categoryNames[catNr] = enrollmentInfo.catName;
    //     this.categoryTypes[catNr] = enrollmentInfo.cat_type;
    //     this.categoryIds[catNr] = enrollmentInfo._id;
    //     this.assignmentMode = assMode;
    // }

    private setData(obsbl, playerInfos, assMode, singleCatId?: string) {
        if (obsbl.length === 0) {
            // set all the data to be passed to the next steps (BracketComponent)
            let singleCatNr = -1;
            for (let catNr = 0; catNr < this.enrollments.length; catNr++) {
                const enrollmentInfo = this.enrollments[catNr];
                if (singleCatId && singleCatId === enrollmentInfo._id) {
                    singleCatNr = catNr;
                    // don't continue since the players array must also be set for the BracketComponent to take over
                    // continue;
                }
                this.players[catNr] = enrollmentInfo.players;
                this.logger.debug('players:', enrollmentInfo.catName, enrollmentInfo.players.length);
                this.categoryNames[catNr] = enrollmentInfo.catName;
                this.categoryTypes[catNr] = enrollmentInfo.cat_type;
                this.categoryIds[catNr] = enrollmentInfo._id;
                this.assignmentMode = assMode;
            }

            if (singleCatNr >= 0) {
                this.showNewDraw = singleCatNr;
                // this.router.navigate(['/ko/cat', {id: this.tournamentId, catId: singleCatNr, newDraw: 'true'}]);
            }

        } else {
            // first update points of players before passing data to the draw mechanism
            observableForkJoin(obsbl).subscribe({
                next: response => {
                    if (playerInfos.length !== response['length']) {
                        this.logger.error('received incorrect answer from server', playerInfos.length, '!==', response['length']);
                    }
                    for (let i = 0; i < response['length']; i++) {
                        if (response[i]['success'] === true) {
                            playerInfos[i].playerInfo.points = response[i]['result'];
                            this.logger.debug('updated points of', playerInfos[i].playerInfo.first_name, playerInfos[i].playerInfo.last_name, 'to', response[i]['result']);
                        } else {
                            this.logger.error('could not update points of', playerInfos[i].playerInfo.first_name, playerInfos[i].playerInfo.last_name, response[i]['error']);
                        }
                    }

                    // set all the data to be passed to the next steps (BracketComponent)
                    let singleCatNr = -1;
                    for (let catNr = 0; catNr < this.enrollments.length; catNr++) {
                        const enrollmentInfo = this.enrollments[catNr];
                        if (singleCatId && singleCatId === enrollmentInfo._id) {
                            singleCatNr = catNr;
                            // // carefull with alreadyDone below!
                            // continue;
                        }
                        this.players[catNr] = enrollmentInfo.players;
                        this.logger.debug('players:', enrollmentInfo.catName, enrollmentInfo.players.length);
                        this.categoryNames[catNr] = enrollmentInfo.catName;
                        this.categoryTypes[catNr] = enrollmentInfo.cat_type;
                        this.categoryIds[catNr] = enrollmentInfo._id;
                        this.assignmentMode = assMode;

                        const alreadyDone = [];
                        for (let pi = 0; pi < playerInfos.length; pi++) {
                            const updated = playerInfos[pi];
                            if (updated.catId === enrollmentInfo._id) {
                                // found an updated player (moved into this category)
                                for (let p = 0; p < this.players[catNr].length; p++) {
                                    const pp = this.players[catNr][p];
                                    if (pp._id === updated.playerInfo._id) {
                                        pp.points = updated.playerInfo.points;
                                        alreadyDone.push(pi);
                                        break;
                                    }
                                }
                            }
                        }
                        // remove element that has been treated already
                        for (let i = alreadyDone.length - 1; i >= 0; i--) {
                            playerInfos.splice(alreadyDone[i], 1);
                        }
                    }
                    if (playerInfos.length > 0) {
                        this.logger.debug(playerInfos.length, 'playerInfos remain, probably multi-hop, e.g.', JSON.stringify(playerInfos[0]));
                    }

                    if (singleCatNr >= 0) {
                        this.showNewDraw = singleCatNr;
                        // this.router.navigate(['/ko/cat', {id: this.tournamentId, catId: singleCatNr, newDraw: 'true'}]);
                    }
                },
                error: error => {
                    this.logger.error('Could not retrieve rankings for players that had to be moved to another category', error);
                    this.alertService.error($localize`Could not retrieve rankings for players that had to be moved to another category`, error);
                }
            });
        }
    }

    private initIoConnection(): void {
        if (this.ioConnection = this.socketService.initSocket()) {
            // first init, register listeners
            this.socketService.onMessage(
                (msg: UpdateEvent) => {
                    if (msg.tournamentId === this.tournamentId) {
                        this.logger.debug('received event for category', msg.categoryId);
                        const bracketsarray = this.brackets.toArray();
                        if (bracketsarray.length === 0) {
                            this.logger.debug('ignoring, probably MunichCup');
                            return;
                        }
                        for (let i = 0; i < bracketsarray.length; i++) {
                            if (bracketsarray[i].categoryId === msg.categoryId) {
                                bracketsarray[i].updateWithMessage(msg);
                                return;
                            }
                        }
                        this.logger.warn('category for SocketIO message not found', msg.categoryId);
                    }
                }
            );

            this.socketService.onEvent(SockEvent.OPEN, () =>
                this.logger.debug('tournament connected')
            );

            this.socketService.onEvent(SockEvent.CLOSE, () =>
                this.logger.debug('tournament disconnected')
            );
        }
    }

    movePlayers(from: EnrollmentInfo, to: EnrollmentInfo) {
        if (from === to) {
            return true;
        }
        for (let p = 0; p < from.players.length; p++) {
            const playerInfo = from.players[p];
            // add to new cat
            to.players.push(playerInfo);
            // need to use points in the new ranking
            this.playersToUpdatePoints.push({ catId: to._id, playerInfo: playerInfo });

            // save for later results generation
            this.obsbls.push(this.tournamentsService.addReassignment(this.tournamentId, playerInfo._id, from._id, to._id));
        }
        this.logger.debug('moved', from.players.length, 'players from', from.catName,
            'to', to.catName, 'which has now', to.players.length, 'players');
        // remove all from old cat
        from.players = [];
    }

    executeObsbls() {
        observableForkJoin(this.obsbls).subscribe({
            next: response => {
                this.logger.debug('updated', response.length, 'reassignments in DB');
            },
            error: error => {
                this.logger.error('Could not update reassignments in DB', error);
                this.alertService.error($localize`Could not update reassignments in DB`, error);
            }
        });
    }

    moveJuniorUp(fromAge: number, from: EnrollmentInfo): boolean {
        let nextage = fromAge + 2;
        while (nextage <= 18) {
            for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
                if (this.enrollments[oCatNr].gender === from.gender && this.enrollments[oCatNr].maximum_age === nextage) {
                    // move and let the next round check whether there ae enough players
                    this.movePlayers(from, this.enrollments[oCatNr]);
                    return true;
                }
            }
            nextage += 2;
        }
        // next Uxx category not found, move to Open / Damen
        const searchGender = (from.gender === 2) ? 0 : from.gender;
        for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
            if (this.enrollments[oCatNr].gender === searchGender && this.enrollments[oCatNr].cat_type === 'singles' &&
                this.enrollments[oCatNr].minimum_age === 0 && this.enrollments[oCatNr].maximum_age === 999) {
                this.movePlayers(from, this.enrollments[oCatNr]);
                return true;
            }
        }
        // Open category not found, fail
        return false;
    }

    // puts players from the given cat to the next best fitting
    // returns true if a reassignment was done
    // catNr will be a Uxx category
    reassignJunior(catNr): boolean {
        const enrollmentInfo = this.enrollments[catNr];

        const maxAge = enrollmentInfo.maximum_age;
        if (maxAge === 12 || maxAge === 14) {
            // see if merging with the other gender of U12 suffices
            const otherGender = 3 - enrollmentInfo.gender;
            for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
                if (this.enrollments[oCatNr].gender === otherGender && this.enrollments[oCatNr].maximum_age === maxAge) {
                    // found other gender
                    const otherEnrollmentInfo = this.enrollments[oCatNr];
                    if (enrollmentInfo.players.length + otherEnrollmentInfo.players.length >= 4) {
                        // enough players in merged category, move and done
                        this.movePlayers(enrollmentInfo, otherEnrollmentInfo);
                        return true;

                    } else {
                        // not enough players, move players up (same gender)
                        return this.moveJuniorUp(maxAge, enrollmentInfo);
                    }
                }
            }
            // category for same age but other gender not found, move up (same gender)
            return this.moveJuniorUp(maxAge, enrollmentInfo);

        } else {
            // U16 or U18
            return this.moveJuniorUp(maxAge, enrollmentInfo);
        }
    }

    // puts players from the given cat to the next best fitting
    // returns true if a reassignment was done
    // catNr refers to an adult cat
    reassign(catNr: number): boolean {
        const enrollmentInfo = this.enrollments[catNr];
        const fromAge = enrollmentInfo.minimum_age;

        let nextage = fromAge - 5;
        while (nextage >= 30) {
            for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
                if (this.enrollments[oCatNr].gender === enrollmentInfo.gender && this.enrollments[oCatNr].cat_type === enrollmentInfo.cat_type && this.enrollments[oCatNr].minimum_age === nextage) {
                    // move and let the next round check whether there ae enough players
                    this.movePlayers(enrollmentInfo, this.enrollments[oCatNr]);
                    return true;
                }
            }
            nextage -= 5;
        }

        // special cases: mixed O40 doubles to mixed doubles (O50 etc. should have been moved to O40 above already)
        // special cases: female O40 doubles to female doubles (O50 etc. should have been moved to O40 above already)
        // gender === 3 is mixed doubles
        if ((enrollmentInfo.gender === 3 || enrollmentInfo.gender === 1) && enrollmentInfo.minimum_age === 40 && enrollmentInfo.cat_type === 'doubles' && enrollmentInfo.maximum_age === 999) {
            for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
                if (this.enrollments[oCatNr].gender === enrollmentInfo.gender && this.enrollments[oCatNr].cat_type === enrollmentInfo.cat_type &&
                    this.enrollments[oCatNr].minimum_age === 0 && this.enrollments[oCatNr].maximum_age === enrollmentInfo.maximum_age) {
                    this.movePlayers(enrollmentInfo, this.enrollments[oCatNr]);
                    return true;
                }
            }
        }

        // special cases: Single Women are too few - move to Open; female/mixed doubles and male O40 doubles (or O50 etc. if no O40 found above) to open doubles
        // gender === 1 is single women or female doubles; gender === 3 is mixed
        // gender === 2 with minimum_age >= 40 male doubles O40/O50/...
        if ((enrollmentInfo.gender === 2 && enrollmentInfo.minimum_age >= 40 && enrollmentInfo.cat_type === 'doubles')
            || ((enrollmentInfo.gender === 1 || enrollmentInfo.gender === 3) && enrollmentInfo.minimum_age === 0) && enrollmentInfo.maximum_age === 999) {
            const searchGender2 = 0;
            for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
                if (this.enrollments[oCatNr].gender === searchGender2 && this.enrollments[oCatNr].cat_type === enrollmentInfo.cat_type &&
                    this.enrollments[oCatNr].minimum_age === 0 && this.enrollments[oCatNr].maximum_age === 999) {
                    this.movePlayers(enrollmentInfo, this.enrollments[oCatNr]);
                    return true;
                }
            }
        }

        // previous single Üxx category not found, move to Open / Damen
        const searchGender = (enrollmentInfo.gender === 2) ? 0 : enrollmentInfo.gender;
        for (let oCatNr = 0; oCatNr < this.enrollments.length; oCatNr++) {
            if (this.enrollments[oCatNr].gender === searchGender && this.enrollments[oCatNr].cat_type === 'singles' &&
                this.enrollments[oCatNr].minimum_age === 0 && this.enrollments[oCatNr].maximum_age === 999) {
                if (enrollmentInfo.catName === this.enrollments[oCatNr].catName) {
                    // already have Open / Damen
                    return false;
                }
                this.movePlayers(enrollmentInfo, this.enrollments[oCatNr]);
                return true;
            }
        }
        // Open / Damen category not found, fail
        return false;
    }

    needsReassignment(enrollmentInfo: EnrollmentInfo): boolean {
        if (enrollmentInfo.players.length === 0) {
            return false;
        }

        return enrollmentInfo.players.length < this.MINPLAYERSPERCATEGORY * (enrollmentInfo.cat_type === 'doubles' ? 2 : 1);
    }

    linkClick(catNr) {
        this.router.navigate(['/ko/cat', { id: this.tournamentId, catId: catNr }]);
    }

    pushLocalResults() {
        this.logger.warn('pushing local match scores to DB', this.tournamentName, this.tournamentId, 'localStorage.length=', localStorage.length);
        this.matchdbService.pushLocalResults();
    }

    updateScores() {
        this.disableReloadButton = true;
        setTimeout(() => {
            this.disableReloadButton = false;
        }, 5000);

        this.reloadService.reload(0);

        // this.categoryNames = [];
        // this.tournamentsService.getDrawnCategories(this.tournamentId).subscribe(
        //     response => {
        //         this.logger.trace('retrieved drawn categories', response);
        //         if (response.success === true) {
        //             this.assignmentMode = response.result.assignmentMode;
        //             if (!this.assignmentMode) {
        //                 this.assignmentMode = 'DCV';
        //             }
        //             this.categoryIds = response.result.catIds;
        //             this.categoryNames = response.result.catNames;
        //
        //         } else {
        //             this.logger.error('Could not retrieve drawn categories tournament:', response.error);
        //             this.alertService.error($localize`Could not retrieve drawn categories tournament`, response.error);
        //         }
        //     },
        //     error => {
        //         this.logger.error('Could not retrieve drawn categories:', error);
        //         this.alertService.error($localize`Could not retrieve drawn categories`, error);
        //     }
        // );
    }

    hasPassed(): boolean {
        if (this.tournament) {
            return new Date().getTime() - Date.parse(this.tournament.end_date) > 24 * 60 * 60 * 1000;
        }
        return false;
    }

    haveLocalData(): boolean {
        if (localStorage.length > 1) {
            for (let i = 0; i < localStorage.length; i++) {
                const key = localStorage.key(i);
                if (key !== 'currentUser' && key.indexOf('/') !== -1) {
                    try {
                        // const data = localStorage.getItem(key);
                        const keysplit = key.split('/');
                        const tournamentId = keysplit[0];
                        // const categoryId = keysplit[1];
                        if (tournamentId === this.tournamentId) {
                            return true;
                        }
                    } catch (err) {
                        continue;
                    }
                }
            }
        }
        return false;
    }

    saveData() {
        if (this.haveLocalData()) {
            const localData = {};
            for (let i = 0; i < localStorage.length; i++) {
                const key = localStorage.key(i);
                if (key !== 'currentUser' && key.indexOf('/') !== -1) {
                    try {
                        const data = localStorage.getItem(key);
                        const keysplit = key.split('/');
                        const tournamentId = keysplit[0];
                        const categoryId = keysplit[1];
                        if (tournamentId === this.tournamentId) {
                            localData[categoryId] = JSON.parse(data);
                        }
                    } catch (err) {
                        continue;
                    }
                }
            }
            if (localData) {
                const json = JSON.stringify(localData);
                const blob = new Blob([json], { type: 'application/json' });
                saveAs(blob, this.tournamentName + '.json');
            }
        } else {
            const obsvbls = [];
            for (let c = 0; c < this.categoryIds.length; c++) {
                const catId = this.categoryIds[c];
                obsvbls.push(this.matchdbService.getTournamentData(this.tournamentId, catId));
            }
            observableForkJoin(obsvbls).subscribe({
                next: values => {
                    const data = {};
                    for (let i = 0; i < values.length; i++) {
                        data[this.categoryIds[i]] = values[i]['result'];
                    }
                    const json = JSON.stringify(data);
                    const blob = new Blob([json], { type: 'application/json' });
                    saveAs(blob, this.tournamentName + '.json');
                },
                error: error => {
                    this.logger.error('Could not retrieve tournament data', error);
                }
        });
        }
    }

    addPointsToRanking() {
        const willBeSetLater = undefined;
        // this.brackets.forEach(bracket => bracket.addPointsToRanking());
        if (this.assignmentMode.indexOf('MunichCup') >= 0) {
            const infos: ResultInfo = { tournament: this.tournament, results: undefined, category: this.tournament.categories[0], maxPoints: this.tournament.points, isDouble: false, groups: willBeSetLater, results_b: undefined, ko: undefined, ko_b: undefined, players: this.players[0], resultsRanking: undefined, doBRound: false, resultsFromOtherCats: undefined, resultsRankingFromOtherCats: undefined };
            this.tournamentsService.addPointsToRanking(infos, false);
        } else {
            this.brackets.forEach(bracket => bracket.addPointsToRanking());
        }
    }
    addPointsToDMQualiRanking() {
        this.brackets.forEach(bracket => bracket.addPointsToDMQualiRanking());
    }
}
