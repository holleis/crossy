import { Component, OnInit, Input, ViewChildren, QueryList, ElementRef, EventEmitter, Output } from '@angular/core';
import { EnrollmentInfo } from '../../cat-info';
import { Round } from '../munichcup/round';
import { TournamentsService } from '../../tournaments.service';
import { Tournament } from '../../tournament';
import { MatchdbService } from '../matchdb.service';
import { Group } from '../../group';
import { Match } from '../../match';
import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../../../util/alert/alert.service';
import { PlayerRef } from '../../playerref';
import { UserService } from '../../../auth/_services/user.service';
import { Role } from '../../../user/role';
import { MatDialog } from '@angular/material/dialog';
import { YesNoDialogComponent } from '../../../util/yesno-dialog.component';
import { SocketService, SockEvent, UpdateEvent } from '../../../util/websockets/socket.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
    selector: 'app-munichcup-groups',
    templateUrl: './munichcup-groups.component.html',
    styleUrls: ['./munichcup-groups.component.css'],
})
export class MunichcupGroupsComponent implements OnInit {

    NUMBER_OF_DAYS = 21 - 1;

    groupSize = 6;

    @Input() assignmentMode = 'MunichCup (Gruppen)';
    @Input() catType = 'singles'; // or doubles
    @Input() tournamentId: string;
    tournament: Tournament;
    @Input() categoryId: string;
    @Input() newDraw: boolean;
    // this is the master array; all other sortedPlayers reference this; don't change the order!
    @Input() allPlayers: EnrollmentInfo['players'] = [];
    @Output() allPlayersChange = new EventEmitter<EnrollmentInfo['players']>();

    isFinalRound = false;
    rounds: Round[] = [];
    newFrom: moment.Moment;
    newTo: moment.Moment;

    playedCurrentRound: string[] = [];

    isdraw: boolean[][] = [[]];

    ioConnection: any;

    scorelistener = new Subject<{roundNr: number, match: Match}>();

    displayedColumns: string[] = ['rank', 'points', 'name', 'setsWon', 'setsLost', 'ptsRel', 'ptsWon', 'ptsLost'];
    rankingTableSource;

    myPlayerId;
    myRoles;

    subject = 'MunichCup - Nächste Runde';
    emailToAll: string;
    emailToAllTemplate = 'Liebe MunichCup-Spieler,\n\n\
    \
    die Auslosung ist beendet, der neue Spieltag beginnt am Mittwoch {{START}} und endet am Dienstag {{END}}.\n\n\
    Der Link zu der Auslosung unter dem Ihr auch Eure Ergebnisse eintragen könnt ist:\n\n\
    \
    https://crossy.paul-holleis.de/ko;id={{TOURNAMENTID}}\n\n\
    \
    Versucht bitte so bald wie möglich mit Eurem Gegner einen Termin und Ort zu finden.\n\n\
    \
    Bitte sagt doch AUCH über WhatsApp kurz Bescheid, wie Euer Match ausgegangen ist, dann wissen alle gleich, was so los ist!\n\n\
    \
    Viel Spaß, Paul';

    showScoreInput: { match: Match, roundNr: number, set: number, player: number, nextTabIndex: number, partnerHas?: number };
    @ViewChildren('minp') matchInputs: QueryList<ElementRef>;

    constructor(
        private tournamentsService: TournamentsService,
        private matchdbService: MatchdbService,
        private logger: NGXLogger,
        private alertService: AlertService,
        private userService: UserService,
        public dialog: MatDialog,
        private socketService: SocketService
    ) {
        const debounce = this.scorelistener.pipe(
            debounceTime(3000),
            distinctUntilChanged(),
            switchMap((newscore: {roundNr: number, match: Match}) => {
                this.logger.trace('score update:', newscore);
                this.onScoreChange(newscore.match);
                return [];
            })
        );
        debounce.subscribe();
    }

    ngOnInit() {
        const user = this.userService.getCurrentUser();
        if (user) {
            this.myPlayerId = user.player_id;
            this.myRoles = user.roles;
        }

        if (this.newDraw) {
            if (!this.allPlayers) {
                return;
            }

            if (this.allPlayers.length < 6) {
                this.alertService.error($localize`Need at least 6 players!`);
                return;
            }
            if (this.allPlayers.length % 2 === 1) {
                this.alertService.error($localize`Need an even number of players (multiple of 2)!`);
                return;
            }

            // all start with 0 points
            for (let p = 0; p < this.allPlayers.length; p++) {
                this.allPlayers[p].points = 0;
            }

            // create first round
            // assume today's date as start, will be corrected later
            const start = moment().format('DD.MM.YYYY');
            const end = moment().add(this.NUMBER_OF_DAYS, 'days').format('DD.MM.YYYY');
            const round = new Round(start, end);
            this.rounds.push(round);

            // a round stores PlayerRef[] to the allPlayers array of real Players
            const roundPlayers = [];
            for (let p = 0; p < this.allPlayers.length; p++) {
                const pRef = new PlayerRef();
                pRef.playerListIdx = p;
                roundPlayers[p] = pRef;
            }
            round.sortedPlayers = roundPlayers;
            this.allPlayersChange.emit(this.allPlayers);

            // get real start date from tournament

            try {
                this.tournamentsService.getTournament(this.tournamentId).subscribe({
                    next: response => {
                        if (response.success === true) {
                            if (response.result && response.result.start_date) {
                                this.tournament = response.result;
                                this.rounds[0].start = response.result.start_date;
                                const endDate = moment(response.result.start_date, 'DD.MM.YYYY').add(this.NUMBER_OF_DAYS, 'days');
                                this.rounds[0].end = endDate.format('DD.MM.YYYY');
                                this.newFrom = endDate.clone().add(1, 'days');
                                this.newTo = endDate.clone().add(this.NUMBER_OF_DAYS, 'days');

                                this.setupRound(0);

                                this.storeToDB();
                                this.updateEmailTemplate();
                            }
                        } else {
                            this.logger.error('could not get tournament', this.tournamentId, response.error);
                        }
                    },
                    error: error => {
                        this.logger.error('could not get tournament', this.tournamentId, error);
                    }
                });
            } catch (err) {
                this.logger.error('could not get tournament', this.tournamentId, err);
            }

        } else {
            this.readStateFromDB();

            // tslint:disable-next-line: no-bitwise
            if ((this.myRoles & Role.ROLE_DCV) === 0 && (this.myRoles & Role.ROLE_PAUL) === 0) {
                // get tournament to have the responsibles (see getReadOnly())

                try {
                    this.tournamentsService.getTournament(this.tournamentId).subscribe(
                        response => {
                            if (response.success === true) {
                                if (response.result && response.result.responsibles) {
                                    this.tournament = response.result;
                                }
                            } else {
                                this.logger.error('could not get tournament', this.tournamentId, response.error);
                            }
                        },
                        error => {
                            this.logger.error('could not get tournament', this.tournamentId, error);
                        }
                    );
                } catch (err) {
                    this.logger.error('could not get tournament', this.tournamentId, err);
                }
            }
        }

        this.initIoConnection();
    }

    showQuickInput(roundNr: number, match: Match, set: number, player: number, nextTabIndex: number): void {
        // this.logger.debug('calling ...')
        setTimeout(() => {
            // this.logger.debug('... set now')
            const partnerHas = match.score[set][player === 0 ? 'p2' : 'p1'];
            this.showScoreInput = { roundNr, match, set, player, nextTabIndex, partnerHas };
        }, 750)
    }
    hideQuickInput(): void {
        // this.logger.debug('set to undefined ...');
        setTimeout(() => {
            this.showScoreInput = undefined;
            // this.logger.debug('... undef now')
        }, 300);
    }
    enterScore(score: number): void {
        if (this.showScoreInput.player === 0) {
            this.showScoreInput.match.score[this.showScoreInput.set].p1 = score;
        } else if (this.showScoreInput.player === 1) {
            this.showScoreInput.match.score[this.showScoreInput.set].p2 = score;
        } else {
            this.logger.warn('error in input: ' + score);
        }
        this.scorelistener.next({ roundNr: this.showScoreInput.roundNr, match: this.showScoreInput.match });
        // go to next input or close
        if (this.showScoreInput.player === 1 && Match.checkWinner(this.showScoreInput.match)) {
            // probably finished
            this.showScoreInput = undefined;
        } else {
            for (let i = 0; i < this.matchInputs.length; i++) {
                const input = this.matchInputs.get(i);
                if (input.nativeElement.tabIndex === this.showScoreInput.nextTabIndex) {
                    input.nativeElement.focus();
                    // this.logger.debug('focusing on ' + input.nativeElement.tabIndex);
                    break;
                }
            }
        }
    }

    private initIoConnection(): void {
        if (this.ioConnection = this.socketService.initSocket()) {
            // first init, register listeners
            this.socketService.onMessage(
                (msg: UpdateEvent) => {
                    if (msg.tournamentId === this.tournamentId &&
                        this.categoryId === this.categoryId && msg.type === SockEvent.MATCHUPDATE) {

                        const message = msg.message;
                        this.logger.debug('message received', message);
                        try {
                            const match = JSON.parse(message) as Match;
                            if (match.score[0].p1 === -1 && match.score[0].p2 === -1) {
                                match.score[0].p1 = 0;
                                match.score[0].p2 = 0;
                                this.isdraw[match.matchNr.level][match.matchNr.num] = true;
                            } else {
                                this.isdraw[match.matchNr.level][match.matchNr.num] = false;
                            }
                            const round = this.rounds[match.matchNr.level];
                            round.matches[match.matchNr.num].score = match.score;
                            this.updatePlayerPoints();
                        } catch (err) {
                            this.logger.error('could not parse websocket message for matchupdate', message);
                        }
                    }
                }
            );

            this.socketService.onEvent(SockEvent.OPEN, () =>
                this.logger.debug('munichcupgroups connected')
            );

            this.socketService.onEvent(SockEvent.CLOSE, () =>
                this.logger.debug('munichcupgroups disconnected')
            );
        }
    }

    isFinalRoundChanged() {
        this.storeToDB();

        this.rankingTableSource = this.getRanking();
    }

    readStateFromDB() {
        this.matchdbService.getTournamentData(this.tournamentId, this.categoryId).subscribe(
            response => {
                if (response.success === true && response.result) {
                    this.allPlayers = response.result.players;
                    const groups = response.result.groups;

                    // restore data from groups

                    this.isFinalRound = false;
                    for (let g = 0; g < groups.length; g++) {
                        this.isdraw[g] = [];
                        const group = groups[g];
                        const nrStartEnd = group.name; // 0;01.01.2018;15.01.2018
                        const nrStartEndSplit = nrStartEnd.split(';');
                        const round = new Round(nrStartEndSplit[1], nrStartEndSplit[2]);
                        if (nrStartEndSplit.length >= 4) {
                            this.isFinalRound = true;
                        }

                        const endDate = moment(nrStartEndSplit[2], 'DD.MM.YYYY');
                        this.newFrom = endDate.clone().add(1, 'days');
                        this.newTo = endDate.clone().add(21, 'days');

                        round.sortedPlayers = group.players;
                        round.matches = group.matches;
                        // restore isdraw state (encoded by -1 score)
                        for (let m = 0; m < round.matches.length; m++) {
                            const mm = round.matches[m];
                            if (mm.score[0].p1 === -1 && mm.score[0].p2 === -1) {
                                this.isdraw[g][m] = true;
                                mm.score[0].p1 = 0;
                                mm.score[0].p2 = 0;
                            }
                        }

                        this.rounds[g] = round;
                    }
                    this.updatePlayerPoints();
                    this.updateEmailTemplate();
                    this.allPlayersChange.emit(this.allPlayers);

                } else if (!response.result) {
                    this.logger.trace('no stored data found for', this.tournamentId);

                } else {
                    this.logger.error('Could not retrieve tournament data:', response.error);
                    this.alertService.error($localize`Could not retrieve tournament data`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve tournament data:', error);
                this.alertService.error($localize`Could not retrieve tournament data`, error);
            }
        );
    }

    storeToDB() {
        // model the rounds as groups and save to DB
        const rounds: Group[] = [];
        for (let r = 0; r < this.rounds.length; r++) {
            const theRound = this.rounds[r];
            // encode the start and end of the round in the group name ...
            // also encode if it is the last round
            let groupName = r.toString() + ';' + theRound.start + ';' + theRound.end;
            if (this.isFinalRound && r === this.rounds.length - 1) {
                groupName += ';final';
            }
            const roundModel = new Group(theRound.sortedPlayers.length, groupName);
            roundModel.players = theRound.sortedPlayers;
            roundModel.matches = JSON.parse(JSON.stringify(theRound.matches));

            // encode isdraw as -1 score
            if (!this.isdraw[r]) {
                this.isdraw[r] = [];
            }
            for (let m = 0; m < roundModel.matches.length; m++) {
                if (this.isdraw[r][m]) {
                    const mm = roundModel.matches[m];
                    mm.score[0].p1 = -1;
                    mm.score[0].p2 = -1;
                } else {
                    this.isdraw[r][m] = false;
                }
            }

            rounds[r] = roundModel;
        }

        this.matchdbService.storeTournamentData(this.tournamentId, this.categoryId, this.allPlayers, rounds, undefined, undefined, this.assignmentMode).subscribe(
            response2 => {
                if (response2.success === true) {
                    this.logger.debug('stored ok');

                } else {
                    this.logger.error('Could not store tournament data:', response2.error);
                    this.alertService.error($localize`Could not store tournament data`, response2.error);
                }
            },
            error => {
                this.logger.error('Could not store tournament data:', error);
                this.alertService.error($localize`Could not store tournament data`, error);
            }
        );
    }

    updateEmailTemplate() {
        if (this.rounds.length) {
            this.emailToAll = this.emailToAllTemplate
            .replace('{{START}}', this.rounds[this.rounds.length - 1].start)
            .replace('{{END}}', this.rounds[this.rounds.length - 1].end)
            .replace('{{TOURNAMENTID}}', this.tournamentId);
        }
    }

    onScoreChange(match: Match) {
        this.logger.debug('onScoreChange', match);

        // store in DB
        this.matchdbService.updateMatch(this.tournamentId, this.categoryId, match).subscribe(
            response => {
                if (response.success !== true) {
                    this.logger.error('Failed to store score. Will try again on next change', response.error);
                    this.alertService.error($localize`Failed to store score. Will try again on next change`, response.error);
                } else {
                    this.logger.debug('successfully updated score');
                    // this.socketService.send(SockEvent.MATCHUPDATE, JSON.stringify(match));
                    this.socketService.send(SockEvent.MATCHUPDATE, this.tournamentId, this.categoryId, JSON.stringify(match));
                }
            },
            error => {
                this.logger.error('Failed to store score. Will try again on next change:', error);
                this.alertService.error($localize`Failed to store score. Will try again on next change`, error);
            }
        );

        this.updatePlayerPoints();
    }

    isdrawChanged(isdraw: boolean, match: Match) {
        const matchCopy = JSON.parse(JSON.stringify(match));
        if (isdraw) {
            // encode as -1 score
            matchCopy.score[0].p1 = -1;
            matchCopy.score[0].p2 = -1;
        } else {
            matchCopy.score[0].p1 = 0;
            matchCopy.score[0].p2 = 0;
        }
        this.onScoreChange(matchCopy);
    }

    updatePlayerPoints() {
        // update player points
        // first reset all to 0

        for (let p = 0; p < this.allPlayers.length; p++) {
            this.allPlayers[p].points = 0;
        }
        this.playedCurrentRound = [];
        // count all points

        for (let r = 0; r < this.rounds.length; r++) {
            // need to go through all rounds
            const round = this.rounds[r];
            for (let m = 0; m < round.matches.length; m++) {
                // need to check all matches
                const theMatch = round.matches[m];
                const wins = {p1: 0, p2: 0};
                for (let s = 0; s < theMatch.score.length; s++) {
                    // count all set points
                    Match.checkWinnerSetWins(theMatch.score[s], wins);
                }
                // add to players

                if (theMatch.player1.bye !== true) {
                    this.allPlayers[theMatch.player1.playerListIdx].points += wins.p1;
                }
                if (theMatch.player2.bye !== true) {
                    this.allPlayers[theMatch.player2.playerListIdx].points += wins.p2;
                }

                if (r === this.rounds.length - 1) {
                    // current round

                    if (wins.p1 >= 3 || wins.p2 >= 3 || this.isdraw[r][m]) {
                        // match done
                        this.playedCurrentRound.push(this.allPlayers[theMatch.player1.playerListIdx]._id);
                        this.playedCurrentRound.push(this.allPlayers[theMatch.player2.playerListIdx]._id);
                    }
                }
            }
        }

        this.rankingTableSource = this.getRanking();
    }

    hasPlayed(player: {_id: string} ) {
        return this.playedCurrentRound.indexOf(player._id) > -1;
    }

    sortPlayerRefs(playerRefs: PlayerRef[]) {
        // TODO this repeats code of getRanking() !!

        const allPlayersCopy = this.allPlayers.slice();

        // calculate points from matches to break ties
        const matches = this.rounds[this.rounds.length - 1].matches;
        for (let m = 0; m < matches.length; m++) {
            const match = matches[m];
            match.player1.calcPoints = 0;
            match.player2.calcPoints = 0;
            Group.calculateCalcPointsMatch(match);
            const p1 = match.player1;
            const p2 = match.player2;
            (allPlayersCopy[p1.playerListIdx] as any).calcPoints = p1.calcPoints;
            (allPlayersCopy[p2.playerListIdx] as any).calcPoints = p2.calcPoints;
            (allPlayersCopy[p1.playerListIdx] as any).calc = this.getCalc(p1.calcPoints);
            (allPlayersCopy[p2.playerListIdx] as any).calc = this.getCalc(p2.calcPoints);
        }

        playerRefs.sort((pRef1: PlayerRef, pRef2: PlayerRef) => {
            if (typeof pRef1.playerListIdx === 'undefined' || typeof pRef2.playerListIdx === 'undefined') {
                this.logger.error('playerListIdx should not be undefined while sorting');
                return 0;
            }
            const p1 = allPlayersCopy[pRef1.playerListIdx];
            const p2 = allPlayersCopy[pRef2.playerListIdx];
            if (p1.points === p2.points) {
                if ((p2 as any).calcPoints !== (p1 as any).calcPoints) {
                    // use only matches of the last round
                    return (p2 as any).calcPoints - (p1 as any).calcPoints;
                } else {
                    if (p1.first_name < p2.first_name) {
                        return -1;
                    } else if (p1.first_name > p2.first_name) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            } else {
                return p2.points - p1.points;
            }
        });
    }

    getCalc(orgpts: number): {setsWon: number, setsLost: number, ptsRel: number, ptsWon: number, ptsLost: number} {
        let pts = orgpts % Group.gamePts;
        const ret: {setsWon: number, setsLost: number, ptsRel: number, ptsWon: number, ptsLost: number} = {} as any;
        ret.setsWon = Math.round(pts / Group.setsWonPts);
        pts = pts - Math.round(pts / Group.setsWonPts) * Group.setsWonPts;

        ret.setsLost = Math.round(pts / Group.setsLostPts);
        pts = pts - Math.round(pts / Group.setsLostPts) * Group.setsLostPts;

        ret.ptsRel = Math.round(pts / Group.ptsRelPts);
        pts = pts - Math.round(pts / Group.ptsRelPts) * Group.ptsRelPts;

        ret.ptsWon = Math.round(pts / Group.ptsWonPts);
        pts = pts - Math.round(pts / Group.ptsWonPts) * Group.ptsWonPts;

        ret.ptsLost = Math.round(pts / Group.ptsLostPts);

        return ret;
    }

        // getCalcString(orgpts: number): string {
    //     let pts = orgpts % Group.gamePts;
    //     let str = '';

    //     str = str.concat('Sätze gewonnen', Math.round(pts / Group.setsWonPts) + ', ');
    //     pts = pts - Math.round(pts / Group.setsWonPts) * Group.setsWonPts;

    //     str = str.concat('Sätze verloren', Math.round(pts / Group.setsLostPts) + ', ');
    //     pts = pts - Math.round(pts / Group.setsLostPts) * Group.setsLostPts;

    //     str = str.concat('Punktedifferenz', Math.round(pts / Group.ptsRelPts) + ', ');
    //     pts = pts - Math.round(pts / Group.ptsRelPts) * Group.ptsRelPts;

    //     str = str.concat('Punkte gewonnen', Math.round(pts / Group.ptsWonPts) + ', ');
    //     pts = pts - Math.round(pts / Group.ptsWonPts) * Group.ptsWonPts;

    //     str = str.concat('Punkte verloren', Math.round(pts / Group.ptsLostPts) + '');

    //     return str;
    // }

    getRanking(): {first_name: string, last_name: string, points: number, rank: number, _id: string}[] {
        // TODO this repeats code of sortPlayerRefs() !!

        const allPlayersCopy = this.allPlayers.slice();

        this.displayedColumns = ['rank', 'points', 'name', 'setsWon', 'setsLost', 'ptsRel', 'ptsWon', 'ptsLost'];
        const ranking = [];
        if (!this.isFinalRound) {
            // calculate points from matches to break ties
            const matches = this.rounds[this.rounds.length - 1].matches;
            for (let m = 0; m < matches.length; m++) {
                const match = matches[m];
                match.player1.calcPoints = 0;
                match.player2.calcPoints = 0;
                Group.calculateCalcPointsMatch(match);
                const p1 = match.player1;
                const p2 = match.player2;
                (allPlayersCopy[p1.playerListIdx] as any).calcPoints = p1.calcPoints;
                (allPlayersCopy[p2.playerListIdx] as any).calcPoints = p2.calcPoints;
                (allPlayersCopy[p1.playerListIdx] as any).calc = this.getCalc(p1.calcPoints);
                (allPlayersCopy[p2.playerListIdx] as any).calc = this.getCalc(p2.calcPoints);
            }

            allPlayersCopy.sort((p1, p2) => {
                if (p1.points === p2.points) {
                    if ((p2 as any).calcPoints !== (p1 as any).calcPoints) {
                        // use only matches of the last round
                        return (p2 as any).calcPoints - (p1 as any).calcPoints;
                    } else {
                        if (p1.first_name < p2.first_name) {
                            return -1;
                        } else if (p1.first_name > p2.first_name) {
                            return 1;
                        } else {
                            return 0;
                        }
                    }
                } else {
                    return p2.points - p1.points;
                }
            });
            const groupSizes = this.getGroupSizes(this.allPlayers.length);
            let groupCnt = 0;
            let rank = 1;
            let jumped = 1;
            let lastpoints = -1;
            for (let p = 0; p < allPlayersCopy.length; p++) {
                (allPlayersCopy[p] as any).hasPlayed = this.hasPlayed(allPlayersCopy[p]);
                if (p > 0) {
                    if (allPlayersCopy[p].points !== lastpoints) {
                        rank += jumped;
                        jumped = 1;
                    } else {
                        jumped++;
                    }
                }
                if (p >= groupSizes[groupCnt]) {
                    groupCnt += 1;
                    if (groupSizes.length > groupCnt) {
                        groupSizes[groupCnt] += groupSizes[groupCnt - 1];
                    }
                }
                ranking.push({
                    first_name: allPlayersCopy[p].first_name,
                    last_name: allPlayersCopy[p].last_name,
                    points: allPlayersCopy[p].points,
                    _id: allPlayersCopy[p]._id,
                    rank: rank,
                    calcPoints: (allPlayersCopy[p]as any).calcPoints,
                    // calcString: (allPlayersCopy[p]as any).calcString,
                    calc: (allPlayersCopy[p]as any).calc,
                    group: groupCnt,
                    hasPlayed: (allPlayersCopy[p]as any).hasPlayed
                });
                lastpoints = allPlayersCopy[p].points;
            }
        } else {
            // this is the final round
            // draw breaking is done by looking at all matches that involved exactly those players
            // this.displayedColumns = ['rank', 'points', 'name', 'directGamesWon', 'overallGamesWon', 'overallGamesLost', 'overallSetsLost', 'overallPointsWon', 'overallPointsLost'];
            this.displayedColumns = ['rank', 'points', 'name', 'overallGamesWon', 'overallGamesLost', 'overallSetsLost', 'overallPointsWon', 'overallPointsLost'];
            
            // initial sort by overall set points
            allPlayersCopy.sort((p1, p2) => p2.points - p1.points);

            // // find all with same points
            // let rank = 1;
            // let jumped = 1;
            // let lastpoints = -1;
            for (let p = 0; p < allPlayersCopy.length; p++) {
                (allPlayersCopy[p] as any).hasPlayed = this.hasPlayed(allPlayersCopy[p]);
                // allPlayersCopy[p]['directGamesWon'] = undefined;
                // if (p > 0) {
                //     if (allPlayersCopy[p].points !== lastpoints) {
                //         if (jumped > 1) {
                //             // have last [jumped] players with same points
                //             const playerIds = allPlayersCopy.slice(p - jumped, p).map((pl) => pl._id);
                //             const dgw = this.findBest(this.allPlayers, playerIds);
                //             for (let i = p - jumped; i <= p - 1; i++) {
                //                 if (!ranking[i].calc) {
                //                     ranking[i].calc = {};
                //                 }
                //                 ranking[i].calc.directGamesWon = dgw[i - (p - jumped)];
                //             }
                //         }
                //         rank += jumped;
                //         jumped = 1;
                //     } else {
                //         jumped++;
                //     }
                // }
                ranking.push({
                    first_name: allPlayersCopy[p].first_name,
                    last_name: allPlayersCopy[p].last_name,
                    points: allPlayersCopy[p].points,
                    _id: allPlayersCopy[p]._id,
                    // rank: rank,
                    hasPlayed: (allPlayersCopy[p] as any).hasPlayed,
                    calc: { 
                        overallGamesWon: MunichcupGroupsComponent.getOverallGames(true, this.allPlayers, allPlayersCopy[p]._id, this.rounds),
                        overallGamesLost: MunichcupGroupsComponent.getOverallGames(false, this.allPlayers, allPlayersCopy[p]._id, this.rounds),
                        // overallSetsWon: MunichcupGroupsComponent.getOverallSets(true, this.allPlayers, allPlayersCopy[p]._id, this.rounds),
                        overallSetsLost: MunichcupGroupsComponent.getOverallSets(false, this.allPlayers, allPlayersCopy[p]._id, this.rounds),
                        overallPointsWon: MunichcupGroupsComponent.getOverallPoints(true, this.allPlayers, allPlayersCopy[p]._id, this.rounds),
                        overallPointsLost: MunichcupGroupsComponent.getOverallPoints(false, this.allPlayers, allPlayersCopy[p]._id, this.rounds),
                    },
                });
                // lastpoints = allPlayersCopy[p].points;
            }
            ranking.sort((p1, p2) => {
                if (p1.points === p2.points) {
                    if (p1.calc.overallGamesWon === p2.calc.overallGamesWon) {
                        if (p1.calc.overallGamesLost === p2.calc.overallGamesLost) {
                            if (p1.calc.overallSetsLost === p2.calc.overallSetsLost) {
                                if (p1.calc.overallPointsWon === p2.calc.overallPointsWon) {
                                    // if (p1.calc.overallPointsLost === p2.calc.overallPointsLost) {
                                    //     if (p1.calc.directGamesWon === p2.calc.directGamesWon) {
                                    //         return 0;
                                    //     } else {
                                    //         return p2.calc.directGamesWon - p1.calc.directGamesWon;
                                    //     }
                                    // } else {
                                        return p1.calc.overallPointsLost - p2.calc.overallPointsLost;
                                    // }
                                } else {
                                    return p2.calc.overallPointsWon - p1.calc.overallPointsWon;
                                }
                            } else {
                                return p1.calc.overallSetsLost - p2.calc.overallSetsLost;
                            }
                        } else {
                            return p1.calc.overallGamesLost - p2.calc.overallGamesLost;
                        }
                    } else {
                        return p2.calc.overallGamesWon - p1.calc.overallGamesWon;
                    }
                } else {
                    return p2.points - p1.points;
                }
            });
            ranking.forEach((r, idx) => {
                r.rank = idx + 1;
            });
        }
        return ranking;
    }

    static getOverallGames(won: boolean, players: EnrollmentInfo['players'], playerId: string, rounds: Round[]): number {
        let games = 0;
        for (let r = 0; r < rounds.length; r++) {
            const round = rounds[r];
            const matches = round.matches;
            for (let m = 0; m < matches.length; m++) {
                const match = matches[m];
                const p1 = match.player1;
                const p2 = match.player2;
                const p1Id = players[p1.playerListIdx]._id;
                const p2Id = players[p2.playerListIdx]._id;
                if (playerId === p1Id) {
                    const winner = Match.checkWinner(match);
                    if ((won && winner === -1) || (!won && winner === 1)) {
                        games += 1;
                    }
                    break;
                } else if (playerId === p2Id) {
                    const winner = Match.checkWinner(match);
                    if ((won && winner === 1) || (!won && winner === -1)) {
                        games += 1;
                    }
                    break;
                }
            }
        }
        return games;
    }

    static getOverallSets(won: boolean, players: EnrollmentInfo['players'], playerId: string, rounds: Round[]): number {
        let sets = 0;
        for (let r = 0; r < rounds.length; r++) {
            const round = rounds[r];
            const matches = round.matches;
            for (let m = 0; m < matches.length; m++) {
                const match = matches[m];
                const p1 = match.player1;
                const p2 = match.player2;
                const p1Id = players[p1.playerListIdx]._id;
                const p2Id = players[p2.playerListIdx]._id;
                for (let s = 0; s < match.score.length; s++) {
                    const set = match.score[s];
                    const winner = Match.checkWinnerSet(set);
                    if (playerId === p1Id) {
                        if ((won && winner === -1) || (!won && winner === 1)) {
                            sets += 1;
                        }
                    } else if (playerId === p2Id) {
                        if ((won && winner === 1) || (!won && winner === -1)) {
                            sets += 1;
                        }
                    }
                }
            }
        }
        return sets;
    }

    static getOverallPoints(won: boolean, players: EnrollmentInfo['players'], playerId: string, rounds: Round[]): number {
        let points = 0;
        for (let r = 0; r < rounds.length; r++) {
            const round = rounds[r];
            const matches = round.matches;
            for (let m = 0; m < matches.length; m++) {
                const match = matches[m];
                const p1 = match.player1;
                const p2 = match.player2;
                const p1Id = players[p1.playerListIdx]._id;
                const p2Id = players[p2.playerListIdx]._id;
                for (let s = 0; s < match.score.length; s++) {
                    const set = match.score[s];
                    if ((playerId === p1Id && won) || (playerId === p2Id && !won)) {
                        points += set.p1;
                    } else if ((playerId === p2Id && won) || (playerId === p1Id && !won)) {
                        points += set.p2;
                    }
                }
            }
        }
        return points;
    }

    findBest(players: EnrollmentInfo['players'], playerIds: string[]): number[] {
        const dgw = [];
        dgw.length = playerIds.length;
        dgw.fill(0, 0, playerIds.length);
        for (let r = 0; r < this.rounds.length; r++) {
            const round = this.rounds[r];
            const matches = round.matches;
            for (let m = 0; m < matches.length; m++) {
                const match = matches[m];
                const p1 = match.player1;
                const p2 = match.player2;
                const p1Id = players[p1.playerListIdx]._id;
                const p2Id = players[p2.playerListIdx]._id;
                const p1Idx = playerIds.indexOf(p1Id);
                const p2Idx = playerIds.indexOf(p2Id);
                if (p1Idx >= 0 && p2Idx >= 0) {
                    // the match involves players from the given "list"
                    const winner = Match.checkWinner(match);
                    if (winner === -1) {
                        dgw[p1Idx] += 1;
                        dgw[p2Idx] -= 1;
                    } else if (winner === 1) {
                        dgw[p2Idx] += 1;
                        dgw[p1Idx] -= 1;
                    }
                }
            }
        }
        return dgw;
    }

    havePlayedLastRound(currentRound: number, p1: PlayerRef, p2: PlayerRef): boolean {
        if (!currentRound) {
            return false;
        }
        const thisRoundP1 = p1.playerListIdx;
        const thisRoundP2 = p2.playerListIdx;
        const lastRound = this.rounds[currentRound - 1];
        const lastRoundMatches = lastRound.matches;
        for (let m = 0; m < lastRoundMatches.length; m++) {
            const lastRoundMatch = lastRoundMatches[m];
            const lastRoundP1 = lastRoundMatch.player1.playerListIdx;
            const lastRoundP2 = lastRoundMatch.player2.playerListIdx;
            if ((lastRoundP1 === thisRoundP1 && lastRoundP2 === thisRoundP2) || (lastRoundP1 === thisRoundP2 && lastRoundP2 === thisRoundP1)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Could state that a draw should be avoided, e.g. if the same players would
     * play against each other again.
     * Normally just return false (everything is valid).
     */
    isException(roundNr: number, players: PlayerRef[], from: number, to: number): boolean {
        if (!roundNr) {
            return false;
        }
        for (let p = from; p < to; p += 2) {
            if (this.havePlayedLastRound(roundNr, players[p], players[p + 1])) {
                return true;
            }
        }
        return false;
    }

    // getGroupOfPlayerNr(playerNr: number): number {
    //     const rest = this.allPlayers.length % 6;
    //     const n = this.allPlayers.length / 6;
    //     if (rest === 0) {
    //         // groups of 6
    //         return playerNr / 6;
    //     }
    //     if (rest === 2 || rest === 4) {
    //         // rest 2: (n-1)*6, 4, 4
    //         // rest 4: (n-1)*6, 4, 6
    //         if (playerNr < (n - 1) * 6) {
    //             return playerNr / 6;
    //         }
    //         if (playerNr < (n - 1) * 6 + 4) {
    //             return n - 1;
    //         }
    //         return n;
    //     }
    //     // should not happen since this.allPlayers.length is even
    //     return -1;
    // }

    /**
     * Groups of 6. If the number is not a multiple of 6, first (or first and second) will be 4.
     * @param nrPlayers must be an even integer >= 6
     * @returns array of group sizes, e.g. [4, 6, 6] for 16 players; [] if invalid input
     *
     * Examples:
     * 6: [6], 8: [4, 4], 10: [4, 6], 12: [6, 6]
     * 14: [4, 4, 6], 16: [4, 6, 6], 18: [6, 6, 6]
     * 50: [4, 4, 6, 6, 6, 6, 6, 6, 6]
     */
    getGroupSizes(nrPlayers: number): number[] {
        if (nrPlayers % 2 !== 0 || nrPlayers < 6 || Math.floor(nrPlayers) !== nrPlayers) {
            return [];
        }
        const rest = nrPlayers % 6;
        const n = Math.floor(nrPlayers / 6);
        if (rest === 0) {
            // groups of 6
            const arr = new Array(n);
            for (let i = 0; i < n; i++) {
                arr[i] = 6;
            }
            return arr;
        }
        if (rest === 2 || rest === 4) {
            // rest 2: 4, 4, (n-1)*6
            // rest 4: 4, 6, (n-1)*6
            const arr = new Array(n + 1);
            for (let i = 2; i < n + 1; i++) {
                arr[i] = 6;
            }
            arr[0] = 4;
            arr[1] = 2 + rest;
            return arr;
        }
    }

    setupRound(roundNr: number) {
        const roundPlayers = this.rounds[roundNr].sortedPlayers;
        if (roundPlayers.length) {
            if (roundNr === 0) {
                // for very first round, randomize all
                this.shuffleArray(roundPlayers, 0, roundPlayers.length - 1);
            } else {
                // randomize those within the same group
                const groupSizes = this.getGroupSizes(this.allPlayers.length);
                let cnt = 0;
                for (let g = 0; g < groupSizes.length; g++) {
                    const groupSize = groupSizes[g];
                    const maxTries = 100;
                    let tryNr = 0;
                    do {
                        this.shuffleArray(roundPlayers, cnt, cnt + groupSize - 1);
                    } while (tryNr++ < maxTries && this.isException(roundNr, roundPlayers, cnt, cnt + groupSize - 1));
                    cnt += groupSize;
                }
            }

            // create list of matches
            for (let p = 0; p < roundPlayers.length; p += 2) {
                const p1Ref = new PlayerRef();
                p1Ref.playerListIdx = roundPlayers[p].playerListIdx;
                const p2Ref = new PlayerRef();
                p2Ref.playerListIdx = roundPlayers[p + 1].playerListIdx;
                const match = new Match(p1Ref, p2Ref, {level: roundNr.toString(), num: Math.floor(p / 2), ko_type: 'M'});
                match.score = [{p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}];
                this.rounds[roundNr].matches[Math.floor(p / 2)] = match;
            }
        } else {
            this.alertService.error($localize`No players for this round #${roundNr}:roundNr:`);
        }
    }

    addRound() {
        if (this.newFrom && this.newTo && (this.newTo > this.newFrom)) {
            if (this.playedCurrentRound.length !== this.allPlayers.length) {
                const dialogRef = this.dialog.open(YesNoDialogComponent, {
                    data: { text: $localize`Are you sure? There are UNFINISHED matches!!` }
                });

                dialogRef.afterClosed().subscribe(result => {
                    if (result === true) {
                        this.doAddRound();
                    }
                });
            } else {
                this.doAddRound();
            }
        }
    }

    doAddRound() {
        const round = new Round(this.newFrom.format('DD.MM.YYYY'), this.newTo.format('DD.MM.YYYY'));
        this.rounds.push(round);
        this.isdraw.push([]);

        const roundPlayers = [];
        for (let p = 0; p < this.allPlayers.length; p++) {
            const pRef = new PlayerRef();
            pRef.playerListIdx = p;
            roundPlayers[p] = pRef;
        }
        this.sortPlayerRefs(roundPlayers);
        round.sortedPlayers = roundPlayers;

        this.setupRound(this.rounds.length - 1);

        this.newFrom = this.newTo.clone().add(1, 'days');
        this.newTo = this.newFrom.clone().add(this.NUMBER_OF_DAYS, 'days');

        this.updatePlayerPoints();
        this.storeToDB();
        this.updateEmailTemplate();
        this.allPlayersChange.emit(this.allPlayers);
    }

    removeRound(rIdx: number) {
        if (rIdx >= 0 && rIdx < this.rounds.length) {
            this.rounds.splice(rIdx, 1);
            this.storeToDB();
            this.updatePlayerPoints();
            this.newTo = this.newFrom.clone().subtract(1, 'days');
            this.newFrom = this.newTo.clone().subtract(this.NUMBER_OF_DAYS, 'days');
            this.updateEmailTemplate();
        }
    }

    // helper to randomize assignment, adapted from
    // http://stackoverflow.com/quesions/2450954/how-to-randomize-shuffle-a-javascript-array

    private shuffleArray(array: Array<any>, from: number, to: number) {
        for (let i = to; i > from; i--) {
            const j = from + Math.floor(Math.random() * (i - from));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    calculateSum(match: Match): {p1: number, p2: number} {
        const wins = {p1: 0, p2: 0};
        Match.checkWinnerSetWins(match.score[0], wins);
        Match.checkWinnerSetWins(match.score[1], wins);
        if (match.score[2]) {
            Match.checkWinnerSetWins(match.score[2], wins);
        }
        if (match.score[3]) {
            Match.checkWinnerSetWins(match.score[3], wins);
        }
        if (match.score[4]) {
            Match.checkWinnerSetWins(match.score[4], wins);
        }
        return wins;
    }

    isFinished(match: Match, c, m): boolean {
        return this.isdraw[c][m] || Match.checkWinner(match) !== 0;
    }

    isAdmin(): boolean {
        return (this.myRoles & Role.ROLE_PAUL) > 0;
    }

    canSendEmail(): boolean {
        // DCV is allowed

        // tslint:disable-next-line: no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            return true;
        }
        // organiser is allowed

        if (this.myPlayerId && this.tournament?.responsibles?.indexOf(this.myPlayerId) >= 0) {
            return true;
        }
        return false;
    }

    sendEmailToAll() {
        this.logger.debug('sending email to all:', this.emailToAll);
        // some delay between email send calls
        let scheduleMS = 500;
        for (let p = 0; p < this.allPlayers.length; p++) {
            const player = this.allPlayers[p];
            try {
                this.userService.getByPlayerId(player._id).subscribe(
                    response => {
                        if (response.success === true) {
                            if (response.result && response.result.email) {
                                setTimeout(() =>
                                    this.sendEmail(this.emailToAll, response.result.email, this.subject),
                                    scheduleMS,
                                );
                                scheduleMS += 500;
                            }
                        } else {
                            this.logger.error('could not get email for player', player.first_name, player.last_name, response.error);
                            this.alertService.error($localize`could not get email for player ${player.first_name}:firstName: ${player.last_name}:lastName:`, response.error);
                        }
                    },
                    error => {
                        this.logger.error('could not get email for player', player.first_name, player.last_name, error);
                        this.alertService.error($localize`could not get email for player ${player.first_name}:firstName: ${player.last_name}:lastName:`, error);
                    }
                );
            } catch (err) {
                this.logger.error('could not get email for player', player.first_name, player.last_name, err);
                this.alertService.error($localize`could not get email for player ${player.first_name}:firstName: ${player.last_name}:lastName:`, err);
            }
        }
    }

    sendEmail(content, email, subject) {
        this.userService.sendEmail(content, email, subject).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('successfully sent email to', response.result);
                    this.alertService.success($localize`Successfully sent email`);
                } else {
                    this.logger.error('could not send email to', email, response.error);
                    this.alertService.error($localize`could not send email to ${email}:email:`, response.error);
                }
            },
            error => {
                this.logger.error('could not send email to', email, error);
                this.alertService.error($localize`could not send email to ${email}:email:`, error);
            }
        );
    }

    getReadOnly(match: Match) {
        // DCV is allowed
        // tslint:disable-next-line:no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            return false;
        }
        // BYE cannot be changed

        if (match.player2.bye === true) {
            return true;
        }
        // organiser is allowed

        if (this.myPlayerId && this.tournament && this.tournament.responsibles && this.tournament.responsibles.indexOf(this.myPlayerId) >= 0) {
            return false;
        }
        // allowed if user is one of the players

        return this.allPlayers[match.player1.playerListIdx]._id !== this.myPlayerId &&
        this.allPlayers[match.player2.playerListIdx]._id !== this.myPlayerId;
    }

    cannotAddRounds(): boolean {
        // have to allow creating the next round before all games are
        // played since some might be 0:0
        // if (this.playedCurrentRound.length != this.allPlayers.length) {
        //     return true;
        // }

        // DCV is allowed

        // tslint:disable-next-line: no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            return false;
        }
        // organiser is allowed

        if (this.myPlayerId && this.tournament?.responsibles?.indexOf(this.myPlayerId) >= 0) {
            return false;
        }
        return true;
    }

    allZeroes(match: Match): boolean {
        for (let s = 0; s < match.score.length; s++) {
            if (match.score[s].p1 !== 0 || match.score[s].p2 !== 0) {
                return false;
            }
        }
        return true;
    }
}
