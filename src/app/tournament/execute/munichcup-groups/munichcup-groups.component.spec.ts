import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MunichcupGroupsComponent } from './munichcup-groups.component';

describe('MunichcupComponent', () => {
  let component: MunichcupGroupsComponent;
  let fixture: ComponentFixture<MunichcupGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MunichcupGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MunichcupGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
