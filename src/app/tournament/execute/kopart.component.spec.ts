import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KopartComponent } from './kopart.component';

describe('KopartComponent', () => {
  let component: KopartComponent;
  let fixture: ComponentFixture<KopartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KopartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KopartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
