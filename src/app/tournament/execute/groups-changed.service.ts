import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class GroupsChangedService {
    private statusSubject = new BehaviorSubject<{ groupNr: number, categoryId: string }>({ groupNr: -1, categoryId: '' });
    groupChangeStatus = this.statusSubject.asObservable();

    groupsChange(groupNr: number, categoryId: string) {
        // https://blog.angularindepth.com/
        // everything-you-need-to-know-about-the-expressionchangedafterithasbeencheckederror-error-e3fd9ce7dbb4
        setTimeout(() => {
            this.statusSubject.next({ groupNr, categoryId });
        });
    }
}
