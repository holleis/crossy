import { Component, OnInit } from '@angular/core';
import { TournamentsService } from './tournaments.service';
import { Tournament } from './tournament';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../util/alert/alert.service';
import { UserService } from '../auth/_services/user.service';

@Component({
    selector: 'app-show',
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.css'],
})
export class ShowComponent implements OnInit {

    constructor(
        private tournamentsService: TournamentsService,
        private router: Router,
        private route: ActivatedRoute,
        private logger: NGXLogger,
        private alertService: AlertService,
        private userService: UserService,
    ) { }

    tournamentId: string;
    tournament: Tournament;
    announcement: string;
    drawExists = false;

    ngOnInit() {
        // see if we received an id through the URL
        this.drawExists = false;
        this.tournamentId = this.route.snapshot.params.id;
        if (this.tournamentId) {
            this.tournamentsService.getTournament(this.tournamentId).subscribe(response => {
                this.logger.debug('retrieving tournament', this.tournamentId);
                try {
                    if (response.success === true) {
                        this.tournament = response.result;
                        this.announcement = this.tournamentsService.createAnnouncement(this.tournament, this.tournament.details);
                        this.logger.trace('successfully retrieved', this.tournament, 'created', this.announcement);

                        if (this.tournamentId && this.tournament && this.tournament.categories && this.tournament.categories.length > 0) {
                            this.tournamentsService.getDrawnCategories(this.tournamentId).subscribe(
                                response => {
                                    this.logger.debug('retrieving tournament', this.tournamentId);
                                    if (response.success === true) {
                                        this.drawExists = response.result && response.result.catIds && response.result.catIds.length > 0;
                                    } else {
                                        this.logger.error('error retrieving tournament', response.error);
                                    }
                                },
                                error => {
                                    if (error.status === 404) {
                                        // ok, we don't have a draw yet
                                    } else {
                                        this.alertService.error($localize`Could not retrieve tournament`, response.error);
                                        this.logger.error('error retrieving draws for tournament', response.error);
                                    }
                                }
                            );
                        }
                    } else {
                        this.logger.error('error retrieving tournament', response.error);
                        this.alertService.error($localize`Could not retrieve tournament`, response.error);
                    }
                } catch (err) {
                    this.logger.error('error retrieving tournament', err);
                    this.alertService.error($localize`Could not retrieve tournament`, err);
                }
            });
        } else {
            this.router.navigate(['/calendar']);
        }
    }

    goToEnroll(tournament: Tournament) {
        this.router.navigate(['/enroll', { id: tournament._id }]);
    }
    goToCalendar() {
        this.router.navigate(['/calendar']);
    }

    goHome() {
        if (this.userService.getCurrentUser()) {
            this.router.navigate(['/home']);
        } else {
            this.router.navigate(['/overview']);
        }
    }

    goToDraw(tournament: Tournament) {
        this.router.navigate(['/ko', { id: tournament._id }]);
    }
}
