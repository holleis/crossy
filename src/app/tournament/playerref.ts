import { Ko } from './ko';
import { Group } from './group';
import { NGXLogger } from 'ngx-logger';

export class PlayerRef {

    playerListIdx: number;
    groupIdx: {groupNr: number, winnerNr: number};
    // ko_type is 'A' or 'B' (or 'G' for group)
    koIdx: {level: string, num: number, ko_type: string};
    bye = false;

    // for calculating orders in groups etc.
    calcPoints = 0;

    static isBye(playerRef: PlayerRef) {
        if (typeof playerRef.playerListIdx === 'undefined') {
            // walk through until the last reference to an actual player or BYE
            if (playerRef.bye) {
                return true;
            }

            if (typeof playerRef.groupIdx !== 'undefined') {
                // index into groups
                return false;

            } else if (typeof playerRef.koIdx !== 'undefined') {
                // index into ko, look there
                // if (!ko) {
                //     logger.error('no KO object for recursive search in PlayerRef.isBye!');
                //     // TODO what to do?!
                //     return false;
                // }
                // playerRef = ko.matches[playerRef.koIdx.level][playerRef.koIdx.num].winner;

                // this is a second+ level entry; a BYE cannot propagate to 2nd level or beyond
                return false;

            } else {
                // no reference to actual player found, using BYEs
                return true;
            }
        }

        return false;
    }

    static update(logger: NGXLogger, playerRefToUpdate: PlayerRef, playerRef: PlayerRef, ko?: Ko, groups?: Group[], recurse = true) {
        if (!recurse) {
            playerRefToUpdate.playerListIdx = playerRef.playerListIdx;
            playerRefToUpdate.groupIdx = playerRef.groupIdx;
            playerRefToUpdate.koIdx = playerRef.koIdx;
            playerRefToUpdate.bye = playerRef.bye;
            playerRefToUpdate.calcPoints = playerRef.calcPoints;
            return;
        }

        while (typeof playerRef.playerListIdx === 'undefined') {
            // walk through until the last reference to an actual player or BYE
            if (typeof playerRef.groupIdx !== 'undefined') {
                if (playerRef.groupIdx.groupNr === -1) {
                    // KO with no group phase
                    playerRef.playerListIdx = playerRef.groupIdx.winnerNr;
                    playerRef.groupIdx = undefined;
                } else if (typeof groups === 'undefined') {
                    logger.error('error: unexpected player reference to group within group, fall back to BYE');
                    playerRef.groupIdx = undefined;
                    playerRef.bye = true;
                } else {
                    // index into groups, look there
                    playerRef = groups[playerRef.groupIdx.groupNr].winners[playerRef.groupIdx.winnerNr];
                }

            } else if (typeof playerRef.koIdx !== 'undefined') {
                if (typeof ko === 'undefined') {
                    logger.error('error: unexpected player reference to ko within group, fall back to BYE');
                    playerRef.groupIdx = undefined;
                    playerRef.bye = true;
                } else {
                    // index into ko, look there
                    playerRef = ko.matches[playerRef.koIdx.level][playerRef.koIdx.num].winner;
                }

            } else if (playerRef.bye) {
                playerRefToUpdate.playerListIdx = undefined;
                playerRefToUpdate.groupIdx = undefined;
                playerRefToUpdate.koIdx = undefined;
                playerRefToUpdate.bye = true;
                return;

            } else {
                // no reference to actual player found, using BYEs
                // logger.debug('error: backtracking to actual player failed, last entry:', playerRef);
                playerRef = new PlayerRef();
                playerRef.bye = true;
            }
        }

        playerRefToUpdate.calcPoints = playerRef.calcPoints;
        playerRefToUpdate.playerListIdx = playerRef.playerListIdx;
        playerRefToUpdate.groupIdx = undefined;
        playerRefToUpdate.koIdx = undefined;
        playerRefToUpdate.bye = false;
    }


    static getPlayerListIdx(logger: NGXLogger, playerRef: PlayerRef, ko?: Ko, groups?: Group[]): -1 | undefined | number {
        while (typeof playerRef.playerListIdx === 'undefined') {
            // walk through until the last reference to an actual player or BYE
            if (typeof playerRef.groupIdx !== 'undefined') {
                if (playerRef.groupIdx.groupNr === -1) {
                    // KO with no group phase
                    playerRef.playerListIdx = playerRef.groupIdx.winnerNr;
                    playerRef.groupIdx = undefined;
                } else if (typeof groups === 'undefined') {
                    logger.error('error: unexpected player reference to group within group');
                    return undefined;
                } else {
                    // index into groups, look there
                    playerRef = groups[playerRef.groupIdx.groupNr].winners[playerRef.groupIdx.winnerNr];
                }

            } else if (typeof playerRef.koIdx !== 'undefined') {
                if (typeof ko === 'undefined') {
                    logger.error('error: unexpected player reference to ko within group');
                    return undefined;
                } else {
                    // index into ko, look there
                    playerRef = ko.matches[playerRef.koIdx.level][playerRef.koIdx.num].winner;
                }

            } else if (playerRef.bye) {
                return -1;

            } else {
                // no reference to actual player found
                // logger.debug('error: backtracking to actual player failed, last entry:', playerRef);
                return undefined;
            }
        }

        if (playerRef.bye) {
            return -1;
        }

        return playerRef.playerListIdx;
    }
}
