import { PlayerRef } from './playerref';
import { Match } from './match';
import { NGXLogger } from 'ngx-logger';

export class Group {

    static readonly gamePts =    100000;
    static readonly setsWonPts =  10000;
    static readonly setsLostPts = -1000;
    static readonly ptsRelPts =      10;
    static readonly ptsWonPts =       0.01;
    static readonly ptsLostPts =     -0.00001;
    static readonly rankingPts =     -0.00000001;

    name = '';
    groupSize = 4;
    // seed order, could be doubles
    players: PlayerRef[] = [];
    matches: Match[] = [];
    // winning order; don't set new objects - references are used in KO
    winners: PlayerRef[] = [];

    playerStats: {gamesWon: number, gamesLost: number, setsWon: number, setsLost: number, pointsWon: number, pointsLost: number, seed: number}[] = [];
    possibleRanks: number[][] = [];

    constructor(gSize: number, gName: string) {
        this.groupSize = gSize;
        this.name = gName;

        // initialize winners; don't set new objects - references are used in KO
        for (let i = 0; i < this.groupSize; i++) {
            this.winners.push(new PlayerRef());
        }

        // // TODO check when to use group
        // Group.updateWinners(this.logger, );
    }

    static updateWinners(logger: NGXLogger, group: Group) {
        Group.calculateCalcPoints(group);

        // Group.calculateSures(group);

        // sort according to calcPoints
        // can't change players array; need to use 'update' function on winners array
        const sortedPlayers = group.players.slice();
        sortedPlayers.sort(function(p1: PlayerRef, p2: PlayerRef) {
            // inverse according to points (i.e. descending)
            return p2.calcPoints - p1.calcPoints;
        });
        for (let i = 0; i < sortedPlayers.length; i++) {
            // don't just replace winners with new array! references are needed
            // update function doesn't need ko and groups since players always come from players list
            PlayerRef.update(logger, group.winners[i], sortedPlayers[i]);
        }
        // this.logger.debug('update group winners', group.winners);
    }

    static calculateCalcPointsMatch(match: Match) {
        let winner = Match.checkWinner(match);
        // 1st: match wins
        if (winner === -1) {
            match.player1.calcPoints += Group.gamePts;
        } else if (winner === 1) {
            match.player2.calcPoints += Group.gamePts;
        }
        let p1PtsWon = 0;
        let p1PtsLost = 0;
        let p2PtsWon = 0;
        let p2PtsLost = 0;
        for (let s = 0; s < match.score.length; s++) {
            const set = match.score[s];
            winner = Match.checkWinnerSet(set);
            // 2nd: set wins
            // 3rd: set losses
            if (winner === -1) {
                match.player1.calcPoints += Group.setsWonPts;
                match.player2.calcPoints += Group.setsLostPts;
            } else if (winner === 1) {
                match.player2.calcPoints += Group.setsWonPts;
                match.player1.calcPoints += Group.setsLostPts;
            }
            p1PtsWon += set.p1;
            p1PtsLost += set.p2;
            p2PtsWon += set.p2;
            p2PtsLost += set.p1;
        }
        // 4th: relative points
        // 5th: point wins
        // 6th: point losses
        match.player1.calcPoints += (p1PtsWon - p1PtsLost) * Group.ptsRelPts;
        match.player1.calcPoints += p1PtsWon * Group.ptsWonPts;
        match.player1.calcPoints += p1PtsLost * Group.ptsLostPts;
        match.player2.calcPoints += (p2PtsWon - p2PtsLost) * Group.ptsRelPts;
        match.player2.calcPoints += p2PtsWon * Group.ptsWonPts;
        match.player2.calcPoints += p2PtsLost * Group.ptsLostPts;
    }

    private static calculateCalcPoints(group: Group) {
        for (let i = 0; i < group.players.length; i++) {
            // 7th: seed ranking position
            group.players[i].calcPoints = (group.players[i].playerListIdx + 1) * Group.rankingPts;
        }

        // weigh each match
        for (let i = 0; i < group.matches.length; i++) {
            const match = group.matches[i];
            this.calculateCalcPointsMatch(match);
        }
    }

    // private static calculateSures(group: Group) {
    //     Group.calculateStats(this.logger, (group);

    //     // not played matches
    //     const unknownMatches = [];
    //     for (let m = 0; m < group.matches.length; m++) {
    //         if (Match.checkWinner(group.matches[m]) === 0) {
    //             unknownMatches.push(m);
    //         }
    //     }
    // }

    static calculateStats(logger: NGXLogger, group: Group) {
        for (let i = 0; i < group.players.length; i++) {
            if (!group.playerStats) {
                group.playerStats = [];
            }
            group.playerStats[PlayerRef.getPlayerListIdx(logger, group.players[i])] = {seed: group.players[i].playerListIdx + 1, gamesWon: 0, gamesLost: 0, setsWon: 0, setsLost: 0, pointsWon: 0, pointsLost: 0};
        }

        for (let i = 0; i < group.matches.length; i++) {
            const match = group.matches[i];

            const p1 = group.playerStats[PlayerRef.getPlayerListIdx(logger, match.player1)];
            const p2 = group.playerStats[PlayerRef.getPlayerListIdx(logger, match.player2)];

            let winner = Match.checkWinner(match);
            // 1st: match wins
            if (winner === -1) {
                p1.gamesWon++;
                p2.gamesLost++;
            } else if (winner === 1) {
                p2.gamesWon++;
                p1.gamesLost++;
            }
            for (let s = 0; s < match.score.length; s++) {
                const set = match.score[s];
                winner = Match.checkWinnerSet(set);
                if (winner === -1) {
                    p1.setsWon++;
                    p2.setsLost++;
                } else if (winner === 1) {
                    p2.setsWon++;
                    p1.setsLost++;
                }
                p1.pointsWon += set.p1;
                p1.pointsLost += set.p2;
                p2.pointsWon += set.p2;
                p2.pointsLost += set.p1;
            }
        }
    }
}
