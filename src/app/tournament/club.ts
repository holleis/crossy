export class Club {

    _id: string;
    name = '';
    location = '';
    abbreviation = '';
    nation = '';
    region = '';
    city = '';
    established = '';
    responsible = '';
    active = true;
    links = [];
    comments = '';
    logos = [];
    postal_address = '';
    email = '';
    bank_account = '';
    tsid = '';
}
