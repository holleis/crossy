import { Player } from "../player/player";

export interface EnrollmentInfo {
    _id: string;
    tournamentName: string;
    tournament_type: string;
    catName: string;
    cat_type: string;
    gender: number;
    minimum_age: number;
    maximum_age: number;
    myPlayerFits: boolean;
    players: Player[];
}
