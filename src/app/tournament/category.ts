export class Category {

    _id: string;
    federation = ''; // DCV
    name = ''; // DCV Damen
    cat_type = ''; // singles / doubles
    gender = 0; // open=0, female=1, male=2, mixed=3
    minimum_age = 0; // e.g. 40 for O40
    maximum_age = 999; // e.g. 18 for U18
    active = true; // used in tournaments
}
