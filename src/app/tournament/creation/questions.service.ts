import { Injectable } from '@angular/core';
import { Club } from '../club';
import { VariableToText } from '../../util/variabletotext';
import { Tournament } from '../tournament';
import { ClubService } from '../../club/club.service';
import { PlayerService } from '../../player/player.service';
import { CategoriesService } from '../../ranking/categories.service';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { AlertService } from '../../util/alert/alert.service';

@Injectable()
export class QuestionsService {

    constructor(
        private clubService: ClubService,
        private categoriesService: CategoriesService,
        private playerService: PlayerService,
        private alertService: AlertService,
        private logger: NGXLogger
    ) { }


    createQuestion(label: string, type: string, example: any, value?: any)
            : {label: string, type: string, example: any, value: any} {
        this.logger.trace('       create question for', label, type, example, value);
        if (type === undefined) {
            // this.logger.debug('looking at', label, type, example);
            // try to find best match

            const dateRegexp = RegExp('^\\d\\d\.\\d\\d\.\\d\\d\\d\\d$');
            if (example instanceof Date || dateRegexp.test(example.toString())) {
                this.logger.trace('   date');
                type = 'date';
                // TODO check
                // example = example.getDate() + '.' + example.getMonth() + 1 + '.' + example.getFullYear();
            } else if (example === false || example === true) {
                this.logger.trace('   bool');
                type = 'checkbox';
            } else if (parseInt(example, 10) === example) {
                this.logger.trace('   int');
                type = 'number';
            } else {
                type = 'text';
            }
        }

        label = VariableToText.tr(label);
        // label = this.tr.anslate(label);
        return {label: label, type: type, example: example, value: value};
    }

    // getQuestions(): {label: string, type: string, example: any}[] {
    //     return [{label: 'title', type: 'text', example: 'Moon Open'},
    //             {label: 'sponsor', type: 'text', example: 'Speedminton'},
    //             {label: 'start_date', type: 'date', example: '2018-02-18'}];
    // }

    getQuestions(values?: Tournament, organiserClubId?: string): {options: any, questions: {label: string, type: string, example: any}[]} {
        const tournament = new Tournament();
        const questions = [];
        const options = {};

        for (const prop in tournament) {
            if (prop === '_id' || prop === 'event_type' || prop === 'links' || prop === 'details' || prop.indexOf('___link') >= 0) {
                continue;
            }
            if (tournament.hasOwnProperty(prop)) {
                const val = tournament[prop];
                // special treatment for object properties
                let newQ;
                if (typeof val === 'object' && !(val instanceof Date)) {
                    if (prop === 'organiser') {
                        // special treatment of organiser Club within Tournament type ...
                        let value;
                        if (values && values.organiser) {
                            if (values.organiser._id) {
                                value = values.organiser._id;
                            } else {
                                value = values.organiser;
                            }
                        }
                        newQ = this.createQuestion('organiser', 'dropdown', this.clubService.getClubNames(undefined), value);
                        questions.push(newQ);
                    } else if (prop === 'categories') {
                        // special treatment of Categories[] within Tournament type ...

                        let ttypeAsFederation: string;
                        if (values.tournament_type === 'DCV' || values.tournament_type === 'ICO') {
                            // values.categories.filter((cat) => cat.federation === values.federation);
                            ttypeAsFederation = values.tournament_type;
                        }
                        let catIds;
                        if (values && values.categories) {
                            catIds = values.categories.map((cat) => (cat._id ? cat._id : cat));
                        }
                        newQ = this.createQuestion('categories', 'checkboxes', this.categoriesService.getCategoryNames(ttypeAsFederation), catIds || undefined);
                        questions.push(newQ);
                    } else if (prop === 'responsibles') {
                        // special treatment of responsibles (Player[]) within Tournament type ...

                        let value;
                        if (values && values.responsibles) {
                            value = values.responsibles;
                        }
                        newQ = this.createQuestion('responsibles', 'checkboxes',
                            this.playerService.getPlayersForClubs([organiserClubId]), value);
                        questions.push(newQ);

                    } else {
                        for (const tournamentprop in val) {
                            if (val.hasOwnProperty(tournamentprop)) {
                                const propval = val[tournamentprop];
                                const value = (values && typeof values[tournamentprop] !== 'undefined') ?
                                    values[tournamentprop] : undefined;
                                questions.push(this.createQuestion(tournamentprop, undefined, propval, value));
                            }
                        }
                    }
                } else if (prop === 'nation') {
                    newQ = this.createQuestion('nation', 'dropdown', this.playerService.getNations(),
                        (values && values.nation) ? values.nation : undefined);
                    questions.push(newQ);
                } else if (prop === 'federation') {
                    newQ = this.createQuestion('federation', 'dropdown', this.playerService.getFederations(),
                        (values && values.federation) ? values.federation : undefined)
                    questions.push(newQ);
                } else if (prop === 'tournament_type') {
                    newQ = this.createQuestion('tournament_type', 'dropdown', ['DCV', 'ICO', 'MunichCup', 'MunichCup (Gruppen)'],
                        (values && values.tournament_type) ? values.tournament_type : undefined)
                    questions.push(newQ);
                } else {
                    const value = (values && typeof values[prop] !== 'undefined') ? values[prop] : undefined;
                    newQ = this.createQuestion(prop, undefined, val, value);
                    if (newQ) {
                        questions.push(newQ);
                    }
                }
                if (newQ && tournament.hasOwnProperty(prop + '___link')) {
                    newQ.link = tournament[prop + '___link'];
                }
            }
        }

        // TODO optimize: no need to have 2 phases (above and below) any more

        const required = ['Organiser', 'Title', 'Start Date', 'Location', 'Nation', 'Federation', 'Points', 'Tournament Type'];
        // this.logger.debug('the questions', questions);

        for (let q = 0; q < questions.length; q++) {
            if (questions[q].example instanceof Observable) {
                this.logger.trace('getting data for question ', questions[q].label, 'of type', questions[q].type);
                questions[q].example.subscribe(
                    response => {
                        if (response.success === true) {
                            if (questions[q].type === 'dropdown' || questions[q].type === 'checkboxes') {
                                const data = response.result;
                                const names = [];
                                if (questions[q].label === VariableToText.tr('organiser')) {
                                    data.sort((c1: Club, c2: Club) => {
                                        if (c1.nation && c1.nation === 'GER') {
                                            if (c2.nation && c2.nation === 'GER') {
                                                return c1.name.toUpperCase() < c2.name.toUpperCase() ? -1 :
                                                    (c1.name.toUpperCase() > c2.name.toUpperCase() ? 1 : 0);
                                            }
                                            return -1;
                                        }
                                        if (c2.nation && c2.nation === 'GER') {
                                            return 1;
                                        }
                                        return c1.name.toUpperCase() < c2.name.toUpperCase() ? -1 :
                                            (c1.name.toUpperCase() > c2.name.toUpperCase() ? 1 : 0);
                                    });
                                }
                                if (questions[q].label === VariableToText.tr('responsibles')) {
                                    for (let i = 0; i < data.length; i++) {
                                        names.push({_id: data[i]._id, name: data[i].first_name + ' ' + data[i].last_name});
                                    }
                                } else {
                                    for (let i = 0; i < data.length; i++) {
                                        names.push({_id: data[i]._id, name: data[i].name});
                                    }
                                }
                                // store options for dropdown in a separate array for the html template
                                options[questions[q].label] = names;
                                this.logger.trace('store for ', questions[q].label, ':', names);
                                // this.logger.debug('stored for', questions[q].label, 'options', names);

                            } else {
                                this.logger.error('dont know what to do with Observable question with type',
                                questions[q].type);
                            }
                        } else {
                            this.logger.error('error getting data from ', questions[q], response.error);
                            this.alertService.error($localize`Error getting data for ${response.error}:label:`, response.error);
                        }
                    },
                    error => {
                        this.logger.error('error getting data from ', questions[q], error);
                        this.alertService.error($localize`Error getting data for ${questions[q].label}:label:`, error);
                    }
                );
            } else if (questions[q].type === 'dropdown') {
                const names = [];
                for (let i = 0; i < questions[q].example.length; i++) {
                    names.push({_id: questions[q].example[i], name: questions[q].example[i]});
                }
                // store options for dropdown in a separate array for the html template
                options[questions[q].label] = names;
            }

            // set required flag
            questions[q].required = false;
            if (required.indexOf(questions[q].label) !== -1) {
                questions[q].required = true;
                // messes up the automatic question generation ...
                // if (questions[q].type === 'date') {
                //     questions[q].label = questions[q].label + ' *';
                // }
            }
        }

        return {options: options, questions: questions};
    }
}
