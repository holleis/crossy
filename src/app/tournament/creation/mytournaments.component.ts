import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MytournamentsService } from './mytournaments.service';
import { VariableToText } from '../../util/variabletotext';
import { Tournament } from '../tournament';
import { AlertService } from '../../util/alert/alert.service';
import { MatDialog } from '@angular/material/dialog';
import { YesNoDialogComponent } from '../../util/yesno-dialog.component';
import { TSImportDialogComponent } from '../../util/ts-import-dialog.component';
import { ClubService } from '../../club/club.service';
import { NGXLogger } from 'ngx-logger';
import { PlayerService } from '../../player/player.service';
import { QuestionsService } from './questions.service';
import { CategoriesService } from '../../ranking/categories.service';
import { CreationService } from './creation.service';
import { TournamentsService } from '../tournaments.service';
import { UserService } from '../../auth/_services/user.service';
import { Role } from '../../user/role';
import { Title } from '@angular/platform-browser';

import * as moment from 'moment';
import * as QuillNamespace from 'quill';
const Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
import { Router } from '@angular/router';
import { MatchdbService } from '../execute/matchdb.service';
import { RankingService } from '../../ranking/ranking.service';
// import * as ImageResizeNS from 'quill-image-resize-module';
// let ImageResize: any = ImageResizeNS;
Quill.register('modules/imageResize', ImageResize);


@Component({
    selector: 'app-mytournaments',
    templateUrl: './mytournaments.component.html',
    styleUrls: ['./mytournaments.component.scss'],
    providers: [MytournamentsService, CategoriesService, PlayerService,
        QuestionsService, ClubService, CreationService, MatchdbService]
})

export class MytournamentsComponent implements OnInit {

    mytournaments: Tournament[];

    editTournament: Tournament;
    announcement: string;
    details: string;
    questions = [];
    options = {};
    myClubId: string;
    myRoles = 0;
    myPlayer: string;

    quillOptions =  {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            // ['blockquote', 'code-block'],

            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            // [{ 'direction': 'rtl' }],                         // text direction

            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],

            ['clean'],                                         // remove formatting button

            ['link', 'image']                         // link and image, video
        ],
        imageResize: {}
    };

    busy = -1;

    constructor(
        private myTournamentService: MytournamentsService,
        private tournamentsService: TournamentsService,
        private alertService: AlertService,
        public dialog: MatDialog,
        private clubService: ClubService,
        private questionsService: QuestionsService,
        private creationService: CreationService,
        private logger: NGXLogger,
        private matchdbService: MatchdbService,
        private router: Router,
        private userService: UserService,
        private titleService: Title,
        private rankingService: RankingService,
    ) { }

    @ViewChild('topElem') topElem: ElementRef;

    
    ngOnInit() {
        this.titleService.setTitle('Crossy');
        const user = this.userService.getCurrentUser();
        if (user) {
            this.myRoles = user.roles;
            this.myPlayer = user.player_id;
            // tslint:disable-next-line: no-bitwise
            if ((user.roles & Role.ROLE_PAUL) > 0 || (user.roles & Role.ROLE_DCV) > 0) {
                this.myClubId = '*';
                this.getMyTournaments(this.myClubId, true);
            } else {
                this.clubService.getMainClubIdForPlayer(user.player_id).subscribe(
                    response => {
                        if (response.success === true) {
                            this.myClubId = response.result;
                            this.getMyTournaments(this.myClubId);
                        } else {
                            this.logger.debug('error retrieving club', response.error);
                            this.alertService.error($localize`Error retrieving your club`, response.error);
                        }
                    },
                    error => {
                        this.logger.error('error retrieving club', error);
                        this.alertService.error($localize`Error retrieving your club`, error);
                    }
                );
            }
        }
    }

    getMyTournaments(clubId: string, all = false) {
        this.logger.debug('asking for tournaments for', clubId);
        this.myTournamentService.getMyTournaments(clubId, all).subscribe(
            response => {
                if (response.success) {
                    response.result.sort((t1, t2) => t1.start_date_epoch > t2.start_date_epoch ? -1 : 1);
                    this.mytournaments = response.result;
                    this.logger.debug('received', this.mytournaments.length, 'tournaments');
                    this.logger.trace('received my tournaments', this.mytournaments);
                } else {
                    this.logger.error('error retrieving tournaments', response.error);
                    this.alertService.error($localize`Error retrieving tournaments `, response.error);
                }
                this.topElem.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
            },
            error => {
                this.logger.error('error retrieving tournaments', error);
                this.alertService.error($localize`Error retrieving tournaments `, error);
            }
        );
    }

    reloadResponsibles(org: string, questions?): void {
        // reload 'responsibles' question values
        const q = questions || this.questionsService.getQuestions(new Tournament(), org);
        for (let i = 0; i < q.questions.length; i++) {
            if ((q.questions[i].origLabel || q.questions[i].label) === 'Responsibles') {
                try {
                    this.options[$localize`Responsibles`] = undefined;
                    q.questions[i].example.subscribe((response) => {
                        const resp = [];
                        for (let i = 0; i < response.result.length; i++) {
                            resp.push({ _id: response.result[i]._id, name: response.result[i].first_name + ' ' + response.result[i].last_name });
                        }
                        this.options[$localize`Responsibles`] = resp;
                    });
                } catch (error) {
                    this.logger.error('error getting data from ', q.questions[i], error);
                    this.alertService.error($localize`Error getting data for ${q.questions[i].label}:label:`, error);
                }
                break;
            }
        }
    }

    selectChanged(label: string, event: {value: string}) {
        // if (VariableToText.back(label) === 'organiser') {
        if (label === $localize`Organiser`) {
            this.reloadResponsibles(event.value);
        } else if (label === $localize`Tournament Type`) {
            // reload 'categories' question values
            const defaults = new Tournament();
            defaults.tournament_type = event.value;
            // find current organiser, otherwise call to getQuestions fails
            let respId: string;
            for (let i = 0; i < this.questions.length; i++) {
                const question = this.questions[i];
                if (question.label === 'Organiser') {
                    respId = question.value;
                }
            }
            const q = this.questionsService.getQuestions(defaults, respId);
            for (let i = 0; i < q.questions.length; i++) {
                if (q.questions[i].label === 'Categories') {
                    try {
                        this.options[$localize`Categories`] = undefined;
                        q.questions[i].example.subscribe((response) => {
                            const cats = [];
                            for (let i = 0; i < response.result.length; i++) {
                                cats.push({_id: response.result[i]._id, name: response.result[i].name});
                            }
                            this.options[$localize`Categories`] = cats;
                        });
                    } catch (error) {
                        this.logger.error('error getting data from ', q.questions[i], error);
                        this.alertService.error($localize`Error getting data for ${q.questions[i].label}:label:`, error);
                    }
                    break;
                }
            }
        }
    }

    private renameOption(options: { [name: string]: unknown }, from: string, to: string): void {
        if (options && options[from]) {
            options[to] = options[from];
            options[from] = undefined;
        }
    }

    private translateQuestions(questions: { label: string, origLabel: string }[], options: { [name: string]: unknown }): void {
        for (let q = 0; q < questions.length; q++) {
            const question = questions[q];
            const label = question.label;
            question.origLabel = label;
            switch (label) {
                case 'Title': question.label = $localize`Title`; break;
                case 'Location': question.label = $localize`Location`; break;
                case 'Organiser': question.label = $localize`Organiser`; this.renameOption(options, label, question.label); break;
                case 'Responsibles': question.label = $localize`Responsibles`; this.renameOption(options, label, question.label); break;
                case 'Start Date': question.label = $localize`Start Date`; break;
                case 'End Date': question.label = $localize`End Date`; break;
                case 'Registration Deadline': question.label = $localize`Registration Deadline`; break;
                case 'Nation': question.label = $localize`Nation`; this.renameOption(options, label, question.label); break;
                case 'Federation': question.label = $localize`Federation`; this.renameOption(options, label, question.label); break;
                case 'Tournament Type': question.label = $localize`Tournament Type`; this.renameOption(options, label, question.label); break;
                case 'Categories': question.label = $localize`Categories`; this.renameOption(options, label, question.label); break;
                case 'Points': question.label = $localize`Points`; break;
            }
        }
    }

    edit(tournament: Tournament) {
        // // clone to avoid that the original dates etc. are messed up
        // this.editTournament = JSON.parse(JSON.stringify(tournament));
        this.editTournament = tournament;
        this.logger.trace('starting tournament edit mode', tournament.title);

        const q = this.questionsService.getQuestions(this.editTournament, this.editTournament.organiser._id);
        this.questions = q.questions;
        this.options = q.options;
        this.translateQuestions(this.questions, this.options);
        this.reloadResponsibles(this.editTournament.organiser._id, q);

        for (let i = 0; i < this.questions.length; i++) {
            if (this.questions[i].type === 'date') {
                // this.questions[i].value += 'T00:00:00.000Z'
                this.questions[i].value = moment(this.questions[i].value, 'DD.MM.YYYY');
            }
        }

        if (this.editTournament.details == null) {
            this.editTournament.details = $localize`:@@tournament details:`;
        }
        this.details = this.editTournament.details;

        this.announcement = this.tournamentsService.createAnnouncement(this.editTournament, this.details);
        this.logger.trace('built string', this.announcement);
    }

    delete(tournament: Tournament) {
        const dialogRef = this.dialog.open(YesNoDialogComponent, {
            data: { text: $localize`Delete the tournament?` }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === true) {
                // TODO also delete enrollments for this tournament
                this.myTournamentService.deleteTournament(tournament).subscribe(
                    response => {
                        if (response.success === true) {
                            // search the tournament in the local array
                            let idx = -1;
                            for (let i = 0; i < this.mytournaments.length; i++) {
                                if (this.mytournaments[i] === tournament) {
                                    idx = i;
                                    break;
                                }
                            }
                            if (idx >= 0) {
                                // delete it locally
                                this.mytournaments.splice(idx, 1);
                                this.logger.debug('deleted tournament', tournament.title);
                            } else {
                                this.logger.debug('error: tournament', tournament, 'not found in', this.mytournaments);
                                // no need to show error, desired functionality achieved
                            }
                        } else {
                            this.logger.error('error deleting the tournament', response.error);
                            this.alertService.error($localize`Error deleting the tournament `, response.error);
                        }
                        // this.logger.debug('built strings', this.announcements);
                    },
                    error => {
                        this.logger.error('error deleting the tournament', error);
                        this.alertService.error($localize`Error deleting the tournament `, error);
                    }
                );
            }
        });
    }


    save() {
        this.editTournament.details = this.details;
        for (let q = 0; q < this.questions.length; q++) {
            if (this.questions[q].value) {
                // create tournament object for DB
                this.editTournament[VariableToText.back(this.questions[q].origLabel)] = this.questions[q].value;
            } else {
                this.editTournament[VariableToText.back(this.questions[q].origLabel)] = undefined;
            }
        }
        this.logger.debug('updating tournament', this.editTournament);

        // save in DB
        this.creationService.updateTournament(this.editTournament).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('tournament updated');
                    this.logger.trace('tournament updated', response.result);
                    this.alertService.success($localize`Tournament successfully updated`);
                    this.editTournament = undefined;
                    if (this.myClubId) {
                        this.getMyTournaments(this.myClubId);
                    }
                } else {
                    this.logger.error('Could not save tournament', response.error);
                    this.alertService.error($localize`Could not save tournament`, response.error);
                }
            },
            error => {
                this.logger.error('Could not save tournament', error);
                this.alertService.error($localize`Could not save tournament`, error);
            }
        );
    }

    cancel() {
        this.editTournament = undefined;
        setTimeout(() => this.topElem.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' }), 0);
    }

    selectAll(select: any, values) {
        // if (select.name === 'Categories') {
        select.update.emit(values.map((val) => val._id));
        // }
    }
    deselectAll(select: any) {
        // if (select.name === 'Categories') {
        select.update.emit([]);
        // }
    }
    closeMatSelect(select: any) {
        select.close();
    }

    startTournament(tId: string, newDraw: boolean) {
        if (newDraw) {
            const dialogRef = this.dialog.open(YesNoDialogComponent, {
                data: { text: $localize`ATTENTION! This will DELETE all ongoing games if any!!!` }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result === true) {
                    this.tournamentsService.setNewDraw(tId, newDraw);
                    this.router.navigate(['/ko', {id: tId}]);
                }
            });
        } else {
            this.tournamentsService.setNewDraw(tId, newDraw);
            this.router.navigate(['/ko', {id: tId}]);
        }
    }

    removeDraw(tId: string) {
        const dialogRef = this.dialog.open(YesNoDialogComponent, {
            data: { text: $localize`ATTENTION! This will DELETE all ongoing games if any!!!` }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result === true) {
                this.matchdbService.removeTournamentData(tId).subscribe(
                    response => {
                        if (response.success === true) {
                            this.logger.debug('Removed tournament data');
                            this.alertService.success($localize`Removed draw`);

                        } else {
                            this.logger.error('Could not remove tournament data:', response.error);
                            this.alertService.error($localize`Could not remove tournament data`, response.error);
                        }
                    },
                    error => {
                        this.logger.error('Could not remove tournament data:', error);
                        this.alertService.error($localize`Could not remove tournament data`, error);
                    }
                );
            }
        });
    }

    importEnrollment(tIdx: number) {
        if (this.mytournaments[tIdx].ts_tid1) {
            this.doImportEnrollment(tIdx, this.mytournaments[tIdx].ts_tid1);
        } else {
            const dialogRef = this.dialog.open(TSImportDialogComponent);
            dialogRef.afterClosed().subscribe((info) => {
                if (info && info.ts_tid1) {
                    this.doImportEnrollment(tIdx, info.ts_tid1, info.ts_tid2);
                }
            });
        }
    }

    doImportEnrollment(tIdx: number, ts_tid1: string, ts_tid2?: string) {
        this.busy = tIdx;
        const tournamentId = this.mytournaments[tIdx]._id;
        const federation = this.mytournaments[tIdx].federation;
        this.tournamentsService.importPlayersFromTS(tournamentId, federation, ts_tid1, ts_tid2).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug(response.result, 'players enrolled');
                    this.alertService.success($localize`Successfully enrolled ${response.result.toString()}:nrOf: players`);
                    if (response.error) {
                        const errs = JSON.parse(response.error);
                        if (errs) {
                            if (errs.length) {
                                let str = '';
                                for (let e = 0; e < errs.length; e++) {
                                    const err = errs[e];
                                    str += err.cat + ': ' + err.player + (err.partner ? ' (as partner)' : '') + err.err + '\n';
                                }
                                this.logger.debug('error importing enrolled players', response.error);
                                this.alertService.error($localize`Error importing enrolled players`, str);
                            } else {
                                // all good, empty error array
                            }
                        } else {
                            this.logger.debug('error importing enrolled players', response.error);
                            this.alertService.error($localize`Error importing enrolled players`, response.error);
                        }
                    }
                } else {
                    this.logger.debug('error importing enrolled players', response.error);
                    this.alertService.error($localize`Error importing enrolled players`, response.error);
                }
                this.busy = -1;
            },
            error => {
                this.logger.error('error importing enrolled players', error);
                this.alertService.error($localize`Error importing enrolled players`, error);
                this.busy = -1;
            }
        );
    }

    uploadFilesForRankings(tIdx: number, fevent: Event) {
        this.busy = tIdx;
        const fileList: FileList = (fevent.target as any as { files: FileList }).files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const fr = new FileReader();
            const mycomp = this;
            fr.readAsText(file);
            fr.onloadend = () => {
                const entries = [];
                let cat: string;
                const contents = fr.result.toString();
                const error = fr.error;
                if (error != null) {
                    mycomp.logger.error('File could not be read! Code ' + error.code);
                    return;
                } else {
                    mycomp.logger.debug('importing', contents);
                    // DCV Open
                    // Qualifikation;Name;SpielerID;A/B-Turnier
                    // 1;Nico Franke;2194;A

                    // Mixed Doppel
                    // Qualifikation,Name,SpielerID
                    // 1,Anna Hubert,4134
                    // ,Adrian Lutz,3580

                    const strsplit = contents.split('\n');
                    let lineCnt = -1;
                    let lastrank: number;
                    let rankIdx = -1;
                    let nameIdx = -1;
                    let tsidIdx = -1;
                    let aorbIdx = -1;
                    for (let i = 0; i < strsplit.length; i++) {
                        const line = strsplit[i].trim();
                        if (line === '') {
                            continue;
                        }
                        lineCnt += 1;
                        if (lineCnt === 0) {
                            // category
                            // "Final Positions of Open"
                            cat = line.split(/[;,]/)[0];
                            const match = /".*of (.*)"/.exec(cat);
                            if (match?.[1]) {
                                cat = match[1];
                            }
                            continue;
                        }
                        if (lineCnt === 1) {
                            // headers
                            const linesplit = line.split(/[;,]/);
                            for (let i = 0; i < linesplit.length; i++) {
                                const hd = linesplit[i];
                                switch (hd) {
                                    case '"Position"':
                                        rankIdx = i;
                                        break;
                                    case '"Name"':
                                        nameIdx = i;
                                        break;
                                    case '"Member ID"':
                                        tsidIdx = i;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            continue;
                        }
                        aorbIdx = Math.max(rankIdx, nameIdx, tsidIdx) + 1;
                        // "","","1","S�nke Kaatz"
                        let rank: number, name: string, tsid: string, bround = false;
                        // mycomp.logger.debug('working on line', line);
                        const linesplit = line.split(/[;,]/);
                        if (rankIdx >= 0 && linesplit[rankIdx]) {
                            const rankstr = linesplit[rankIdx].replace(/"/g, '').trim();
                            if (rankstr) {
                                rank = parseInt(rankstr, 10);
                                lastrank = rank;
                            } else {
                                rank = lastrank;
                            }
                        }
                        if (nameIdx >= 0 && linesplit[nameIdx]) {
                            name = linesplit[nameIdx].replace(/"/g, '').trim();
                        }
                        if (tsidIdx >= 0 && linesplit[tsidIdx]) {
                            tsid = linesplit[tsidIdx].replace(/"/g, '').trim();
                        }
                        if (aorbIdx >= 0 && linesplit[aorbIdx]) {
                            const aorb = linesplit[aorbIdx].replace(/"/g, '').trim();
                            if (aorb.toUpperCase() === 'B') {
                                bround = true;
                            }
                        }
                        mycomp.logger.debug('working with', rank, '/', name, '/', tsid, '/', bround);
                        entries.push({ rank, name, tsid, bround });
                    }
                }
                const tournamentId = mycomp.mytournaments[tIdx]._id;
                const federation = mycomp.mytournaments[tIdx].federation;
                mycomp.tournamentsService.sendFileDataForRanking(tournamentId, federation, cat, JSON.stringify(entries)).subscribe({
                    next: response => {
                        if (response.success === true) {
                            mycomp.logger.debug(response.result, 'entries added');
                            mycomp.alertService.success($localize`Successfully added ${response.result.toString()}:nrOf: entries`);
                            mycomp.rankingService.updateLeaderboard().subscribe({
                                next: _ => {
                                    mycomp.logger.debug('successfully called updateLeaderboard');
                                },
                                error: error2 => {
                                    mycomp.logger.error('Could not update leaderboard:', error2);
                                    mycomp.alertService.error($localize`Error adding`, error2);
                                }
                            });
                            if (response.error) {
                                const errs = JSON.parse(response.error);
                                if (errs) {
                                    if (errs.length) {
                                        let str = '';
                                        for (let e = 0; e < errs.length; e++) {
                                            const err = errs[e];
                                            str += err + '\n';
                                        }
                                        mycomp.logger.debug('error updating ranking', response.error);
                                        mycomp.alertService.error($localize`Error updating ranking\n`, str);
                                    } else {
                                        // all good, empty error array
                                    }
                                } else {
                                    mycomp.logger.debug('error updating ranking', response.error);
                                    mycomp.alertService.error($localize`Error updating ranking\n`, response.error);
                                }
                            }
                        } else {
                            mycomp.logger.debug('error updating ranking', response.error);
                            mycomp.alertService.error($localize`Error updating ranking\n`, response.error);
                        }
                        mycomp.busy = -1;
                    },
                    error: error => {
                        mycomp.logger.debug('error updating ranking', error);
                        mycomp.alertService.error($localize`Error updating ranking\n`, error);
                        mycomp.busy = -1;
                    }
                });
            };
        }
    }

    importTSPointsToRanking(tIdx: number, dmQuali: boolean) {
        if (this.mytournaments[tIdx].ts_tid2) {
            this.doImportTSPointsToRanking(tIdx, this.mytournaments[tIdx].ts_tid1, this.mytournaments[tIdx].ts_tid2, dmQuali);
        } else {
            const dialogRef = this.dialog.open(TSImportDialogComponent);
            dialogRef.afterClosed().subscribe((info) => {
                if (info && info.ts_tid2) {
                    this.doImportTSPointsToRanking(tIdx, info.ts_tid1, info.ts_tid2, dmQuali);
                }
            });
        }
    }

    doImportTSPointsToRanking(tIdx: number, ts_tid1: string, ts_tid2: string, dmQuali: boolean) {
        this.busy = tIdx;
        const tournamentId = this.mytournaments[tIdx]._id;
        const federation = this.mytournaments[tIdx].federation;
        this.tournamentsService.importTSPointsToRanking(tournamentId, federation, ts_tid1, ts_tid2, dmQuali).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug(response.result, 'entries added');
                    this.alertService.success($localize`Successfully added ${response.result.toString()}:nrOf: entries`);
                    if (!dmQuali) {
                        this.rankingService.updateLeaderboard().subscribe(
                            (_: unknown) => {
                                this.logger.debug('successfully called updateLeaderboard');
                            },
                            (error2: unknown) => {
                                this.logger.error('Could not update leaderboard:', error2);
                                this.alertService.error($localize`Error adding`, error2);
                            }
                        );
                    }
                    if (response.error) {
                        const errs = JSON.parse(response.error);
                        if (errs) {
                            if (errs.length) {
                                let str = '';
                                for (let e = 0; e < errs.length; e++) {
                                    const err = errs[e];
                                    str += err + '\n';
                                }
                                this.logger.debug('error updating ranking', response.error);
                                this.alertService.error($localize`Error updating ranking\n`, str);
                            } else {
                                // all good, empty error array
                            }
                        } else {
                            this.logger.debug('error updating ranking', response.error);
                            this.alertService.error($localize`Error updating ranking\n`, response.error);
                        }
                    }
                } else {
                    this.logger.debug('error updating ranking', response.error);
                    this.alertService.error($localize`Error updating ranking\n`, response.error);
                }
                this.busy = -1;
            },
            error => {
                this.logger.debug('error updating ranking', error);
                this.alertService.error($localize`Error updating ranking\n`, error);
                this.busy = -1;
            }
        );
    }

    disableOrganiserButton(tournament: Tournament): boolean {
        // tslint:disable-next-line: no-bitwise
        if ((this.myRoles & Role.ROLE_PAUL) > 0 || (this.myRoles & Role.ROLE_DCV) > 0) {
            return false;
        }
        if (this.myPlayer && tournament?.responsibles?.indexOf(this.myPlayer) >= 0) {
            return false;
        }
        return true;
    }

    getImportTooltip(tournament: Tournament & { hasDraw: boolean }): string {
        return tournament.hasDraw ? $localize`First remove the current draw` : this.disableOrganiserButton(tournament) ? $localize`Only Organiser or DCV can do this!` : $localize`Will first remove all currently enrolled players!`;
    }
}
