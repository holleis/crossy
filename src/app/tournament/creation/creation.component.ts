import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../ranking/categories.service';
import { PlayerService } from '../../player/player.service';
import { QuestionsService } from './questions.service';
import { VariableToText } from '../../util/variabletotext';
import { Tournament } from '../tournament';
import { CreationService } from './creation.service';
import { ClubService } from '../../club/club.service';
import { AlertService } from '../../util/alert/alert.service';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';
import { UserService } from '../../auth/_services/user.service';
import { Player } from '../../player/player';
import { TranslatorService } from '../../util/translate/translator.service';
// import { TournamentsService } from '../tournaments.service';

@Component({
    selector: 'app-creation',
    templateUrl: './creation.component.html',
    styleUrls: ['./creation.component.scss'],
    providers: [CategoriesService, PlayerService, QuestionsService,
        ClubService, CreationService, UserService]
})

export class CreationComponent implements OnInit {

    questions = [];
    options = {};
    details = '';

    tournament: Tournament;
    organiserPlayer: Player;

    quillOptions =  {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            // ['blockquote', 'code-block'],

            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            // [{ 'direction': 'rtl' }],                         // text direction

            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

            [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],

            ['clean'],                                         // remove formatting button

            ['link', 'image']                         // link and image, video
        ],
        imageResize: {}
    };

    loading = false;

    constructor(
        private questionsService: QuestionsService,
        private creationService: CreationService,
        private alertService: AlertService,
        private router: Router,
        private logger: NGXLogger,
        private userService: UserService,
        private playerService: PlayerService,
        public tr: TranslatorService,
        // private tournamentsService: TournamentsService,
    ) { }

    ngOnInit() {
        const defaults = new Tournament();
        defaults.title = '';
        defaults.location = '';
        defaults.start_date = undefined;
        defaults.end_date = undefined;
        defaults.registration_deadline = undefined;
        defaults.responsibles = [];

        const user = this.userService.getCurrentUser();
        if (user) {
            const myPlayerId = user.player_id;
            try {
                this.playerService.getPlayer(myPlayerId).subscribe(response => {
                    if (response.success === true && response.result && response.result.clubs && response.result.clubs.length > 0) {
                        this.organiserPlayer = response.result;
                        defaults.organiser = this.organiserPlayer.clubs[0];
                        defaults.responsibles = [this.organiserPlayer._id];

                        const q = this.questionsService.getQuestions(defaults, this.organiserPlayer.clubs[this.organiserPlayer.mainclubindex]._id);
                        this.questions = q.questions;
                        this.options = q.options;
                    }
                });
            } catch (err) {
                // ignore
            }
        } else {
            const q = this.questionsService.getQuestions(defaults);
            this.questions = q.questions;
            this.options = q.options;
        }
    }

    selectChanged(label: string, event: {value: string}) {
        if (VariableToText.back(label) === 'organiser') {
            // reload 'responsibles' question values
            const defaults = new Tournament();
            const q = this.questionsService.getQuestions(defaults, event.value);
            for (let i = 0; i < q.questions.length; i++) {
                if (q.questions[i].label === 'Responsibles') {
                    try {
                        this.options['Responsibles'] = undefined;
                        q.questions[i].example.subscribe((response) => {
                            const resp = [];
                            for (let i = 0; i < response.result.length; i++) {
                                resp.push({_id: response.result[i]._id, name: response.result[i].first_name + ' ' + response.result[i].last_name});
                            }
                            this.options['Responsibles'] = resp;
                        });
                    } catch (error) {
                        this.logger.error('error getting data from ', q.questions[i], error);
                        this.alertService.error($localize`Error getting data for ${q.questions[i].label}:label:`, error);
                    }
                    break;
                }
            }
        } else if (VariableToText.back(label) === 'tournament_type') {
            // reload 'categories' question values
            const defaults = new Tournament();
            defaults.tournament_type = event.value;
            // find current organiser, otherwise call to getQuestions fails
            let respId: string;
            for (let i = 0; i < this.questions.length; i++) {
                const question = this.questions[i];
                if (question.label === 'Organiser') {
                    respId = question.value;
                }
            }
            const q = this.questionsService.getQuestions(defaults, respId);
            for (let i = 0; i < q.questions.length; i++) {
                if (q.questions[i].label === 'Categories') {
                    try {
                        this.options['Categories'] = undefined;
                        q.questions[i].example.subscribe((response) => {
                            const cats = [];
                            for (let i = 0; i < response.result.length; i++) {
                                cats.push({_id: response.result[i]._id, name: response.result[i].name});
                            }
                            this.options['Categories'] = cats;
                        });
                    } catch (error) {
                        this.logger.error('error getting data from ', q.questions[i], error);
                        this.alertService.error($localize`Error getting data for ${q.questions[i].label}:label:`, error);
                    }
                    break;
                }
            }
        }
    }

    create() {
        this.tournament = new Tournament();
        for (let q = 0; q < this.questions.length; q++) {
            if (this.questions[q].value) {
                // create tournament object for DB
                this.tournament[VariableToText.back(this.questions[q].label)] = this.questions[q].value;
            } else {
                this.tournament[VariableToText.back(this.questions[q].label)] = undefined;
            }
        }
        this.tournament.details = this.details;
        this.tournament.event_type = 'tournament';
        this.logger.debug('saving tournament', this.tournament);

        // save in DB
        this.creationService.addTournament(this.tournament).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('tournament saved', response.result);
                    this.router.navigate(['/mytournaments']);
                } else {
                    this.logger.error('Could not save tournament', response.error);
                    this.alertService.error($localize`Could not save tournament`, response.error);
                }
            },
            error => {
                this.logger.error('Could not save tournament', error);
                this.alertService.error($localize`Could not save tournament`, error);
            }
        );
    }

    selectAll(select: any, values: { _id: unknown }[]) {
        if (select.name === 'Categories') {
            select.update.emit(values.map((val) => val._id));
        }
    }
    deselectAll(select: any) {
        if (select.name === 'Categories') {
            select.update.emit([]);
        }
    }
    closeMatSelect(select: any) {
        select.close();
    }

    // // called from template; tries to load tournament info from TS
    // loadFromTS(tournamentName: string): void {
    //     this.loading = true;
    //     this.tournamentsService.getInfoFromTS(tournamentName).subscribe(
    //         response => {
    //             if (response.success === true) {
    //                 const props = Object.getOwnPropertyNames(response.result);
    //                 for (let p = 0; p < props.length; p++) {
    //                     const prop = props[p];
    //                     for (let q = 0; q < this.questions.length; q++) {
    //                         const question = this.questions[q];
    //                         if (question.label === prop) {
    //                             question.value = response.result[prop];
    //                             break;
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 this.logger.debug('error importing data from TS: %s', response.error);
    //                 this.alertService.error($localize`Error importing data`, response.error);
    //             }
    //             this.loading = false;
    //         },
    //         error => {
    //             this.logger.error('error importing data from TS: %s', error);
    //             this.alertService.error($localize`Error importing data`, error);
    //             this.loading = false;
    //         }
    //     );
    // }
}
