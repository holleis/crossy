import { TestBed, inject } from '@angular/core/testing';

import { CategoriesService } from '../../ranking/categories.service';
import { QuestionsService } from './questions.service';

describe('QuestionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoriesService, QuestionsService]
    });
  });

  it('should be created', inject([CategoriesService, QuestionsService], (service: QuestionsService) => {
    expect(service).toBeTruthy();
  }));
});
