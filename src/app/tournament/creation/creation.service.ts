import { Injectable } from '@angular/core';
import { Tournament } from '../tournament';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../globals';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class CreationService {

    // URL to web api
    private url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/';

    constructor(
        private http: HttpClient,
        private logger: NGXLogger
    ) {}

    addTournament(tournament: Tournament) {
        this.logger.debug('sending tournament', tournament);
        return this.http.post<{
            'success': boolean,
            'result': Tournament,
            'error': string
        }>(this.url, tournament);
    }

    updateTournament(tournament: Tournament) {
        this.logger.debug('updating tournament', tournament);
        return this.http.put<{
            'success': boolean,
            'result': Tournament,
            'error': string
        }>(this.url + encodeURI(tournament._id), tournament);
    }
}
