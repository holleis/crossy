import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Globals } from '../../globals';
import { Tournament } from '../tournament';
import * as moment from 'moment';


@Injectable()
export class MytournamentsService {

    constructor(
        private http: HttpClient
    ) {}

    getMyTournaments(club: string, all = false) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/club/' + encodeURI(club);
        let httpParams = new HttpParams().set('from', all ? '0' : moment().subtract(1, 'year').valueOf().toString());
        return this.http.get<{
            'success': boolean,
            'result': Tournament[],
            'error': string
        }>(url, { params: httpParams });
    }

    deleteTournament(tournament: Tournament) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/' + encodeURI(tournament._id);
        return this.http.delete<{
            'success': boolean,
            'result': Tournament,
            'error': string
        }>(url);
    }
}
