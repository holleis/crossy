import { TestBed, inject } from '@angular/core/testing';

import { MytournamentsService } from './mytournaments.service';

describe('MytournamentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MytournamentsService]
    });
  });

  it('should be created', inject([MytournamentsService], (service: MytournamentsService) => {
    expect(service).toBeTruthy();
  }));
});
