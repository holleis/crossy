import { Category } from './category';
import { Event } from '../calendar/event';
import { DateTime } from 'luxon';

export class Tournament extends Event {
    registration_deadline: string = DateTime.utc().toFormat('dd.MM.yyyy');
    registration_deadline_epoch: number;
    nation = 'GER';
    federation = 'DCV';
    tournament_type = 'DCV';
    categories: Category[] = [];
    points = 250;
    ts_tid1 = 'EDB5ADCC-7FEE-41AD-8373-746027AD90F6';
    ts_tid1___link = 'https://ico.tournamentsoftware.com/sport/events.aspx?id=EDB5ADCC-7FEE-41AD-8373-746027AD90F6';
    ts_tid2 = '147470';
    ts_tid2___link = 'https://ico.tournamentsoftware.com/ranking/tournament.aspx?id=25666&tournament=147470';
    hasDraw: boolean;
}
