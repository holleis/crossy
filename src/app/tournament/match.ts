import { PlayerRef } from './playerref';

export class Match {
    // indicates whether the scores have been successfully transferred to the server
    saved: boolean;
    player1: PlayerRef;
    player2: PlayerRef;
    score: Array<{p1: number, p2: number}>;
    winner: PlayerRef;
    // level is 0, 1, ... for Ko, groupNr (0, 1, ...) for group (changed from groupName ('A') for group)
    // ko_type is 'A' ro 'B' for A/B round KOs, or 'G' for group matches
    matchNr: {level: string, num: number, ko_type: string};

    constructor(p1Ref: PlayerRef, p2Ref: PlayerRef, mNr?: {level: string, num: number, ko_type: string}) {
        this.score = [{p1: 0, p2: 0}, {p1: 0, p2: 0}, {p1: 0, p2: 0}];

        this.player1 = p1Ref;
        this.player2 = p2Ref;

        if (mNr) {
            this.matchNr = mNr;
        }

        // initialize winner; don't set new objects - references are used in KO
        this.winner = new PlayerRef();
    }

    // returns
    // -1 if p1 winner
    // 0 if no winner
    // 1 if p2 winner
    static checkWinnerSet(set: {p1: number, p2: number}): -1 | 0 | 1 {
        if ((set.p1 > 15 || set.p2 > 15) && Math.abs(set.p1 - set.p2) >= 2) {
            if ((set.p1 > 16 || set.p2 > 16) && Math.abs(set.p1 - set.p2) > 2) {
                // invalid
                return 0;
            }
            if (set.p1 > set.p2) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
    // returns
    // -1 if p1 winner
    // 0 if no winner
    // 1 if p2 winner
    static checkWinner(match: Match): -1 | 0 | 1 {
        if (PlayerRef.isBye(match.player1)) {
            return 1;
        } else if (PlayerRef.isBye(match.player2)) {
            return -1;
        }
        // remove null entries
        for (let m = 0; m < match.score.length; m++) {
            const set = match.score[m];
            if (set && set.p1 == undefined && set.p2 == undefined) {
                match.score.splice(m, 1);
                m -= 1;
            }
        }
        const wins = {p1: 0, p2: 0};
        if (!Match.checkWinnerSetWins(match.score[0], wins)) {
            return 0;
        }
        if (!Match.checkWinnerSetWins(match.score[1], wins)) {
            return 0;
        }
        if (match.score[2]) {
            if (!Match.checkWinnerSetWins(match.score[2], wins) && (match.score[2].p1 !== 0 || match.score[2].p2 !== 0)) {
                return 0;
            }
        }
        let neededWins = 2;
        if (match.score[3]) {
            if (!Match.checkWinnerSetWins(match.score[3], wins) && (match.score[3].p1 !== 0 || match.score[3].p2 !== 0)) {
                return 0;
            }
            neededWins = 3;
        }
        if (match.score[4]) {
            if (!Match.checkWinnerSetWins(match.score[4], wins) && (match.score[4].p1 !== 0 || match.score[4].p2 !== 0)) {
                return 0;
            }
        }
        if (wins.p1 === neededWins) {
            return -1;
        } else if (wins.p2 === neededWins) {
            return 1;
        } else {
            return 0;
        }
    }

    // wins:{p1, p2} if player1 wins the set, p1 is increased
    // returns true if set is finished and valid
    static checkWinnerSetWins(set: {p1: number, p2: number}, wins: {p1: number, p2: number}): boolean {
        if ((set.p1 > 15 || set.p2 > 15) && Math.abs(set.p1 - set.p2) >= 2) {
            if ((set.p1 > 16 || set.p2 > 16) && Math.abs(set.p1 - set.p2) > 2) {
                // invalid
                return false;
            }
            if (set.p1 > set.p2) {
                wins.p1++;
            } else {
                wins.p2++;
            }
            return true;
        }
        return false;
    }
}
