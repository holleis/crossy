import { Component, OnInit } from '@angular/core';
import { Match } from '../match';
import { SocketService, SockEvent, UpdateEvent } from '../../util/websockets/socket.service';
import { MatchdbService } from '../execute/matchdb.service';
import { NGXLogger } from 'ngx-logger';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from '../../util/alert/alert.service';
import { UserService } from '../../auth/_services/user.service';
import { Role } from '../../user/role';
import { TournamentsService } from '../tournaments.service';
import { EnrollmentInfo } from '../cat-info';
import { Group } from '../group';
import { Ko } from '../ko';
import { Tournament } from '../tournament';
import { forkJoin as observableForkJoin, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Category } from '../category';
import { PlayerRef } from '../playerref';
import { TranslatorService } from '../../util/translate/translator.service';

interface MyMatch {category: string; catId: string;
    matchNr: {level: string, num: number, ko_type: string};
    score: Array<{p1: number, p2: number}>;
    player1: string;
    partner1: string;
    player2: string;
    partner2: string;
    match: Match;
}

@Component({
    selector: 'app-enter-scores',
    templateUrl: './enter-scores.component.html',
    styleUrls: ['./enter-scores.component.css'],
})
export class EnterScoresComponent implements OnInit {

    constructor(
        private socketService: SocketService,
        private matchdbService: MatchdbService,
        private logger: NGXLogger,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private userService: UserService,
        private tournamentsService: TournamentsService,
        public tr: TranslatorService,
    ) {
        const debounce = this.scorelistener.pipe(
            debounceTime(5000),
            distinctUntilChanged(),
            switchMap((m: any) => {
                this.logger.trace('score update:', m.match);
                this.onScoreChange(m.match);
                return [];
            })
        );
        debounce.subscribe();
    }

    canMakeChanges = false;

    tournamentId: string;
    tournament: Tournament;
    categoryNames: string[] = [];
    categoryIds: string[] = [];
    category: Category[] = [];
    myplayers: EnrollmentInfo['players'][] = [];
    groups: Group[][] = [];
    ko: Ko[] = [];
    ko_b: Ko[] = [];

    mymatches: MyMatch[] = [];
    filteredMyMatches: MyMatch[] = [];
    finishedMatches: MyMatch[] = [];

    scorelistener = new Subject<{set: number, playerNr: number, value: number}>();
    // socketIO connection for live updates
    ioConnection: any;

    filter = '';
    loading = true;

    ngOnInit() {
        this.tournamentId = this.route.snapshot.params.id;

        if (!this.tournamentId) {
            this.alertService.error($localize`Need to know the tournament ID. Please ask the organiser.`);
            return;
        }

        this.initIoConnection();

        // see if user is allowed to enter scores
        const user = this.userService.getCurrentUser();
        // tslint:disable-next-line: no-bitwise
        if (user && ((user.roles & Role.ROLE_PAUL) > 0 || (user.roles & Role.ROLE_DCV) > 0)) {
            this.canMakeChanges = true;
        }

        if (!this.canMakeChanges) {
            this.tournamentsService.getTournament(this.tournamentId).subscribe(
                response => {
                    this.logger.trace('retrieve tournament for canMakeChanges (responsibles)', this.tournamentId, response);
                    if (response.success === true) {
                        this.tournament = response.result;
                        if (user && response.result && response.result.responsibles) {
                            if (response.result.responsibles.indexOf(user.player_id) > -1) {
                                this.canMakeChanges = true;
                            }
                            this.alertService.error($localize`Need to be logged in and organiser or DCV to enter scores!`);
                        } else {
                            this.logger.trace('no responsibles for tournament');
                        }
                    } else {
                        this.logger.error('Could not retrieve tournament:', response.error);
                    }
                },
                error => {
                    this.logger.error('Could not retrieve tournament:', error);
                }
            );
        }

        this.tournamentsService.getDrawnCategories(this.tournamentId).subscribe(
            response => {
                this.logger.trace('retrieved drawn categories', response);
                if (response.success === true) {
                    this.categoryIds = response.result.catIds;
                    this.categoryNames = response.result.catNames;

                    this.getTournamentData();

                } else {
                    this.logger.error('Could not retrieve drawn categories tournament:', response.error);
                    this.alertService.error($localize`Could not retrieve drawn categories tournament`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve drawn categories:', error);
                this.alertService.error($localize`Could not retrieve drawn categories`, error);
            }
        );
    }

    filterChanged() {
        this.filter = this.filter.toLowerCase();
        this.filteredMyMatches = this.mymatches.filter(mymatch =>
            mymatch.player1.toLowerCase().indexOf(this.filter) >= 0 ||
            mymatch.player2.toLowerCase().indexOf(this.filter) >= 0 ||
            (mymatch.partner1 && mymatch.partner1.toLowerCase().indexOf(this.filter) >= 0) ||
            (mymatch.partner2 && mymatch.partner2.toLowerCase().indexOf(this.filter) >= 0));
    }

    private initIoConnection(): void {
        if (this.ioConnection = this.socketService.initSocket()) {
            // first init, register listeners
            this.socketService.onMessage(
                (msg: UpdateEvent) => {
                    if (msg?.tournamentId === this.tournamentId) {
                        this.logger.debug('received event for category', msg.categoryId);
                        this.updateWithMessage(msg);
                    }
                }
            );
            this.socketService.onEvent(SockEvent.OPEN, () =>
                this.logger.debug('enterscores connected')
            );

            this.socketService.onEvent(SockEvent.CLOSE, () =>
                this.logger.debug('enterscores disconnected')
            );
        }
    }

    updateWithMessage(msg: {type: string, tournamentId: string, categoryId: string, message: string}) {
        if (msg.tournamentId === this.tournamentId) {
            const message = msg.message;
            this.logger.debug('match update message received', message);
            try {
                const match = JSON.parse(message) as Match;
                // legacy treatment: initially, group matches stored groupNames 'A', 'B' etc. as level
                let groupNr = parseInt(match.matchNr.level, 10);
                if (isNaN(groupNr)) {
                    groupNr = match.matchNr.level.charCodeAt(0) - 65;
                }
                // search for match and update scores
                let found = false;
                for (let m = 0; m < this.mymatches.length; m++) {
                    const mymatch = this.mymatches[m];
                    if (msg.categoryId === mymatch.catId && match.matchNr.level === mymatch.matchNr.level &&
                        match.matchNr.num === mymatch.matchNr.num && match.matchNr.ko_type === mymatch.matchNr.ko_type) {

                        this.logger.debug('match found - updating', message);
                        mymatch.match.score = match.score;
                        mymatch.score = mymatch.match.score;
                        // remove from list if finished
                        if (Match.checkWinner(match) !== 0) {
                            this.mymatches.splice(m, 1);
                            this.finishedMatches.push(mymatch);
                            this.filterChanged();
                        }
                        found = true;
                        break;
                    }
                }
                // search for match and update scores, could be finished
                for (let m = 0; !found && m < this.finishedMatches.length; m++) {
                    const mymatch = this.finishedMatches[m];
                    if (msg.categoryId === mymatch.catId && match.matchNr.level === mymatch.matchNr.level &&
                        match.matchNr.num === mymatch.matchNr.num && match.matchNr.ko_type === mymatch.matchNr.ko_type) {

                        mymatch.match.score = match.score;
                        mymatch.score = mymatch.match.score;
                        // add to list if not finished
                        if (Match.checkWinner(match) === 0) {
                            this.finishedMatches.splice(m, 1);
                            this.mymatches.push(mymatch);
                            this.filterChanged();
                        }
                        break;
                    }
                }

            } catch (err) {
                this.logger.error('could not parse websocket message for matchupdate', message, err);
            }
        }
    }

    getTournamentData() {
        const obsbls = [];
        for (let c = 0; c < this.categoryIds.length; c++) {
            const catId = this.categoryIds[c];
            obsbls.push(this.matchdbService.getTournamentData(this.tournamentId, catId));
        }
        observableForkJoin(obsbls).subscribe(
            responses => {
                for (let r = 0; r < responses.length; r++) {
                    const response = responses[r] as any;
                    if (response.success === true && response.result) {
                        this.myplayers[r] = response.result.players;
                        this.groups[r] = response.result.groups;
                        this.tournament = response.result.tournament;
                        this.category[r] = response.result.category;

                        // correct that all matches now have different PlayerRef objects than the group.players
                        for (let g = 0; g < this.groups[r].length; g++) {
                            const group = this.groups[r][g];
                            const plyrs = new Array(group.players.length);
                            for (let p = 0; p < group.players.length; p++) {
                                const groupPlayer = group.players[p];
                                plyrs[groupPlayer.playerListIdx] = groupPlayer;
                            }
                            for (let m = 0; m < group.matches.length; m++) {
                                const match = group.matches[m];
                                // get the player with the same playerListIdx
                                match.player1 = plyrs[match.player1.playerListIdx];
                                match.player2 = plyrs[match.player2.playerListIdx];
                            }
                        }
                        this.ko[r] = response.result.ko;
                        this.ko_b[r] = response.result.ko_b;

                    } else if (!response.result) {
                        this.logger.trace('no stored data found for', this.tournamentId);

                    } else {
                        this.logger.error('Could not retrieve tournament data:', response.error);
                        this.alertService.error($localize`Could not retrieve tournament data`, response.error);
                    }
                }

                this.getMatches();

            },
            error => {
                this.logger.error('Could not retrieve tournament data:', error);
                this.alertService.error($localize`Could not retrieve tournament data`, error);
            }
        );
    }

    getMatches() {
        this.matchdbService.getMatches(this.tournamentId).subscribe(
            response => {
                if (response.success === true) {
                    // result: {
                    //     category: string,
                    //     matches: Match[],
                    //     players: {last_name: string, first_name: string, partner: {last_name: string, first_name: string}}[]
                    // }[];
                    this.mymatches = [];
                    this.finishedMatches = [];
                    for (let r = 0; r < response.result.length; r++) {
                        const catName = response.result[r].category;
                        const catId = response.result[r].categoryId;
                        let catIdx;
                        for (let c = 0; c < this.categoryNames.length; c++) {
                            if (this.categoryNames[c] === catName) {
                                catIdx = c;
                                break;
                            }
                        }
                        if (typeof catIdx === 'undefined') {
                            this.logger.error('match category with name', catName, 'not found in tournament data', this.categoryNames);
                            this.alertService.error($localize`match category with name ${catName}:category: not found in tournament data`, JSON.stringify(this.categoryNames));
                            return;
                        }

                        const matches = response.result[r].matches;
                        for (let m = 0; m < matches.length; m++) {
                            const match = matches[m];
                            let ko = this.ko[catIdx];
                            if (match.player1.koIdx) {
                                if (match.player1.koIdx.ko_type === 'B') {
                                    ko = this.ko_b[catIdx];
                                }
                            }
                            const pIdx = PlayerRef.getPlayerListIdx(this.logger, match.player1, ko, this.groups[catIdx]);
                            const player1 = this.myplayers[catIdx][pIdx];
                            const pIdx2 = PlayerRef.getPlayerListIdx(this.logger, match.player2, ko, this.groups[catIdx]);
                            const player2 = this.myplayers[catIdx][pIdx2];

                            if (player1 && player2) {
                                // TODO extract from player
                                const partner1 = player1.partner ? player1.partner['first_name'] + ' ' + player1.partner['last_name'] : undefined;
                                const partner2 = player2.partner ? player2.partner['first_name'] + ' ' + player2.partner['last_name'] : undefined;

                                const mymatch = {category: catName, catId: catId, matchNr: match.matchNr, score: match.score,
                                    player1: player1.first_name + ' ' + player1.last_name, partner1: partner1,
                                    player2: player2.first_name + ' ' + player2.last_name, partner2: partner2,
                                    match: match
                                };
                                if (Match.checkWinner(match) === 0) {
                                    this.mymatches.push(mymatch);
                                } else {
                                    this.finishedMatches.push(mymatch);
                                }
                            }
                        }
                    }
                    this.filteredMyMatches = this.mymatches;
                    this.loading = false;

                } else {
                    this.logger.error('Could not retrieve tournament:', response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve tournament:', error);
            }
        );
    }

    onScoreChange(mymatch: MyMatch) {
        this.logger.debug('onScoreChange', mymatch);
        const match = mymatch.match;
        const winner = Match.checkWinner(match);

        let catIdx;
        for (let c = 0; c < this.categoryNames.length; c++) {
            if (this.categoryNames[c] === mymatch.category) {
                catIdx = c;
                break;
            }
        }
        if (typeof catIdx === 'undefined') {
            this.logger.error('match category with name', mymatch.category, 'not found in tournament data', this.categoryNames);
            this.alertService.error($localize`match category with name ${mymatch.category}:category: not found in tournament data`, JSON.stringify(this.categoryNames));
            return;
        }
        let ko = this.ko[catIdx];
        if (match.player1.koIdx && match.player1.koIdx.ko_type === 'B') {
            ko = this.ko_b[catIdx];
        }

        if (winner === -1) {
            if (!match.winner) {
                match.winner = match.player1;
            } else {
                // don't do winner = player1 since we want to keep the object reference the same
                PlayerRef.update(this.logger, match.winner, match.player1, ko, this.groups[catIdx], false);
            }
        } else if (winner === 1) {
            if (!match.winner) {
                match.winner = match.player2;
            } else {
                PlayerRef.update(this.logger, match.winner, match.player2, ko, this.groups[catIdx], false);
            }
        } else {
            if (!match.winner) {
                match.winner = new PlayerRef();
            } else {
                PlayerRef.update(this.logger, match.winner, new PlayerRef());
            }
        }
        mymatch.score = mymatch.match.score;

        // store in DB
        this.matchdbService.updateMatchWithSubscribe(this.tournamentId, this.categoryIds[catIdx], match,
            this.myplayers[catIdx], this.groups[catIdx], this.ko[catIdx], this.ko_b[catIdx], this.matchSavedSuccessfully);

        if (match.player1.koIdx) {
            this.socketService.send(SockEvent.KOMATCHUPDATE, this.tournamentId, this.categoryIds[catIdx], JSON.stringify(match));
        } else {
            this.socketService.send(SockEvent.GROUPMATCHUPDATE, this.tournamentId, this.categoryIds[catIdx], JSON.stringify(match));
        }

        // remove from list if finished
        if (Match.checkWinner(match) !== 0) {
            const idx = this.mymatches.indexOf(mymatch);
            if (idx >= 0 && idx < this.mymatches.length) {
                this.mymatches.splice(idx, 1);
                this.finishedMatches.push(mymatch);
                this.filterChanged();
            }
        }
    }

    getLevel(matchNr): string {
        if (matchNr.ko_type === 'G') {
            return String.fromCharCode(parseInt(matchNr.level, 10) + 65);
        } else {
            const lev = parseInt(matchNr.level, 10);
            if (lev === 0) {
                return 'Final';
            } else if (lev === 1) {
                return 'Semi-Finals';
            } else if (lev === 2) {
                return 'Quarter-Finals';
            } else {
                return 'Round of ' + 2 ** (lev + 1);
            }
        }
    }

    matchSavedSuccessfully(match: Match) {
        match.saved = true;
        setTimeout(() => delete match.saved, 3000);
    }
}
