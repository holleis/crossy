import { Match } from './match';

export class Ko {

    // number of levels, e.g. size == 3 for 8 players
    size: number;
    // e.g. for size == 3
    // matches[2] = [match, match, match, match]
    // matches[1] = [match, match]
    // matches[0] = [match]
    matches: Array<Array<Match>>;
    playerOrder: number[];

    constructor(numberOfPlayers: number = 8) {
        const l2 = Math.log2(numberOfPlayers);
        if (Math.trunc(l2) === l2) {
            // numberOfPlayers is power of 2, use log2
            this.size = l2;
        } else {
            // take log2 of next larger power of 2
            // tslint:disable-next-line:no-bitwise
            this.size = Math.log2((numberOfPlayers << 1) & (~numberOfPlayers));
        }
        // this.logger.debug('for', numberOfPlayers, 'players, we use bracket of size', this.size);
    }

    setMatches(newMatches) {
        this.matches = newMatches;
        this.size = this.matches.length;
    }
}
