import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { Tournament } from './tournament';
import { NGXLogger } from 'ngx-logger';
import { EnrollmentInfo } from './cat-info';
import { Category } from './category';
import { AlertService } from '../util/alert/alert.service';
import { Ranking } from '../ranking/ranking';
import { PickPlayerComponent } from './execute/pickplayer.component';
import { PlayerRef } from './playerref';
import { MatDialog } from '@angular/material/dialog';
import { MatchdbService } from './execute/matchdb.service';
import { RankingService } from '../ranking/ranking.service';
import { Match } from './match';
import { Ko } from './ko';
import { Group } from './group';
import { Player } from '../player/player';
import { DateTime } from 'luxon';
import { MunichcupGroupsComponent } from './execute/munichcup-groups/munichcup-groups.component';
import { Round } from './execute/munichcup/round';

export interface Entry {
    rank: number;
    player: Player;
    partner: Player;
    points: number;
}

export interface RankingEntry {
    updated?: string;
    rank: number;
    points: number;
    player: Player;
    tournament?: string;
    bracket?: string;
    nrPlayers?: number;
    abPlayed?: boolean;
    everyRank?: boolean;
    betweenRanks?: boolean;
}

export interface ResultInfo {
    tournament?: Tournament;
    maxPoints?: number;
    // rank & points for DM Quali ranking
    results?: Entry[];
    resultsFromOtherCats?;
    // rank & points for DCV ranking
    resultsRanking?: Entry[];
    resultsRankingFromOtherCats?;
    groups?: Group[];
    players?: EnrollmentInfo['players'];
    // rank & points for B-round
    results_b?: Entry[];
    category?: Category;
    ko?: Ko;
    ko_b?: Ko;
    isDouble?: boolean;
    doBRound?: boolean;
}

@Injectable()
export class TournamentsService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger,
        private alertService: AlertService,
        private dialog: MatDialog,
        private matchdbService: MatchdbService,
        private rankingService: RankingService,
    ) { }

    tournament: Tournament;
    newDraws: string[] = [];

    // temporary storage, info transfer
    setNewDraw(tournamentId: string, newDraw: boolean) {
        // ensure it is contained only once, even after multiple calls with newDraw===true
        this.newDraws = this.newDraws.filter(value => value !== tournamentId);
        if (newDraw) {
            this.newDraws.push(tournamentId);
        }
    }
    needNewDraw(tournamentId: string): boolean {
        if (this.newDraws.indexOf(tournamentId) !== -1) {
            // remove if true
            this.newDraws = this.newDraws.filter(value => value !== tournamentId);
            return true;
        } else {
            return false;
        }
    }

    getDrawnCategories(tournamentId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/drawnCategories/' + encodeURI(tournamentId);
        return this.http.get<{ 'success': boolean, 'result': { catIds: string[], catNames: string[], assignmentMode: string }, 'error': string }>(url);
    }

    getTournament(id: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/id/' + encodeURI(id);
        return this.http.get<{ 'success': boolean, 'result': Tournament, 'error': string }>(url);
    }

    getInfoFromTS(tournamentName: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/tsData/' + encodeURI(tournamentName);
        return this.http.get<{ 'success': boolean, 'result': any, 'error': string }>(url);
    }

    importPlayersFromTS(tournamentId: string, federation: string, ts_tid1: string, ts_tid2: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/import';
        return this.http.post<{ 'success': boolean, 'result': number, 'error': string }>(url, { tournamentId, federation, ts_tid1, ts_tid2 });
    }

    sendFileDataForRanking(tournamentId: string, federation: string, cat: string, content: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/importFileRanking';
        return this.http.post<{ 'success': boolean, 'result': number, 'error': string }>(url, { tournamentId, federation, cat, content });
    }

    importTSPointsToRanking(tournamentId: string, federation: string, ts_tid1: string, ts_tid2: string, dmQuali: boolean) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/importTSRanking';
        return this.http.post<{ 'success': boolean, 'result': number, 'error': string }>(url, { tournamentId, federation, ts_tid1, ts_tid2, dmQuali });
    }

    getFutureTournaments(onlyEnrollable: boolean) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/future/' + (onlyEnrollable ? '1' : '0');
        this.logger.trace('calling getFutureTournaments at', url);
        return this.http.get<{
            'success': boolean,
            'result': Tournament[], 'error': string
        }>(url);
    }

    getEnrollments(tournamentId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/enrollment/' + encodeURI(tournamentId);
        return this.http.get<{
            'success': boolean,
            'result': EnrollmentInfo[],
            'error': string
        }>(url);
    }

    getEnrollmentForCat(tournamentId: string, catId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/enrollmentforcat/' + encodeURI(tournamentId) + '/' + encodeURI(catId);
        return this.http.get<{
            'success': boolean,
            'result': EnrollmentInfo,
            'error': string
        }>(url);
    }

    getReassignments(tournamentId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/reassignments/' + encodeURI(tournamentId);
        return this.http.get<{
            'success': boolean,
            'result': { player: string, fromCat: { _id: string, name: string, cat_type: string, maximum_age: number }, toCat: { _id: string, name: string, cat_type: string, maximum_age: number } }[],
            'error': string
        }>(url);
    }

    resetReassignments(tournamentId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/reassignments/' + encodeURI(tournamentId);
        return this.http.delete<{
            'success': boolean,
            'result': string,
            'error': string
        }>(url);
    }

    addReassignment(tournamentId: string, playerId: string, fromCatId: string, toCatId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/reassignments/' + encodeURI(tournamentId);
        return this.http.post<{
            'success': boolean,
            'result': string,
            'error': string
        }>(url, { tournamentId: tournamentId, playerId: playerId, fromCatId: fromCatId, toCatId: toCatId });
    }

    getCategory(catId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/categories/' + encodeURI(catId);
        return this.http.get<{
            'success': boolean,
            'result': Category,
            'error': string
        }>(url);
    }

    createAnnouncement(tournament: Tournament, details): string {
        this.logger.debug('createAnnouncement', tournament);
        let ann = '';

        ann += `<p>${tournament.start_date} (${$localize`Registration Deadline`} ${tournament.registration_deadline})</p>`;
        ann += `<p>${tournament.location}, ${tournament.organiser.name}</p>`;
        ann += `<p>${$localize`Categories`}:`;

        // const startTag = '<p><em>';
        // const middleTag = '</em><b>';
        // const endTag = '</b></p>';

        // if (!tournament.end_date || tournament.start_date === tournament.end_date) {
        //     ann += startTag + 'on ' + middleTag + tournament.start_date + endTag + '\n';
        // } else {
        //     ann += startTag + 'from ' + middleTag + tournament.start_date + ' to ' + tournament.end_date + endTag + '\n';
        // }

        // if (tournament.registration_deadline) {
        //     ann += startTag + '(registration deadline: ' + middleTag + tournament.registration_deadline + ')' + endTag + '\n';
        // }

        // if (tournament.location) {
        //     ann += startTag + 'in ' + middleTag + tournament.location + endTag + '\n';
        // }

        // if (tournament.organiser) {
        //     // TODO check whether we have the .name here (see creation.service.ts)
        //     ann += startTag + 'organised by ' + middleTag + tournament.organiser.name + endTag + '\n';
        // }

        if (tournament.categories && tournament.categories.length > 0) {
            tournament.categories.sort(function (c1: { _id, name }, c2: { _id, name }) {
                const str1 = c1.name;
                const str2 = c2.name;
                if (str1 === 'DCV Open' || str1 === 'DCV Damen') {
                    return -10;
                }
                if (str2 === 'DCV Open' || str2 === 'DCV Damen') {
                    return 5;
                }
                return str1 < str2 ? -1 : (str1 > str2 ? 1 : 0);
            });
            // ann += startTag + 'You can play in these categories' + middleTag + endTag + '\n';
            const cats = tournament.categories;
            this.logger.trace('cats', cats);

            if (tournament.categories.length > 5) {
                ann += '<p>';
                for (let i = 0; i < cats.length; i++) {
                    ann += cats[i].name;
                    if (i < cats.length - 1) {
                        ann += ', ';
                    }
                }
                ann += '</p>\n';
            } else {
                ann += '<ul>';
                for (let i = 0; i < cats.length; i++) {
                    ann += '<li>' + cats[i].name + '</li>';
                }
                ann += '</ul>\n';
            }
            if (tournament.organiser.name === '*') {
                ann += ' ... and probably some doubles, see \
                    <a href="https://ico.tournamentsoftware.com/" target="_blank">Tournament Software</a>';
            }
        }

        return ann + '<br/>' + (details || '');
    }

    getLooserOf(match: Match, ko: Ko, players: EnrollmentInfo['players'], groups: Group[]) {
        // TODO treat cases of isBye and undefined

        if (PlayerRef.isBye(match.player1) || PlayerRef.isBye(match.player2)) {
            return undefined;
        }

        if (typeof match.winner.playerListIdx === 'undefined') {
            return undefined;
        }

        if (PlayerRef.getPlayerListIdx(this.logger, match.player1, ko, groups) === match.winner.playerListIdx) {
            return players[PlayerRef.getPlayerListIdx(this.logger, match.player2, ko, groups)];
        } else {
            return players[PlayerRef.getPlayerListIdx(this.logger, match.player1, ko, groups)];
        }
    }

    getPointRatio(nrPlayersRange: number, place: number): number {
        if (place === 1) {
            return 1;
        }
        switch (nrPlayersRange) {
            case 5:
                switch (place) {
                    case 2: return 0.5;
                    case 3: return 0.25;
                    case 4: return 0.125;
                    case 5: return 0.05;
                    default:
                        this.logger.error('getPointRatio: for 5 players got', place);
                        return 0.05;
                }
            case 10:
                switch (place) {
                    case 2: return 0.67;
                    case 3: case 4: return 0.34;
                    case 5: return 0.075;
                    default: return 0.05;
                }
            case 19:
                switch (place) {
                    case 2: return 0.75;
                    case 3: case 4: return 0.5;
                    case 5: case 6: case 7: case 8: return 0.250;
                    case 9: return 0.075;
                    default: return 0.05;
                }
            case 35:
                if (place === 2) { return 0.8; } else
                    if (place === 3 || place === 4) { return 0.6; } else
                        if (place >= 5 && place <= 8) { return 0.4; } else
                            if (place >= 9 && place <= 16) { return 0.2; } else
                                if (place === 17) { return 0.075; } else { return 0.05; }
            case 67:
                if (place === 2) { return 0.835; } else
                    if (place === 3 || place === 4) { return 0.67; } else
                        if (place >= 5 && place <= 8) { return 0.505; } else
                            if (place >= 9 && place <= 16) { return 0.34; } else
                                if (place >= 17 && place <= 32) { return 0.175; } else
                                    if (place === 33) { return 0.075; } else { return 0.05; }
            case 131:
                if (place === 2) { return 0.858; } else
                    if (place === 3 || place === 4) { return 0.715; } else
                        if (place >= 5 && place <= 8) { return 0.573; } else
                            if (place >= 9 && place <= 16) { return 0.43; } else
                                if (place >= 17 && place <= 32) { return 0.288; } else
                                    if (place >= 33 && place <= 64) { return 0.145; } else
                                        if (place === 65) { return 0.075; } else { return 0.05; }
            default:
                this.logger.error('getPointRatio: unknown nrPlayersRange', nrPlayersRange);
                return 0.05;
        }
    }

    getPoints(place: number, nrPlayers: number, b_round: boolean, dmqualiScheme: boolean, isDM: boolean, maxPoints: number, abStyle = false): number {
        if (!dmqualiScheme && isDM) {
            // no points for the Deutsche Meisterschaft for national ranking (but for the DM qualification ranking)
            return 0;
        }

        let maxpoints = maxPoints;
        if (dmqualiScheme && maxpoints !== 50) {
            maxpoints = 300;
        }

        if (dmqualiScheme && !b_round) {
            // TODO allranks played ("every")?
            // TODO "between"?
            return this.rankingService.calcPointsA(place, nrPlayers, false, false, abStyle, maxpoints);
            // if (place === 1) return 300;
            // else if (place === 2) return 251;
            // else if (place === 3 || place === 4) return 214;
            // else if (place <= 8) return 182;
            // else if (place <= 12) return 150;
            // else if (place <= 16) return 126;
            // // use B-Scheme for DM Quali tournaments that didn't use A/B mode (e.g. DM)
            // else if (place === 17) return 100;
            // else if (place === 18) return 90;
            // else if (place === 19 || place === 20) return 85;
            // else if (place <= 24) return 76;
            // else if (place <= 28) return 63;
            // else if (place <= 32) return 53;
            // else return 26;

            // ICO 250 also generate 300 points for DM Quali (2018-09-20)
            // } else if (maxpoints === 250) {
            //     // ICO A-scheme
            //     if (place === 1) return 250;
            //     else if (place === 2) return 209;
            //     else if (place === 3 || place === 4) return 178;
            //     else if (place <= 8) return 152;
            //     else if (place <= 12) return 125;
            //     else if (place <= 16) return 105;
            //     // use B-Scheme for ICO DM Quali tournaments (they don't use A/B mode (e.g. DM))
            //     else if (place === 17) return 100;
            //     else if (place === 18) return 90;
            //     else if (place === 19 || place === 20) return 85;
            //     else if (place <= 24) return 76;
            //     else if (place <= 28) return 63;
            //     else if (place <= 32) return 53;
            //     else return 26;
            // } else {
            //     this.logger.error('Unknown number of points for tournament:', maxpoints, '- cannot calculate points');
            //     return 0;
            // }
        }
        if (b_round) {
            // TODO allranks played ("every")?
            return this.rankingService.calcPointsB(place, nrPlayers, false);
            // if (place === 1) return 100;
            // else if (place === 2) return 90;
            // else if (place === 3 || place === 4) return 85;
            // else if (place <= 8) return 76;
            // else if (place <= 12) return 63;
            // else if (place <= 16) return 53;
            // else return 26;
        }

        // TODO check and distinguish between DCV and ICO (http://crossminton.org/wp-content/uploads/2015/12/Point_distribution_for_international_rankings_2016-2.pdf)
        // 98% equal to http://crossminton.de/download/PunkteverteilungNational.pdf
        if (nrPlayers <= 3) {
            return 0;
        } else if (nrPlayers <= 5) {
            return this.correctPoints(Math.round(this.getPointRatio(5, place) * maxpoints), place, nrPlayers);
        } else if (nrPlayers <= 10) {
            return this.correctPoints(Math.round(this.getPointRatio(10, place) * maxpoints), place, nrPlayers);
        } else if (nrPlayers <= 19) {
            return this.correctPoints(Math.round(this.getPointRatio(19, place) * maxpoints), place, nrPlayers);
        } else if (nrPlayers <= 35) {
            return this.correctPoints(Math.round(this.getPointRatio(35, place) * maxpoints), place, nrPlayers);
        } else if (nrPlayers <= 67) {
            return this.correctPoints(Math.round(this.getPointRatio(67, place) * maxpoints), place, nrPlayers);
        } else if (nrPlayers <= 131) {
            return this.correctPoints(Math.round(this.getPointRatio(131, place) * maxpoints), place, nrPlayers);
        }

        this.logger.error('no idea how many points for', nrPlayers, 'players!');
        return 0;
    }

    correctPoints(points: number, _place: number, _nrPlayers: number) {
        // 300 pts DCV tournament 3rd group place
        if (points === 23) { return 24; }

        return points;
    }

    calculateResults(tournamentId: string, infos: ResultInfo, callback?: (infos: ResultInfo) => void) {
        if (!infos.groups) {
            this.matchdbService.getTournamentData(tournamentId, infos.category._id).subscribe({
                next: response => {
                    if (response.success === true && response.result) {
                        infos.groups = response.result.groups;
                        if (!infos.players) {
                            infos.players = response.result.players;
                        }
                        this.doCalculateResults(infos, callback);
                    }
                },
                error: error => {
                    this.logger.error('Could not retrieve tournament data:', error);
                    this.alertService.error($localize`Could not retrieve tournament data`, error);
                }
            });
        } else {
            if (infos.tournament?.points) {
                this.doCalculateResults(infos, callback);

            } else {
                this.getTournament(tournamentId).subscribe({
                    next: response => {
                        this.logger.trace('retrieve torunament for canMakeChanges (responsibles)', tournamentId, response);
                        if (response.success === true) {
                            infos.tournament = response.result;
                            if (!infos.groups) {
                                infos.groups = infos.tournament['groups'];
                            }
                            this.doCalculateResults(infos, callback);
                        } else {
                            this.logger.error('Could not retrieve tournament for canMakeChanges:', response.error);
                            this.alertService.error($localize`Could not retrieve tournament:`, response.error);
                            return;
                        }
                    },
                    error: error => {
                        this.logger.error('Could not retrieve tournament for canMakeChanges:', error);
                        this.alertService.error($localize`Could not retrieve tournament:`, error);
                        return;
                    }
                });
            }
        }
    }

    doCalculateResults(infos: ResultInfo, callback?: (infos: ResultInfo) => void) {
        infos.results = [];
        infos.results_b = [];
        infos.resultsFromOtherCats = {};
        infos.resultsRanking = [];
        infos.resultsRankingFromOtherCats = {};

        if (infos.tournament && infos.tournament.points) {
            infos.maxPoints = infos.tournament.points * 1.0;
        } else {
            this.logger.error('cannot read tournament points ', infos.tournament);
        }

        // let organiser assign 0 points to excused players
        // first: see if any player never got any points
        const notZeroPlayers = [];
        if (infos.groups?.length) {
            for (let g = 0; g < infos.groups.length; g++) {
                const group = infos.groups[g];
                for (let m = 0; m < group.matches.length; m++) {
                    const match = group.matches[m];
                    if (match.score[0].p1 > 0 || match.score[1].p1 > 0 || match.score[2].p1 > 0) {
                        const pIdx = PlayerRef.getPlayerListIdx(this.logger, match.player1);
                        if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                            const p = infos.players[pIdx];
                            if (notZeroPlayers.indexOf(p) < 0) {
                                notZeroPlayers.push(p);
                            }
                        }
                    }
                    if (match.score[0].p2 > 0 || match.score[1].p2 > 0 || match.score[2].p2 > 0) {
                        const pIdx = PlayerRef.getPlayerListIdx(this.logger, match.player2);
                        if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                            const p = infos.players[pIdx];
                            if (notZeroPlayers.indexOf(p) < 0) {
                                notZeroPlayers.push(p);
                            }
                        }
                    }
                }
            }
        } else {
            // only KO
            for (let k1 = 0; k1 < infos.ko.matches.length; k1++) {
                const level = infos.ko.matches[k1];
                for (let m = 0; m < level.length; m++) {
                    const match = level[m];
                    if (match.score[0].p1 > 0 || match.score[1].p1 > 0 || match.score[2].p1 > 0) {
                        const pIdx = PlayerRef.getPlayerListIdx(this.logger, match.player1, infos.ko);
                        if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                            const p = infos.players[pIdx];
                            if (notZeroPlayers.indexOf(p) < 0) {
                                notZeroPlayers.push(p);
                            }
                        }
                    }
                    if (match.score[0].p2 > 0 || match.score[1].p2 > 0 || match.score[2].p2 > 0) {
                        const pIdx = PlayerRef.getPlayerListIdx(this.logger, match.player2, infos.ko);
                        if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                            const p = infos.players[pIdx];
                            if (notZeroPlayers.indexOf(p) < 0) {
                                notZeroPlayers.push(p);
                            }
                        }
                    }
                }
            }
        }
        const potentialZeroPlayers = [];
        if (notZeroPlayers.length !== infos.players.length) {
            for (let p = 0; p < infos.players.length; p++) {
                if (notZeroPlayers.indexOf(infos.players[p]) < 0) {
                    potentialZeroPlayers.push(infos.players[p]);
                }
            }
        }
        this.logger.debug(potentialZeroPlayers.length, 'players have never scored any points in', infos.category);

        // second: ask organiser which of those players should be assigned 0 points
        let zeroPlayers = potentialZeroPlayers.slice();
        if (potentialZeroPlayers.length > 0) {
            const dialogRef = this.dialog.open(PickPlayerComponent, {
                data: { players: potentialZeroPlayers, selectedPlayers: zeroPlayers }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    for (let zp = 0; zp < result.length; zp++) {
                        const zPlayer = result[zp];
                        this.logger.debug('setting points of', zPlayer.last_name, zPlayer.first_name, 'to zero for category', infos.category);
                    }
                    zeroPlayers = result.map((zp: Player) => zp._id);
                }
                this.dodoCalculateResults(zeroPlayers, infos, callback);
            });
        } else {
            this.dodoCalculateResults(zeroPlayers, infos, callback);
        }
    }

    calculateMunichCupGroupsRanking(infos: ResultInfo): any[] {
        // TODO this repeats code of MunichcupGroupsComponent.getRanking() !!

        const allPlayersCopy = infos.players.slice();

        const ranking = [];
        if (infos.groups?.length !== 10) {
            return [];
        }

        // this is the final round
        // draw breaking is done by looking at all matches that involved exactly those players

        // initial sort by overall set points
        allPlayersCopy.sort((p1, p2) => p2.points - p1.points);

        // convert Group[] into Round[]
        const rounds: Round[] = [];
        for (let g = 0; g < infos.groups.length; g++) {
            const group = infos.groups[g];
            rounds.push({ sortedPlayers: undefined, matches: group.matches });
        }

        // // find all with same points
        // let rank = 1;
        // let jumped = 1;
        // let lastpoints = -1;
        for (let p = 0; p < allPlayersCopy.length; p++) {
            // (allPlayersCopy[p] as any).hasPlayed = this.hasPlayed(allPlayersCopy[p]);

            ranking.push({
                // first_name: allPlayersCopy[p].first_name,
                // last_name: allPlayersCopy[p].last_name,
                // _id: allPlayersCopy[p]._id,
                player: allPlayersCopy[p],
                // rank: rank,
                points: allPlayersCopy[p].points,
                hasPlayed: (allPlayersCopy[p] as any).hasPlayed,
                calc: {
                    overallGamesWon: MunichcupGroupsComponent.getOverallGames(true, infos.players, allPlayersCopy[p]._id, rounds),
                    overallGamesLost: MunichcupGroupsComponent.getOverallGames(false, infos.players, allPlayersCopy[p]._id, rounds),
                    // overallSetsWon: this.getOverallSets(true, allPlayersCopy, allPlayersCopy[p]._id),
                    overallSetsLost: MunichcupGroupsComponent.getOverallSets(false, infos.players, allPlayersCopy[p]._id, rounds),
                    overallPointsWon: MunichcupGroupsComponent.getOverallPoints(true, infos.players, allPlayersCopy[p]._id, rounds),
                    overallPointsLost: MunichcupGroupsComponent.getOverallPoints(false, infos.players, allPlayersCopy[p]._id, rounds),
                },
            });
            // lastpoints = allPlayersCopy[p].points;
        }
        ranking.sort((p1, p2) => {
            if (p1.points === p2.points) {
                if (p1.calc.overallGamesWon === p2.calc.overallGamesWon) {
                    if (p1.calc.overallGamesLost === p2.calc.overallGamesLost) {
                        if (p1.calc.overallSetsLost === p2.calc.overallSetsLost) {
                            if (p1.calc.overallPointsWon === p2.calc.overallPointsWon) {
                                // if (p1.calc.overallPointsLost === p2.calc.overallPointsLost) {
                                //     if (p1.calc.directGamesWon === p2.calc.directGamesWon) {
                                //         return 0;
                                //     } else {
                                //         return p2.calc.directGamesWon - p1.calc.directGamesWon;
                                //     }
                                // } else {
                                return p1.calc.overallPointsLost - p2.calc.overallPointsLost;
                                // }
                            } else {
                                return p2.calc.overallPointsWon - p1.calc.overallPointsWon;
                            }
                        } else {
                            return p1.calc.overallSetsLost - p2.calc.overallSetsLost;
                        }
                    } else {
                        return p1.calc.overallGamesLost - p2.calc.overallGamesLost;
                    }
                } else {
                    return p2.calc.overallGamesWon - p1.calc.overallGamesWon;
                }
            } else {
                return p2.points - p1.points;
            }
        });
        ranking.forEach((r, idx) => {
            r.rank = idx + 1;
        });
        return ranking;
    }

    dodoCalculateResults(zeroPlayers: string[], infos: ResultInfo, callback?: (infos: ResultInfo) => void) {
        // decide which rankings should be displayed
        const isDMQuali = this.rankingService.isRankingTournament(infos.tournament, [infos.category]);
        const isDM = infos.tournament?.title.indexOf('Deutsche Einzel-Meisterschaft') >= 0 || infos.tournament?.title.indexOf('Deutsche Doppel-Meisterschaft') >= 0;

        // normally true as DCV same as DM Quali (only false for 50 points tournaments):
        // update 2024: German 50 points tournament count as 50 points for DMQuali
        // const dmqualiScheme = infos.tournament.points !== 50;
        const dmqualiScheme = true;

        // KO (A-round)
        // overall winner KO
        let resultCnt = 0;
        let resultCntRanking = 0;
        let place = 1;
        if (infos.tournament.tournament_type === 'MunichCup (Gruppen)') {
            const ranking = this.calculateMunichCupGroupsRanking(infos);
            for (let p = 0; p < ranking.length; p++) {
                const entry = ranking[p];
                const points = this.getPoints(entry.rank, ranking.length, false, dmqualiScheme, isDM, infos.maxPoints, false);
                // "resultsRanking" since results is for dmQuali
                // MunichCup does not count for dmQuali
                infos.resultsRanking[resultCnt++] = { rank: entry.rank, player: entry.player, partner: undefined, points };
            }
        } else {
            if (infos.ko?.matches?.length) {
                const pIdx = infos.ko.matches[0][0].winner.playerListIdx;
                if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                    const p = infos.players[pIdx];
                    let pp: Player;
                    if (infos.isDouble) {
                        pp = p.partner as unknown as Player;
                    }
                    if (isDMQuali) {
                        infos.results[resultCnt++] = { rank: 1, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints, infos.tournament.tournament_type === 'DCV') };
                    }
                    infos.resultsRanking[resultCntRanking++] = { rank: 1, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                }

                // losers of each stage will be 2nd, 3rd, 5th, 9th, 17th, ...
                place = 2;
                for (let i = 0; i < infos.ko.matches.length; i++) {
                    for (let m = 0; m < infos.ko.matches[i].length; m++) {
                        const match = infos.ko.matches[i][m];
                        const p = this.getLooserOf(match, infos.ko, infos.players, infos.groups);
                        if (p) {
                            let pp: Player;
                            if (infos.isDouble) {
                                pp = p.partner;
                            }
                            if (isDMQuali) {
                                infos.results[resultCnt++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                            }
                            infos.resultsRanking[resultCntRanking++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                        }
                    }
                    place += (2 ** i);
                }
            } else {
                // only group phase; (prob. juniors), 1 group of 4 or 5
                for (let placeIdx = 0; placeIdx < 2; placeIdx++) {
                    let placesToAdd = 0;
                    for (let g = 0; g < infos.groups.length; g++) {
                        const group = infos.groups[g];
                        if (placeIdx < group.groupSize) {
                            placesToAdd += 1;
                            const pIdx = group.winners[placeIdx].playerListIdx;
                            if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                                const p = infos.players[pIdx];
                                let pp: Player;
                                if (infos.isDouble) {
                                    pp = p.partner;
                                }
                                if (isDMQuali) {
                                    infos.results[resultCnt++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                                }
                                infos.resultsRanking[resultCntRanking++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                            }
                        }
                    }
                    place += placesToAdd;
                }
            }

            // 3rd, 4th, and 5th in group
            if (!infos.doBRound) {
                for (let placeIdx = 2; placeIdx < 5; placeIdx++) {
                    let placesToAdd = 0;
                    for (let g = 0; g < infos.groups.length; g++) {
                        const group = infos.groups[g];
                        if (placeIdx < group.groupSize) {
                            placesToAdd += 1;
                            const pIdx = group.winners[placeIdx].playerListIdx;
                            if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                                const p = infos.players[pIdx];
                                let pp: Player;
                                if (infos.isDouble) {
                                    pp = p.partner;
                                }
                                if (isDMQuali) {
                                    infos.results[resultCnt++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                                }
                                infos.resultsRanking[resultCntRanking++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, false, dmqualiScheme, isDM, infos.maxPoints) };
                            }
                        }
                    }
                    place += placesToAdd;
                }
            }

            // B-Round
            if (infos.doBRound) {
                // overall winner KO
                resultCnt = 0;
                const pIdx = infos.ko_b.matches[0][0].winner.playerListIdx;
                if (typeof pIdx !== 'undefined' && pIdx >= 0 && pIdx < infos.players.length) {
                    const p = infos.players[pIdx];
                    let pp;
                    if (infos.isDouble) {
                        pp = p.partner;
                    }
                    if (isDMQuali) {
                        infos.results_b[resultCnt++] = { rank: 1, player: p, partner: pp, points: this.getPoints(1, infos.players.length, true, dmqualiScheme, isDM, infos.maxPoints) };
                    }
                    infos.resultsRanking[resultCntRanking++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, true, dmqualiScheme, isDM, infos.maxPoints) };
                }

                // losers of each stage will be 2nd, 3rd, 5th, 9th, 17th, ...
                let rankingplace = place + 1;
                place = 2;
                for (let i = 0; i < infos.ko_b.matches.length; i++) {
                    for (let m = 0; m < infos.ko_b.matches[i].length; m++) {
                        const match = infos.ko_b.matches[i][m];
                        const p = this.getLooserOf(match, infos.ko_b, infos.players, infos.groups);
                        if (p) {
                            let pp: Player;
                            if (infos.isDouble) {
                                pp = p.partner;
                            }
                            if (isDMQuali) {
                                infos.results_b[resultCnt++] = { rank: place, player: p, partner: pp, points: this.getPoints(place, infos.players.length, true, dmqualiScheme, isDM, infos.maxPoints) };
                            }
                            infos.resultsRanking[resultCntRanking++] = { rank: rankingplace, player: p, partner: pp, points: this.getPoints(rankingplace, infos.players.length, true, dmqualiScheme, isDM, infos.maxPoints) };
                        }
                    }
                    place += (2 ** i);
                    rankingplace += (2 ** i);
                }
            }

            for (let r = 0; r < infos.results.length; r++) {
                const res = infos.results[r];
                if (zeroPlayers.indexOf(res.player._id) >= 0) {
                    res.points = 0;
                    res.rank = 900;
                }
            }
            for (let r = 0; r < infos.resultsRanking.length; r++) {
                const res = infos.resultsRanking[r];
                if (zeroPlayers.indexOf(res.player._id) >= 0) {
                    res.points = 0;
                    res.rank = 900;
                }
            }
            for (let r = 0; r < infos.results_b.length; r++) {
                const res = infos.results_b[r];
                if (zeroPlayers.indexOf(res.player._id) >= 0) {
                    res.points = 0;
                    res.rank = 900;
                }
            }

            infos.resultsRanking.sort((r1, r2) => (r2.points - r2.rank) - (r1.points - r1.rank));
            infos.results.sort((r1, r2) => (r2.points - r2.rank) - (r1.points - r1.rank));
            infos.results_b.sort((r1, r2) => (r2.points - r2.rank) - (r1.points - r1.rank));
        }

        const sendResults = [];
        for (let r = 0; r < infos.results.length; r++) {
            const result = infos.results[r];
            const partner = result.partner ? result.partner._id : undefined;
            sendResults.push({ rank: result.rank, player: result.player._id, partner: partner, points: result.points });
        }
        const sendResultsRanking = [];
        for (let r = 0; r < infos.resultsRanking.length; r++) {
            const partner = infos.resultsRanking[r].partner ? infos.resultsRanking[r].partner._id : undefined;
            sendResultsRanking.push({ rank: infos.resultsRanking[r].rank, player: infos.resultsRanking[r].player._id, partner: partner, points: infos.resultsRanking[r].points });
        }
        const sendResults_b = [];
        for (let r = 0; r < infos.results_b.length; r++) {
            const partner = infos.results_b[r].partner ? infos.results[r].partner._id : undefined;
            sendResults_b.push({ rank: infos.results_b[r].rank, player: infos.results_b[r].player._id, partner: partner, points: infos.results_b[r].points });
        }
        this.matchdbService.updateResults(infos.tournament._id, infos.category._id, sendResults, sendResults_b, sendResultsRanking).subscribe({
            next: response => {
                if (response.success === true) {
                    this.logger.debug('results stored ok');
                } else {
                    this.logger.error('Could not update results in the cloud!', response.error);
                    this.alertService.error($localize`Could not update results in the cloud!`, response.error);
                }
            },
            error: error => {
                this.logger.error('Could not update results in the cloud!', error);
                this.alertService.error($localize`Could not update results in the cloud!`, error);
            }
        });

        // get info for players who played in a different category than enrolled
        this.getReassignments(infos.tournament._id).subscribe({
            next: response => {
                if (response.success === true) {
                    // ra: [{player, fromCat: {_id, name}, toCat: {_id, name}}]
                    let reassignments = response.result;
                    reassignments = reassignments.filter(r => r.toCat._id === infos.category._id.toString());
                    if (reassignments.length > 0) {
                        // have some players who have been assigned from another to this category
                        for (let r = 0; r < reassignments.length; r++) {
                            const ra = reassignments[r];
                            let entry;
                            let isBRound = false;
                            for (let i = 0; i < infos.results.length; i++) {
                                if (infos.results[i].player._id === ra.player) {
                                    entry = infos.results[i];
                                    break;
                                }
                            }
                            for (let i = 0; !entry && i < infos.results_b.length; i++) {
                                if (infos.results_b[i].player._id === ra.player) {
                                    entry = infos.results_b[i];
                                    isBRound = true;
                                    break;
                                }
                            }
                            if (entry) {
                                if (!infos.resultsFromOtherCats[ra.fromCat.name]) {
                                    infos.resultsFromOtherCats[ra.fromCat.name] = [];
                                }
                                infos.resultsFromOtherCats[ra.fromCat.name].push({ rank: entry.rank, player: entry.player, partner: entry.partner, points: entry.points, bround: isBRound, cat_type: ra.fromCat.cat_type, maximum_age: ra.fromCat.maximum_age });
                                this.logger.debug('assigning', entry.points, 'points to', entry.player.last_name, entry.player.first_name, 'also for category', ra.fromCat._id);
                            }
                        }
                        for (const res of Object.getOwnPropertyNames(infos.resultsFromOtherCats)) {
                            infos.resultsFromOtherCats[res].sort((r1, r2) => (r2.points - r2.rank * 100000) - (r1.points - r1.rank * 100000));
                        }

                        // same for ranking for normal ranking (not DM Quali)
                        for (let r = 0; r < reassignments.length; r++) {
                            const ra = reassignments[r];
                            let entry;
                            for (let i = 0; i < infos.resultsRanking.length; i++) {
                                if (infos.resultsRanking[i].player._id === ra.player) {
                                    entry = infos.resultsRanking[i];
                                    break;
                                }
                            }
                            if (entry) {
                                if (!infos.resultsRankingFromOtherCats[ra.fromCat.name]) {
                                    infos.resultsRankingFromOtherCats[ra.fromCat.name] = [];
                                }
                                infos.resultsRankingFromOtherCats[ra.fromCat.name].push({ rank: entry.rank, player: entry.player, partner: entry.partner, points: entry.points, bround: false, cat_type: ra.fromCat.cat_type, maximum_age: ra.fromCat.maximum_age });
                                this.logger.debug('assigning', entry.points, 'points to', entry.player.last_name, entry.player.first_name, 'also for category', ra.fromCat._id);
                            }
                        }
                        for (const res of Object.getOwnPropertyNames(infos.resultsRankingFromOtherCats)) {
                            infos.resultsRankingFromOtherCats[res].sort((r1, r2) => (r2.points - r2.rank * 100000) - (r1.points - r1.rank * 100000));
                        }
                    }
                    if (typeof callback === 'function') {
                        callback(infos);
                    }

                } else {
                    this.logger.error('Could not get reassignments!', response.error);
                    this.alertService.error($localize`Could not get reassignments!`, response.error);
                }
            },
            error: error => {
                this.logger.error('Could not get reassignments!', error);
                this.alertService.error($localize`Could not get reassignments!`, error);
            }
        });
    }

    translateICOtoDCV(catName: string, forDMQuali = true, year: string): string {
        switch (catName) {
            case 'ICO Women':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Damen`;
            case 'ICO Women O40':
            case 'ICO O40 Women':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Ü40 Damen`;
            case 'ICO Women O50':
            case 'ICO O50 Women':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Ü50 Damen`;
            case 'ICO Women O60':
            case 'ICO O60 Women':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Ü60 Damen`;
            case 'ICO Open':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Open`;
            case 'ICO Men O40':
            case 'ICO Seniors O40':
            case 'ICO O40 Men':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Ü40 Herren`;
            case 'ICO Men O50':
            case 'ICO Seniors O50':
            case 'ICO O50 Men':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Ü50 Herren`;
            case 'ICO Men O60':
            case 'ICO Seniors O60':
            case 'ICO O60 Men':
                return `${forDMQuali ? `DM Quali ${year}` : 'DCV'} Ü60 Herren`;
            default:
                if (forDMQuali) {
                    this.logger.warn('error, ICO category could not be mapped to DM Quali DCV: ' + catName);
                    return catName;
                }
                switch (catName) {
                    case 'ICO Seniors Doubles O40':
                        return 'DCV Ü40 Open Doppel';
                    case 'ICO Seniors Doubles O50':
                        return 'DCV Ü50 Open Doppel';
                    case 'ICO Seniors Doubles O60':
                        return 'DCV Ü60 Open Doppel';
                    case 'ICO Seniors Doubles O70':
                        return 'DCV Ü70 Open Doppel';
                    case 'ICO Open Doubles':
                        return 'DCV Open Doppel';
                    case 'ICO Female Doubles':
                        return 'DCV Damen Doppel';
                    case 'ICO Mixed Doubles':
                        return 'DCV Mixed Doppel';
                    case 'ICO U18 Male':
                        return 'DCV U18 Männlich';
                    case 'ICO U16 Male':
                        return 'DCV U16 Männlich';
                    case 'ICO U14 Male':
                        return 'DCV U14 Männlich';
                    case 'ICO U12 Male':
                        return 'DCV U12 Männlich';
                    case 'ICO U18 Female':
                        return 'DCV U18 Weiblich';
                    case 'ICO U16 Female':
                        return 'DCV U16 Weiblich';
                    case 'ICO U14 Female':
                        return 'DCV U14 Weiblich';
                    case 'ICO U12 Female':
                        return 'DCV U12 Weiblich';
                    case 'ICO U18 Female Doubles':
                        return 'DCV U18 Weiblich Doppel';
                    case 'ICO U18 Male Doubles': case 'ICO U18 Open Doubles':
                        return 'DCV U18 Open Doppel';
                    default:
                        this.logger.warn('error, ICO category could not be mapped to DCV: ' + catName);
                        return catName;
                }
        }
    }

    addPointsToRanking(infos: ResultInfo, isQuali: boolean): void {
        this.calculateResults(infos.tournament._id, infos, () => {
            if (isQuali && (!infos.results || infos.category.cat_type !== 'singles' || infos.category.maximum_age <= 18)) {
                // DM Quali: don't try adding doubles or junior categories
                return;
            }

            // convert normal into qualifying ranking
            let cat = infos.category.name;
            if (isQuali && infos.category.name.indexOf('DCV') === 0) {
                const year = new Date(infos.tournament.start_date_epoch).getFullYear().toString();
                cat = infos.category.name.replace('DCV', 'DM Quali ' + year);
            } else if (infos.category.name.indexOf('ICO') === 0) {
                const year = new Date(infos.tournament.start_date_epoch).getFullYear().toString();
                cat = this.translateICOtoDCV(infos.category.name, isQuali, year);
            } else if (!isQuali && infos.category.name.indexOf('DCV') !== 0) {
                this.logger.error('Could not add rankings for unknown category ', infos.category.name);
                this.alertService.error($localize`:@@could not add ranking for category:Could not add rankings for category ${infos.category.name.toString()}:category:`);
                return;
            }

            const results = isQuali ? infos.results : infos.resultsRanking;
            const toUpdate: Ranking[] = [];
            for (let r = 0; r < results.length; r++) {
                const entry = results[r];
                if (entry.points === 0) {
                    continue;
                }
                if (isQuali && !entry.player.nation?.includes('GER')) {
                    // player must be German for DM Quali
                    continue;
                }
                const ranking: Ranking = new Ranking();
                ranking.bracket = 'A';
                ranking.federation = infos.category.federation;
                ranking.category = cat as unknown as Category;
                ranking.tournament = infos.tournament.title;
                ranking.tournamentId = infos.tournament._id as unknown as Tournament;
                ranking.updated = (DateTime.fromFormat(infos.tournament.start_date, 'd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'd.MM.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.MM.yyyy')).toISODate();
                ranking.player = entry.player;
                ranking.points = entry.points;
                ranking.rank = entry.rank;

                this.logger.debug('adding', ranking);
                toUpdate.push(ranking);

                // no doubles for DM quali
                if (!isQuali && entry.partner) {
                    const ranking: Ranking = new Ranking();
                    ranking.bracket = 'A';
                    ranking.federation = infos.category.federation;
                    ranking.category = cat as unknown as Category;
                    ranking.tournament = infos.tournament.title;
                    ranking.tournamentId = infos.tournament._id as unknown as Tournament;
                    ranking.updated = (DateTime.fromFormat(infos.tournament.start_date, 'd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'd.MM.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.MM.yyyy')).toISODate();
                    ranking.player = entry.partner;
                    ranking.points = entry.points;
                    ranking.rank = entry.rank;
                    this.logger.debug('adding', ranking);
                    toUpdate.push(ranking);
                }
            }

            const results_b = isQuali ? infos.results_b : [];
            for (let r = 0; r < results_b.length; r++) {
                const entry = results_b[r];
                if (entry.points === 0) {
                    continue;
                }
                if (isQuali && !entry.player.nation?.includes('GER')) {
                    // player must be German for DM Quali
                    continue;
                }
                const ranking: Ranking = new Ranking();
                ranking.bracket = 'B';
                ranking.federation = infos.category.federation;
                ranking.category = cat as unknown as Category;
                ranking.tournament = infos.tournament.title;
                ranking.tournamentId = infos.tournament._id as unknown as Tournament;
                ranking.updated = (DateTime.fromFormat(infos.tournament.start_date, 'd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'd.MM.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.MM.yyyy')).toISODate();
                ranking.player = entry.player;
                ranking.points = entry.points;
                ranking.rank = entry.rank;

                this.logger.debug('adding', ranking);
                toUpdate.push(ranking);

                // no doubles for DM quali
                if (isQuali && entry.partner) {
                    const ranking: Ranking = new Ranking();
                    ranking.bracket = 'B';
                    ranking.federation = infos.category.federation;
                    ranking.category = cat as unknown as Category;
                    ranking.tournament = infos.tournament.title;
                    ranking.tournamentId = infos.tournament._id as unknown as Tournament;
                    ranking.updated = (DateTime.fromFormat(infos.tournament.start_date, 'd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'd.MM.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.MM.yyyy')).toISODate();
                    ranking.player = entry.partner;
                    ranking.points = entry.points;
                    ranking.rank = entry.rank;
                    this.logger.debug('adding', ranking);
                    toUpdate.push(ranking);
                }
            }


            for (const catName of Object.getOwnPropertyNames(infos.resultsFromOtherCats)) {
                if (infos.resultsFromOtherCats[catName].length > 0) {
                    // } else if (infos.category.name.indexOf('ICO') === 0) {
                    cat = catName;
                    // convert normal into qualifying ranking
                    if (isQuali && catName.indexOf('DCV') === 0) {
                        const year = new Date(infos.tournament.start_date_epoch).getFullYear().toString();
                        cat = catName.replace('DCV', 'DM Quali ' + year);
                    } else if (catName.indexOf('ICO') === 0) {
                        const year = new Date(infos.tournament.start_date_epoch).getFullYear().toString();
                        cat = this.translateICOtoDCV(catName, isQuali, year);
                    } else if (!isQuali && catName.indexOf('DCV') !== 0) {
                        this.logger.error('Could not add rankings for unknown category ', catName);
                        this.alertService.error($localize`:@@could not add ranking for category:Could not add rankings for category ${catName}:category:`);
                    }
                    for (let r = 0; r < infos.resultsFromOtherCats[catName].length; r++) {
                        const result = infos.resultsFromOtherCats[catName][r];
                        if (isQuali) {
                            if (result.cat_type !== 'singles' || result.maximum_age <= 18 || result.points === 0) {
                                // ignore doubles and junior categories
                                continue;
                            }
                            if (!result.player.nation?.includes('GER')) {
                                // player must be German for DM Quali
                                // TODO might have double nationality
                                continue;
                            }
                        }
                        const ranking: Ranking = new Ranking();
                        ranking.bracket = 'A';
                        ranking.federation = infos.category.federation;
                        ranking.category = cat as unknown as Category;
                        ranking.tournament = infos.tournament.title;
                        ranking.tournamentId = infos.tournament._id as unknown as Tournament;
                        ranking.updated = (DateTime.fromFormat(infos.tournament.start_date, 'd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'd.MM.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.MM.yyyy')).toISODate();
                        ranking.points = result.points;
                        ranking.player = result.player;
                        ranking.rank = result.rank;
                        this.logger.debug('adding', ranking);
                        toUpdate.push(ranking);

                        // no doubles for DM quali
                        if (!isQuali && result.partner) {
                            const ranking: Ranking = new Ranking();
                            ranking.bracket = 'A';
                            ranking.federation = infos.category.federation;
                            ranking.category = cat as unknown as Category;
                            ranking.tournament = infos.tournament.title;
                            ranking.tournamentId = infos.tournament._id as unknown as Tournament;
                            ranking.updated = (DateTime.fromFormat(infos.tournament.start_date, 'd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.M.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'd.MM.yyyy') || DateTime.fromFormat(infos.tournament.start_date, 'dd.MM.yyyy')).toISODate();
                            ranking.points = result.points;
                            ranking.player = result.partner;
                            ranking.rank = result.rank;
                            this.logger.debug('adding', ranking);
                            toUpdate.push(ranking);
                        }
                    }
                }
            }
            this.rankingService.addRankings(toUpdate).subscribe({
                next: response => {
                    this.logger.debug('added / updated', response.result, 'entries');
                    this.alertService.success($localize`Added or changed ${response.result.toString()}:nrOf: entries`);
                    this.rankingService.updateLeaderboard().subscribe(
                        _ => {
                            this.logger.debug('successfully called updateLeaderboard');
                        },
                        error2 => {
                            this.logger.error('Could not update leaderboard:', error2);
                            this.alertService.error($localize`Error adding`, error2);
                        }
                    );
                },
                error: error => {
                    this.logger.error('Could not add rankings for category ', infos.category.name, error);
                    this.alertService.error($localize`Could not add rankings for category ${infos.category.name}:category:`, error);
                }
            });
        });
    }

    findEntryInRanking(entry: Entry, existRanking: RankingEntry[]): RankingEntry {
        for (let e = 0; e < existRanking.length; e++) {
            const existEntry = existRanking[e];
            if (entry.player._id === existEntry.player._id) {
                return existEntry;
            }
        }
        return undefined;
    }
    isEntryDifferent(entry: Entry, existEntry: RankingEntry): boolean {
        return !(entry.player._id !== existEntry.player._id
            || entry.points !== existEntry.points
            || entry.rank !== existEntry.rank);
    }

    //     addPointsToRanking(infos: ResultInfo): void {
    //         if (!infos.resultsRanking) {
    //             return;
    //         }

    //         // TODO also add points to national ranking for ICO categories (depends on the country in which the tournament was played)

    //         // 1) retrieve existing entries for the given category and tournament to avoid adding duplicates
    //         this.rankingService.getRanking(infos.category.name, true, infos.tournament.title).subscribe(
    //             response => {
    //                 if (response.success === true) {
    //                     const existRanking = response.result.ranking;
    //                     const toUpdate: Entry[] = [];

    //                     // 2) go through infos.resultsRanking (and resultsRankingFromOtherCats) and find all new/different entries
    //                     for (let r = 0; r < infos.resultsRanking.length; r++) {
    //                         const entry = infos.resultsRanking[r]; // SÖNKE + CHRISTOPH
    //                         const existEntry = this.findEntryInRanking(entry, existRanking);
    //                         if (!existEntry || this.isEntryDifferent(entry, existEntry)) {
    //                             toUpdate.push(entry);
    //                         }
    //                     }

    //                     // 3) add info to entries and add all new/different entries
    //                     const newRankings: Ranking[] = [];
    //                     for (let u = 0; u < toUpdate.length; u++) {
    //                         const entry = toUpdate[u];
    //                         const ranking: Ranking = new Ranking();
    //                         ranking.bracket = 'A';
    //                         ranking.federation = infos.category.federation;
    //                         ranking.category = infos.category._id as unknown as Category;
    //                         ranking.tournament = infos.tournament.title;
    //                         ranking.tournamentId = infos.tournament._id as unknown as Tournament;
    //                         ranking.updated = infos.tournament.start_date;
    //                         ranking.player = entry.player;
    //                         ranking.points = entry.points;
    //                         ranking.rank = entry.rank;

    //                         this.logger.debug('adding', ranking);
    //                         newRankings.push(ranking);

    //                         if (entry.partner) {
    //                             const ranking: Ranking = new Ranking();
    //                             ranking.bracket = 'A';
    //                             ranking.federation = infos.category.federation;
    //                             ranking.category = infos.category._id as unknown as Category;
    //                             ranking.tournament = infos.tournament.title;
    //                             ranking.tournamentId = infos.tournament._id as unknown as Tournament;
    //                             ranking.updated = infos.tournament.start_date;
    //                             ranking.player = entry.partner;
    //                             ranking.points = entry.points;
    //                             ranking.rank = entry.rank;
    //                             this.logger.debug('adding', ranking);
    //                             newRankings.push(ranking);
    //                         }
    //                     }
    //                     this.rankingService.addRankings(newRankings).subscribe(
    //                         response => {
    //                             if (response.success === true) {
    //                                 this.alertService.error($localize`Successfully added`);
    //                             } else {
    //                                 this.logger.error('error', response.error);
    //                                 this.alertService.error($localize`error`, response.error);
    //                             }
    //                         },
    //                         error => {
    //                             this.logger.error('error', error);
    //                             this.alertService.error($localize`error`, error);
    //                         }
    //                     );
    //                 } else {
    //                     this.logger.error('error', response.error);
    //                     this.alertService.error($localize`error`, response.error);
    //                 }
    //             },
    //             error => {
    //                 this.logger.error('error', error);
    //                 this.alertService.error($localize`error`, error);
    //             }
    //         );


    //         //     this.rankingService.getRanking(infos.category.name, true).subscribe(
    //         //         response => {
    //         //             if (response.success === true) {
    //         //                 const existRanking = response.result.ranking;
    //         //                 this.logger.warn('now looking at category', infos.category.name, 'with entries', existRanking.length);

    //         //                 // update points
    //         //                 for (let r = 0; r < infos.resultsRanking.length; r++) {
    //         //                     const entry = infos.resultsRanking[r]; // SÖNKE + CHRISTOPH
    //         //                     let foundPlayer = false;
    //         //                     // if there is no partner, dont search for it
    //         //                     let foundPartner = !entry.partner;
    //         //                     for (let er = 0; er < existRanking.length; er++) {
    //         //                         const existRank = existRanking[er]; // SÖNKE
    //         //                         if (!foundPlayer && existRank.player[0].last_name === entry.player[0].last_name && existRank.player[0].first_name === entry.player[0].first_name) {
    //         //                             existRank.points += entry.points;
    //         //                             this.logger.log('UPDATING', existRank.player[0].last_name, 'from', existRank.points - entry.points, 'to points', existRank.points);
    //         //                             foundPlayer = true;
    //         //                         }
    //         //                         if (!foundPartner && (existRank.player[0].last_name === entry.partner.last_name && existRank.player[0].first_name === entry.partner.first_name)) {
    //         //                             existRank.points += entry.points;
    //         //                             this.logger.log('UPDATING', existRank.player[0].last_name, 'from', existRank.points - entry.points, 'to points', existRank.points);
    //         //                             foundPartner = true;
    //         //                         }
    //         //                         if (foundPlayer && foundPartner) {
    //         //                             break;
    //         //                         }
    //         //                     }
    //         //                     if (!foundPlayer) {
    //         //                         existRanking.push({ rank: -1, points: entry.points, player: [entry.player] });
    //         //                     }
    //         //                     if (entry.partner && !foundPartner) {
    //         //                         existRanking.push({ rank: -1, points: entry.points, player: [entry.partner] });
    //         //                     }
    //         //                 }

    //         //                 // update ranks
    //         //                 existRanking.sort((r1, r2) => r2.points - r1.points);

    //         //                 let rank = 1;
    //         //                 let jumped = 1;
    //         //                 let lastpoints = -1;
    //         //                 for (let r = 0; r < existRanking.length; r++) {
    //         //                     if (r > 0) {
    //         //                         if (existRanking[r].points !== lastpoints) {
    //         //                             rank += jumped;
    //         //                             jumped = 1;
    //         //                         } else {
    //         //                             jumped++;
    //         //                         }
    //         //                     }
    //         //                     existRanking[r].rank = rank;
    //         //                     lastpoints = existRanking[r].points;
    //         //                 }

    //         //                 // delete all and insert new
    //         //                 this.rankingService.clearRanking(infos.category.name).subscribe(
    //         //                     response2 => {
    //         //                         if (response2.success === true) {
    //         //                             const obsvbls = [];
    //         //                             for (let r = 0; r < existRanking.length; r++) {
    //         //                                 const ranking: Ranking = new Ranking();
    //         //                                 ranking.bracket = 'A';
    //         //                                 ranking.federation = infos.category.federation;
    //         //                                 ranking.category = infos.category.name as any;
    //         //                                 ranking.player = existRanking[r].player[0]._id as any;
    //         //                                 ranking.rank = existRanking[r].rank;
    //         //                                 ranking.points = existRanking[r].points;
    //         //                                 ranking.rank = existRanking[r].rank;
    //         //                                 obsvbls.push(this.rankingService.addRanking(ranking));
    //         //                             }
    //         //                             forkJoin(obsvbls).subscribe(
    //         //                                 values => {
    //         //                                     this.logger.debug('added', values.length, 'entries');
    //         //                                 },
    //         //                                 error => {
    //         //                                     this.logger.error('Could not add rankings for category', infos.category.name, error);
    //         //                                 }
    //         //                             );
    //         //                         } else { this.logger.error('error', response2.error); this.alertService.error($localize`error`, response2.error); }
    //         //                     }, error => { this.logger.error('error', error); this.alertService.error($localize`error`, error); }
    //         //                 );

    //         //             } else { this.logger.error('error', response.error); this.alertService.error($localize`error`, response.error); }
    //         //         }, error => { this.logger.error('error', error); this.alertService.error($localize`error`, error); }
    //         //     );

    //         //     for (const catName of Object.getOwnPropertyNames(infos.resultsRankingFromOtherCats)) {
    //         //         if (infos.resultsRankingFromOtherCats[catName].length > 0) {
    //         //             this.rankingService.getRanking(catName, true).subscribe(
    //         //                 response => {
    //         //                     if (response.success === true) {
    //         //                         const existRanking = response.result.ranking;
    //         //                         this.logger.warn('now looking at category', catName, 'with entries', existRanking.length);

    //         //                         // update points
    //         //                         for (let r = 0; r < infos.resultsRankingFromOtherCats[catName].length; r++) {
    //         //                             const entry = infos.resultsRankingFromOtherCats[catName][r];
    //         //                             let foundPlayer = false;
    //         //                             // if there is no partner, dont search for it
    //         //                             let foundPartner = (!entry.partner);
    //         //                             for (let er = 0; er < existRanking.length; er++) {
    //         //                                 const existRank = existRanking[er]; // SÖNKE
    //         //                                 if (!foundPlayer && existRank.player[0].last_name === entry.player[0].last_name && existRank.player[0].first_name === entry.player[0].first_name) {
    //         //                                     existRank.points += entry.points;
    //         //                                     this.logger.log('UPDATING', existRank.player[0].last_name, 'from', existRank.points - entry.points, 'to points', existRank.points);
    //         //                                     foundPlayer = true;
    //         //                                 }
    //         //                                 if (!foundPartner && (existRank.player[0].last_name === entry.partner.last_name && existRank.player[0].first_name === entry.partner.first_name)) {
    //         //                                     existRank.points += entry.points;
    //         //                                     this.logger.log('UPDATING', existRank.player[0].last_name, 'from', existRank.points - entry.points, 'to points', existRank.points);
    //         //                                     foundPartner = true;
    //         //                                 }
    //         //                                 if (foundPlayer && foundPartner) {
    //         //                                     break;
    //         //                                 }
    //         //                             }
    //         //                             if (!foundPlayer) {
    //         //                                 existRanking.push({ rank: -1, points: entry.points, player: [entry.player] });
    //         //                             }
    //         //                             if (entry.partner && !foundPartner) {
    //         //                                 existRanking.push({ rank: -1, points: entry.points, player: [entry.partner] });
    //         //                             }
    //         //                         }

    //         //                         // update ranks
    //         //                         existRanking.sort((r1, r2) => r2.points - r1.points);

    //         //                         let rank = 1;
    //         //                         let jumped = 1;
    //         //                         let lastpoints = -1;
    //         //                         for (let r = 0; r < existRanking.length; r++) {
    //         //                             if (r > 0) {
    //         //                                 if (existRanking[r].points !== lastpoints) {
    //         //                                     rank += jumped;
    //         //                                     jumped = 1;
    //         //                                 } else {
    //         //                                     jumped++;
    //         //                                 }
    //         //                             }
    //         //                             existRanking[r].rank = rank;
    //         //                             lastpoints = existRanking[r].points;
    //         //                         }

    //         //                         // delete all and insert new
    //         //                         this.rankingService.clearRanking(catName).subscribe(
    //         //                             response2 => {
    //         //                                 if (response2.success === true) {
    //         //                                     const obsvbls = [];
    //         //                                     for (let r = 0; r < existRanking.length; r++) {
    //         //                                         const ranking: Ranking = new Ranking();
    //         //                                         ranking.bracket = 'A';
    //         //                                         ranking.federation = infos.category.federation;
    //         //                                         ranking.category = catName as any;
    //         //                                         ranking.player = existRanking[r].player[0]._id as any;
    //         //                                         ranking.rank = existRanking[r].rank;
    //         //                                         ranking.points = existRanking[r].points;
    //         //                                         ranking.rank = existRanking[r].rank;
    //         //                                         obsvbls.push(this.rankingService.addRanking(ranking));
    //         //                                     }
    //         //                                     forkJoin(obsvbls).subscribe(
    //         //                                         values => {
    //         //                                             this.logger.debug('added', values.length, 'entries');
    //         //                                         },
    //         //                                         error => {
    //         //                                             this.alertService.error($localize`Could not add rankings for category ${infos.category.name}:category:`, error);
    //         //                                             this.logger.error('Could not add rankings for category', infos.category.name, error);
    //         //                                         }
    //         //                                     );
    //         //                                 } else { this.logger.error('error', response2.error); this.alertService.error($localize`error`, response2.error); }
    //         //                             }, error => { this.logger.error('error', error); this.alertService.error($localize`error`, error); }
    //         //                         );

    //         //                     } else { this.logger.error('error', response.error); this.alertService.error($localize`error`, response.error); }
    //         //                 }, error => { this.logger.error('error', error); this.alertService.error($localize`error`, error); }
    //         //             );
    //         //         }
    //         //     }
    //     }
}
