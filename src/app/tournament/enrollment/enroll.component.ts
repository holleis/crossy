import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { TournamentsService } from '../tournaments.service';
import { Tournament } from '../tournament';
import { Player } from '../../player/player';
import { PlayerService } from '../../player/player.service';
import { AlertService } from '../../util/alert/alert.service';
import { NGXLogger } from 'ngx-logger';
import { Router, ActivatedRoute } from '@angular/router';
import { EnrollService } from './enroll.service';
import { MatDialog } from '@angular/material/dialog';
import { ChooseComponent } from './choose.component';
import { ChoosePartnerComponent } from './choose-partner.component';
import { Club } from '../club';
import { EnrollmentInfo } from '../cat-info';
import { UserService } from '../../auth/_services/user.service';
import { Role } from '../../user/role';
import { Observable } from 'rxjs';
import { forkJoin as observableForkJoin } from 'rxjs';
import moment from 'moment';
import { DateTime } from 'luxon';


@Component({
    selector: 'app-enroll',
    templateUrl: './enroll.component.html',
    styleUrls: ['./enroll.component.css'],
    providers: [PlayerService]
})
export class EnrollComponent implements OnInit, OnDestroy {

    constructor(
        private tournamentsService: TournamentsService,
        private alertService: AlertService,
        private logger: NGXLogger,
        public changeDetectorRef: ChangeDetectorRef,
        public media: MediaMatcher,
        private router: Router,
        private route: ActivatedRoute,
        private enrollService: EnrollService,
        private playerService: PlayerService,
        public dialog: MatDialog,
        private userService: UserService
    ) {
        this.mobileQuery = media.matchMedia('(max-width: 500px)');
        // this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        // this.mobileQuery.addListener(this._mobileQueryListener);
        try {
            // Chrome & Firefox
            this.mobileQuery.addEventListener('change', () => changeDetectorRef.detectChanges());
        } catch (e1) {
            try {
                // Safari
                this.mobileQuery.addListener(changeDetectorRef.detectChanges);
            } catch (e2) {
                // ignore
                this.logger.error('error: could not assign small screen listener');
            }
        }    }

    tournaments: Tournament[];
    // each category in each tournament has a EnrollmentInfo
    enrollments: EnrollmentInfo[][] = [];

    displayedColumns = ['number', 'name', 'club', 'nation', 'rank', 'points'];
    smallDisplayedColumns = ['number', 'player', 'rank'];

    // store info about player to avoid unnecessary API calls
    myPlayer: {
        _id: string,
        gender: number,
        date_of_birth: Date,
        clubs: Club[],
        roles: number,
        last_name: string,
        first_name: string,
        nation: string[],
        mainclubidex: number,
        partner: string
    } = {_id: undefined, gender: undefined, date_of_birth: undefined, clubs: undefined,
        roles: 0, last_name: undefined, first_name: undefined, nation: undefined, mainclubidex: undefined, partner: undefined};

    mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;

    Math: Math = Math

    obsvbls: Observable<any>[] = [];
    updating = false;
    one = false;


    ngOnInit() {
        const user = this.userService.getCurrentUser();
        if (user) {
            this.myPlayer._id = user.player_id;
            this.myPlayer.roles = 0;
            if (user.roles) {
                this.myPlayer.roles = user.roles;
            }
        } else {
            this.logger.debug('enroll: not logged in, redirect to login');
            this.router.navigate(['/login']);
        }

        const tId = this.route.snapshot.params.id;
        if (tId) {
            this.one = true;
            this.getOneTournament(tId);
        } else {
            this.one = false;
            this.getFutureTournaments();
        }
    }

    ngOnDestroy() {
        this.logger.debug('ngOnDestroy in enroll component');
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    sortDoubles(catInfo: EnrollmentInfo) {
        // sort partners together
        if (catInfo.cat_type !== 'singles' && catInfo.players && catInfo.players.length > 0) {
            const orderedPlayers = [];
            const playersCopy = catInfo.players.slice();
            playersCopy.sort((p1, p2) => p1.rank - p2.rank);
            while (playersCopy.length > 0) {
                const p = playersCopy.pop();
                if (!p.partner) {
                    // no partner yet
                    orderedPlayers.push(p);
                } else {
                    // search for partner to insert together
                    let found = false;
                    for (let i = 0; i < playersCopy.length; i++) {
                        if (playersCopy[i]._id === (p.partner._id || p.partner).toString()) {
                            orderedPlayers.unshift(playersCopy[i]);
                            orderedPlayers.unshift(p);
                            playersCopy.splice(i, 1);
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        // specified player not found!
                        p.partner = undefined;
                        orderedPlayers.push(p);
                    }
                }
            }
            catInfo.players = orderedPlayers;
        }
    }

    getOneTournament(tId: string) {
        this.tournamentsService.getTournament(tId).subscribe(
            response => {
                this.logger.trace('retrieve the given tournaments', response);
                if (response.success === true) {
                    this.tournaments = [response.result];
                    if (response.result.tournament_type === 'MunichCup (Gruppen)') {
                        this.displayedColumns = this.displayedColumns.filter(c => c !== 'rank');
                        this.smallDisplayedColumns = this.smallDisplayedColumns.filter(c => c !== 'rank');
                    } else {
                        if (!this.displayedColumns.includes('rank')) {
                            this.displayedColumns.push('rank');
                        }
                        if (!this.smallDisplayedColumns.includes('rank')) {
                            this.smallDisplayedColumns.push('rank');
                        }
                    }
                    this.logger.debug('retrieved the tournament');
                    try {
                        this.getEnrollments();
                    } catch (err) {
                        this.logger.error('Could not retrieve the given tournaments:', err);
                        this.alertService.error($localize`Could not retrieve the given tournaments`, err);
                    }
                } else {
                    this.logger.error('Could not retrieve the given tournaments:', response.error);
                    this.alertService.error($localize`Could not retrieve the given tournaments`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve the given tournaments:', error);
                this.alertService.error($localize`Could not retrieve the given tournaments`, error);
            }
        );
    }

    getFutureTournaments() {
        this.tournamentsService.getFutureTournaments(true).subscribe(
            response => {
                this.logger.trace('retrieve future tournaments', response);
                if (response.success === true) {
                    this.tournaments = response.result;
                    this.logger.debug('retrieved', this.tournaments.length, 'future tournaments');
                    try {
                        this.getEnrollments();
                    } catch (err) {
                        this.logger.error('Could not retrieve future tournaments:', err);
                        this.alertService.error($localize`Could not retrieve future tournaments`, err);
                    }
                } else {
                    this.logger.error('Could not retrieve future tournaments:', response.error);
                    this.alertService.error($localize`Could not retrieve future tournaments`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve future tournaments:', error);
                this.alertService.error($localize`Could not retrieve future tournaments`, error);
            }
        );
    }

    getEnrollments() {
        // first get info about player (age, gender)
        this.playerService.getPlayer(this.myPlayer._id).subscribe(
            response => {
                if (response.success === true) {
                    const player = response.result;
                    this.myPlayer.gender = player.gender;
                    if (isNaN(new Date(player.date_of_birth).getTime())) {
                        // deprecated
                        this.myPlayer.date_of_birth = moment(player.date_of_birth, 'DD.MM.YYYY').toDate();
                    } else {
                        this.myPlayer.date_of_birth = new Date(player.date_of_birth);
                    }
                    this.myPlayer.clubs = player.clubs;
                    this.myPlayer.last_name = player.last_name;
                    this.myPlayer.first_name = player.first_name;
                    this.myPlayer.nation = player.nation;
                    this.myPlayer.mainclubidex = player.mainclubindex;

                    for (let t = 0; t < this.tournaments.length; t++) {
                        this.getEnrollment(t);
                    }
                } else {
                    this.logger.error('error getPlayer', response.error);
                    this.alertService.error($localize`Unable to get player info for enrollment`, response.error);
                }
            }
        );

        // this.enrollments[idx] = [{_id: "5a82e8aaf7b719719df7d8cf", catName: 'DCV Damen', players: [
        //     { "_id" : "5a1ecc017b7c6161238fd51a", "last_name" : "FRÖMTER",
        //       "first_name" : "Jessica", "nation" : "GER", "club" : "Bla Club", partner: '' },
        // ]},
    }

    getEnrollment(idx: number) {
        const tournamentId = this.tournaments[idx]._id;
        this.logger.debug('getEnrollment for', tournamentId);
        const age = this.playerService.getMyAge(this.myPlayer.date_of_birth, this.tournaments[idx].start_date);
        this.logger.trace('getMyAge', this.myPlayer.date_of_birth, this.tournaments[idx].start_date, 'results in', age);

        this.tournamentsService.getEnrollments(tournamentId).subscribe({
            next: response => {
                if (response.success === true) {
                    this.logger.trace(response.result);
                    for (let c = 0; c < response.result.length; c++) {
                        const cat = response.result[c];
                        this.logger.trace('myPlayerFits', this.myPlayer.gender, age,
                            cat.gender, cat.minimum_age, cat.maximum_age);
                        const myPlayerFits = this.playerService.playerFits(this.myPlayer.gender, age,
                            cat.gender, cat.minimum_age, cat.maximum_age);
                        this.logger.trace('   results in', myPlayerFits);
                        response.result[c].myPlayerFits = myPlayerFits;
                    }
                    this.enrollments[idx] = response.result;

                    for (let e = 0; e < this.enrollments[idx].length; e++) {
                        this.sortDoubles(this.enrollments[idx][e]);
                    }

                    // fill with remaining (no one enrolled) categories
                    //     enrollments: {_id: string, tournamentName: string, catName: string, gender: number,
                    //          minimum_age: number, maximum_age: number, myPlayerFits: boolean, players: {
                    //          _id: string, last_name: string, first_name: string,
                    //          nation: string, club: string, partner: string }[] }[][] = [];
                    const haveCatIds = response.result.map((cat) => cat._id);
                    for (let c = 0; c < this.tournaments[idx].categories.length; c++) {
                        const cat = this.tournaments[idx].categories[c];
                        if (!cat.active || haveCatIds.indexOf(cat._id) !== -1) {
                            // already included
                            continue;
                        }
                        const myPlayerFits = this.playerService.playerFits(this.myPlayer.gender, age,
                            cat.gender, cat.minimum_age, cat.maximum_age);
                        this.enrollments[idx].push({
                            _id: cat._id,
                            tournamentName: this.tournaments[idx].title,
                            tournament_type: this.tournaments[idx].tournament_type,
                            catName: cat.name,
                            cat_type: cat.cat_type,
                            gender: cat.gender,
                            minimum_age: cat.minimum_age,
                            maximum_age: cat.maximum_age,
                            myPlayerFits: myPlayerFits,
                            players: []
                        });
                    }

                } else {
                    this.logger.error('error getEnrollment for', tournamentId, response.error);
                    this.alertService.error($localize`Unable to get enrollment info`, response.error);
                }
            },
            error: error => {
                this.logger.error('error getEnrollment for', tournamentId, error);
                this.alertService.error($localize`Unable to get enrollment info`, error);
            }
        });
    }

    getEnrollmentForCat(idx: number, catIdx: number) {
        const tournamentId = this.tournaments[idx]._id;
        this.logger.debug('getEnrollment for', tournamentId, catIdx);
        const age = this.playerService.getMyAge(this.myPlayer.date_of_birth, this.tournaments[idx].start_date);
        this.logger.trace('getMyAge', this.myPlayer.date_of_birth, this.tournaments[idx].start_date, 'results in', age);

        this.tournamentsService.getEnrollmentForCat(tournamentId, this.enrollments[idx][catIdx]._id).subscribe({
            next: response => {
                if (response.success === true) {
                    this.logger.trace(response.result);
                    const eInfo = response.result;
                    if (eInfo) {
                        this.logger.trace('calling myPlayerFits', this.myPlayer.gender, age,
                            eInfo.gender, eInfo.minimum_age, eInfo.maximum_age);
                        const myPlayerFits = this.playerService.playerFits(this.myPlayer.gender, age,
                            eInfo.gender, eInfo.minimum_age, eInfo.maximum_age);
                        this.logger.trace('   results in', myPlayerFits);
                        eInfo.myPlayerFits = myPlayerFits;

                        this.enrollments[idx][catIdx] = eInfo;

                        for (let e = 0; e < this.enrollments[idx].length; e++) {
                            this.sortDoubles(this.enrollments[idx][e]);
                        }
                    } else if (this.enrollments[idx] && this.enrollments[idx][catIdx]) {
                        this.enrollments[idx][catIdx].players = [];
                    }

                } else {
                    this.logger.error('error getEnrollment for', tournamentId, response.error);
                    this.alertService.error($localize`Unable to get enrollment info`, response.error);
                }
            },
            error: error => {
                this.logger.error('error getEnrollment for', tournamentId, error);
                this.alertService.error($localize`Unable to get enrollment info`, error);
            }
        });
    }

    // check whether current player is enrolled in the given tournament / category
    amEnrolled(idx: number, catName): boolean {
        if (!this.enrollments[idx]) {
            this.logger.error('asking for yet unknown tournament index, should not happen');
            return false;
        }
        const cats = this.enrollments[idx];
        if (!cats || cats.length === 0) {
            this.logger.debug('tournament without categories?!');
            return false;
        }
        let cat;
        for (let c = 0; c < cats.length; c++) {
            if (cats[c].catName === catName) {
                cat = cats[c];
                break;
            }
        }
        if (!cat) {
            this.logger.error('unknown cat name supplied:', catName, 'for tournament idx', idx);
            return false;
        }
        let playerFound = false;
        for (let p = 0; p < cat.players.length; p++) {
            if (this.myPlayer._id === cat.players[p]._id) {
                playerFound = true;
                break;
            }
        }
        return playerFound;
    }

    // remove enrollment from the tournament/category
    removeMe(tIdx: number, cat: EnrollmentInfo) {
        const tournamentId = this.tournaments[tIdx]._id;
        this.logger.debug('removing me', this.myPlayer._id, 'from tournament', tournamentId, 'category', cat._id);
        let partnerId;
        if (this.myPlayer.partner) {
            partnerId = this.myPlayer.partner;
        }
        this.enrollService.unenroll(tournamentId, cat._id, this.myPlayer._id, partnerId).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug(response.result);
                    this.alertService.success($localize`Successfully removed your enrollment!`);
                    this.getEnrollment(tIdx);
                } else {
                    this.logger.error('error unenrolling', response.error);
                    this.alertService.error($localize`Could not remove your enrollment`, response.error);
                }
            },
            error => {
                this.logger.error('error unenrolling', error);
                this.alertService.error($localize`Could not remove your enrollment`, error);
            }
        );
    }

    // enroll in the tournament/category
    addMe(tIdx: number, cat: EnrollmentInfo) {
        if (cat.cat_type !== 'singles') {
            const availablePlayers = cat.players.filter(p => !p.partner && p._id !== this.myPlayer._id);
            if (availablePlayers.length > 0) {
                const dialogRef = this.dialog.open(ChoosePartnerComponent, {
                    data: {players: availablePlayers, catName: cat.catName, forPlayer: 'you'}
                });
                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        this.myPlayer.partner = result; // this is the ID of the partner player
                        if (this.myPlayer.partner) {
                            this.doAddMe(tIdx, cat, this.myPlayer.partner);
                        }
                    } else {
                        this.doAddMe(tIdx, cat, undefined);
                    }
                });
            } else {
                this.doAddMe(tIdx, cat, undefined);
            }
        } else {
            this.doAddMe(tIdx, cat, undefined);
        }
    }

    doAddMe(tIdx: number, cat: EnrollmentInfo, partnerId: string) {
        const tournamentId = this.tournaments[tIdx]._id;
        const categoryId = cat._id;
        this.logger.debug('adding me', this.myPlayer._id, 'to tournament', tournamentId, 'category', categoryId);
        this.enrollService.enroll(tournamentId, categoryId, this.myPlayer._id, partnerId).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug(response.result);
                    this.alertService.success($localize`Successfully enrolled!`);
                    this.getEnrollment(tIdx);
                } else {
                    this.logger.error('error enrolling', response.error);
                    this.alertService.error($localize`Error enrolling`, response.error);
                }
            },
            error => {
                this.logger.error('error enrolling', error);
                this.alertService.error($localize`Error enrolling tournaments`, error);
            }
        );
    }

    // allow enrolling players of user's club in the tournament/category
    addOthers(tIdx: number, enrollment: EnrollmentInfo, catIdx: number) {
        const dialogRef = this.dialog.open(ChooseComponent, {
            data: {
                clubs: this.myPlayer.clubs,
                tournament: this.tournaments[tIdx],
                enrollment: enrollment,
                roles: this.myPlayer.roles
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.logger.debug(result);

                let dontUpdate = false;
                for (let p = 0; p < result.enroll.length; p++) {
                    const player = result.enroll[p];

                    if (enrollment.cat_type !== 'singles') {
                        const availablePlayers = enrollment.players.filter(p => !p.partner && p._id !== player._id);
                        if (availablePlayers.length > 0) {
                            const dialogRef = this.dialog.open(ChoosePartnerComponent, {
                                data: {
                                    players: availablePlayers,
                                    catName: enrollment.catName,
                                    forPlayer: player.last_name + ' ' + player.first_name
                                }
                            });
                            dontUpdate = true;
                            dialogRef.afterClosed().subscribe(partnerResult => {
                                if (partnerResult) {
                                    player.partner = {_id: partnerResult};
                                }
                                this.doAddOthers(player, tIdx, enrollment);
                                this.execute(tIdx, catIdx);
                            });
                        } else {
                            this.doAddOthers(player, tIdx, enrollment);
                        }

                    } else {
                        this.doAddOthers(player, tIdx, enrollment);
                    }
                }
                for (let p = 0; p < result.unenroll.length; p++) {
                    const player = result.unenroll[p];
                    this.doRemoveOthers(player, tIdx, enrollment);
                }
                if (!dontUpdate) {
                    // update will be done after double dialog is finished
                    this.execute(tIdx, catIdx);
                }
            }
        });
    }

    execute(tIdx: number, catIdx: number) {
        // execute all
        this.updating = true;
        observableForkJoin(this.obsvbls).subscribe(_values => {
            this.obsvbls = [];
            this.logger.debug('all enrollments processed');
            this.getEnrollmentForCat(tIdx, catIdx);
            this.updating = false;
        });
    }

    doAddOthers(player: Player, tIdx: number, enrollment: EnrollmentInfo) {
        this.logger.debug('adding', player.first_name, player.last_name, 'to tournament',
            this.tournaments[tIdx].title, 'category', enrollment.catName);
        let partnerId;
        if (player.partner && typeof player.partner === 'string') {
            partnerId = player.partner;
        } else if (player.partner && player.partner._id) {
            partnerId = player.partner._id;
        }
        this.obsvbls.push(this.enrollService.enroll(this.tournaments[tIdx]._id, enrollment._id, player._id, partnerId));
    }

    doRemoveOthers(player: Player, tIdx: number, enrollment: EnrollmentInfo) {
        this.logger.debug('removing', player.first_name, player.last_name, 'from tournament',
            this.tournaments[tIdx].title, 'category', enrollment.catName);
        let partnerId;
        if (player.partner && typeof player.partner === 'string') {
            partnerId = player.partner;
        } else if (player.partner && player.partner._id) {
            partnerId = player.partner._id;
        }
        this.obsvbls.push(this.enrollService.unenroll(this.tournaments[tIdx]._id, enrollment._id, player._id, partnerId));
    }

    doRemovePartner(playerId: string, tIdx: number, enrollment: EnrollmentInfo) {
        this.logger.debug('removing partner of', playerId, 'from tournament',
            this.tournaments[tIdx].title, 'category', enrollment.catName);
        this.obsvbls.push(this.enrollService.enroll(this.tournaments[tIdx]._id, enrollment._id, playerId, undefined));
    }

    click(player: any, enrollment: EnrollmentInfo, tIdx: number, catIdx: number) {
        this.logger.debug('clicked on', player);
        if (enrollment.cat_type !== 'singles') {
            const origPartner = player.partner;
            const availablePlayers = enrollment.players
                .filter(p => (!p.partner && p._id !== player._id) || p._id === origPartner)
                .sort((p1, p2) => {return ((p1.last_name + p1.first_name) < (p2.last_name + p2.first_name) ? -1 :
                    ((p1.last_name + p1.first_name) > (p2.last_name + p2.first_name) ? 1 : 0))});
            const dialogRef = this.dialog.open(ChoosePartnerComponent, {
                data: {
                    partner: origPartner,
                    players: availablePlayers,
                    catName: enrollment.catName,
                    forPlayer: player.last_name + ' ' + player.first_name,
                    showRemoveButton: true,
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'remove') {
                    this.doRemoveOthers(player, tIdx, enrollment);
                    this.execute(tIdx, catIdx);
                } else if (origPartner !== result) {
                    player.partner = result;
                    this.doAddOthers(player, tIdx, enrollment);
                    if (!result) {
                        this.doRemovePartner(origPartner, tIdx, enrollment);
                    }
                    this.execute(tIdx, catIdx);
                } else {
                    this.logger.debug('no change:', origPartner, player.partner);
                }
            });
        }
    }

    // returns false if the player can still register
    afterDeadline(tournament: Tournament): boolean {
        // DCV is allowed to add players afterwards
        if ((this.myPlayer.roles & Role.ROLE_PAUL) > 0 || (this.myPlayer.roles & Role.ROLE_DCV) > 0) {
            return false;
        }
        // organiser is allowed
        if (this.myPlayer && this.myPlayer._id && tournament && tournament.responsibles && tournament.responsibles.indexOf(this.myPlayer._id) >= 0) {
            return false;
        }

        let regDeadline = tournament.registration_deadline;
        if (regDeadline) {
            const regDeadlineDT = DateTime.fromFormat(regDeadline, 'dd.MM.yyyy');
            const today = DateTime.utc().plus({days: 1});
            return today > regDeadlineDT;
        }
        return false;
    }

    // import {MatTableDataSource} from '@angular/material';
    // dataSource = new MatTableDataSource(ELEMENT_DATA);
    //
    // applyFilter(filterValue: string) {
    //   filterValue = filterValue.trim(); // Remove whitespace
    //   filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    //   this.dataSource.filter = filterValue;
    // }

}
