import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../globals';
import { NGXLogger } from 'ngx-logger';
import moment from 'moment';

@Injectable()
export class EnrollService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger,
    ) { }

    enroll(tournamentId: string, categoryId: string, playerId: string, partnerId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/enroll/';
        const now = moment().format();
        const enrollment = {tournament: tournamentId, category: categoryId, player: playerId, partner: partnerId, updated: now};
        this.logger.trace('calling POST', url, 'with', enrollment);
        return this.http.post<{
            'success': boolean,
            'result': string,
            'error': string
        }>(url, enrollment);
    }

    unenroll(tournamentId: string, categoryId: string, playerId: string, partnerId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/tournaments/unenroll/';
        const enrollment = {tournament: tournamentId, category: categoryId, player: playerId, partner: partnerId};
        this.logger.trace('calling POST', url, 'with', enrollment);
        return this.http.post<{
            'success': boolean,
            'result': string,
            'error': string
        }>(url, enrollment);
    }
}
