import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlayerService } from '../../player/player.service';
import { EnrollmentInfo } from '../cat-info';
import { ClubService } from '../../club/club.service';

@Component({
  selector: 'app-choose-partner',
  templateUrl: './choose-partner.component.html',
  styleUrls: ['./choose-partner.component.css'],
  providers: [PlayerService, ClubService]
})
export class ChoosePartnerComponent implements OnInit {

    constructor (
        public dialogRef: MatDialogRef<ChoosePartnerComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {
            catName: string,
            forPlayer: string,
            players: EnrollmentInfo['players'],
            partner: any,
            showRemoveButton: boolean
        },
    ) { }

    ngOnInit() {
    }

    closeMatSelect(select: any) {
        select.close();
    }
}
