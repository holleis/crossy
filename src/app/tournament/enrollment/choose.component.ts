import { Component, OnInit, Inject } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Player } from '../../player/player';
import { PlayerService } from '../../player/player.service';
import { AlertService } from '../../util/alert/alert.service';
import { Tournament } from '../tournament';
import { Club } from '../club';
import { EnrollmentInfo } from '../cat-info';
import { Role } from '../../user/role';
import { ClubService } from '../../club/club.service';
import difference from 'lodash-es/difference';

@Component({
    selector: 'app-choose',
    templateUrl: './choose.component.html',
    styleUrls: ['./choose.component.css'],
    providers: [PlayerService, ClubService]
})
export class ChooseComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ChooseComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {
            clubs: Club[],
            tournament: Tournament,
            enrollment: EnrollmentInfo,
            roles: number
        },
        private logger: NGXLogger,
        private playerService: PlayerService,
        private alertService: AlertService,
        private clubService: ClubService
    ) { }

    clubs: Club[];
    clubIds: string[];
    tournament: Tournament;
    enrollment: EnrollmentInfo;
    roles: number;

    allPlayers: Player[];
    allPlayersObj = {};
    enrolledPlayers: string[];
    origEnrolledPlayers: string[];

    canSave = false;

    ngOnInit() {
        this.clubs = this.data.clubs;
        // this.clubIds = this.data.clubs.map((club) => club._id);
        this.clubService.sortClubs(this.clubs);
        this.tournament = this.data.tournament;
        this.enrollment = this.data.enrollment;
        this.roles = this.data.roles;

        // tslint:disable-next-line:no-bitwise
        if ((this.roles & Role.ROLE_ENROLLALL) > 0) {
            this.getClubs();
            this.getPlayers([]);
        } else {
            this.getPlayers(this.clubs.map((club) => club._id));
        }
    }

    // see which players belong to one of the selected clubs
    getFilteredPlayers() {
        if (!this.clubIds) {
            return [];
        }
        const filtered = this.allPlayers.filter(player => {
            // intersection(player.clubs, this.clubIds).length > 0
            for (let pc = 0; pc < player.clubs?.length; pc++) {
                for (let ci = 0; ci < this.clubIds.length; ci++) {
                    if (!player.clubs[pc] ||this.clubIds[ci] === (player.clubs[pc]._id ?? player.clubs[pc].toString())) {
                        return true;
                    }
                }
                return false;
            }
        });
        filtered.sort((p1: Player, p2: Player) => {
            return p1.first_name < p2.first_name ? -1 : p1.first_name > p2.first_name ? 1 :
                p1.last_name < p2.last_name ? -1 : p1.last_name > p2.last_name ? 1 : 0;
        });
        this.logger.trace('filtered for clubs', this.clubIds, 'remain', filtered.length, 'players');
        return filtered;
    }

    loadInternational() {
      this.allPlayers = [];
      // tslint:disable-next-line:no-bitwise
      if ((this.roles & Role.ROLE_ENROLLALL) > 0) {
          this.getPlayers([]);
      } else {
          this.getPlayers(this.clubs.map((club) => club._id));
      }
    }

    getPlayers(clubIds: string[]) {
        // this.playerService.getPlayersForClubsWithPoints(clubIds, catId, germanOnly).subscribe(
        this.playerService.getPlayersForClubs(clubIds).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('received', response.result.length, 'players');
                    this.logger.trace(response);
                    this.allPlayers = response.result;

                    // filter those who don't fit to the category
                    this.allPlayers = this.allPlayers.filter(player => this.playerService.playerFits(
                        player.gender, this.playerService.getMyAge(player.date_of_birth, this.tournament.start_date),
                        this.enrollment.gender, this.enrollment.minimum_age, this.enrollment.maximum_age));
                    this.logger.debug('after filter, remain', this.allPlayers.length, 'players for category', this.enrollment.catName);

                    for (let p = 0; p < this.allPlayers.length; p++) {
                        const player = this.allPlayers[p];
                        this.allPlayersObj[player._id] = player;
                        // add partner if any from enrollment
                        for (let ep = 0; ep < this.enrollment.players.length; ep++) {
                            const eplayer = this.enrollment.players[ep];
                            if (eplayer._id === player._id) {
                                if (eplayer.partner) {
                                    player.partner = {_id: (eplayer.partner._id || eplayer.partner).toString()};
                                }
                            }
                        }
                    }

                    // preselect own club(s)
                    this.clubIds = this.data.clubs.map(c => c._id);

                    // preSelect All of those that are already enrolled
                    this.enrolledPlayers = [];
                    for (let p = 0; p < this.enrollment.players.length; p++) {
                        const player = this.enrollment.players[p];
                        if (this.allPlayersObj[player._id]) {
                            this.enrolledPlayers.push(player._id);
                            // preselect a club for each enrolled player - otherwise those would be removed on open+save
                            if (player['club']) {
                                for (let c = 0; c < this.clubs.length; c++) {
                                    const club = this.clubs[c];
                                    if (player['club'] === club.name) {
                                        this.logger.trace('selecting and disabling club', player['club']);
                                        if (this.clubIds.indexOf(club._id) === -1) {
                                            this.logger.trace('found player with new club', player['club']);
                                            this.clubIds.push(club._id);
                                        }
                                        // club['disabled'] = true;
                                        break;
                                    }
                                }
                            }
                            // if (player.clubs) {
                            //     for (let c = 0; c < this.clubs.length; c++) {
                            //         const club = this.clubs[c];
                            //         if (player.clubs[0].name === club.name) {
                            //             this.logger.trace('selecting and disabling club', player.clubs[0].name);
                            //             if (this.clubIds.indexOf(club._id) === -1) {
                            //                 this.logger.trace('found player with new club', player.clubs[0].name);
                            //                 this.clubIds.push(club._id);
                            //             }
                            //             // club['disabled'] = true;
                            //             break;
                            //         }
                            //     }
                            // }
                        }
                    }
                    this.logger.trace('already enrolled:', this.enrolledPlayers);
                    this.origEnrolledPlayers = this.enrolledPlayers.slice();

                    this.canSave = true;
                } else {
                    this.logger.error('Could not retrieve players for clubs', clubIds, response.error);
                    this.alertService.warn($localize`Problems retrieving players of your club`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve players for clubs', clubIds, error);
                this.alertService.warn($localize`Problems retrieving players of your club`, error);
            }
        );
    }

    getClubs(cb?: () => void) {
        this.clubService.getAllClubs().subscribe(
            response => {
                if (response.success === true) {
                    this.logger.debug('received', response.result.length, 'clubs');
                    this.clubs = response.result;
                    this.clubService.sortClubs(this.clubs);
                    if (typeof cb === 'function') {
                        cb();
                    }
                } else {
                    this.logger.error('Could not retrieve clubs', response.error);
                    this.alertService.warn($localize`Problems retrieving all clubs`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve clubs', error);
                this.alertService.warn($localize`Problems retrieving clubs`, error);
            }
        );
    }

    getAgeText(player: Player): string {
        if (!player.date_of_birth) {
            return '???';
        }
        let dty: number;
        if (player.date_of_birth.toString().length <= '11.11.2011'.length) {
            // deprecated date string format
            const dty2 = player.date_of_birth.toString().split('.')[2]; // dd.MM.yyyy
            dty = parseInt(dty2, 10);
        } else {
            const date = new Date(player.date_of_birth);
            dty = date.getFullYear();
        }
        if (dty <= 1900 || dty === 2018) {
            return '???';
        }
        let tournamentYear: number;
        if (this.tournament.start_date) {
            const dspl = this.tournament.start_date.split('.');
            if (dspl.length === 3) {
                tournamentYear = parseInt(dspl[2], 10);
            }
        }
        if (!tournamentYear) {
            // use today's year as backup
            tournamentYear = new Date().getFullYear();
        }
        return (tournamentYear - dty).toString();
    }
    getGenderText(player: Player): string {
        if (player.gender === 1) {
            return 'female';
        } else if (player.gender === 2) {
            return 'male';
        } else {
            return '???';
        }
    }

    selectAll(select: any, values) {
        select.update.emit(values.map((val) => val._id));
    }
    deselectAllClubs(_select: any) {
        // select.update.emit([]);
        this.clubIds = [];
        this.enrolledPlayers = [];
    }
    deselectAll(_select: any) {
        // select.update.emit([]);
        this.enrolledPlayers = [];
    }
    // closeMatSelect(select: any) {
    //     select.close();
    // }
    save() {
        const allpossibleplayers = this.getFilteredPlayers().map(p => p._id);
        this.enrolledPlayers = this.enrolledPlayers.filter(p => allpossibleplayers.indexOf(p) >= 0);
        // find which players need to be enrolled
        const enrollPlayers = difference(this.enrolledPlayers, this.origEnrolledPlayers);
        // replace array of IDs with full player objects
        for (let p = 0; p < enrollPlayers.length; p++) {
            const playerId = enrollPlayers[p];
            if (this.allPlayersObj[playerId]) {
                enrollPlayers[p] = this.allPlayersObj[playerId];
            }
        }

        // find which players need to be unenrolled
        const unenrollPlayers = difference(this.origEnrolledPlayers, this.enrolledPlayers);
        // replace array of IDs with full player objects
        for (let p = 0; p < unenrollPlayers.length; p++) {
            const playerId = unenrollPlayers[p];
            if (this.allPlayersObj[playerId]) {
                unenrollPlayers[p] = this.allPlayersObj[playerId];
            }
        }

        this.logger.trace('all', this.allPlayers, 'orig', this.origEnrolledPlayers,
            'now', this.enrolledPlayers, 'enroll', enrollPlayers, 'unenroll', unenrollPlayers);

        this.dialogRef.close({enroll: enrollPlayers, unenroll: unenrollPlayers});
    }
}
