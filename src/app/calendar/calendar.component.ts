import { Component, OnInit, Input, Inject, LOCALE_ID } from '@angular/core';
import { Event } from './event';
import { CalendarService } from './calendar.service';
import { MatTableDataSource } from '@angular/material/table';
import { AlertService } from '../util/alert/alert.service';
import { NGXLogger } from 'ngx-logger';
import { Tournament } from '../tournament/tournament';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DateTime } from 'luxon';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
    providers: [CalendarService]
})

export class CalendarComponent implements OnInit {

    constructor(
        private calendarService: CalendarService,
        private alertService: AlertService,
        private logger: NGXLogger,
        private router: Router,
        @Inject(LOCALE_ID) private locale: string,
    ) {
        // initialize event years
        const currentYear = new Date().getFullYear();

        this.eventYears[0] = {label: this.undefinedLabel, value: 0};
        for (let y = 2018; y <= currentYear + 2; y++) {
            this.eventYears[y - 2018 + 1] = {label: y.toString(), value: y};
        }

        moment.locale(this.locale);
        this.eventMonths = [this.undefinedLabel, ...moment.months()];
    }

    undefinedLabel = '-- Filter --';

    eventMonths = [];
    eventYears: {value: number, label: string}[] = [];

    @Input() showall = true;
    @Input() selectedEventMonth = 0;
    @Input() selectedEventYear = 0;

    dataSource;
    dataSourcePast;
    displayedEventColumns = ['date', 'title', 'location'];

    ngOnInit() {
        this.getCalendar();
        this.getCalendarPast();
    }

    onChange() {
        this.getCalendar();
    }

    getCalendar() {
        if (this.selectedEventYear === 0) {
            // use current year if month is set but year is not
            if (this.selectedEventMonth !== 0) {
                this.selectedEventYear = new Date().getFullYear();
            }
        }

        this.calendarService.getCalendar(this.selectedEventMonth, this.selectedEventYear, this.showall).subscribe(
            response => {
                this.logger.trace('getCalendar response:', response);
                if (response.success === true) {
                    this.dataSource = new MatTableDataSource(response.result);
                } else {
                    this.logger.error('Could not retrieve events. Please try again later.', response.error);
                    this.alertService.error($localize`Could not retrieve events. Please try again later.`, response.error);
                    this.dataSource = new MatTableDataSource([]);
                }
            },
            error => {
                this.logger.error('Could not retrieve events. Please try again later.', error);
                this.alertService.error($localize`Could not retrieve events. Please try again later.`);
            }
        );
    }

    getCalendarPast() {
        const selectedEventYear = parseInt(DateTime.utc().toFormat('yyyy'), 10);
        const selectedEventMonth = 0;

        this.calendarService.getCalendarPast(selectedEventMonth, selectedEventYear, false).subscribe(
            response => {
                this.logger.trace('getCalendarPast response:', response);
                if (response.success === true) {
                    response.result.sort((e1, e2) => {
                        if (/munichcup/i.test(e1.title)) return -1;
                        if (/munichcup/i.test(e2.title)) return 1;
                        if (moment(e1.start_date, "DD.MM.YYYY").isAfter(moment(e2.start_date, "DD.MM.YYYY"))) return -1; else return 1;
                    });
                    this.dataSourcePast = new MatTableDataSource(response.result);
                } else {
                    this.logger.error('Could not retrieve past events. Please try again later.', response.error);
                    this.alertService.error($localize`Could not retrieve past events. Please try again later.`, response.error);
                    this.dataSource = new MatTableDataSource([]);
                }
            },
            error => {
                this.logger.error('Could not retrieve past events. Please try again later.', error);
                this.alertService.error($localize`Could not retrieve past events. Please try again later.`);
            }
        );
    }

    isCrossyEvent(event: Event) {
        if (event.organiser && event.organiser.name !== '*') {
            return 'crossyEvent';
        }
        return '';
    }

    select(tournament: Tournament) {
        this.logger.trace('selected', tournament.title);
        this.router.navigate(['/showTournament', {id: tournament._id}]);
    }
}
