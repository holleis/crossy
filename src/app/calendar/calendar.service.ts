import { Injectable } from '@angular/core';
import { Event } from './event';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class CalendarService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger
    ) { }

    /**
     * number: 0 (undefined), 1-12
     * year: e.g. 2018
     */
    getCalendar(month: number, year: number, showall: boolean) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/calendar/' +
            year + '/' + month + '/' + (showall ? '1' : '0');
        this.logger.debug(url);
        return this.http.get<{success: boolean, result: Event[], error: string}>(url);
    }

    // get events up to today
    getCalendarPast(month: number, year: number, showall: boolean) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/calendarpast/' +
            year + '/' + month + '/' + (showall ? '1' : '0');
        this.logger.debug(url);
        return this.http.get<{success: boolean, result: Event[], error: string}>(url);
    }

    addEvent(event: Event) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/calendar/';
        this.logger.debug('adding event', event);
        return this.http.post<{
            'success': boolean,
            'result': Event,
            'error': string
        }>(url, event);
    }
}
