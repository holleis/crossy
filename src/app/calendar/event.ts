import { Club } from '../tournament/club';
import { DateTime } from 'luxon';

export class Event {

    _id: string;
    title = '1st Speedminton Moon-Open';
    location = 'Dark side of the moon';
    organiser: Club = new Club();
    responsibles: string[] = []; // Player ID []
    start_date: string = DateTime.utc().toFormat('dd.MM.yyyy');
    start_date_epoch: number;
    end_date: string = DateTime.utc().toFormat('dd.MM.yyyy');
    end_date_epoch: number;
    event_type = 'tournament';
    links = 'WWW, Facebook, ...';
    details = '';
}
