import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Ranking } from './ranking/ranking';
import { User } from './auth/user';
import { Role } from './user/role';
import { UserService } from './auth/_services/user.service';
import { Subject } from 'rxjs';
import { LoginChangedService } from './auth/_services/loginchanged.service';
import { NGXLogger } from 'ngx-logger';
import { takeUntil } from 'rxjs/operators';
import { OverlayContainer } from '@angular/cdk/overlay';
import { environment } from '../environments/environment';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [LoginChangedService]
})

export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

    loggedInUser: User = null;
    title = 'Crossy';
    ranking: Ranking;

    mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;
    private ngUnsubscribe = new Subject();

    darkmodeMode: 'auto' | 'lightmode' | 'darkmode' = 'lightmode';
    darkColorSchemeMatcher: MediaQueryList;
    darkColorSchemeChanged: () => void;
    isDarkmode = false;

    latestWebVersion: string;
    buildNr: string;

    @ViewChild('snav') sideNav: MatSidenav;

    constructor(
        private loginChangedService: LoginChangedService,
        public changeDetectorRef: ChangeDetectorRef,
        public media: MediaMatcher,
        private userService: UserService,
        private logger: NGXLogger,
        private overlayContainer: OverlayContainer,
        private changeDetRef: ChangeDetectorRef,
        public mediaMatcher: MediaMatcher,
    ) {
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        // this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        // this.mobileQuery.addListener(this._mobileQueryListener);
        try {
            // Chrome & Firefox
            this.mobileQuery.addEventListener('change', () => changeDetectorRef.detectChanges());
        } catch (e1) {
            try {
                // Safari
                this.mobileQuery.addListener(changeDetectorRef.detectChanges);
            } catch (e2) {
                // ignore
                this.logger.error('error: could not assign small screen listener');
            }
        }
    }

    ngOnInit(): void {
        this.latestWebVersion = environment.latestWebVersion;
        this.buildNr = environment.buildNr;
        // !needs to be registered before the loginChangedService!
        // called when user changes dark mode in user settings
        this.userService.darkmodeMode$
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(dm => {
                this.darkmodeMode = dm;
                this.isDarkmode = this.userService.isDarkModeEnabled(dm);
                this.updateOverlayClass();
            }
            );
        this.darkmodeMode = this.userService.getDarkmode();
        this.isDarkmode = this.userService.isDarkModeEnabled();

        // !needs to be registered after the darkmodeMode subscription!
        this.darkColorSchemeMatcher = this.mediaMatcher.matchMedia('(prefers-color-scheme: dark)');
        this.darkColorSchemeChanged = () => {
            if (this.userService.getDarkmode() === 'auto') {
                // update all listeners
                this.userService.storeDarkmode('auto');
                this.darkmodeMode = 'auto';
                this.isDarkmode = this.userService.isDarkModeEnabled('auto');
                this.updateOverlayClass();
                this.changeDetRef.detectChanges();
            }
        }

        try {
            // Chrome & Firefox
            this.darkColorSchemeMatcher.addEventListener('change', () => this.darkColorSchemeChanged);
        } catch (e1) {
            try {
                // Safari
                this.darkColorSchemeMatcher.addListener(this.darkColorSchemeChanged);
            } catch (e2) {
                // ignore
                this.logger.error('error: could not assign darkColorSchemeCahnge listener');
            }
        }

        // TODO check localStorage for token to see if still logged in
        // this.loginStateChanged(true);

        this.loginChangedService.loginStatus
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                status => this.loginStateChanged(status)
            );
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.sideNav?.toggle(false);
            setTimeout(() => {
                this.sideNav?.toggle(true);
            }, 1);
        }, 1);
    }

    ngOnDestroy(): void {
        this.darkColorSchemeMatcher.removeListener(this.darkColorSchemeChanged);
        this.ngUnsubscribe.next('');
        this.ngUnsubscribe.complete();
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    isAllowed(route: string): boolean {
        if (route === 'users') {
            return this.loggedInUser &&
                 (this.loggedInUser.roles & Role.ROLE_PAUL) > 0;
                // ((this.loggedInUser.roles & Role.ROLE_DCV) > 0 ||
                //  (this.loggedInUser.roles & Role.ROLE_PAUL) > 0);
        }
        if (route === 'dcv') {
            return this.loggedInUser &&
                ((this.loggedInUser.roles & Role.ROLE_DCV) > 0 ||
                 (this.loggedInUser.roles & Role.ROLE_PAUL) > 0);
        }
        return false;
    }

    async loginStateChanged(status: boolean) {
        let currentUser;
        if (status) {
            currentUser = this.userService.getCurrentUser();
        }

        if (status && currentUser) {
            if (this.loggedInUser && this.loggedInUser._id === currentUser._id) {
                this.logger.debug('user still logged in');
              // still logged in, nothing to do
            } else {
                this.logger.debug('new or different user logged in!');
                this.loggedInUser = currentUser;
            }
        } else {
            this.logger.debug('no user logged in');
            // not logged in
            this.loggedInUser = null;
        }
    }

    updateOverlayClass(): void {
        // in template for the app component:
        // <div style="height: 100%;" [ngClass]="{'force-dark-mode': darkmodeMode === 'darkmode', 'force-light-mode': darkmodeMode === 'lightmode', 'auto-mode': darkmodeMode === 'auto'}">
        // needs to be put on overlay separately (https://material.angular.io/guide/theming#multiple-themes, https://material.angular.io/cdk/overlay/overview)
        if (this.darkmodeMode === 'darkmode') {
            this.overlayContainer.getContainerElement().classList.remove('auto-mode');
            this.overlayContainer.getContainerElement().classList.remove('force-light-mode');
            this.overlayContainer.getContainerElement().classList.add('force-dark-mode');
        } else if (this.darkmodeMode === 'lightmode') {
            this.overlayContainer.getContainerElement().classList.remove('auto-mode');
            this.overlayContainer.getContainerElement().classList.remove('force-dark-mode');
            this.overlayContainer.getContainerElement().classList.add('force-light-mode');
        } else if (this.darkmodeMode === 'auto') {
            this.overlayContainer.getContainerElement().classList.remove('force-light-mode');
            this.overlayContainer.getContainerElement().classList.remove('force-dark-mode');
            this.overlayContainer.getContainerElement().classList.add('auto-mode');
        }
    }
}
