import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { Category } from '../tournament/category';

@Injectable()
export class CategoriesService {

    constructor(private http: HttpClient) {}

    getCategoryNames(federation: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/categories/names/';
        if (federation) {
            return this.http.get<{
                'success': boolean,
                'result': {_id: string, name: string}[],
                'error': string
            }>(url + encodeURI(federation));
        } else {
            return this.http.get<{
                'success': boolean,
                'result': {_id: string, name: string}[],
                'error': string
            }>(url + 'ALL');
        }
    }

    getCategories() {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/categories';
        return this.http.get<{
            'success': boolean,
            'result': Category[],
            'error': string
        }>(url);
    }
}
