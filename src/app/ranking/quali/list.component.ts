import { TournamentsService } from './../../tournament/tournaments.service';
import { Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { RankingService } from '../ranking.service';
import { NGXLogger } from 'ngx-logger';
import { Player } from '../../player/player';
import { Ranking } from '../ranking';
import { AlertService } from '../../util/alert/alert.service';
import { PlayerService } from '../../player/player.service';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { EditEntryComponent } from './edit-entry.component';
import { DateTime } from 'luxon';
import { Tournament } from '../../tournament/tournament';
import { UserService } from '../../auth/_services/user.service';
import { Role } from '../../user/role';
import { map, Observable, Subject, takeUntil } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    providers: [PlayerService]
})
export class ListComponent implements OnInit, OnDestroy {
    year = 2018;

    catNames = ['DM Quali ' + this.year + ' Open', 'DM Quali ' + this.year + ' Damen', 'DM Quali ' + this.year + ' Ü40 Damen', 'DM Quali ' + this.year + ' Ü50 Damen', 'DM Quali ' + this.year + ' Ü40 Herren', 'DM Quali ' + this.year + ' Ü50 Herren'];

    rankings: {player: Player, points: number, details: string[], totalrank: number}[][] = [];
    // myPlayerId: string;
    myRoles: number;
    rankingTable: {player: Player, points: number, details: {points: number, rank: number, bround: boolean, pointsdontcount: boolean}[], totalrank: number}[][] = [];
    // stores for all categories the list of tournament titles to be displayed
    allTournaments: string[][] = [];
    allTournamentIds: Tournament[][] = [];
    dates: Date[][] = [];
    tourPlus: { t?: string, d?: Date, id?: string, onlyDoubles?: boolean, onlySingles?: boolean, upcoming?: boolean }[][] = [];

    // addMode = -1;
    waiting = false;
    waitingUpcoming = false;
    waitingDmPart = false;
    // expanded = -1;

    newRanking: Ranking;
    nrPlayers = 10;
    everyRank = false;
    btree = false;
    betweenRanks = false;
    abPlayed = false;

    // catName => player._id => category.name => yes | unknown | no
    dmpart: { [playerId: string]: { [catName: string]: string; }; };

    countRankingsRetrieved = 0;

    // outputCtrl: FormControl;
    // playerCtrl: FormControl;
    // selectedPlayer: Player;
    // players: Player[] = [];
    // filteredPlayers: Observable<Player[]>;
    // @ViewChild('input', { read: MatAutocompleteTrigger, static: false }) autoComplete: MatAutocompleteTrigger;

    // @ViewChildren('h1', { read: ElementRef }) headings: ElementRef[];
    private _headings: QueryList<ElementRef>;
    get headings() { return this._headings; }
    @ViewChildren('tableHeading') set headings(hds: QueryList<ElementRef>) {
        this._headings = hds;
    }
    @ViewChild('top') linkTable: ElementRef;

    // unfolded: boolean[] = [];
    private ngUnsubscribe = new Subject();
    isSmall$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 700px)')
        .pipe(map(result => result.matches));

    uploadTournamentName: string;

    constructor(
        private rankingService: RankingService,
        private userService: UserService,
        private alertService: AlertService,
        private router: Router,
        // private playerService: PlayerService,
        private breakpointObserver: BreakpointObserver,
        private logger: NGXLogger,
        private titleService: Title,
        private dialog: MatDialog,
        private route: ActivatedRoute,
        private tournamentService: TournamentsService,
    ) {
        // this.playerCtrl = new FormControl();
        // // this.outputCtrl = new FormControl();
        // this.filteredPlayers = this.playerCtrl.valueChanges
        //     .pipe(
        //         startWith(''),
        //         map(player => player ? this.filterPlayers(player) : this.players.slice())
        //     );
    }

    ngOnInit() {
        this.router.events
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((e: any) => {
                if (e instanceof NavigationEnd && e.url.indexOf('dmquali') >= 0) {
                    // only pass and debounce interesting events
                    this.init();
                }
            }
        );

        this.init();
    }

    ngOnDestroy(): void {
        this.ngUnsubscribe.next('');
        this.ngUnsubscribe.complete();
    }

    init() {
        if (this.route.snapshot.queryParams.year && !isNaN(parseInt(this.route.snapshot.queryParams.year))) {
            this.year = parseInt(this.route.snapshot.queryParams.year, 10);
        } else {
            this.year = new Date().getFullYear();
        }
        this.catNames = [
            'DM Quali ' + this.year + ' Open',
            'DM Quali ' + this.year + ' Damen',
            'DM Quali ' + this.year + ' Ü40 Damen',
            'DM Quali ' + this.year + ' Ü50 Damen',
            'DM Quali ' + this.year + ' Ü60 Damen',
            'DM Quali ' + this.year + ' Ü40 Herren',
            'DM Quali ' + this.year + ' Ü50 Herren',
            'DM Quali ' + this.year + ' Ü60 Herren'];

        this.titleService.setTitle('DM Quali ' + this.year + ' Ranglisten');

        this.dmpart = {};
        for (let c = 0; c < this.catNames.length; c++) {
            const catName = this.catNames[c];
            this.dmpart[catName] = {};
        }

        const user = this.userService.getCurrentUser();
        if (user) {
            // this.myPlayerId = user.player_id;
            this.myRoles = user.roles;
            // // tslint:disable-next-line: no-bitwise
            // if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            //     this.getDCVPlayers();
            // }
        }

        this.newRanking = new Ranking();
        // initial defaults
        this.newRanking.bracket = 'A';
        this.newRanking.rank = 1;
        this.newRanking.points = 300;
        this.newRanking.federation = 'DCV';
        this.newRanking.nrPlayers = 10;
        this.newRanking.everyRank = false;
        this.newRanking.betweenRanks = false;
        this.newRanking.abPlayed = false;

        this.countRankingsRetrieved = 0;
        this.waiting = true;
        for (let c = 0; c < this.catNames.length; c++) {
            const catName = this.catNames[c];
            this.getQualiRanking(catName, c, -1, this.catNames.length);
        }

        this.waitingDmPart = true;
        this.rankingService.getDMParticipations(this.year.toString()).subscribe({
            next: response => {
                this.waitingDmPart = false;
                if (response.success === true) {
                    const res = response.result;
                    for (let r = 0; r < res.length; r++) {
                        const data = res[r];
                        const entry = this.dmpart[data.catName];
                        if (!entry) {
                            this.dmpart[data.catName] = { [data.player]: data.dmpart };
                        } else {
                            this.dmpart[data.catName][data.player] = data.dmpart;
                        }
                    }
                } else {
                    this.logger.error('Could not get DM participations', response.error);
                }
            },
            error: error => {
                this.waitingDmPart = false;
                this.logger.error('Could not get DM participations', error);
            }
        });
    }

    scrollTo(catName: string) {
        if (catName === 'top') {
            this.linkTable.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            return;
        }
        const headings = this.headings.toArray();
        for (const heading of headings) {
            if (heading?.nativeElement?.innerText === catName) {
                heading.nativeElement.scrollIntoView({ behavior: 'smooth' });
                break;
            }
        }
    }

    getQualiRanking(catName: string, catIdx: number, expandIdx: number, totalNrOfRankings: number = Number.MAX_SAFE_INTEGER) {
        this.tourPlus = [];
        this.allTournaments = [];
        this.rankingTable = [];
        this.dates = [];
        this.waiting = true;
        this.rankingService.getQualiRanking(catName, true).subscribe(
            response => {
                this.countRankingsRetrieved += 1;
                if (response.success === true) {
                    const rawranking = response.result.ranking;
                    const listing = {};
                    // const listing: { [name: string]: { player: Player, points: number[], details: string[], pointsdontcount?: boolean[], totalrank?: number }} = {};

                    // for the table view, first collect all tournaments
                    const table = {};
                    this.allTournaments[catIdx] = [];
                    this.allTournamentIds[catIdx] = [];
                    let t = 0;
                    if (!this.dates[catIdx]) this.dates[catIdx] = [];
                    for (let r = 0; r < rawranking.length; r++) {
                        if (this.allTournaments[catIdx].indexOf(rawranking[r].tournament) < 0) {
                            this.allTournaments[catIdx].push(rawranking[r].tournament);
                            this.allTournamentIds[catIdx].push(rawranking[r].tournamentId);
                            this.dates[catIdx][t] = rawranking[r].updated ? DateTime.fromISO(rawranking[r].updated).toJSDate() : undefined;
                            t += 1;
                        }
                    }

                    const nrPlayersPerTournament = {};
                    for (let r = 0; r < rawranking.length; r++) {
                        const detail = rawranking[r].points + ' Pkt. für Platz ' + rawranking[r].rank + ' (' + (rawranking[r].bracket === 'B' ? 'B-Runde ' : '') + rawranking[r].tournament + ')';
                        const name = rawranking[r].player[0].first_name + ' ' + rawranking[r].player[0].last_name;
                        if (typeof listing[name] === 'undefined') {
                            listing[name] = {player: rawranking[r].player[0], points: [rawranking[r].points], details: [detail]};
                        } else {
                            // push points in order to get the highest 5 entries later
                            listing[name].points.push(rawranking[r].points);
                            listing[name].details.push(detail);
                        }

                        const tournament = rawranking[r].tournament;
                        if (!nrPlayersPerTournament[tournament]) {
                            if (rawranking[r].nrPlayers) {
                                nrPlayersPerTournament[tournament] = rawranking[r].nrPlayers;
                            } else {
                                // calculate
                                let cnt = 0;
                                for (let r2 = 0; r2 < rawranking.length; r2++) {
                                    if (rawranking[r2].tournament === tournament) {
                                        cnt++;
                                    }
                                }
                                nrPlayersPerTournament[tournament] = cnt;
                            }
                        }

                        const tIdx = this.allTournaments[catIdx].indexOf(tournament);
                        if (typeof table[name] === 'undefined') {
                            const details = [];
                            for (let t = 0; t < this.allTournaments[catIdx].length; t++) {
                                details[t] = {points: -1, rank: -1, bround: false};
                            }
                            // details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B'};
                            details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B',
                                    abPlayed: rawranking[r].abPlayed || false, nrPlayers: rawranking[r].nrPlayers || nrPlayersPerTournament[tournament], everyRank: rawranking[r].everyRank || false, betweenRanks: rawranking[r].betweenRanks || false};
                            table[name] = {player: rawranking[r].player[0], points: [rawranking[r].points], details};
                        } else {
                            // table[name].details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B'};
                            table[name].details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B',
                                    abPlayed: rawranking[r].abPlayed, nrPlayers: rawranking[r].nrPlayers, everyRank: rawranking[r].everyRank, betweenRanks: rawranking[r].betweenRanks};
                            table[name].points.push(rawranking[r].points);
                        }
                    }

                    // use highest 5 entries only
                    const names = Object.getOwnPropertyNames(listing);
                    for (let n = 0; n < names.length; n++) {
                        const l_entry = listing[names[n]];
                        if (l_entry) {
                            // mark those that are not in the 5 highest entries
                            const idcs = l_entry.points.map((p: number, i: number) => ({ i: i, p: p })).sort((p1: {i: number, p: number}, p2: {i: number, p: number}) => p2.p - p1.p).slice(0, 5).map((p: any) => p.i);
                            l_entry.pointsdontcount = [];
                            for (let i = 0; i < idcs.length; i++) {
                                const idc = idcs[i];
                                l_entry.pointsdontcount[idc] = false;
                            }
                            // sum highest 5 entries
                            l_entry.points = l_entry.points
                                .sort((p1: number, p2: number) => p2 - p1)
                                .slice(0, 5)
                                .reduce((p1: number, p2: number) => p1 + p2, 0);
                        }
                        const t_entry = table[names[n]];
                        if (t_entry) {
                            t_entry.points = l_entry.points;
                            // get indices of tournaments with highest 5 points for this player (grey out others)
                            const idcs = t_entry.details.map((d: any, i: number) => ({ i: i, d: d})).sort((d1: any, d2: any) => d2.d.points - d1.d.points).slice(0, 5).map((d: any) => d.i);
                            for (let i = 0; i < idcs.length; i++) {
                                const idc = idcs[i];
                                t_entry.details[idc].pointsdontcount = false;
                            }
                        }
                    }

                    this.rankingTable[catIdx] = Object.values(table);
                    this.rankingTable[catIdx].sort((r1, r2) => r2.points - r1.points);

                    this.rankings[catIdx] = Object.values(listing);
                    this.rankings[catIdx].sort((r1, r2) => r2.points - r1.points);

                    let rank = 1;
                    let jumped = 1;
                    let lastpoints = -1;
                    for (let p = 0; p < this.rankings[catIdx].length; p++) {
                        if (p > 0) {
                            if (this.rankings[catIdx][p].points !== lastpoints) {
                                rank += jumped;
                                jumped = 1;
                            } else {
                                jumped++;
                            }
                        }
                        this.rankings[catIdx][p].totalrank = rank;
                        this.rankingTable[catIdx][p].totalrank = rank;
                        lastpoints = this.rankings[catIdx][p].points;
                    }

                    //
                    // for (let e = 0; e < this.rankings[idx].length; e++) {
                    //     const entry = this.rankings[idx][e];
                    //     for (let t = 0; t < this.allTournaments[idx].length; t++) {
                    //         this.logger.debug(entry.details[t].points);
                    //     }
                    // }

                    // if (expandIdx >= 0) {
                    //     this.expanded = expandIdx;
                    // }
                } else {
                    this.logger.error('Could not retrieve rankings for', catName, response.error);
                }
                this.waiting = false;

                if (this.countRankingsRetrieved === totalNrOfRankings) {
                    // all rankings retrieved
                    this.addUpcomingTournaments();
                }
            },
            error => {
                this.countRankingsRetrieved += 1;
                this.logger.error('Could not retrieve categories for ', catName, error);
                this.waiting = false;
            }
        );
    }

    addUpcomingTournaments() {
        this.waitingUpcoming = true;
        this.tournamentService.getFutureTournaments(false).subscribe({
            next: response => {
                this.waitingUpcoming = false;
                if (response.success === true) {
                    response.result = response.result.filter((t) => new Date(t.start_date_epoch).getFullYear() === this.year);
                    response.result.sort((t1, t2) => t1.start_date_epoch - t2.start_date_epoch);
                    for (let t = 0; t < response.result.length; t++) {
                        const tournament = response.result[t];
                        const onlyDoubles = this.rankingService.hasNoSingles(tournament);
                        if (this.rankingService.isRankingTournament(tournament, tournament.categories) && !onlyDoubles) {
                            for (let catNr = 0; catNr < this.allTournaments.length; catNr++) {
                                this.allTournaments[catNr].push(tournament.title + ' ' + tournament.start_date);
                                if (!this.tourPlus[catNr]) this.tourPlus[catNr] = [];
                                this.tourPlus[catNr][this.allTournaments[catNr].length - 1] = { upcoming: true };
                            }
                        }
                    }
                } else {
                    this.logger.error('Could not get future tournaments', response.error);
                }
            },
            error: error => {
                this.waitingUpcoming = false;
                this.logger.error('Could not get future tournaments ', error);
            }
        });
    }

    // getDCVPlayers() {
    //     this.playerService.getPlayersPerNation('GER').subscribe(
    //         response => {
    //             if (response.success === true) {
    //                 this.players = response.result;
    //             } else {
    //                 this.logger.error('Could not retrieve players', response.error);
    //             }
    //         },
    //         error => {
    //             this.logger.error('Could not retrieve players ', error);
    //         }
    //     );
    // }

    canAdd(): boolean {
        // tslint:disable-next-line: no-bitwise
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            // DCV is allowed
            return true;
        }
        return false;
    }

    dmpartChanged(catName: string, playerId: string, dmpart: 'unknown' | 'yes' | 'no'): void {
        const prevValue = this.dmpart[catName][playerId];
        this.dmpart[catName][playerId] = dmpart;
        console.log(`change to dmpart ${catName} / ${playerId}; now ${this.dmpart[catName][playerId]}`);
        this.waitingDmPart = true;
        this.rankingService.setDMParticipation(this.year.toString(), catName, playerId, this.dmpart[catName][playerId]).subscribe({
            next: response => {
                this.waitingDmPart = false;
                if (response.success === true) {
                    if (dmpart === 'yes') {
                        // check whether the player also appears in other catgories
                        for (let c = 0; c < this.catNames.length; c++) {
                            const cn = this.catNames[c];
                            if (cn !== catName) {
                                if (!this.dmpart[cn][playerId] || this.dmpart[cn][playerId] === 'yes' || this.dmpart[cn][playerId] === 'unknown') {
                                    this.waitingDmPart = true;
                                    this.rankingService.setDMParticipation(this.year.toString(), cn, playerId, 'no').subscribe({
                                        next: response2 => {
                                            this.waitingDmPart = false;
                                            if (response2.success === true) {
                                                this.dmpart[cn][playerId] = 'no';
                                            } else {
                                                this.logger.error('Could not set DM participation', response2.error);
                                                this.dmpart[catName][playerId] = prevValue;
                                                this.alertService.error($localize`:@@Could not set DM participation:`, response2.error);
                                            }
                                        },
                                        error: error => {
                                            this.waitingDmPart = false;
                                            this.logger.error('Could not set DM participation', error);
                                            this.dmpart[catName][playerId] = prevValue;
                                            this.alertService.error($localize`:@@Could not set DM participation:`, error);
                                        }
                                    });
                                }
                            }
                        }
                    }
                } else {
                    this.logger.error('Could not set DM participation', response.error);
                    this.dmpart[catName][playerId] = prevValue;
                    this.alertService.error($localize`:@@Could not set DM participation:`, response.error);
                }
            },
            error: error => {
                this.waitingDmPart = false;
                this.logger.error('Could not set DM participation', error);
                this.dmpart[catName][playerId] = prevValue;
                this.alertService.error($localize`:@@Could not set DM participation:`, error);
            }
        });
    }

    // addPoints(rIdx: number, playerName?: string, reload: boolean = true) {
    //     this.waiting = true;
    //     let searchName = playerName;
    //     if (!searchName) {
    //         searchName = this.playerCtrl.value;
    //     }
    //     let playerId;
    //     for (let p = 0; p < this.players.length; p++) {
    //         if (this.players[p].first_name + ' ' + this.players[p].last_name === searchName) {
    //             playerId = this.players[p]._id;
    //             break;
    //         }
    //     }
    //     if (!playerId) {
    //         this.logger.debug('error: player not found');
    //         this.alertService.error($localize`:@@Player not found!:`);
    //         return;
    //     }
    //     const catName = this.catNames[rIdx];
    //     this.logger.debug('adding new ranking:', this.newRanking, 'for', playerId, 'in', catName);

    //     if (this.newRanking) {
    //         this.newRanking.federation = 'DCV';
    //         this.newRanking.category = catName as any;
    //         this.newRanking.player = playerId;
    //         this.newRanking.nrPlayers = this.nrPlayers;
    //         this.newRanking.everyRank = this.everyRank;
    //         this.newRanking.betweenRanks = this.betweenRanks;
    //         this.newRanking.abPlayed = this.abPlayed;

    //         this.rankingService.addRanking(this.newRanking).subscribe(
    //             response => {
    //                 this.logger.debug('update qualification ranking:', response);
    //                 if (response.success === true) {
    //                     this.logger.debug('successfully updated qualification ranking');
    //                     this.alertService.success($localize`:@@Added entry:`);
    //                     // clear player field
    //                     this.newRanking.player = undefined;
    //                     this.playerCtrl.reset();
    //                     if (reload) {
    //                         this.getQualiRanking(catName, rIdx, rIdx);
    //                     } else {
    //                         this.waiting = false;
    //                     }
    //                     this.rankingService.updateLeaderboard().subscribe(
    //                         _ => {
    //                             this.logger.debug('successfully called updateLeaderboard');
    //                         },
    //                         error2 => {
    //                             this.logger.error('Could not update leaderboard:', error2);
    //                             this.alertService.error($localize`:@@Error adding:`, error2);
    //                         }
    //                     );
    //                 } else {
    //                     this.logger.error('Could not update qualification ranking:', response.error);
    //                     this.alertService.error($localize`:@@Error adding:`, response.error);
    //                     this.waiting = false;
    //                 }
    //             },
    //             error => {
    //                 this.logger.error('Could not update qualification ranking:', error);
    //                 this.alertService.error($localize`:@@Error adding:`, error);
    //                 this.waiting = false;
    //             }
    //         );
    //     }
    // }

    // add(rIdx: number) {
    //     this.addMode = rIdx;
    // }

    // cancel() {
    //     this.addMode = -1;
    // }

    // filterPlayers(name: string) {
    //     return this.players.filter(player =>
    //         (player.first_name + ' ' + player.last_name).toLowerCase().indexOf(name.toLowerCase()) >= 0);
    // }

    // unfold(eIdx: number) {
    //     this.unfolded[eIdx] = !this.isunfolded(eIdx);
    // }

    // isunfolded(eIdx: number): boolean {
    //     return this.unfolded[eIdx];
    // }

    treeChanged() {
        this.newRanking.bracket = this.btree ? 'B' : 'A';
        this.rankchanged();
    }

    rankchanged() {
        if (this.nrPlayers < 16) {
            this.abPlayed = false;
        }
        if (this.newRanking.bracket === 'A') {
            this.newRanking.points = this.rankingService.calcPointsA(this.newRanking.rank, this.nrPlayers, this.everyRank, this.betweenRanks, this.abPlayed);
            // switch (this.newRanking.rank) {
            //     case 1: this.newRanking.points = 300; break;
            //     case 2: this.newRanking.points = 251; break;
            //     case 3: case 4: this.newRanking.points = 214; break;
            //     case 5: case 6: case 7: case 8: this.newRanking.points = 182; break;
            //     default:
            //         if (this.newRanking.rank <= 12) {
            //             this.newRanking.points = 150;
            //         } else if (this.newRanking.rank <= 16) {
            //             this.newRanking.points = 126;
            //         } else {
            //             this.newRanking.points = 0;
            //         }
            // }
        } else {
            this.newRanking.points = this.rankingService.calcPointsB(this.newRanking.rank, this.nrPlayers, this.everyRank);
            // switch (this.newRanking.rank) {
            //     case 1: this.newRanking.points = 100; break;
            //     case 2: this.newRanking.points = 90; break;
            //     case 3: case 4: this.newRanking.points = 85; break;
            //     case 5: case 6: case 7: case 8: this.newRanking.points = 76; break;
            //     default:
            //         if (this.newRanking.rank <= 12) {
            //             this.newRanking.points = 63;
            //         } else if (this.newRanking.rank <= 16) {
            //             this.newRanking.points = 53;
            //         } else {
            //             this.newRanking.points = 26;
            //         }
            // }
        }
    }

    changeEntry(entry, rIdx: number, tIdx: number) {
        this.logger.debug(entry);

        this.waiting = true;
        const playerId = entry.player._id;
        const catName = this.catNames[rIdx];

        const editRanking = new Ranking();
        editRanking.category = catName as any;
        editRanking.player = playerId;
        editRanking.tournament = this.allTournaments[rIdx][tIdx];
        editRanking.bracket = entry.details[tIdx].bround ? 'B' : 'A';
        editRanking.nrPlayers = entry.details[tIdx].nrPlayers;
        editRanking.everyRank = entry.details[tIdx].everyRank;
        editRanking.betweenRanks = entry.details[tIdx].betweenRanks;
        editRanking.abPlayed = entry.details[tIdx].abPlayed;

        editRanking.rank = entry.details[tIdx].rank;
        editRanking.points = entry.details[tIdx].points;

        const myDialogRef = this.dialog.open(EditEntryComponent, {
            closeOnNavigation: true,
            data: { ranking: editRanking }
        });

        myDialogRef.afterClosed().subscribe(result => {
            if (result) {
                // editRanking.rank = result.rank;
                // editRanking.points = result.points;
                // editRanking.nrPlayers = result.nrPlayers;
                Object.assign(editRanking, result.ranking);

                this.rankingService.changeRanking(editRanking).subscribe(
                    response => {
                        this.logger.debug('update qualification ranking:', response);
                        if (response.success === true) {
                            this.logger.debug('successfully updated qualification ranking');
                            this.alertService.success($localize`:@@Entry updated:`);
                            // clear player field
                            editRanking.player = undefined;
                            // this.playerCtrl.reset();
                            // if (reload) {
                                this.getQualiRanking(catName, rIdx, rIdx);
                            // } else {
                            //     this.waiting = false;
                            // }
                        } else {
                            this.logger.error('Could not update qualification ranking:', response.error);
                            this.alertService.error($localize`:@@Error updating:`, response.error);
                            this.waiting = false;
                        }
                    },
                    error => {
                        this.logger.error('Could not update qualification ranking:', error);
                        this.alertService.error($localize`:@@Error updating:`, error);
                        this.waiting = false;
                    }
                );
            } else {
                this.waiting = false;
            }
        });
    }

    // uploadCSV(fevent, rIdx) {
    //     this.logger.debug('importing for', this.uploadTournamentName);
    //     const fileList: FileList = fevent.target.files;
    //     if (fileList.length > 0) {
    //         const file: File = fileList[0];
    //         const fr = new FileReader();
    //         const listcomp = this;
    //         fr.readAsText(file);
    //         fr.onloadend = () => {
    //             const contents = fr.result.toString();
    //             const error  = fr.error;
    //             if (error != null) {
    //                 this.logger.error('File could not be read! Code ' + error.code);
    //             } else {
    //                 this.logger.debug('importing', contents, 'into', listcomp.catNames[rIdx].substring(14));

    //                 const strsplit = contents.split('\n');
    //                 for (let i = 0; i < strsplit.length; i++) {
    //                     const line = strsplit[i];
    //                     if (line.trim() === '') {
    //                         continue;
    //                     }
    //                     let rank, last_name, first_name, points;
    //                     let bround = false;
    //                     // this.logger.debug('working on line', line);
    //                     const linesplit = line.split(';');
    //                     if (linesplit.length > 0) {
    //                         rank = parseInt(linesplit[0].trim(), 10);
    //                     }
    //                     // 2nd item is empty (Ophardt ID)
    //                     if (linesplit.length > 2) {
    //                         last_name = linesplit[2].trim();
    //                         last_name = last_name.substring(1, last_name.length - 1);
    //                     }
    //                     if (linesplit.length > 3) {
    //                         first_name = linesplit[3].trim();
    //                         first_name = first_name.substring(1, first_name.length - 1);
    //                     }
    //                     if (linesplit.length > 4) {
    //                         const points_txt = linesplit[4].trim();
    //                         points = points_txt.split(', ')[0];
    //                         if (points[0] === '"') {
    //                             bround = true;
    //                             points = parseInt(points.substring(1), 10);
    //                         }
    //                     }
    //                     this.logger.debug('working with', rank, '/', last_name, '/', first_name, '/', points);

    //                     listcomp.newRanking = new Ranking();
    //                     listcomp.newRanking.bracket = bround ? 'B' : 'A';
    //                     listcomp.newRanking.points = points;
    //                     listcomp.newRanking.rank = rank;
    //                     listcomp.newRanking.tournament = listcomp.uploadTournamentName as any;
    //                     listcomp.newRanking.updated = DateTime.now().toISODate();
    //                     listcomp.addPoints(rIdx, first_name + ' ' + last_name, false);
    //                 }
    //                 listcomp.getQualiRanking(listcomp.catNames[rIdx], rIdx, -1);
    //             }
    //         };
    //     }
    // }
}
