import { Player } from '../../player/player';

export class DMParticipation {

    _id: string;
    catName: string;
    year: number;
    player: Player;
    dmpart: string;
}
