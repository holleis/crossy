import { RankingService } from './../ranking.service';
import { Ranking } from '../ranking';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-edit-entry',
    templateUrl: './edit-entry.component.html',
    styleUrls: ['./edit-entry.component.css'],
})
export class EditEntryComponent implements OnInit {

    rank: number;
    points: number;
    suggestedPoints: number;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { ranking: Ranking },
        private dialogRef: MatDialogRef<EditEntryComponent>,
        private rankingService: RankingService,
    ) { }

    ngOnInit() {
        this.rank = this.data.ranking.rank;
        this.points = this.data.ranking.points;
        this.rankChanged();
    }

    save() {
        this.data.ranking.rank = this.rank;
        this.data.ranking.points = this.points;
        this.dialogRef.close(this.data.ranking);
    }

    rankChanged() {
        if (this.data.ranking.nrPlayers < 16) {
            this.data.ranking.abPlayed = false;
        }
        if (this.data.ranking.bracket === 'A') {
            this.suggestedPoints = this.rankingService.calcPointsA(this.rank, this.data.ranking.nrPlayers, this.data.ranking.everyRank, this.data.ranking.betweenRanks, this.data.ranking.abPlayed);
        } else {
            this.suggestedPoints = this.rankingService.calcPointsB(this.rank, this.data.ranking.nrPlayers, this.data.ranking.everyRank);
        }
    }

    useSuggestion() {
        this.points = this.suggestedPoints;
    }

}
