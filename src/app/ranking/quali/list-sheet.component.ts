import { Component, OnInit, ViewChild } from '@angular/core';
import { RankingService } from '../ranking.service';
import { NGXLogger } from 'ngx-logger';
import { UserService } from '../../auth/_services/user.service';
import { Role } from '../../user/role';
import { Player } from '../../player/player';

import { Ranking } from '../ranking';
import { AlertService } from '../../util/alert/alert.service';
import { PlayerService } from '../../player/player.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { Title } from '@angular/platform-browser';
import { DateTime } from 'luxon';

@Component({
    selector: 'app-list-sheet',
    templateUrl: './list-sheet.component.html',
    styleUrls: ['./list-sheet.component.css'],
    providers: [PlayerService]
})
export class ListSheetComponent implements OnInit {

    thisYear = new Date().getFullYear();
    catNames = [`DM Quali ${this.thisYear} Open`, `DM Quali ${this.thisYear} Damen`, `DM Quali ${this.thisYear} Ü40 Damen`, `DM Quali ${this.thisYear} Ü50 Damen`, `DM Quali ${this.thisYear} Ü40 Herren`, `DM Quali ${this.thisYear} Ü50 Herren`];

    rankings: {player: Player, points: number, details: string[], totalrank: number}[][] = [];
    myPlayerId: string;
    myRoles: number;
    rankingTable: {player: Player, points: number, details: {points: number, rank: number, bround: boolean}[], totalrank: number}[][] = [];
    allTournaments: string[][] = [];

    addMode = -1;
    waiting = false;
    expanded = -1;

    newRanking: Ranking;

    // outputCtrl: FormControl;
    playerCtrl: FormControl<string>;
    selectedPlayer: Player;
    players: Player[] = [];
    filteredPlayers: Observable<Player[]>;
    @ViewChild('input', { read: MatAutocompleteTrigger, static: false }) autoComplete: MatAutocompleteTrigger;

    unfolded: boolean[] = [];
    maxEntries = 0;

    uploadTournamentName: string;

    constructor(
        private rankingService: RankingService,
        private userService: UserService,
        private alertService: AlertService,
        // private playerService: PlayerService,
        private logger: NGXLogger,
        private titleService: Title
    ) {
        this.playerCtrl = new FormControl<string>('');
        // this.outputCtrl = new FormControl();
        this.filteredPlayers = this.playerCtrl.valueChanges
            .pipe(
                startWith(''),
                map(player => player ? this.filterPlayers(player) : this.players.slice())
            );
    }

    ngOnInit() {
        this.titleService.setTitle(`DM Quali ${this.thisYear} Ranglisten`);

        this.newRanking = new Ranking();
        // initial defaults
        this.newRanking.bracket = 'A';
        this.newRanking.rank = 1;
        this.newRanking.points = 300;
        this.newRanking.federation = 'DCV';

        for (let c = 0; c < this.catNames.length; c++) {
            const catName = this.catNames[c];
            this.getQualiRanking(catName, c, -1);
        }

        const user = this.userService.getCurrentUser();
        if (user) {
            this.myPlayerId = user.player_id;
            this.myRoles = user.roles;
            // if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            //     this.getDCVPlayers();
            // }
        }
    }

    getQualiRanking(catName, idx, expandIdx) {
        this.rankingService.getQualiRanking(catName, true).subscribe(
            response => {
                if (response.success === true) {
                    const rawranking = response.result.ranking;
                    const listing = {};

                    // for the table view, first collect all tournaments
                    const table = {};
                    this.allTournaments[idx] = [];
                    for (let r = 0; r < rawranking.length; r++) {
                        if (this.allTournaments[idx].indexOf(rawranking[r].tournament) < 0) {
                            this.allTournaments[idx].push(rawranking[r].tournament);
                        }
                    }

                    for (let r = 0; r < rawranking.length; r++) {
                        let detail = rawranking[r].points + ' Pkt. für Platz ' + rawranking[r].rank + ' (' + (rawranking[r].bracket === 'B' ? 'B-Runde ' : '') + rawranking[r].tournament + ')';
                        const name = rawranking[r].player[0].first_name + ' ' + rawranking[r].player[0].last_name;
                        if (typeof listing[name] === 'undefined') {
                            listing[name] = {player: rawranking[r].player[0], points: rawranking[r].points, details: [detail]};
                        } else {
                            listing[name].points += rawranking[r].points;
                            listing[name].details.push(detail);
                        }

                        const tournament = rawranking[r].tournament;
                        const tIdx = this.allTournaments[idx].indexOf(tournament);
                        if (typeof table[name] === 'undefined') {
                            const details = [];
                            for (let t = 0; t < this.allTournaments[idx].length; t++) {
                                details[t] = {points: -1, rank: -1, bround: false};
                            }
                            details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B'};
                            table[name] = {player: rawranking[r].player[0], points: rawranking[r].points, details: details};
                        } else {
                            table[name].details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B'};
                            table[name].points += rawranking[r].points;
                        }
                    }
                    this.rankingTable[idx] = Object.values(table);
                    this.rankingTable[idx].sort((r1, r2) => r2.points - r1.points);

                    this.rankings[idx] = Object.values(listing);
                    this.rankings[idx].sort((r1, r2) => r2.points - r1.points);
                    if (this.rankings[idx].length > this.maxEntries) {
                        this.maxEntries = this.rankings[idx].length;
                    }

                    let rank = 1;
                    let jumped = 1;
                    let lastpoints = -1;
                    for (let p = 0; p < this.rankings[idx].length; p++) {
                        if (p > 0) {
                            if (this.rankings[idx][p].points != lastpoints) {
                                rank += jumped;
                                jumped = 1;
                            } else {
                                jumped++;
                            }
                        }
                        this.rankings[idx][p].totalrank = rank;
                        this.rankingTable[idx][p].totalrank = rank;
                        lastpoints = this.rankings[idx][p].points;
                    }

                    //
                    // for (let e = 0; e < this.rankings[idx].length; e++) {
                    //     const entry = this.rankings[idx][e];
                    //     for (let t = 0; t < this.allTournaments[idx].length; t++) {
                    //         this.logger.debug(entry.details[t].points);
                    //     }
                    // }

                    this.waiting = false;
                    if (expandIdx >= 0) {
                        this.expanded = expandIdx;
                    }

                } else {
                    this.logger.error('Could not retrieve rankings for', catName, response.error);
                    this.waiting = false;
                }
            },
            error => {
                this.logger.error('Could not retrieve categories for ', catName, error);
                this.waiting = false;
            }
        );
    }

    getLargestRange() {
        return Array(this.maxEntries);
    }

    // getDCVPlayers() {
    //     this.playerService.getPlayersPerNation('GER').subscribe(
    //         response => {
    //             if (response.success === true) {
    //                 this.players = response.result;
    //             } else {
    //                 this.logger.error('Could not retrieve players', response.error);
    //             }
    //         },
    //         error => {
    //             this.logger.error('Could not retrieve players ', error);
    //         }
    //     );
    // }

    canAdd(): boolean {
        if ((this.myRoles & Role.ROLE_DCV) > 0 || (this.myRoles & Role.ROLE_PAUL) > 0) {
            // DCV is allowed
            return true;
        }
        return false;
    }

    addPoints(rIdx: number, playerName: string = undefined, reload: boolean = true) {
        this.waiting = true;
        let searchName = playerName;
        if (!searchName) {
            searchName = this.playerCtrl.value;
        }
        let playerId;
        for (let p = 0; p < this.players.length; p++) {
            if (this.players[p].first_name + ' ' + this.players[p].last_name === searchName) {
                playerId = this.players[p]._id;
                break;
            }
        }
        if (!playerId) {
            this.logger.debug('error: player not found');
            this.alertService.error($localize`Player not found!`);
            return;
        }
        const catName = this.catNames[rIdx];
        this.logger.debug('adding new ranking:', this.newRanking, 'for', playerId, 'in', catName);

        if (this.newRanking) {
            this.newRanking.federation = 'DCV';
            this.newRanking.category = catName as any;
            this.newRanking.player = playerId;
            this.rankingService.addRanking(this.newRanking).subscribe(
                response => {
                    this.logger.debug('update qualification ranking:', response);
                    if (response.success === true) {
                        this.logger.debug('successfully updated qualification ranking');
                        this.alertService.success($localize`Added entry`);
                        // clear player field
                        this.newRanking.player = undefined;
                        this.playerCtrl.reset();
                        if (reload) {
                            this.getQualiRanking(catName, rIdx, rIdx);
                        } else {
                            this.waiting = false;
                        }
                        this.rankingService.updateLeaderboard().subscribe(
                            _ => {
                                this.logger.debug('successfully called updateLeaderboard');
                            },
                            error2 => {
                                this.logger.error('Could not update leaderboard:', error2);
                                this.alertService.error($localize`Error adding`, error2);
                            }
                        );
                    } else {
                        this.logger.error('Could not update qualification ranking:', response.error);
                        this.alertService.error($localize`Error adding`, response.error);
                        this.waiting = false;
                    }
                },
                error => {
                    this.logger.error('Could not update qualification ranking:', error);
                    this.alertService.error($localize`Error adding`, error);
                    this.waiting = false;
                }
            );
        }
    }

    add(rIdx: number) {
        this.addMode = rIdx;
    }

    cancel() {
        this.addMode = -1;
    }

    filterPlayers(name: string) {
        return this.players.filter(player =>
            (player.first_name + ' ' + player.last_name).toLowerCase().indexOf(name.toLowerCase()) >= 0);
    }

    unfold(eIdx: number) {
        this.unfolded[eIdx] = !this.isunfolded(eIdx);
    }

    isunfolded(eIdx: number): boolean {
        return this.unfolded[eIdx];
    }

    rankchanged() {
        if (this.newRanking.bracket === 'A') {
            switch (this.newRanking.rank) {
                case 1: this.newRanking.points = 300; break;
                case 2: this.newRanking.points = 251; break;
                case 3: case 4: this.newRanking.points = 214; break;
                case 5: case 6: case 7: case 8: this.newRanking.points = 182; break;
                default:
                    if (this.newRanking.rank <= 12) this.newRanking.points = 150;
                    else if (this.newRanking.rank <= 16) this.newRanking.points = 126;
                    else this.newRanking.points = 0;
            }
        } else {
            switch (this.newRanking.rank) {
                case 1: this.newRanking.points = 100; break;
                case 2: this.newRanking.points = 90; break;
                case 3: case 4: this.newRanking.points = 85; break;
                case 5: case 6: case 7: case 8: this.newRanking.points = 76; break;
                default:
                    if (this.newRanking.rank <= 12) this.newRanking.points = 63;
                    else if (this.newRanking.rank <= 16) this.newRanking.points = 53;
                    else this.newRanking.points = 26;
            }
        }
    }

    uploadCSV(fevent, rIdx) {
        this.logger.debug('importing for', this.uploadTournamentName);
        const fileList: FileList = fevent.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const fr = new FileReader();
            const listcomp = this;
            fr.readAsText(file);
            fr.onloadend = () => {
                let contents = fr.result.toString();
                let error  = fr.error;
                if (error != null) {
                    this.logger.error("File could not be read! Code " + error.code);
                } else {
                    this.logger.debug('importing', contents, 'into', listcomp.catNames[rIdx].substring(14));

                    const strsplit = contents.split('\n');
                    for (let i = 0; i < strsplit.length; i++) {
                        const line = strsplit[i];
                        if (line.trim() === '') {
                            continue;
                        }
                        let rank, last_name, first_name, points;
                        let bround = false;
                        // this.logger.debug('working on line', line);
                        const linesplit = line.split(';');
                        if (linesplit.length > 0) {
                            rank = parseInt(linesplit[0].trim());
                        }
                        // 2nd item is empty (Ophardt ID)
                        if (linesplit.length > 2) {
                            last_name = linesplit[2].trim();
                            last_name = last_name.substring(1, last_name.length -1);
                        }
                        if (linesplit.length > 3) {
                            first_name = linesplit[3].trim();
                            first_name = first_name.substring(1, first_name.length -1);
                        }
                        if (linesplit.length > 4) {
                            const points_txt = linesplit[4].trim();
                            points = points_txt.split(', ')[0];
                            if (points[0] === '"') {
                                bround = true;
                                points = parseInt(points.substring(1));
                            }
                        }
                        this.logger.debug('working with', rank, '/', last_name, '/', first_name, '/', points);

                        listcomp.newRanking = new Ranking();
                        listcomp.newRanking.bracket = bround ? 'B' : 'A';
                        listcomp.newRanking.points = points;
                        listcomp.newRanking.rank = rank;
                        listcomp.newRanking.tournament = listcomp.uploadTournamentName as any;
                        listcomp.newRanking.updated = DateTime.now().toISODate();
                        listcomp.addPoints(rIdx, first_name + ' ' + last_name, false);
                    }
                    listcomp.getQualiRanking(listcomp.catNames[rIdx], rIdx, -1);
                }
            };
        }
    }
}
