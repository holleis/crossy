import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { Player } from '../player/player';
import { Category } from '../tournament/category';
import { Observable } from 'rxjs';
import { Leaderboard } from './leaderboard';
import { Tournament } from '../tournament/tournament';
import { Ranking } from './ranking';

@Injectable()
export class RankingService {

    constructor(
        private http: HttpClient
    ) {}

    getPlayerRanking(playerId: string, categoryId: string) {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/playerranking/'
            + encodeURI(playerId) + '/' + encodeURI(categoryId);
        return this.http.get<{
            success: boolean,
            result: number,
            error: string}>
            (rankingUrl);
    }

    /**
     * Returns all ranking entries of the past 12 months.
     * @param categoryName 
     * @param showall ATTENTION: does not have any effect currently! Always returnes everything
     * @param tournamentName 
     * @returns 
     */
    getRanking(categoryName: string, showall: boolean, tournamentName?: string): Observable<{ success: boolean, result: { 'ranking': { updated?: string, rank: number, points: number, player: Player, tournament?: string, tournamentId?: Tournament, bracket?: string, nrPlayers?: number, abPlayed?: boolean, everyRank?: boolean, betweenRanks?: boolean }[] }, error: string }> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/ranking/'
            + encodeURI(categoryName) + '/' + (showall ? '1' : '0') + '/' + (tournamentName ? encodeURI(tournamentName) : '_all_');
        return this.http.get<{
            success: boolean,
            result: {
                'category': Category,
                'lastupdate': string,
                'ranking': {
                    updated: string,
                    rank: number,
                    points: number,
                    tournament?: string,
                    tournamentId?: Tournament,
                    bracket?: string,
                    player: Player,
                    nrPlayers?: number, abPlayed?: boolean, everyRank?: boolean, betweenRanks?: boolean
                }[],
            },
            error: string}>
            (rankingUrl);
    }

    getRankingsXls(): Observable<Blob> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/rankingsxls/';
        return this.http.get(rankingUrl, { responseType: 'blob' as const });
    }

    getLeaderboard(): Observable<{ success: boolean, result: Leaderboard[], error: string }> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/leaderboard/';
        return this.http.get<{
            success: boolean,
            result: Leaderboard[],
            error: string}>
            (rankingUrl);
    }

    updateLeaderboard(): Observable<{ success: boolean, result: string, error: string }> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/leaderboard/update';
        return this.http.post<{
            success: boolean,
            result: string,
            error: string}>
            (rankingUrl, undefined);
    }

    /**
     * Decides whether at least one category is Adults Singles.
     * @param cats list of categories; must each have .cat_type and .maximum_age
     * @returns true if at least one category is Adults Singles
     */
    hasDMQualiCat(cats: Category[]): boolean {
        if (cats && cats.length > 0) {
            for (let c = 0; c < cats.length; c++) {
                const cat = cats[c];
                if (cat.cat_type === 'singles' && cat.maximum_age > 18) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Decides whether we have a Qualifikationsturnier (DCV Spielordnung, März 2018 / Dez. 2019, / Juni 2020 §11(3))
     * 2024: added German 50 points tournaments
     * needs to be played in Germany; no double and no junior categories
     * @param tournament 
     * @param cats if given, at least one of them must qualify hasDMQualiCat
     * @returns true if should count for rankings
     */
    isRankingTournament(tournament: Tournament, cats?: Category[]) {
        // return tournament && tournament.nation === 'GER' && (!cats || this.hasDMQualiCat(cats)) &&
        return tournament && tournament.nation === 'GER' &&
            (tournament.points === 300 || // (a) alle nationalen Ranglistenturniere (300er Turniere)
                tournament.title.indexOf('German Open') >= 0 || // (b) German Open mit den 300er Punkteschlüssel
                (tournament.federation === 'ICO' && tournament.points === 250) || // (d) die vier 250er ICO-Turniere
                (tournament.federation === 'ICO' && tournament.points === 500) || // (c) das 500er ICO-Turnier
                (tournament.federation === 'ICO' && tournament.points === 100) || // (e) die 100er ICO-Turniere
                (tournament.federation === 'ICO' && tournament.points === 50)); // (e) die dt. 50er ICO-Turniere
    }

    hasNoDoubles(tournament: Tournament) {
        const cats = tournament?.categories;
        for (let c = 0; c < cats?.length; c++) {
            const cat = cats[c];
            if (cat.cat_type === 'doubles') {
                return false;
            }
        }
        return true;
    }
    hasNoSingles(tournament: Tournament) {
        const cats = tournament?.categories;
        for (let c = 0; c < cats?.length; c++) {
            const cat = cats[c];
            if (cat.cat_type === 'singles') {
                return false;
            }
        }
        return true;
    }

    getQualiRanking(categoryName: string, showall: boolean): Observable<{success: boolean, result:
        { 'ranking': { updated: string, rank: number, points: number, player: Player, tournament: string, tournamentId: Tournament, bracket: string, nrPlayers: number, abPlayed: boolean, everyRank: boolean, betweenRanks: boolean}[]}, error: string}> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/qualiranking/'
            + encodeURI(categoryName) + '/' + (showall ? '1' : '0');
        return this.http.get<{
            success: boolean,
            result: {
                'ranking': {
                    updated: string,
                    rank: number,
                    points: number,
                    tournament: string,
                    tournamentId: Tournament,
                    bracket: string,
                    player: Player,
                    nrPlayers: number, abPlayed: boolean, everyRank: boolean, betweenRanks: boolean
                }[],
            },
            error: string}>
            (rankingUrl);
    }

    getRankings(categoryIds: string[], showall: boolean): Observable<{success: boolean, result: {'category': Category, 'lastupdate': string, 'ranking': {rank: number, points: number, player: Player, tournament: string}[]}[], error: string}> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/rankings/'
            + encodeURI(categoryIds.join('&')) + '/' + (showall ? '1' : '0');
        return this.http.get<{
            success: boolean,
            result: {
                'category': Category,
                'lastupdate': string,
                'ranking': {
                    rank: number,
                    points: number,
                    player: Player,
                    tournament: string
                }[]
            }[],
            error: string}>
            (rankingUrl);
    }

    addRanking(ranking: Ranking): Observable<{success: boolean, result: boolean, error: string}> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/ranking/';
        return this.http.post<{
            success: boolean,
            result: boolean,
            error: string}>
            (rankingUrl, ranking);
    }

    addRankings(rankings: Ranking[]): Observable<{success: boolean, result: boolean, error: string}> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/rankings/';
        return this.http.post<{
            success: boolean,
            result: boolean,
            error: string}>
            (rankingUrl, rankings);
    }

    changeRanking(ranking: Ranking): Observable<{success: boolean, result: boolean, error: string}> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/ranking/';
        return this.http.put<{
            success: boolean,
            result: boolean,
            error: string}>
            (rankingUrl, ranking);
    }

    clearRanking(catName: string): Observable<{success: boolean, result: boolean, error: string}> {
        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/ranking/' + catName;
        return this.http.delete<{
            success: boolean,
            result: boolean,
            error: string}>
            (rankingUrl);
    }

    setDMParticipation(year: string, catName: string, playerId: string, dmpart: string): Observable<{success: boolean, result: boolean, error: string}> {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/dmpart/' + year + '/' + catName;
        return this.http.post<{
            success: boolean,
            result: boolean,
            error: string}>
            (url, { playerId, dmpart });
    }

    getDMParticipations(year: string): Observable<{success: boolean, result: { catName: string, player: string, dmpart: string }[], error: string}> {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/dmpart/' + year;
        return this.http.get<{
            success: boolean,
            result: { catName: string, player: string, dmpart: string }[],
            error: string}>
            (url);
    }

    cfm(pts: number, maxpoints: number): number {
        return Math.round(pts * maxpoints / 300);
    }

    /**
     * @param rank rank of player for whom the points should be calculated
     * @param players number of players in that category
     * @param every true if every place has been played
     * @param between ???
     * @param ab true if A/B mode has been played
     */
    calcPointsA(rank: number, players: number, every: boolean, between: boolean, ab: boolean, maxpoints = 300): number {
        const mp = maxpoints;

        if (rank === 1) {
            return this.cfm(300, mp);
        }

        if (players <= 3) {
            return 0;

        } else if (players <= 5) {
            if (rank === 5) {
                return this.cfm(20, mp);
            }
            if (rank < 5) {
                return this.cfm(Math.round(300 / Math.pow(2, rank - 1) + 0.1), mp);
            }
        } else if (players <= 10) {
            if (rank === 2) { return this.cfm(210, mp); }
            if (rank === 3) { return every ? this.cfm(165, mp) : this.cfm(120, mp); }
            if (rank === 4) { return this.cfm(120, mp); }
            const plus = every ? this.cfm(5, mp) : 0;
            if (rank === 5) { return this.cfm(40, mp) + plus; }
            if (rank === 6) { return this.cfm(40, mp); }
            if (rank === 7) { return this.cfm(30, mp) + plus; }
            if (rank === 8) { return this.cfm(30, mp); }
            if (rank === 9) { return this.cfm(20, mp) + plus; }
            if (rank === 10) { return this.cfm(20, mp); }

        } else if (players <= 15) {
            if (rank === 2) { return this.cfm(240, mp); }
            let plus = every ? this.cfm(30, mp) : 0;
            if (rank === 3) { return this.cfm(180, mp) + plus; }
            if (rank === 4) { return this.cfm(180, mp); }
            if (rank === 5) { return this.cfm(120, mp) + plus; }
            if (rank === 6) { return this.cfm(120, mp); }

            plus = every ? this.cfm(10.0 / 3.0, mp) : 0;
            if (rank === 7) { return Math.round(this.cfm(40, mp) + 2 * plus); }
            if (rank === 8) { return Math.round(this.cfm(40, mp) + plus); }
            if (rank === 9) { return this.cfm(40, mp); }
            if (rank === 10) { return Math.round(this.cfm(30, mp) + 2 * plus); }
            if (rank === 11) { return Math.round(this.cfm(30, mp) + plus); }
            if (rank === 12) { return this.cfm(30, mp); }
            if (rank === 13) { return Math.round(this.cfm(20, mp) + 2 * plus); }
            if (rank === 14) { return Math.round(this.cfm(20, mp) + plus); }
            if (rank === 15) { return this.cfm(20, mp); }

        } else { // now >= 16 TN
            if (!ab) {
                // if no A/B was played (ICO), need group points
                // we ignore case that without A/B, >16 TN, every place could be played
                const nrGroups = Math.floor(players / 4.0);

                if (players <= 19) {
                    if (rank === 2) {
                        return this.cfm(240, mp);
                    } else if (rank <= 4) {
                        return this.cfm(180, mp);
                    } else if (rank <= 8) {
                        return this.cfm(120, mp);
                    } else {
                        if (rank <= 12) { return this.cfm(40, mp); }
                        if (rank <= 16) { return this.cfm(30, mp); }
                        if (rank <= players) { return this.cfm(20, mp); }
                    }
                } else if (players <= 35) {
                    if (rank === 2) {
                        return this.cfm(255, mp);
                    } else if (rank <= 4) {
                        return this.cfm(210, mp);
                    } else if (rank <= 8) {
                        return this.cfm(165, mp);
                    } else if (rank <= 2 * nrGroups) { // 9 + 2 * nrGroups - 8 - 1
                        return this.cfm(120, mp);
                    } else {
                        if (rank <= 3 * nrGroups) { return this.cfm(40, mp); } // 9 + 3 * nrGroups - 8 + nrGroups - 1
                        if (rank <= 4 * nrGroups) { return this.cfm(30, mp); }
                        if (rank <= players) { return this.cfm(20, mp); }
                    }
                } else if (players <= 67) {
                    if (rank === 2) {
                        return this.cfm(264, mp);
                    } else if (rank <= 4) {
                        return this.cfm(228, mp);
                    } else if (rank <= 8) {
                        return this.cfm(192, mp);
                    } else if (rank <= 16) {
                        return this.cfm(156, mp);
                    } else if (rank <= 2 * nrGroups) {
                        return this.cfm(120, mp);
                    } else {
                        if (rank <= 3 * nrGroups) { return this.cfm(40, mp); }
                        if (rank <= 4 * nrGroups) { return this.cfm(30, mp); }
                        if (rank <= players) { return this.cfm(20, mp); }
                    }
                }

            } else { // now A/B played and >= 16 TN

                if (players <= 23) {
                    // quarterfinal, last 8 in A
                    if (rank === 2) { return this.cfm(240, mp); }
                    let plus = every ? this.cfm(30, mp) : 0;
                    if (rank === 3) { return this.cfm(180, mp) + plus; }
                    if (rank === 4) { return this.cfm(180, mp); }
                    plus = every ? this.cfm(30.0 / 2.0, mp) : 0;
                    const plusBetween = (between && !every) ? this.cfm(30, mp) : 0;
                    if (rank === 5) { return this.cfm(120, mp) + plusBetween + 3 * plus; }
                    if (rank === 6) { return this.cfm(120, mp) + plusBetween + 2 * plus; }
                    if (rank === 7) { return this.cfm(120, mp) + plus; }
                    if (rank === 8) { return this.cfm(120, mp); }

                } else if (players <= 48) {
                    if (rank === 2) { return this.cfm(255, mp); }

                    if (rank <= 4) {
                        const plus = every ? this.cfm(45.0 / 2.0, mp) : 0;
                        // if (rank === 3) { return this.cfm(210, mp) + plus; }
                        // if (rank === 4) { return this.cfm(210, mp); }
                        return Math.round(this.cfm(210, mp) + (4 - rank) * plus);

                    } else if (rank <= 8) {
                        const plus = every ? this.cfm(45.0 / 4.0, mp) : 0;
                        return Math.round(this.cfm(165, mp) + (8 - rank) * plus);

                    } else if (rank <= 16) {
                        const plus = every ? this.cfm(45.0 / 8.0, mp) : 0;
                        const plusBetween = (between && !every && rank >= 9 && rank <= 12) ? Math.round(this.cfm(45 / 2.0, mp)) : 0;
                        return Math.round(this.cfm(120, mp) + plusBetween + (16 - rank) * plus);
                    }
                }
            }
        }

        return 0;
    }

    calcPointsB(rank: number, players: number, every: boolean): number {
        if (players < 16) {
            return 0;
        }

        if (rank === 1) {
            return 100;
        }

        const koBPlayers = players >= 24 ? players - 16 : players - 8;
        const nrSteps = Math.floor(Math.log2(koBPlayers) - 0.001) + 1;
        const step = (100 - 20) / nrSteps;
        const level = Math.floor(Math.log2(rank) - 0.001) + 1;

        if (!every) {
            return Math.round(100 - level * step);

        } else {
            const betweenSteps = Math.pow(2, level - 1);
            const betweenStep = step / betweenSteps;
            const betweenLevel = 2 * betweenSteps - rank;

            return Math.round(100 - level * step + betweenLevel * betweenStep);

            // e.g. players = 17, rank = 3
            // koBPlayers = 9
            // nrSteps = 4
            // step = 80 / 4 = 20
            // level = 2
            // betweenSteps = 2^1 = 2
            // betweenStep = 20 / 2 = 10
            // betweenLevel = 2*2 - 3 = 1
            // points = 100 - 2*20 + 1*10 = 70
        }
    }


    // /**
    //  * @param rank rank of player for whom the points should be calculated
    //  * @param players number of players in that category
    //  * @param every true if every place has been played
    //  * @param between ???
    //  * @param ab true if A/B mode has been played
    //  */
    // calcPointsA(rank: number, players: number, every: boolean, between: boolean, ab: boolean): number {
    //     if (rank === 1) {
    //         return 300;
    //     }

    //     if (players <= 3) {
    //         return 0;

    //     } else if (players <= 5) {
    //         if (rank === 5) {
    //             return 20;
    //         } else if (rank < 5) {
    //             return Math.round(300 / Math.pow(2, rank - 1) + 0.1);
    //         }

    //     } else if (players <= 10) {
    //         if (rank === 2) { return 210; }
    //         if (rank === 3) { return every ? 165 : 120; }
    //         if (rank === 4) { return 120; }
    //         const plus = every ? 5 : 0;
    //         if (rank === 5) { return 40 + plus; }
    //         if (rank === 6) { return 40; }
    //         if (rank === 7) { return 30 + plus; }
    //         if (rank === 8) { return 30; }
    //         if (rank === 9) { return 20 + plus; }
    //         if (rank === 10) { return 20; }

    //     } else if (players <= 15) {
    //         if (rank === 2) { return 240; }
    //         let plus = every ? 30 : 0;
    //         if (rank === 3) { return 180 + plus; }
    //         if (rank === 4) { return 180; }
    //         if (rank === 5) { return 120 + plus; }
    //         if (rank === 6) { return 120; }

    //         plus = every ? 10.0 / 3.0 : 0;
    //         if (rank === 7) { return Math.round(40 + 2 * plus); }
    //         if (rank === 8) { return Math.round(40 + plus); }
    //         if (rank === 9) { return 40; }
    //         if (rank === 10) { return Math.round(30 + 2 * plus); }
    //         if (rank === 11) { return Math.round(30 + plus); }
    //         if (rank === 12) { return 30; }
    //         if (rank === 13) { return Math.round(20 + 2 * plus); }
    //         if (rank === 14) { return Math.round(20 + plus); }
    //         if (rank === 15) { return 20; }

    //     } else { // now >= 16 TN
    //         if (!ab) {
    //             // if no A/B was played (ICO), need group points
    //             // we ignore case that without A/B, >16 TN, every place could be played
    //             const nrGroups = Math.floor(players / 4.0);

    //             if (players <= 19) {
    //                 if (rank === 2) {
    //                     return 240;
    //                 } else if (rank <= 4) {
    //                     return 180;
    //                 } else if (rank <= 8) {
    //                     return 120;
    //                 } else {
    //                     if (rank <= 12) { return 40; }
    //                     if (rank <= 16) { return 30; }
    //                     if (rank <= players) { return 20; }
    //                 }
    //             } else if (players <= 35) {
    //                 if (rank === 2) {
    //                     return 255;
    //                 } else if (rank <= 4) {
    //                     return 210;
    //                 } else if (rank <= 8) {
    //                     return 165;
    //                 } else if (rank <= 2 * nrGroups) { // 9 + 2 * nrGroups - 8 - 1
    //                     return 120;
    //                 } else {
    //                     if (rank <= 3 * nrGroups) { return 40; } // 9 + 3 * nrGroups - 8 + nrGroups - 1
    //                     if (rank <= 4 * nrGroups) { return 30; }
    //                     if (rank <= players) { return 20; }
    //                 }
    //             } else if (players <= 67) {
    //                 if (rank === 2) {
    //                     return 264;
    //                 } else if (rank <= 4) {
    //                     return 228;
    //                 } else if (rank <= 8) {
    //                     return 192;
    //                 } else if (rank <= 16) {
    //                     return 156;
    //                 } else if (rank <= 2 * nrGroups) {
    //                     return 120;
    //                 } else {
    //                     if (rank <= 3 * nrGroups) { return 40; }
    //                     if (rank <= 4 * nrGroups) { return 30; }
    //                     if (rank <= players) { return 20; }
    //                 }
    //             }

    //         } else { // now A/B played and >= 16 TN

    //             if (players <= 23) {
    //                 // quarterfinal, last 8 in A
    //                 if (rank === 2) { return 240; }
    //                 let plus = every ? 30 : 0;
    //                 if (rank === 3) { return 180 + plus; }
    //                 if (rank === 4) { return 180; }
    //                 plus = every ? 30.0 / 2.0 : 0;
    //                 const plusBetween = (between && !every) ? 30 : 0;
    //                 if (rank === 5) { return 120 + plusBetween + 3 * plus; }
    //                 if (rank === 6) { return 120 + plusBetween + 2 * plus; }
    //                 if (rank === 7) { return 120 + plus; }
    //                 if (rank === 8) { return 120; }

    //             } else if (players <= 48) {
    //                 if (rank === 2) { return 255; }

    //                 if (rank <= 4) {
    //                     const plus = every ? 45.0 / 2.0 : 0;
    //                     // if (rank === 3) { return 210 + plus; }
    //                     // if (rank === 4) { return 210; }
    //                     return Math.round(210 + (4 - rank) * plus);

    //                 } else if (rank <= 8) {
    //                     const plus = every ? 45.0 / 4.0 : 0;
    //                     return Math.round(165 + (8 - rank) * plus);

    //                 } else if (rank <= 16) {
    //                     const plus = every ? 45.0 / 8.0 : 0;
    //                     const plusBetween = (between && !every && rank >= 9 && rank <= 12) ? Math.round(45 / 2.0) : 0;
    //                     return Math.round(120 + plusBetween + (16 - rank) * plus);
    //                 }
    //             }
    //         }
    //     }

    //     return 0;
    // }

    // calcPointsB(rank: number, players: number, every: boolean): number {
    //     if (players < 16) {
    //         return 0;
    //     }

    //     if (rank === 1) {
    //         return 100;
    //     }

    //     if (!every) {
    //         const koBPlayers = players >= 24 ? players - 16 : players - 8;
    //         const nrSteps = Math.floor(Math.log2(koBPlayers) - 0.001) + 1;
    //         const step = (100 - 20) / nrSteps;
    //         const level = Math.floor(Math.log2(rank) - 0.001);
    //         return 100 - level * step;

    //     } else {
    //         // TODO
    //     }

    //     // if (players <= 3) {
    //     //     return 0;

    //     // } else if (players <= 5) {
    //     //     if (rank === 5) {
    //     //         return 20;
    //     //     }
    //     //     return Math.round(300 / Math.pow(2, rank - 1) + 0.1);

    //     // } else if (players <= 10) {
    //     //     if (rank === 2) { return 210; }
    //     //     if (rank === 3) { return every ? 165 : 120; }
    //     //     if (rank === 4) { return 120; }
    //     //     const plus = every ? 5 : 0;
    //     //     if (rank === 5) { return 40 + plus; }
    //     //     if (rank === 6) { return 40; }
    //     //     if (rank === 7) { return 30 + plus; }
    //     //     if (rank === 8) { return 30; }
    //     //     if (rank === 9) { return 20 + plus; }
    //     //     if (rank === 10) { return 20; }

    //     // } else if (players <= 16) {
    //     //     if (rank === 2) { return 240; }
    //     //     let plus = every ? 30 : 0;
    //     //     if (rank === 3) { return 180 + plus; }
    //     //     if (rank === 4) { return 180; }
    //     //     if (rank === 5) { return 120 + plus; }
    //     //     if (rank === 6) { return 120; }

    //     //     plus = every ? 10.0 / 3.0 : 0;
    //     //     if (rank === 7) { return Math.round(40 + 2 * plus); }
    //     //     if (rank === 8) { return Math.round(40 + plus); }
    //     //     if (rank === 9) { return 40; }
    //     //     if (rank === 10) { return Math.round(30 + 2 * plus); }
    //     //     if (rank === 11) { return Math.round(30 + plus); }
    //     //     if (rank === 12) { return 30; }
    //     //     if (rank === 13) { return Math.round(20 + 2 * plus); }
    //     //     if (rank === 14) { return Math.round(20 + plus); }
    //     //     if (rank === 15) { return 20; }

    //     // } else if (players <= 23) {
    //     //     // quarterfinal, last 8 in A
    //     //     // const val = 300 - 60 * Math.floor(Math.log2(rank) - 0.1);
    //     //     if (rank === 2) { return 240; }
    //     //     let plus = every ? 30 : 0;
    //     //     if (rank === 3) { return 180 + plus; }
    //     //     if (rank === 4) { return 180; }
    //     //     plus = every ? 30.0 / 2.0 : 0;
    //     //     if (rank === 5) { return 150 + plus; }
    //     //     if (rank === 6) { return 150; }
    //     //     if (rank === 7) { return 120 + plus; }
    //     //     if (rank === 8) { return 120; }

    //     // } else {
    //     //     if (rank === 2) { return 255; }
    //     //     let plus = every ? 45.0 / 2.0 : 0;
    //     //     if (rank === 3) { return 210 + plus; }
    //     //     if (rank === 4) { return 210; }
    //     //     plus = every ? 45.0 / 4.0 : 0;
    //     //     if (rank === 5) { return 165 + 3 * plus; }
    //     //     if (rank === 6) { return 165 + 2 * plus; }
    //     //     if (rank === 7) { return 165 + plus; }
    //     //     if (rank === 8) { return 165; }
    //     //     plus = every ? 45.0 / 8.0 : 0;
    //     //     if (rank === 9) { return 120 + 7 * plus; }
    //     //     if (rank === 10) { return 120 + 6 * plus; }
    //     //     if (rank === 11) { return 120 + 5 * plus; }
    //     //     if (rank === 12) { return 120 + 4 * plus; }
    //     //     if (rank === 13) { return 120 + 3 * plus; }
    //     //     if (rank === 14) { return 120 + 2 * plus; }
    //     //     if (rank === 15) { return 120 + plus; }
    //     //     if (rank === 16) { return 120; }
    //     // }
    // }
}
