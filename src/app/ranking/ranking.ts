import { Player } from '../player/player';
import { Category } from '../tournament/category';
import { Tournament } from '../tournament/tournament';

export class Ranking {

    _id: string;
    federation: string;
    category: Category;
    updated: string;
    rank: number;
    bracket = 'A';
    points: number;
    player: Player;
    tournament: string;
    tournamentId: Tournament;
    nrPlayers: number;
    everyRank: boolean;
    betweenRanks: boolean;
    abPlayed: boolean;

}
