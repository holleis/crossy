
import { Subject } from 'rxjs';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Player } from '../player/player';
import { RankingService } from './ranking.service';
// import { CategoriesService } from './categories.service';
import { NGXLogger } from 'ngx-logger';
import { NavigationEnd, Router } from "@angular/router";
import { ProfilelinkDialogComponent } from '../util/profilelink-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AlertService } from '../util/alert/alert.service';
import { Utils } from '../util/utils';
import { TournamentsService } from '../tournament/tournaments.service';
import { DateTime } from 'luxon';
import { Tournament } from '../tournament/tournament';
import * as moment from 'moment';

@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.scss'],
    // providers: [CategoriesService]
})

export class RankingComponent implements OnInit, OnDestroy {

    readonly DEFAULT_CAT = 'DCV Open';
    readonly DOPPELREGEX = /Doppel|Mixed/i;

    // @Input() rankings: {
    //     'category': Category,
    //     'lastupdate': string,
    //     'ranking': {
    //         rank: number,
    //         points: number,
    //         player: Player
    //     }[]
    // }[];
    @Input() showall = true;
    // @Input() categories: { _id: string, name: string }[] = [];
    // ranking: { category: Category, lastupdate: string, ranking: { rank: number, points: number, player: Player }[] };
    ranking: { totalrank: number, details: any, points: number, player: Player }[];
    lastupdate: Date;

    catName: string;
    tourPlus: { t: string, d: Date, id: string, onlyDoubles?: boolean, onlySingles?: boolean, upcoming?: boolean }[] = [];
    allTournaments: string[] = [];
    allTournamentIds: (Tournament | string)[] = [];
    dates: Date[] = [];
    fullRankingRoute = '/ranking';
    loading = false;

    // rankingLabel = [
    //     $localize`female`,
    //     $localize`Open` + ' / ' + $localize`male`,
    //     $localize`Double` + ' / ' + $localize`Mixed`,
    // ];

    private timeout;
    private ngUnsubscribe = new Subject();
    isSmall$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 450px)')
        .pipe(map(result => result.matches));

    constructor(
        private rankingService: RankingService,
        // private categoriesService: CategoriesService,
        private dialog: MatDialog,
        private logger: NGXLogger,
        private router: Router,
        private breakpointObserver: BreakpointObserver,
        private alertService: AlertService,
        private tournamentService: TournamentsService,
        private utils: Utils,
    ) { }

    ngOnInit() {
        this.router.events
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (e: unknown) => {
                    if (e instanceof NavigationEnd) {
                        const path = this.router.url;
                        // this.logger.debug('path ', path);
                        this.catName = this.pathToCat(path, 'DCV');
                        this.getRanking(this.catName);
                    }
                }
            );
        const path = this.router.url;
        // this.logger.debug('path %s', path);
        this.catName = this.pathToCat(path, 'DCV');
        this.getRanking(this.catName);
        // this.getAllRankings('DCV', this.showall);
    }

    ngOnDestroy(): void {
        this.ngUnsubscribe.next('');
        this.ngUnsubscribe.complete();
    }

    // TODO does not work for ranking/doubles/mixed/ü40
    pathToCat(path: string, federation: string): string {
        path = decodeURI(path);
        let cat = federation + ' ';

        const pathSplit = path.split('/');
        if (!(pathSplit?.length >= 3)) {
            return this.DEFAULT_CAT;
        }
        const type = pathSplit[2]; // female, open, doubles
        const age = pathSplit[3];
        this.fullRankingRoute = '/ranking/' + type + '/' + age;
        if (age[0] === 'u') {
            cat += age.toUpperCase();
            if (type === 'female') {
                cat += ' Weiblich';
            } else {
                cat += ' Männlich';
            }
            return cat;
        } else if (age[0] === 'ü') {
            cat += age.toUpperCase();
            if (type === 'female') {
                cat += ' Damen';
            } else {
                cat += ' Herren';
            }
            return cat;

        } else if (age === 'women') {
            cat += 'Damen';
            if (type === 'doubles') {
                cat += ' Doppel';
            }
            return cat;
        } else if (age === 'open') {
            cat += 'Open';
            if (type === 'doubles') {
                cat += ' Doppel';
            }
            return cat;
        }
        if (type === 'doubles' && age === 'mixed') {
            cat += 'Mixed Doppel';
            return cat;
        }
        this.logger.error('unknown path for category in ranking: %s', path);
    }

    getRanking(catName: string): void {
        if (!catName) {
            this.ranking = undefined;
            return;
        }
        this.tourPlus = [];
        this.timeout = setTimeout(() => {
            this.loading = true;
        }, 250);
        this.rankingService.getRanking(catName, this.showall).subscribe(
            response => {
                if (response.success === true) {
                    // this.ranking = response.result;
                    const rawranking = response.result.ranking;

                    // first collect all tournaments
                    this.lastupdate = undefined;
                    for (let r = 0; r < rawranking.length; r++) {
                        if (this.tourPlus.map(tpd => tpd.t).indexOf(rawranking[r].tournament) < 0) {
                            // use ranking date as date of tournament
                            const date = rawranking[r].updated ? DateTime.fromISO(rawranking[r].updated).toJSDate() : undefined;
                            this.tourPlus.push({ t: rawranking[r].tournament, d: date, id: rawranking[r].tournamentId?.toString() });
                            if (!this.lastupdate || date > this.lastupdate) {
                                this.lastupdate = date;
                            }
                        }
                    }
                    
                    this.tourPlus.filter(tournament => !catName.match(this.DOPPELREGEX) && !tournament?.onlyDoubles || catName.match(this.DOPPELREGEX) && !tournament?.onlySingles);
                    
                    this.tourPlus.sort((t1, t2) => t1.d < t2.d ? 1 : (t1.d > t2.d ? -1 : 0));
                    const table = {};
                    this.allTournaments = this.tourPlus.map(tpd => tpd.t);
                    this.allTournamentIds = this.tourPlus.map(tpd => tpd.id?.toString());
                    this.dates = this.tourPlus.map(tpd => tpd.d);

                    const nrPlayersPerTournament = {};
                    let undefCnt = 1;
                    for (let r = 0; r < rawranking.length; r++) {
                        // const detail = rawranking[r].points + ' Pkt. für Platz ' + rawranking[r].rank + ' (' + (rawranking[r].bracket === 'B' ? 'B-Runde ' : '') + rawranking[r].tournament + ')';
                        let name = rawranking[r].player?.first_name + ' ' + rawranking[r].player?.last_name;
                        if (!name) {
                            name = 'err ' + undefCnt;
                            undefCnt += 1;
                        }
                        // if (typeof listing[name] === 'undefined') {
                        //     listing[name] = { player: rawranking[r].player, points: [rawranking[r].points], details: [detail] };
                        // } else {
                        //     // push points in order to get the highest 5 entries later
                        //     listing[name].points.push(rawranking[r].points);
                        //     listing[name].details.push(detail);
                        // }

                        const tournament = rawranking[r].tournament;
                        if (!nrPlayersPerTournament[tournament]) {
                            if (rawranking[r].nrPlayers) {
                                nrPlayersPerTournament[tournament] = rawranking[r].nrPlayers;
                            } else {
                                // calculate
                                let cnt = 0;
                                for (let r2 = 0; r2 < rawranking.length; r2++) {
                                    if (rawranking[r2].tournament === tournament) {
                                        cnt++;
                                    }
                                }
                                nrPlayersPerTournament[tournament] = cnt;
                            }
                        }

                        const tIdx = this.allTournaments.indexOf(tournament);
                        if (typeof table[name] === 'undefined') {
                            const details = [];
                            for (let t = 0; t < this.allTournaments.length; t++) {
                                details[t] = { points: -1, rank: -1, bround: false };
                            }
                            // details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B'};
                            details[tIdx] = {
                                points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B',
                                abPlayed: rawranking[r].abPlayed || false, nrPlayers: rawranking[r].nrPlayers || nrPlayersPerTournament[tournament], everyRank: rawranking[r].everyRank || false, betweenRanks: rawranking[r].betweenRanks || false
                            };
                            table[name] = { player: rawranking[r].player, points: [rawranking[r].points], details };
                        } else {
                            // table[name].details[tIdx] = {points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B'};
                            table[name].details[tIdx] = {
                                points: rawranking[r].points, rank: rawranking[r].rank, bround: rawranking[r].bracket === 'B',
                                abPlayed: rawranking[r].abPlayed, nrPlayers: rawranking[r].nrPlayers, everyRank: rawranking[r].everyRank, betweenRanks: rawranking[r].betweenRanks
                            };
                            table[name].points.push(rawranking[r].points);
                        }
                    }

                    const tempRanking: { points: number[] }[] = Object.values(table);
                    // points is an array, convert to number (sum up))
                    for (let r = 0; r < tempRanking.length; r++) {
                        const ranky = tempRanking[r];
                        ranky.points = (ranky.points || []).reduce((prev, cur) => prev + cur, 0) as unknown as number[];
                    }
                    this.ranking = tempRanking as any[];
                    this.ranking.sort((r1, r2) => r2.points - r1.points);
                    if (!this.showall) {
                        this.ranking.length = Math.min(this.ranking.length, 5);
                    }

                    let rank = 1;
                    let jumped = 1;
                    let lastpoints = -1;
                    for (let p = 0; p < this.ranking.length; p++) {
                        if (p > 0) {
                            if (this.ranking[p].points !== lastpoints) {
                                rank += jumped;
                                jumped = 1;
                            } else {
                                jumped++;
                            }
                        }
                        this.ranking[p].totalrank = rank;
                        lastpoints = this.ranking[p].points;
                    }

                    this.addUpcomingTournaments();
                } else {
                    this.alertService.error($localize`Could not retrieve rankings for category ${catName}:category: ${response.error}:error:`);
                    this.logger.error('Could not retrieve rankings for category', catName, response.error);
                }
                clearTimeout(this.timeout);
                this.loading = false;
            },
            error => {
                this.logger.error('Could not retrieve rankings for category', catName, error);
                clearTimeout(this.timeout);
                this.loading = false;
            }
        );
    }

    // isCatType(cat: Category): number {
    //     // female=1, male=2, double=3 according to definition in category.ts:
    //     //      open=0, female=1, male=2, mixed=3
    //     if (cat.cat_type === 'doubles') {
    //         return 3;
    //     }
    //     return cat.gender === 0 ? 2 : cat.gender;
    // }

    // // getTypedRankings(cat_type: number):  {'category': Category, 'lastupdate': string,
    // //     'ranking': {rank: number, points: number, player: Player}[]}[] {

    // //     return this.rankings.filter(ranking => this.isCatType(ranking.category) === cat_type, this);
    // // }

    // getRankings(categories: { name: string }[]): void {
    //     if (!categories) {
    //         this.rankings = [];
    //         return;
    //     }
    //     // this.rankingService.getRankings(categories.map(c => c._id), this.showall).subscribe(
    //     //     response => {
    //     //         if (response.success === true) {
    //     //             this.rankings = response.result;
    //     //         } else {
    //     //             this.logger.error('Could not retrieve rankings for', categories, response.error);
    //     //         }
    //     //     },
    //     //     error => {
    //     //         this.logger.error('Could not retrieve categories for categories', categories, error);
    //     //     }
    //     // );

    //     const obsvbls = [];
    //     for (let i = 0; i < categories.length; i++) {
    //         obsvbls.push(this.rankingService.getRanking(categories[i].name, this.showall));
    //     }
    //     // need forkJoin here otherwise sorting of categories is lost

    //     forkJoin(obsvbls).subscribe(
    //         values => {
    //             this.rankings = [];
    //             for (let i = 0; i < values.length; i++) {
    //                 this.rankings.push(values[i]['result']);
    //             }
    //             // this.logger.trace('all rankings retrieved', this.rankings);

    //             // for (let i = 1; i < 4; i++) {
    //             //     this.typedRankings[i - 1] = this.rankings.filter(ranking => this.isCatType(ranking.category) === i, this);
    //             // }
    //         },
    //         error => {
    //             this.logger.error('Could not retrieve rankings for categories', categories, error);
    //         }
    //     );
    // }

    // getAllRankings(federation: string, showall: boolean): void {
    //     this.logger.debug('getAllRankings called for', federation, showall);
    //     // TODO optimmize with only one API call
    //     this.categoriesService.getCategoryNames(federation).subscribe(
    //         response => {
    //             if (response.success === true) {
    //                 this.categories = response.result;
    //                 if (this.categories) {
    //                     this.categories.sort(function (c1: { _id, name }, c2: { _id, name }) {
    //                         const str1 = c1.name;
    //                         const str2 = c2.name;
    //                         if (str1 === 'DCV Open' || str1 === 'DCV Damen') {
    //                             return -10;
    //                         }
    //                         if (str2 === 'DCV Open' || str2 === 'DCV Damen') {
    //                             return 5;
    //                         }
    //                         return str1 < str2 ? -1 : (str1 > str2 ? 1 : 0);
    //                     });
    //                 }
    //                 this.getRankings(this.categories);
    //             } else {
    //                 this.logger.error('Could not retrieve rankings for federation', federation, response.error);
    //             }
    //         },
    //         error => {
    //             this.logger.error('Could not retrieve categories for federation', federation, error);
    //         }
    //     );
    // }

    addUpcomingTournaments() {
        this.tournamentService.getFutureTournaments(false).subscribe({
            next: response => {
                if (response.success === true) {
                    response.result.sort((t1, t2) => t1.start_date_epoch - t2.start_date_epoch);
                    for (let t = 0; t < response.result.length; t++) {
                        const tournament = response.result[t];
                        const onlyDoubles = this.rankingService.hasNoSingles(tournament);
                        const onlySingles = this.rankingService.hasNoDoubles(tournament);
                        if (this.rankingService.isRankingTournament(tournament) && (!this.catName.match(this.DOPPELREGEX) && !onlyDoubles || this.catName.match(this.DOPPELREGEX) && !onlySingles)) {
                            this.allTournaments.push(tournament.title);
                            this.allTournamentIds.push(tournament._id);
                            this.tourPlus.push({ t: tournament.title, d: new Date(tournament.start_date_epoch), id: tournament._id, onlyDoubles, onlySingles, upcoming: true });
                        }
                    }
                } else {
                    this.logger.error('Could not get future tournaments', response.error);
                }
            },
            error: error => {
                this.logger.error('Could not get future tournaments ', error);
            }
        });
    }

    onSelect(player: Player) {
        this.dialog.open(ProfilelinkDialogComponent, {
            data: { tsProfileId: player.tsProfileId }
        });
    }

    exportRanking(): void {
        this.rankingService.getRankingsXls()
            .subscribe({
                next: blob => {
                    this.utils.saveBlobToFileSystem(blob, `DCV Ranking ${moment().format('YYYY-MM-DD')}.xls`);
                    this.alertService.success('Successfully added');
                },
                error: error => {
                    this.logger.error('Could not download ranking ', error);
                }
            });
    }
}
