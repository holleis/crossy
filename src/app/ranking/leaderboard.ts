import { Player } from '../player/player';
import { Category } from '../tournament/category';

export class Leaderboard {

    _id: string;
    category: Category;
    updated: Date;
    rank: number;
    points: number;
    player: Player;
}
