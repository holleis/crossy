import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Globals } from '../../globals';
import { NGXLogger } from 'ngx-logger';
import { User, UserWithInfo } from '../user';
import { Subject } from 'rxjs';
import { MediaMatcher } from '@angular/cdk/layout';

@Injectable()
export class UserService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger,
        public mediaMatcher: MediaMatcher,
    ) { }

    readonly darkmodeLocalStorageName = 'theme';
    private darkmodeMode = new Subject<'auto' | 'lightmode' | 'darkmode'>();
    darkmodeMode$ = this.darkmodeMode.asObservable();
    private currentDarkmodeMode: 'auto' | 'lightmode' | 'darkmode';
    matcher: MediaQueryList;

    getAllWithInfos() {
        return this.http.get<{
            success: boolean, result: UserWithInfo[], error: string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/usersWithInfo');
    }

    getAll() {
        return this.http.get<{
            success: boolean, result: User[], error: string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/users');
    }

    getByPlayerId(playerId: string) {
        return this.http.get<{
            success: boolean, result: User, error: string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/users/player/' + playerId);
    }

    findTSPlayers(searchStr: string) {
        let params = new HttpParams();
        params = params.set('searchStr', encodeURI(searchStr));
        return this.http.get<{
            success: boolean, result: { name: string, tsId: string, tsProfileId: string, club: string }[], error: string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/ts/players/find', { params });
    }

    create(user: any) {
        this.logger.debug('sending new user', user);
        return this.http.post<{
            success: boolean,
            result: { user: { user_id: string, token: string } },
            error: string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/users', user);
    }

    update(user: any) {
        this.logger.debug('updating user with', user);
        return this.http.put<{
            success: boolean,
            result: User,
            error: string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/users', user);
    }

    sendVerificationEmail(userInfo: { _id: string, email: string }) {
        this.logger.debug('sendVerificationEmail for', userInfo);
        return this.http.post<{ success: boolean, result: string, error: string }>
            (Globals.BASE_API_URL + Globals.SUB_API_URL + '/users/verifyemail', userInfo);
    }

    sendEmail(content: string, email: string, subject: string) {
        this.logger.debug('sendEmail to', email);
        return this.http.post<{ success: boolean, result: string, error: string }>
            (Globals.BASE_API_URL + Globals.SUB_API_URL + '/users/email', { content: content, email: email, subject: subject });
    }

    sendPasswordResetEmail(email: string) {
        this.logger.debug('sendPasswordResetEmail for', email);
        return this.http.post<{ success: boolean, result: string, error: string }>
            (Globals.BASE_API_URL + Globals.SUB_API_URL + '/users/resetpasswordemail', { email: email });
    }

    updatePassword(userInfo: { _id: string, password: string, token: string }) {
        this.logger.debug('updatePassword for', userInfo._id, 'with token', userInfo.token);
        return this.http.put<{ success: boolean, result: string, error: string }>
            (Globals.BASE_API_URL + Globals.SUB_API_URL + '/users/password', userInfo);
    }

    getCurrentUserRoles(): number {
        const user = this.getCurrentUser();
        if (user) {
            return user.roles;
        }
        return 0;
    }

    getCurrentUser(): {
        user_id: string,
        first_name: string,
        player_id: string,
        roles: number,
        token: string
    } {
        const user = localStorage.getItem('currentUser');
        this.logger.debug('found logged in user', user);
        if (user) {
            return JSON.parse(user);
        }
        return undefined;
    }

    storeDarkmode(dm: 'lightmode' | 'darkmode' | 'auto'): void {
        if (this.currentDarkmodeMode != dm) {
            this.currentDarkmodeMode = dm;
            localStorage.setItem(this.darkmodeLocalStorageName, dm);
        }
        // call independent of whether the mode changed or not: could be that the media query changed
        this.darkmodeMode.next(dm);
    }

    getDarkmode(): 'lightmode' | 'darkmode' | 'auto' {
        if (this.currentDarkmodeMode) {
            return this.currentDarkmodeMode;
        }
        const dm = localStorage.getItem(this.darkmodeLocalStorageName);
        if (dm && (dm === 'lightmode' || dm === 'darkmode')) {
            return dm;
        }
        return 'lightmode';
    }
    isDarkModeEnabled(dm?: 'auto' | 'lightmode' | 'darkmode'): boolean {
        if (!dm) {
            dm = this.getDarkmode();
        }
        if (dm === 'darkmode') {
            return true;
        }
        if (dm === 'lightmode') {
            return false;
        }
        // darkmode mode is 'auto'; whether darkmode or not depends on @media
        return this.mediaMatcher.matchMedia('(prefers-color-scheme: dark)').matches;
    }
}
