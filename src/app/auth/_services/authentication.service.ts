import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../globals';
import { LoginChangedService } from './loginchanged.service';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class AuthenticationService {

    constructor(
        private http: HttpClient,
        private loginChangedService: LoginChangedService,
        private logger: NGXLogger
    ) { }

    login(username: string, password: string) {
        const params = {username: username, password: password};

        this.logger.debug('calling api/authenticate');
        return this.http.post<any>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/users/authenticate', params);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.loginChangedService.changeLoginStatus(false);
    }
}
