import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../../util/alert/alert.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-resetpassword',
    templateUrl: './resetpassword.component.html',
    styleUrls: ['./resetpassword.component.css'],
    providers: []
})
export class ResetpasswordComponent implements OnInit {

    loading = false;
    myemail: string;
    setPassword = false;
    userId: string;
    token: string;
    mypassword: string;
    repeat_password: string;

    constructor(
        private userService: UserService,
        private logger: NGXLogger,
        private alertService: AlertService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.userId = this.route.snapshot.params.id;
        this.token = this.route.snapshot.params.token;
        if (this.userId && this.token) {
            // allow setting new password
            this.setPassword = true;
        }
    }

    resetPassword() {
        this.loading = true;
        if (this.userId && this.token) {
            // allow setting new password
            this.setPassword = true;
            this.loading = false;

        } else if (this.myemail && this.myemail !== '') {
            this.myemail = this.myemail.toLowerCase();
            this.userService.sendPasswordResetEmail(this.myemail).subscribe(
                response => {
                    if (response.success === true) {
                        this.logger.debug('sendPasswordResetEmail:', response.result);
                        this.alertService.success($localize`Successfully sent email to ${response.result}:email:`);
                    } else {
                        this.logger.error('error in sendPasswordResetEmail', response.error);
                        this.alertService.error($localize`Error sending email`, response.error);
                    }
                    this.loading = false;
                },
                error => {
                    this.logger.error('error in sendPasswordResetEmail', error);
                    this.alertService.error($localize`Error sending email`, error);
                    this.loading = false;
                }
            );
        }
    }

    updatePassword() {
        if (!this.userId || !this.token) {
            this.setPassword = false;
            this.logger.error('cannot update password, no userId or token', this.userId, this.token);
            this.alertService.error($localize`Error: please repeat the password reset process!`);

        } else {
            if (!this.mypassword || this.mypassword === '') {
                this.logger.debug('password is empty', this.userId, this.mypassword);
                this.alertService.error($localize`Error: password cannot be empty!`);
                return;
            }
            if (this.mypassword !== this.repeat_password) {
                this.logger.debug('passwords don\'t match');
                this.alertService.error($localize`Error: repeated password does not match!`);
                return;
            }

            this.loading = true;
            this.userService.updatePassword({_id: this.userId, password: this.mypassword, token: this.token}).subscribe(
                response => {
                    if (response.success === true) {
                        this.logger.debug('successfully updated password:', response.result);
                        this.alertService.success($localize`Success! You can now log in with your new password.`);
                        this.router.navigate(['/login']);

                    } else {
                        this.logger.error('error in updatePassword', response.error);
                        this.alertService.error($localize`Error updating password`, response.error);
                    }
                    this.loading = false;
                },
                error => {
                    this.logger.error('error in updatePassword', error);
                    this.alertService.error($localize`Error updating password`, error);
                    this.loading = false;
                }
            );
        }
    }
}
