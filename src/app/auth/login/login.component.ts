﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../../util/alert/alert.service';
import { AuthenticationService } from '../_services/authentication.service';
import { NGXLogger } from 'ngx-logger';
import { LoginChangedService } from '../_services/loginchanged.service';
import { UserService } from '../_services/user.service';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [AuthenticationService]
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    returnParam: {};
    returnQuery: {};

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private loginChangedService: LoginChangedService,
        private alertService: AlertService,
        private logger: NGXLogger,
        private userService: UserService
    ) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/home'
        this.returnUrl = '/home';
        if (this.route.snapshot.queryParams['returnUrl']) {
            this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
            if (this.returnUrl.indexOf('?') !== -1) {
                // e.g. "/add;d=5?data=ChBF"
                const spl = this.returnUrl.split('?');
                this.logger.debug('1st split', spl);
                const spl2 = spl[1].split('=');
                this.logger.debug('2st split', spl2);
                this.returnUrl = spl[0];
                this.returnQuery = {};
                this.returnQuery[spl2[0]] = spl2[1];
            }
            if (this.returnUrl.indexOf(';') !== -1) {
                // e.g. "/enroll;id=5acc9b9421a1231adc9ac349"
                const spl = this.returnUrl.split(';');
                this.logger.debug('1st split', spl);
                const spl2 = spl[1].split('=');
                this.logger.debug('2st split', spl2);
                this.returnUrl = spl[0];
                this.returnParam = {};
                this.returnParam[spl2[0]] = spl2[1];
            }
        }
        this.logger.debug('init in LoginComponent', this.route.snapshot);

        const userId = this.route.snapshot.params.id;
        if (userId) {
            // this is called from the link in the verification email for the given user
            const userInfo = {_id: userId, email_verified: true};
            this.userService.update(userInfo).subscribe({
                next: response => {
                    if (response.success === true) {
                        this.logger.debug('verified email');
                        this.alertService.success($localize`Email verified! You can now login.`);
                        this.router.navigate(['/login']);

                    } else {
                        this.logger.error('Email verify failed:', response.error);
                        this.alertService.error($localize`Email verify failed`, response.error);
                    }
                },
                error: error => {
                    this.logger.error('Email verify failed:', error);
                    this.alertService.error($localize`Email verify failed`, error);
                }
            });

            return;
        }
    }

    login() {
        this.logger.debug('login() called in login.component');

        this.loading = true;
        this.model.email = this.model.email.toLowerCase();
        this.authenticationService.login(this.model.email, this.model.password).subscribe({
            next: response => {
                this.logger.trace('received login', response);
                // login successful if there's a jwt token in the response
                // {"success": true, "result": {user: {user_id: string,first_name: string,
                //      player_id: string,roles: number.roles,token: string}}, "error": ""}
                if (response?.success === true && response.result?.user?.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(response.result.user));
                    if (!response.result.user.first_name) {
                        response.result.user.first_name = '';
                    }
                    this.loginChangedService.changeLoginStatus(true);
                    this.logger.debug('login successful for', this.model.email, 'forwarding to', this.returnUrl, 'with query', this.returnQuery, 'and param', this.returnParam);
                    this.router.navigate([this.returnUrl, this.returnParam || {}], { queryParams: this.returnQuery });
                }
            },
            error: error => {
                if (!error.error && error.status === 404) {
                    this.logger.error('user not found:', error);
                    this.alertService.error('user not found, try using your email or contact Paul');
                    this.loading = false;
                } else if (!error.error && error.status === 401) {
                    this.logger.error('wrong password:', error);
                    this.alertService.error('wrong password');
                    this.loading = false;
                } else {
                    this.logger.error('error during login:', error);
                    const myerror = error.error.error || error.error;
                    this.alertService.error(myerror);
                    this.loading = false;
                }
            }
        });
    }
}
