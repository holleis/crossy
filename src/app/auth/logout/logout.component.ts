﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService } from '../../util/alert/alert.service';
import { AuthenticationService } from '../_services/authentication.service';
import { Title } from '@angular/platform-browser';

@Component({
    template: '',
    providers: [AuthenticationService]
})

export class LogoutComponent implements OnInit {

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private titleService: Title
    ) { }

    ngOnInit() {
        this.titleService.setTitle('Crossy');
        // reset login status
        this.authenticationService.logout();
        this.alertService.success($localize`You successfully logged out!`);
        this.router.navigate(['/overview']);
    }
}
