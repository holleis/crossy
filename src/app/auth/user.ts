import { Player } from '../player/player';

export class User {
    _id: string;
    last_name: string;
    first_name: string;
    username: string;
    email: string;
    hash: string;
    roles = 0;
    player: Player;
    email_verified: boolean;
    passwordResetToken: String;
}

export class UserWithInfo extends User {
    dcvBeitrag: string[];
    clubs: string;
}
