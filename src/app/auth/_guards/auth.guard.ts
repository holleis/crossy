﻿import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../../util/alert/alert.service';
import { UserService } from '../_services/user.service';
import { Role } from '../../user/role';

@Injectable()
export class AuthGuard  {

    constructor(
        private router: Router,
        private logger: NGXLogger,
        private alertService: AlertService,
        private userService: UserService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.userService.getCurrentUser();
        if (currentUser) {
            // check if special roles are needed
            if (route.url[0].path === 'users') {
                const roles = currentUser.roles;
                this.logger.debug('need role', Role.ROLE_DCV | Role.ROLE_PAUL, 'for path users, have', roles);
                if ((roles & Role.ROLE_DCV) > 0 || (roles & Role.ROLE_PAUL) > 0) {
                    return true;
                }
                this.alertService.error($localize`You do not have access rights for this page. Transferred you to your home page.`);
                this.router.navigate(['/home']);
                return false;
            }

            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.logger.debug('user not logged in, going back to ', state.url);
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}
