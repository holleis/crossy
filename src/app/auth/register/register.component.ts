import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertService } from '../../util/alert/alert.service';
import { UserService } from '../_services/user.service';
import { Player } from '../../player/player';
import { ClubService } from '../../club/club.service';
import { NGXLogger } from 'ngx-logger';
import { Club } from '../../tournament/club';
import { Role } from '../../user/role';
import { throttleTime, takeUntil } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';
import { User } from '../user';

@Component({
    templateUrl: 'register.component.html',
    styleUrls: ['./register.component.css'],
    providers: [ClubService],
})


export class RegisterComponent implements OnInit, OnDestroy {

    model: { first_name?: string, last_name?: string, email?: string, password?: string } = {};
    loading = false;
    searching = false;
    searched = false;
    successfullyRegistered = false;
    proposedPlayers: { name: string, tsId: string, tsProfileId: string, club: string }[] = [];
    selectedPlayer: { name: string, tsId: string, tsProfileId: string, club: string };

    allClubs: { _id: string, name: string }[];
    options: Array<{_id: string, name: string}[]> = [];

    acceptPrivacy = false;

    constructor(
        private userService: UserService,
        private alertService: AlertService,
        private logger: NGXLogger,
        private clubService: ClubService,
    ) { }

    private ngUnsubscribe = new Subject();


    ngOnInit(): void {
        this.getClubNames();
    }

    ngOnDestroy(): void {
        this.ngUnsubscribe.next('');
        this.ngUnsubscribe.complete();
    }

    getClubNames(): void {
        this.clubService.getClubNames()
            .pipe(throttleTime(environment.RELOADTHROTTLE), takeUntil(this.ngUnsubscribe))
            .subscribe(
                response => {
                    if (response.success === true) {
                        this.allClubs = response.result;
                    }
                },
                error => {
                    this.alertService.error($localize`Error getting list of existing clubs`, error);
                }
            );
    }

    searchPlayers(): void {
        this.loading = true;
        this.searching = true;
        this.searched = true;
        const searchStr = (this.model.first_name || '') + (this.model.first_name && this.model.last_name ? ' ' : '') + (this.model.last_name || '');
        this.userService.findTSPlayers(searchStr)
            .pipe(throttleTime(environment.RELOADTHROTTLE), takeUntil(this.ngUnsubscribe))
            .subscribe(
                response => {
                    if (response.success === true) {
                        this.proposedPlayers = response.result;
                    }
                    this.searching = false;
                    this.loading = false;
                },
                error => {
                    this.alertService.error($localize`Error getting list of users from Tournament System`, error);
                    this.searching = false;
                    this.loading = false;
                }
            );
    }

    addRoles(player: any) {
        let role = 0;
        switch (player.first_name + ' ' + player.last_name) {
            case 'Paul HOLLEIS':
                role = Role.ROLE_PAUL;
                break;
            // case 'Friederike POLLMANN':
            case 'Anna HUBERT':
            case 'Nina BACHMANN':
            case 'Katrin HUBER':
            // case 'Nico FRANKE':
                role = Role.ROLE_DCV;
                break;
            case 'Adrian LUTZ':
            case 'Johannes SPÄTH':
            case 'Joschka WUNDERLICH':
                role = Role.ROLE_DCV | Role.ROLE_ORGANISER;
                break;
            // case 'Markus GRAICHEN':
            //     role = Role.ROLE_LICENSE;
            //     break;
            case 'Samuel HOLLEIS':
                role = Role.ROLE_ORGANISER;
                break;
            // case 'Maximilian FRANKE':
            //     role = Role.ROLE_ICO | Role.ROLE_ORGANISER;
            //     break;
            default:
                role = 0;
        }
        player.roles = role;
    }


    // removePlayer(player): void {
    //     this.playerService.deletePlayer(player).subscribe(
    //         response => {
    //             if (response.success === true) {
    //                 this.logger.debug('removed player again');
    //             } else {
    //                 this.logger.debug('error removing player', player, response.error);
    //             }
    //         },
    //         error => {
    //             this.logger.debug('error removing player', player, error);
    //         }
    //     );
    // }

    register() {
        if (!this.selectedPlayer) {
            this.alertService.error($localize`You need be registered at Tournament Software. If you are new, please ask your club admin to make an account at https://ico.tournamentsoftware.com/`);
            return;
        }

        const newPlayer = new Player();
        [newPlayer.first_name, newPlayer.last_name] = this.guessNameParts(this.selectedPlayer.name);
        newPlayer.tsid = this.selectedPlayer.tsId;
        newPlayer.tsProfileId = this.selectedPlayer.tsProfileId;
        newPlayer.data_protection_accepted = true;

        if (this.selectedPlayer.club && this.allClubs?.length) {
            for (let c = 0; c < this.allClubs.length; c++) {
                const club = this.allClubs[c];
                if (club.name.trim().toUpperCase() === this.selectedPlayer.club.trim().toUpperCase()) {
                    newPlayer.clubs = [club._id as unknown as Club];
                    break;
                }
            }
        }

        this.loading = true;
        const newUser: User & { password?: string } = new User();
        newUser.player = newPlayer;
        newUser.first_name = newUser.player.first_name;
        newUser.last_name = newUser.player.last_name;
        newUser.email = this.model.email.toLowerCase();
        newUser.password = this.model.password;
        newUser.email_verified = false;

        // newUser.date_of_birth = this.model.date_of_birth_day + '.' + this.model.date_of_birth_month + '.' + this.model.date_of_birth_year;
        // delete newUser.date_of_birth_day;
        // delete newUser.date_of_birth_month;
        // delete newUser.date_of_birth_year;
        // if (newUser.gender !== '') {
        //     newUser.gender = parseInt(newUser.gender, 10);
        // }
        // if (newUser.handedness !== '') {
        //     newUser.handedness = parseInt(newUser.handedness, 10);
        // }

        this.addRoles(newUser);
        this.logger.debug('model', newUser);

        this.userService.create(newUser).subscribe(
            response => {
                if (response.success === true) {
                    this.alertService.success($localize`Registration successful`);
                    this.successfullyRegistered = true;
                } else {
                    this.alertService.error($localize`Could not create user`, response.error);
                    this.logger.debug('error creating user', newUser, response.error);
                }
                this.loading = false;
            },
            error => {
                this.alertService.error($localize`Could not create user`, error);
                this.logger.debug('error creating user', newUser, error);
                this.loading = false;
            }
        );
    }



    // onEdit() {
    //     if (this.model.first_name && this.model.first_name.length >= 2 || this.model.last_name && this.model.last_name.length >= 2) {
    //         this.proposedPlayers = this.allPlayers.filter(this.checkPlayer, this);
    //         if (this.proposedPlayers.length === 0) {
    //             this.selectedPlayer = undefined;
    //         }
    //     } else {
    //         this.proposedPlayers = [];
    //         this.selectedPlayer = undefined;
    //     }
    // }

    // checkPlayer(player: Player): boolean {
    //     if (this.model.first_name) {
    //         if (this.model.first_name.trim().toUpperCase() !==
    //             player.first_name.substr(0, this.model.first_name.trim().length).toUpperCase()) {
    //             return false;
    //         }
    //     }
    //     if (this.model.last_name) {
    //         if (this.model.last_name.trim().toUpperCase() !==
    //             player.last_name.substr(0, this.model.last_name.trim().length).toUpperCase()) {
    //             return false;
    //         }
    //     }
    //     return true;
    // }

    compareFn(c1: number, c2: number): boolean {
        // this.logger.trace(c1, typeof c1, c2, typeof c2);
        return c1 && c2 ? c1.toString() === c2.toString() : c1 === c2;
    }

    guessNameParts(name: string): string[] {
        const names = name.split(' ');
        let startOfLastName = 1;
        // heuristic to find first / last name split by using the first with more
        // A-Z than a-z as beginning last name (might fail with names with many diacritics)
        for (let n = 0; n < names.length; n++) {
            const namePart = names[n];
            if (((namePart || '').match(/[A-Z]/) || []).length > ((namePart || '').match(/[a-z]/) || []).length) {
                startOfLastName = Math.max(1, n);
                break;
            }
        }
        const first_name = names.slice(0, startOfLastName).join(' ');
        const last_name = names.slice(startOfLastName).join(' ');
        return [first_name, last_name];
    }

    nameChanged(): void {
        this.searched = false;
        this.selectedPlayer = undefined;
        this.proposedPlayers = [];
    }

    onSelectPlayer(index: number) {
        this.selectedPlayer = Object.assign({}, this.proposedPlayers[index]);
        [this.model.first_name, this.model.last_name] = this.guessNameParts(this.selectedPlayer.name);

        // // just guessing
        // this.model.handedness = '1';

        // if (!this.selectedPlayer.date_of_birth || this.selectedPlayer.date_of_birth === '' ||
        //     this.selectedPlayer.date_of_birth === '01.01.2018') {
        //     this.playerService.getBirthday(this.selectedPlayer.first_name, this.selectedPlayer.last_name).subscribe(
        //         data => {
        //             if (data.bd) {
        //                 const dtd = DateTime.fromFormat(data.bd, 'MMM d, yyyy');
        //                 // don't use day / month of birthday
        //                 // this.model.date_of_birth_day = dtd.toFormat('dd');
        //                 // this.model.date_of_birth_month = dtd.toFormat('MM');
        //                 this.model.date_of_birth_day = '01';
        //                 this.model.date_of_birth_month = '01';
        //                 this.model.date_of_birth_year = dtd.toFormat('yyyy');
        //                 this.model.date_of_birth = dtd.toFormat('dd.MM.yyyy');
        //                 this.logger.debug('users bday', data.bd, 'converted in', this.model.date_of_birth_day + '.' +
        //                     this.model.date_of_birth_month + '.' + this.model.date_of_birth_year);
        //             } else {
        //                 this.logger.debug('no birthday found', data);
        //             }
        //         }
        //         // ignore errors
        //     );
        // } else {
        //     this.model.date_of_birth = this.selectedPlayer.date_of_birth;
        //     const dtd = DateTime.fromFormat(this.model.date_of_birth, 'dd.MM.yyyy');
        //     // don't use day / month of birthday
        //     // this.model.date_of_birth_day = dtd.toFormat('dd');
        //     // this.model.date_of_birth_month = dtd.toFormat('MM');
        //     this.model.date_of_birth_day = '01';
        //     this.model.date_of_birth_month = '01';
        //     this.model.date_of_birth_year = dtd.toFormat('yyyy');
        // }

        // if (!this.selectedPlayer.gender || this.selectedPlayer.gender === 0) {
        //     this.playerService.getGender(this.selectedPlayer._id).subscribe(
        //         response => {
        //             if (response.success === true) {
        //                 this.logger.debug('found gender:', response.result);
        //                 this.model.gender = response.result.gender;
        //             } else {
        //                 this.logger.debug('no gender found', response.error);
        //             }
        //         }
        //         // ignore errors
        //     );
        // } else {
        //     this.model.gender = this.selectedPlayer.gender;
        // }
    }
}
