import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../../util/alert/alert.service';
import { UserService } from '../_services/user.service';
import { PlayerService } from '../../player/player.service';
import { Player } from '../../player/player';
import { CategoriesService } from '../../ranking/categories.service';
import { QuestionsService } from '../../tournament/creation/questions.service';
import { ClubService } from '../../club/club.service';
import { NGXLogger } from 'ngx-logger';
import { Club } from '../../tournament/club';
import { Role } from '../../user/role';
import { YesNoDialogComponent } from '../../util/yesno-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../user';
import moment from 'moment';

@Component({
    templateUrl: 'register2.component.html',
    styleUrls: ['./register2.component.css'],
    providers: [PlayerService, CategoriesService, QuestionsService, ClubService]
})


export class Register2Component implements OnInit {

    model: any = {};
    loading = false;

    options: Array<{ _id, name }[]> = [];

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private questionsService: QuestionsService,
        private clubService: ClubService,
        private playerService: PlayerService,
        private logger: NGXLogger,
        public dialog: MatDialog
    ) { }


    ngOnInit(): void {
        // add all possible clubs
        const q = this.questionsService.createQuestion('organiser', 'dropdown', this.clubService.getClubNames(undefined));
        q.example.subscribe(
            response => {
                if (response.success === true) {
                    const data = response.result;
                    data.sort((c1: Club, c2: Club) => {
                        if (c1.nation && c1.nation === 'GER') {
                            if (c2.nation && c2.nation === 'GER') {
                                return c1.name.toUpperCase() < c2.name.toUpperCase() ? -1 :
                                    (c1.name.toUpperCase() > c2.name.toUpperCase() ? 1 : 0);
                            }
                            return -1;
                        }
                        if (c2.nation && c2.nation === 'GER') {
                            return 1;
                        }
                        return c1.name.toUpperCase() < c2.name.toUpperCase() ? -1 :
                            (c1.name.toUpperCase() > c2.name.toUpperCase() ? 1 : 0);
                    });
                    this.options[0] = [];
                    for (let i = 0; i < data.length; i++) {
                        this.options[0].push({ _id: data[i]._id, name: data[i].name });
                    }
                    this.logger.trace('store for ', q.label, ':', this.options[0]);
                    // this.logger.debug('stored for', this.questions[q].label, 'options', names);
                } else {
                    this.logger.error('Could not get list of available clubs. Please try again later.', response.error);
                    this.alertService.error($localize`Could not get list of available clubs. Please try again later.`, response.error);
                }
            },
            error => {
                this.logger.error('Could not get list of available clubs. Please try again later.', error);
                this.alertService.error($localize`Could not get list of available clubs. Please try again later.`, error);
            }
        );

        // add all possible (IOC) countries
        this.playerService.getNations().subscribe(response => this.options[1] = response.result);
    }

    addRoles(player: any) {
        let role = 0;
        switch (player.first_name + ' ' + player.last_name) {
            case 'Paul HOLLEIS':
            // case 'Wolfgang PROBST':
                role = Role.ROLE_PAUL;
                break;
            // case 'Osman KESKIN':
            // case 'Torsten KÖHLER':
            case 'Johannes SPÄTH':
            case 'Friederike POLLMANN':
            case 'Marcel HERRMANN':
            case 'Anna HUBERT':
                role = Role.ROLE_DCV;
                break;
            case 'Frank ZIMMERMANN':
                role = Role.ROLE_LICENSE;
                break;
            case 'Samuel HOLLEIS':
                role = Role.ROLE_ORGANISER;
                break;
            // case 'Charly KNOBLING':
            //     role = Role.ROLE_ICO;
            //     break;
            default:
                role = 0;
        }
        player.roles = role;
    }


    register() {
        const dialogRef = this.dialog.open(YesNoDialogComponent, {
            data: { text: 'Wirklich kein TournamentSoftware Account? Dann bitte weitermachen und \
                eine kurze Email an paulholleis@gmail.com mit dem username! Danke!' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result === true) {
                this.doRegister();
            }
        });
    }

    doRegister() {
        this.model.last_name = this.model.last_name.toUpperCase();

        // create a corresponding player
        const newPlayer = new Player();
        newPlayer.active = true;
        newPlayer.first_name = this.model.first_name;
        newPlayer.last_name = this.model.last_name;
        newPlayer.show_my_biography = true;
        newPlayer.show_my_picture = true;
        newPlayer.clubs = this.model.clubs;
        newPlayer.date_of_birth = moment(this.model.date_of_birth_day + '.' + this.model.date_of_birth_month + '.' + this.model.date_of_birth_year, 'D.M.YYYY').toDate();
        newPlayer.gender = this.model.gender;
        newPlayer.handedness = this.model.handedness;
        newPlayer.nation = this.model.nation;

        const newUser = new User();
        newUser.email_verified = false;
        newUser.email = this.model.email;
        newUser.first_name = this.model.first_name;
        newUser.last_name = this.model.last_name;
        newUser.username = this.model.username;
        newUser['password'] = this.model.password;

        this.addRoles(newUser);

        this.logger.debug('model', newUser);

        this.loading = true;

        this.playerService.createPlayer(newPlayer).subscribe(
            response => {
                if (response.success === true) {
                    if (response.result && response.result._id) {
                        this.model.player = response.result._id;
                        newUser.player = response.result._id as unknown as Player;

                        this.userService.create(newUser).subscribe(
                            response2 => {
                                if (response2.success === true) {
                                    if (response2.result && response2.result.user && response2.result.user.user_id) {
                                        this.userService.sendVerificationEmail(
                                            { _id: response2.result.user.user_id, email: newUser.email }).subscribe(
                                            response3 => {
                                                if (response3.success === true) {
                                                    this.alertService.success('Registration successful - \
                                                        please click in the link in the email we sent to ' + response3.result);
                                                    this.logger.debug('successfully created user', response3.result);
                                                    this.router.navigate(['/login']);
                                                } else {
                                                    this.alertService.error($localize`Could not send verification email`, response3.error);
                                                    this.logger.debug('error sending verification email', newUser, response3.error);
                                                    this.loading = false;
                                                }
                                            },
                                            error => {
                                                this.alertService.error($localize`Could not send verification email`, error);
                                                this.logger.debug('error sending verification email', newUser, error);
                                                this.loading = false;
                                            }
                                        );

                                    } else {
                                        this.alertService.error($localize`Could not create user, wrong response`, response2.error);
                                        this.logger.debug('error creating user, user_id in response undefined',
                                            newUser, response2.result, response2.error);
                                        this.loading = false;
                                    }
                                } else {
                                    this.alertService.error($localize`Could not create user`, response2.error);
                                    this.logger.debug('error creating user', newUser, response2.error);
                                    this.loading = false;
                                }
                            },
                            error => {
                                this.alertService.error($localize`Could not create user`, error);
                                this.logger.debug('error creating user', newUser, error);
                                this.loading = false;
                            }
                        );

                    } else {
                        this.alertService.error($localize`Could not create player, wrong response, no ID`, response.error);
                        this.logger.debug('error creating player, player_id in response undefined',
                            newUser, response.result, response.error);
                        this.loading = false;
                    }
                } else {
                    this.alertService.error($localize`Could not create player`, response.error);
                    this.logger.debug('error creating player', newPlayer, response.error);
                    this.loading = false;
                }
            },
            error => {
                this.alertService.error($localize`Could not create player`, error);
                this.logger.debug('error creating player', newPlayer, error);
                this.loading = false;
            }
        );
    }

    closeMatSelect(select: any) {
        select.close();
    }

    compareFn(c1, c2: number): boolean {
        // this.logger.trace(c1, typeof c1, c2, typeof c2);
        return c1 && c2 ? c1.toString() === c2.toString() : c1 === c2;
    }

}
