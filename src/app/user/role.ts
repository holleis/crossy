export class Role {
    // roles:
    // 0b  128      64     32       16       8       4       2       1
    //     Paul  _______  DCV     ICO   Lizenz Organiser Player  Schiri
    public static readonly ROLE_PAUL =       0b10000000;
    // ROLE_??? =      0b01000000;
    public static readonly ROLE_DCV =        0b00100000;
    public static readonly ROLE_ICO =        0b00010000;
    public static readonly ROLE_LICENSE =    0b00001000;
    public static readonly ROLE_ORGANISER =  0b00000100;
    public static readonly ROLE_PLAYER =     0b00000010;
    public static readonly ROLE_UMPIRE =     0b00000001;
    public static readonly ROLE_ALL_LOGGEDIN = 0;

    // tslint:disable-next-line:no-bitwise
    public static readonly ROLE_ENROLLALL = Role.ROLE_ORGANISER | Role.ROLE_DCV | Role.ROLE_ICO;

    constructor() {}
}
