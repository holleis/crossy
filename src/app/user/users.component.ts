
import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { UserService } from '../auth/_services/user.service';
import { NGXLogger } from 'ngx-logger';
import { AlertService } from '../util/alert/alert.service';
import { UserWithInfo } from '../auth/user';
import { Player } from '../player/player';
import { PlayerService } from '../player/player.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Role } from './role';
import isEqual from 'lodash-es/isEqual';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css'],
    providers: [
        PlayerService
    ]
})
export class UsersComponent implements OnInit, AfterViewInit {

    users: UserWithInfo[];
    dataSource = new MatTableDataSource();
    allColumns = ['name', 'clubs', 'roles', 'username', 'playerid', 'email_verified', 'passwordResetToken'];
    displayedColumns = ['name', 'clubs', 'roles'];

    possibleRoles: {value: number, name: string}[];
    roles = {};

    possibleYears: string[];
    beitragYears = {};

    filterTerm = new FormControl<string>('');
    // beitragFilterYears: boolean[] = [];
    // beitragFilterYearsIndet: boolean[] = [];
    //
    // clubs: Club[];
    // selectedClubs: Club[] = [];

    dataSourceDCV = new MatTableDataSource();
    dcvColumns = ['name', 'clubs', 'playerid'];
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
    loadingDCV = false;
    filterTerm2 = new FormControl<string>('');
    players: Player[];

    constructor(
        private userService: UserService,
        private logger: NGXLogger,
        private alertService: AlertService,
        private playerService: PlayerService
    ) { }

    ngAfterViewInit() {
       this.filterTerm.valueChanges.pipe(
            debounceTime(500),
            distinctUntilChanged(),)
            .subscribe(value => {
                this.logger.trace('filtering users with', value);
                this.dataSource.filter = value;
      });
      this.filterTerm2.valueChanges.pipe(
           debounceTime(500),
           distinctUntilChanged(),)
           .subscribe(value => {
               this.logger.trace('filtering DCV players with', value);
               this.dataSourceDCV.filter = value;
     });

      this.dataSourceDCV.paginator = this.paginator;
    }

    ngOnInit() {
        this.possibleRoles = [
            {value: Role.ROLE_DCV, name: 'DCV'},
            {value: Role.ROLE_ICO, name: 'ICO'},
            {value: Role.ROLE_LICENSE, name: 'LICENCE'},
            {value: Role.ROLE_ORGANISER, name: 'ORGANISER'},
            {value: Role.ROLE_UMPIRE, name: 'UMPIRE'},
        ];
        this.possibleYears = [];
        const thisYear = new Date().getFullYear();
        for (let y = 2018; y < thisYear + 2; y++) {
            this.possibleYears.push(y.toString());
            // // initially set filter to indeterminate
            // this.beitragFilterYearsIndet[y - 2018] = true;
        }

        const roles = this.userService.getCurrentUserRoles();
        // tslint:disable-next-line:no-bitwise
        if ((roles & Role.ROLE_DCV) > 0 || (roles & Role.ROLE_PAUL) > 0 || (roles & Role.ROLE_LICENSE) > 0) {
            this.allColumns.push('email');
            this.allColumns.push('dcvBeitrag');
            this.displayedColumns.push('dcvBeitrag');
            this.dcvColumns.push('dcvBeitrag');
        }

        // this.dataSource.filterPredicate = (data: UserWithInfo, filter: string) => {
        //     // all selected years plus a '!', then all not selected years
        //     this.logger.debug('filter:', filter)
        //     const filters = filter.split('!');
        //     const yesFilter = filters[0];
        //     const noFilter = filters[1];
        //
        //     if (yesFilter && yesFilter.length > 0) {
        //         const yesFilters = yesFilter.split(',');
        //         for (let i = 0; i < yesFilters.length; i++) {
            // // replace with indexOf for IE
        //             if (!data.dcvBeitrag.includes(yesFilters[i])) {
        //                 this.logger.debug('filter, false with yesFilter', yesFilters[i]);
        //                 return false;
        //             }
        //         }
        //     }
        //     if (noFilter && noFilter.length > 0) {
        //         const noFilters = noFilter.split(',');
        //         for (let i = 0; i < noFilters.length; i++) {
            // // replace with indexOf for IE
        //             if (data.dcvBeitrag.includes(noFilters[i])) {
        //                 this.logger.debug('filter, false with noFilter', noFilters[i]);
        //                 return false;
        //             }
        //         }
        //     }
        //     this.logger.debug('filter, true');
        //     return true;
        // };

        this.userService.getAllWithInfos().subscribe(
            response => {
                this.logger.trace('retrieve all users', response);
                if (response.success === true) {
                    this.users = response.result;
                    this.dataSource.data = this.users;
                    this.logger.debug('retrieved all', this.users.length, 'users');

                    for (let u = 0; u < this.users.length; u++) {
                        // extract roles
                        for (let r = 0; r < this.possibleRoles.length; r++) {
                            // tslint:disable-next-line:no-bitwise
                            if (this.possibleRoles[r].value & this.users[u].roles) {
                                if (typeof this.roles[this.users[u]._id] === 'undefined') {
                                    // start array if not yet done
                                    this.roles[this.users[u]._id] = [];
                                }
                                this.roles[this.users[u]._id].push(this.possibleRoles[r].value.toString());
                            }
                        }
                        // extract dcvBeitrag
                        for (let i = 0; i < this.possibleYears.length; i++) {
                            if (this.users[u].dcvBeitrag.indexOf(this.possibleYears[i]) !== -1) {
                                if (typeof this.beitragYears[this.users[u]._id] === 'undefined') {
                                    // start array if not yet done
                                    this.beitragYears[this.users[u]._id] = [];
                                }
                                this.beitragYears[this.users[u]._id].push(this.possibleYears[i]);
                            }
                        }
                    }
                } else {
                    this.logger.error('Could not retrieve all users:', response.error);
                    this.alertService.error($localize`Could not retrieve all users`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve all users:', error);
                this.alertService.error($localize`Could not retrieve all users`, error);
            }
        );

        // get all players of German clubs
        this.loadingDCV = true;
        this.playerService.getPlayersForGermanClubs().subscribe(
            response => {
                this.loadingDCV = false;
                if (response.success === true) {
                    // make clubs readable
                    response.result.map(p => {
                        const clubs = p.clubs?.map(c => c.name).join(',');
                        delete p.clubs;
                        p['myclubs'] = clubs;
                    });
                    this.dataSourceDCV.data = response.result;
                }
            }
        );
    }

    getUser(userId): UserWithInfo {
        for (let i = 0; i < this.users.length; i++) {
            if (this.users[i]._id === userId) {
                return this.users[i];
            }
        }
        return undefined;
    }

    rolesChanged(userId) {
        const user = this.getUser(userId);
        if ((user.first_name === 'Paul' && user.last_name === 'HOLLEIS')) {
            this.logger.debug('ignoring Paul');
        }

        let newrole = 0;
        const roles = this.roles[userId];
        for (let r = 0; r < roles.length; r++) {
            newrole += parseInt(roles[r], 10);
        }
        if (newrole !== user.roles) {
            this.logger.debug('need to update role of user', user.roles, 'with', newrole);
            user.roles = newrole;
            this.userService.update(user).subscribe(
                response => {
                    if (response.success === true) {
                        this.logger.trace('updated user to', response.result);
                        this.alertService.success($localize`Successfully updated user role`);
                    } else {
                        this.logger.error('Could not update user role:', response.error);
                        this.alertService.error($localize`Could not update user role`, response.error);
                    }
                },
                error => {
                    this.logger.error('Could not update user role:', error);
                    this.alertService.error($localize`Could not update user role`, error);
                }
            );
        }
    }

    // beitragFilterClicked(yearNr) {
    //     if (this.beitragFilterYearsIndet[yearNr]) {
    //         this.beitragFilterYearsIndet[yearNr] = false;
    //         this.beitragFilterYears[yearNr] = true;
    //     } else if (this.beitragFilterYears[yearNr]) {
    //         this.beitragFilterYears[yearNr] = false;
    //     } else {
    //         this.beitragFilterYearsIndet[yearNr] = true;
    //         this.beitragFilterYears[yearNr] = false;
    //     }
    //
    //     // create filter for current state
    //     let yesYears = '';
    //     for (let i = 0; i < this.possibleYears.length; i++) {
    //         if (!this.beitragFilterYearsIndet[i] && this.beitragFilterYears[i]) {
    //             yesYears += ',' + this.possibleYears[i];
    //         }
    //     }
    //     if (yesYears[0] === ',') {
    //         yesYears = yesYears.slice(1);
    //     }
    //     let noYears = '';
    //     for (let i = 0; i < this.possibleYears.length; i++) {
    //         if (!this.beitragFilterYearsIndet[i] && !this.beitragFilterYears[i]) {
    //             noYears += ',' + this.possibleYears[i];
    //         }
    //     }
    //     if (noYears[0] === ',') {
    //         noYears = noYears.slice(1);
    //     }
    //     // all selected years plus a '!', then all not selected years
    //     this.dataSource.filter = yesYears + '!' + noYears;
    // }

    dcvBeitragChanged(userId) {
        const user = this.getUser(userId);
        const newbeitrag = [];
        const beitragYears = this.beitragYears[userId];
        for (let i = 0; i < beitragYears.length; i++) {
            newbeitrag.push(beitragYears[i]);
        }
        if (!isEqual(newbeitrag, user.dcvBeitrag)) {
            this.logger.debug('need to update beitrag of player', user.dcvBeitrag, 'with', newbeitrag);

            const playerUpdate = {dcvBeitrag: newbeitrag};
            this.playerService.updatePlayer(user.player.toString(), playerUpdate).subscribe(
                response => {
                    if (response.success === true) {
                        this.logger.trace('updated player to', response.result);
                        this.alertService.success($localize`Successfully updated player beitrag`);
                    } else {
                        this.logger.error('Could not update player beitrag:', response.error);
                        this.alertService.error($localize`Could not update player beitrag`, response.error);
                    }
                },
                error => {
                    this.logger.error('Could not update player beitrag:', error);
                    this.alertService.error($localize`Could not update player beitrag`, error);
                }
            );
        }
    }

    dcvBeitragChangedPlayer(player: Player) {
        const newbeitrag = player.dcvBeitrag;
        this.logger.debug('need to update beitrag of player with', newbeitrag);

        const playerUpdate = {dcvBeitrag: newbeitrag};
        this.playerService.updatePlayer(player._id, playerUpdate).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.trace('updated player to', response.result);
                    this.alertService.success($localize`Successfully updated player beitrag`);
                } else {
                    this.logger.error('Could not update player beitrag:', response.error);
                    this.alertService.error($localize`Could not update player beitrag`, response.error);
                }
            },
            error => {
                this.logger.error('Could not update player beitrag:', error);
                this.alertService.error($localize`Could not update player beitrag`, error);
            }
        );
    }

    // getClubs() {
    //     this.clubService.getAllClubs().subscribe(
    //         response => {
    //             if (response.success === true) {
    //                 this.logger.debug('received', response.result.length, 'clubs');
    //                 this.clubs = response.result;
    //                 this.clubService.sortClubs(this.clubs);
    //             } else {
    //                 this.logger.error('Could not retrieve clubs', response.error);
    //                 this.alertService.warn($localize`Problems retrieving all clubs`, response.error);
    //             }
    //         },
    //         error => {
    //             this.logger.error('Could not retrieve clubs', error);
    //             this.alertService.warn($localize`Problems retrieving clubs`, error);
    //         }
    //     );
    // }
    //
    // selectAll(select: any, values) {
    //     if (select.name === 'clubs') {
    //         select.update.emit(values.map((val) => val._id));
    //     }
    // }
    // deselectAll(select: any) {
    //     if (select.name === 'clubs') {
    //         select.update.emit([]);
    //     }
    // }
}
