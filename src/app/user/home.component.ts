import { Component, OnInit } from '@angular/core';
import { Player } from '../player/player';
import { HomeService } from './home.service';
import { Tournament } from '../tournament/tournament';
import { PlayerService } from '../player/player.service';
import { VariableToText } from '../util/variabletotext';
import { AlertService } from '../util/alert/alert.service';
import { NGXLogger } from 'ngx-logger';
import { MatDialog } from '@angular/material/dialog';
import { ProfilelinkDialogComponent } from '../util/profilelink-dialog.component';
import { UserService } from '../auth/_services/user.service';
import { Role } from './role';
import { MatchdbService } from '../tournament/execute/matchdb.service';
import { Title } from '@angular/platform-browser';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [HomeService, PlayerService]
})

export class HomeComponent implements OnInit {
    myRankings: any;
    myTournaments: Tournament[] = [];
    myFutureTournaments: Tournament[];
    lastupdate: Date;
    user: any;
    myPlayer: Player;
    playerProperties: string[] = [];
    drawExists: boolean[] = [];

    JSON = JSON;

    _isRatherSmall = false;
    isRatherSmall$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 999px)')
        .pipe(map(result => result.matches));

    constructor(
        private homeService: HomeService,
        private playerService: PlayerService,
        private alertService: AlertService,
        private logger: NGXLogger,
        public dialog: MatDialog,
        private userService: UserService,
        private matchdbService: MatchdbService,
        private titleService: Title,
        private breakpointObserver: BreakpointObserver,
    ) { }

    ngOnInit() {
        this.titleService.setTitle('Crossy');

        this.user = this.userService.getCurrentUser();
        if (this.user) {
            this.getMyRankings(this.user.player_id);
            this.getMyPlayer(this.user.player_id);
            this.getMyTournaments(this.user.player_id);
        } else {
            this.playerProperties = [];
        }

        this.isRatherSmall$.subscribe(val => this._isRatherSmall = val);
    }


    getMyTournaments(player_id: string) {
        this.homeService.getMyTournaments(player_id).subscribe(
            response => {
                if (response.success === true) {
                    this.myTournaments = response.result;
                    const today = new Date().valueOf();
                    this.myFutureTournaments = this.myTournaments.filter((t) => t.start_date_epoch >= today);

                    // check if there is a draw for these tournaments
                    for (let t = 0; t < this.myTournaments.length; t++) {
                        this.checkIfDrawExists(t);
                    }
                    this.myFutureTournaments = [];
                } else {
                    this.logger.error('Could not retrieve my tournaments', response.error);
                    this.alertService.warn('Problems retrieving your tournaments. Please try again later. ' + response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve my tournaments', error);
                this.alertService.warn($localize`Problems retrieving your tournaments. Please try again later.`);
            }
        );
    }

    checkIfDrawExists(tIdx) {
        const tournamentId = this.myTournaments[tIdx]._id;
        if (!tournamentId || !this.myTournaments[tIdx].categories || this.myTournaments[tIdx].categories.length == 0) {
            this.drawExists[tIdx] = false;
        } else {
            // categories are not populated, i.e. are the ID string itself
            // const catId = this.myTournaments[tIdx].categories[0].toString();
            this.matchdbService.drawExists(tournamentId).subscribe(response => {
                this.logger.debug('retrieving tournament', tournamentId);
                try {
                    if (response.success === true) {
                        if (response.result && response.result === true) {
                            this.drawExists[tIdx] = true;
                        }
                    } else {
                        this.logger.error('error retrieving tournament', response.error);
                        this.drawExists[tIdx] = false;
                    }
                } catch (err) {
                    this.logger.error('error retrieving tournament', err);
                    this.drawExists[tIdx] = false;
                }
            });
        }
        this.drawExists[tIdx] = false;
    }

    // myRankings is
    // [{"category":"DCV Damen", "results": [{"rank":2,"points":1038,"lastupdate":"2017-11-29",
    // "player":{"id":101,"last_name":"HUBERT","first_name":"Anna"}}, {"rank":3,"points":418,"lastupdate":"2017-11-29",
    // "player":{"id":103,"last_name":"HÜCKINGHAUS","first_name":"Jana"}}, {"rank":3,"points":418,"lastupdate":"2017-11-29",
    // "player":{"id":102,"last_name":"ARENDARSKI","first_name":"Patricia"}}]}, ...]

    getMyRankings(player_id) {
        this.homeService.getMyRankings(player_id).subscribe(
            response => {
                this.myRankings = response.result;
                this.lastupdate = response.result && response.result[0]?.lastupdate;
            },
            error => {
                this.logger.error('Could not retrieve my rankings', error);
                this.alertService.warn($localize`Problems retrieving your ranking. Please try again later.`);
            }
        );
    }

    getMyPlayer(player_id) {
        this.playerService.getPlayer(player_id).subscribe(
            response => {
                if (response.success === true) {
                    this.logger.trace('received my player', response.result);
                    this.myPlayer = response.result;

                    const newprops = [];
                    const props = Object.getOwnPropertyNames(this.myPlayer);
                    for (let i = 0; i < props.length; i++) {
                        if (props[i] !== '_id' && props[i] !== 'id' && props[i] !== 'opid' && props[i] !== 'active' &&
                            props[i] !== 'show_my_picture' && props[i] !== 'show_my_biography' &&
                            props[i] !== 'data_protection_accepted' && props[i] !== 'newsletter_acknowledgement' &&
                            props[i] !== 'language' && props[i] !== 'mainclubindex' && props[i] !== '__v' &&
                            props[i] !== 'picture' && props[i] !== 'email_verified' && props[i] !== 'passwordResetToken' &&
                            this.myPlayer[props[i]] !== '') {
                            newprops.push(props[i]);
                        }
                    }
                    this.playerProperties = newprops;
                } else {
                    this.logger.error('Could not retrieve my player identity', response.error);
                    this.alertService.warn($localize`Problems retrieving your info. Please try again later`, response.error);
                }
            },
            error => {
                this.logger.error('Could not retrieve my player identity', error);
                this.alertService.warn($localize`Problems retrieving your info. Please try again later. `, error);
            }
        );
    }

    onSelect(player: Player) {
        this.dialog.open(ProfilelinkDialogComponent, {
            data: { tsid: player.tsProfileId }
        });
    }

    tr2(prop: string, value: string): string {
        return VariableToText.tr2(prop, value);
    }
    tr(prop: string): string {
        return VariableToText.tr(prop);
    }
    checkRole(roles: number): boolean {
        // TODO check how to share roles App / Server
        // from roleapi.js
        // tslint:disable-next-line:no-bitwise
        return (roles & Role.ROLE_PAUL) > 0;
    }
}
