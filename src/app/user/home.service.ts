import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { NGXLogger } from 'ngx-logger';
import { Tournament } from '../tournament/tournament';

@Injectable()
export class HomeService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger
    ) { }

    getMyRankings(player_id) {
        this.logger.debug('looking for rankings for player', player_id);

        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/myrankings/' + encodeURI(player_id.toString());
        return this.http.get<{
            'success': boolean,
            'result': {
                category: string,
                perCatResult: {
                    rank: number,
                    points: number,
                    player: { last_name: string, first_name: string, tsProfileId: string }
                }[],
                lastupdate: Date,
            }[],
            'error': string
        }>(rankingUrl);
    }

    getMyTournaments(player_id: string) {
        this.logger.debug('looking for tournaments in which the player is enrolled', player_id);

        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/mytournaments/' + encodeURI(player_id);
        return this.http.get<{
            'success': boolean,
            'result': Tournament[],
            'error': string
        }>(rankingUrl);
    }


}
