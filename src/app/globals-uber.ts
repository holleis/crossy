export const Globals = Object.freeze({
    BASE_API_URL: 'https://crossy.paul-holleis.de',
    WEBSOCKET_URL: 'wss://ws.crossy.paul-holleis.de',
    SUB_API_URL: '/api/v1',
    SCRIPTS_BASE_URL: 'https://crossy.paul-holleis.de/',
    IMPORT_CALENDAR_PHP_URL: 'retrieveCalendar.php',
    GET_BIRTHDAY_PHP_URL: 'getbirthday.php?',
    SCORESHEET_PHP_URL: 'ss.php?'
 });
