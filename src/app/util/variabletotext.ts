export class VariableToText {

    static tr(str: string): string {
        const res = str.split('_');
        for (let i = 0; i < res.length; i++) {
            res[i] = res[i].charAt(0).toUpperCase() + res[i].slice(1);
        }
        return res.join(' ');
    }

    static back(str: string): string {
        const res = str.split(' ');
        for (let i = 0; i < res.length; i++) {
            res[i] = res[i].charAt(0).toLowerCase() + res[i].slice(1);
        }
        return res.join('_');
    }

    static tr2(label: string, value: any): string {
        // <span *ngIf="prop === 'gender'"<b>{{prop}}</b>: {{myPlayer[prop] == 1 ? 'female' : 'male'}}</span>
        // <span *ngIf="prop === 'handedness'"<b>{{prop}}</b>: {{myPlayer[prop] == 1 ? 'right' : 'left'}}</span>
        switch (label) {
            case 'gender': {
                return (value === 0 || value === '0') ? '?' : (value === 1 || value === '1') ? 'female' : 'male';
            }
            case 'handedness': {
                return (value === 0 || value === '0') ? '?' : (value === 1 || value === '1') ? 'right' : 'left';
            }
            case 'clubs': {
                let clubs = '';
                for (let c = 0; c < value.length; c++) {
                    clubs += value[c].name + ', ';
                }
                return clubs.substring(0, clubs.length - 2);
            }
            case 'nations': {
                let nations = '';
                for (let c = 0; c < value.length; c++) {
                    nations += value[c].name + ', ';
                }
                return nations.substring(0, nations.length - 2);
            }
            default:
                return value;
        }
    }
}
