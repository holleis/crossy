import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Globals } from '../../globals';
// import { io } from 'socket.io-client';
import WebSocket from 'modern-isomorphic-ws';
import { MessageEvent } from 'modern-isomorphic-ws';

@Injectable()
export class SocketService {

    constructor(
        private logger: NGXLogger,
    ) {}

    private socket: WebSocket;

    public initSocket(): any {
        if (!this.socket) {
            this.logger.debug('ws, initializing new socket');
            try {
                this.socket = new WebSocket(Globals.WEBSOCKET_URL);
            } catch (err) {
                this.logger.debug('ws, error connecting:', err);
            }
        }
        return this.socket;
    }

    public send(event: SockEvent, tournamentId: string, categoryId: string, message: string): void {
        if (!this.socket) {
            this.logger.debug('ERROR: socket not initialized');
            return;
        }
        this.logger.debug('socket: send', event, message, tournamentId, categoryId);
        const str: string = JSON.stringify({ type: event, tournamentId, categoryId, message });
        this.socket.send(str as string);
    }

    public onMessage(cb: (msg: {type: string, tournamentId: string, categoryId: string, message: string}) => void) {
        if (!this.socket) {
            this.logger.debug('ERROR: socket not initialized');
            return;
        }
        this.logger.debug('registering new Message listener');
        this.socket.onmessage = ((event: MessageEvent) => {
            // event.type is actually "message"
            if (typeof event.data === 'string') {
                const msg = JSON.parse(event.data as string);
                if (typeof cb === 'function') {
                    cb(msg);
                }
            } else {
                this.logger.debug(`ERROR: received unknown data type ${event.type} from socket`);
            }
        });
    }

    public onEvent(event: SockEvent, cb: () => void): void {
        if (!this.socket) {
            this.logger.debug('ERROR: socket not initialized');
            return;
        }
        this.logger.debug('ws, registering new Event listener on', event);
        if (event === SockEvent.OPEN) {
            this.socket.onopen = cb;
        } else if (event === SockEvent.CLOSE) {
            this.socket.onclose = cb;
        } else {
            this.logger.debug('ws, tried to register unknwon Event:', event);
        }
    }


    // public send(event: SockEvent, tournamentId: string, categoryId: string, message: string): void {
    //     if (!this.socket) {
    //         this.logger.debug('ERROR: ws, socket not initialized');
    //         return;
    //     }
    //     this.logger.debug('ws, socket: send', event, message, tournamentId, categoryId);
    //     this.socket.send('message', { type: event, tournamentId: tournamentId, categoryId: categoryId, message: message });
    // }

    // public onMessage(): Observable<{ type: string, tournamentId: string, categoryId: string, message: string }> {
    //     if (!this.socket) {
    //         this.logger.debug('ERROR: ws, socket not initialized');
    //         return;
    //     }
    //     this.logger.debug('ws, registering new Message listener');
    //     return new Observable<{ type: string, tournamentId: string, categoryId: string, message: string }>(observer => {
    //         this.socket.onmessage((msg: { type: string, tournamentId: string, categoryId: string, message: string }) => observer.next(msg));
    //     });
    // }

    // public onEvent(event: SockEvent): Observable<any> {
    //     if (!this.socket) {
    //         this.logger.debug('ERROR: ws socket not initialized');
    //         return;
    //     }
    //     this.logger.debug('ws, registering new Event listener on', event);
    //     if (event === SockEvent.CONNECT) {
    //         return new Observable<SockEvent>(observer => {
    //             this.socket.onopen(() => observer.next());
    //         });
    //     } else if (event === SockEvent.DISCONNECT) {
    //         return new Observable<SockEvent>(observer => {
    //             this.socket.onclose(() => observer.next());
    //         });
    //     } else {
    //         this.logger.debug('ws, tried to register unknwon Event:', event);
    //     }
    // }

    // private socket: SocketIOClient.Socket;

    // public initSocket(): SocketIOClient.Socket {
    //     if (!this.socket) {
    //         this.logger.debug('initializing new socket');
    //         this.socket = io(Globals.BASE_API_URL);
    //     }
    //     return this.socket;
    // }

    // public send(event: SockEvent, tournamentId: string, categoryId: string, message: string): void {
    //     if (!this.socket) {
    //         this.logger.debug('ERROR: socket not initialized');
    //         return;
    //     }
    //     this.logger.debug('socket: send', event, message, tournamentId, categoryId);
    //     this.socket.emit('message', {type: event, tournamentId: tournamentId, categoryId: categoryId, message: message});
    // }

    // public onMessage(): Observable<{type: string, tournamentId: string, categoryId: string, message: string}> {
    //     if (!this.socket) {
    //         this.logger.debug('ERROR: socket not initialized');
    //         return;
    //     }
    //     this.logger.debug('registering new Message listener');
    //     return new Observable<{type: string, tournamentId: string, categoryId: string, message: string}>(observer => {
    //         this.socket.on('message', (msg: {type: string, tournamentId: string, categoryId: string, message: string}) => observer.next(msg));
    //     });
    // }

    // public onEvent(event: SockEvent): Observable<any> {
    //     if (!this.socket) {
    //         this.logger.debug('ERROR: socket not initialized');
    //         return;
    //     }
    //     this.logger.debug('registering new Event listener on', event);
    //     return new Observable<SockEvent>(observer => {
    //         this.socket.on(event, () => observer.next());
    //     });
    // }
}

// Socket.io events
export enum SockEvent {
    // CONNECT = 'connect',
    OPEN = 'open',
    CLOSE = 'close',
    MATCHUPDATE = 'matchupdate',
    GROUPMATCHUPDATE = 'groupmatchupdate',
    KOMATCHUPDATE = 'komatchupdate'
}

export interface UpdateEvent {
    type: string;
    tournamentId: string;
    categoryId: string;
    message: string;
}
