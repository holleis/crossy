import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-message-dialog',
    template: `
    <h1 mat-dialog-title>Message!</h1>
    <mat-dialog-content>
        {{data.text}}
    </mat-dialog-content>
    <mat-dialog-actions>
        <button mat-raised-button type="button" mat-dialog-close>OK</button>
    </mat-dialog-actions>
    `
})
export class MessageDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<MessageDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
    }

}
