import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';

@Component({
    selector: 'app-profilelink-dialog',
    template: `
    <h1 mat-dialog-title>TODO: Link to TS Profile</h1>
    <mat-dialog-content>
        <span *ngIf="!data?.tsProfileId; else haveid">
            No profile found :-(
        </span>
        <ng-template #haveid>
            TODO: Link to TS Profile {{data?.tsProfileId}}
        </ng-template>
    </mat-dialog-content>
    <mat-dialog-actions>
        <button mat-raised-button type="button" mat-dialog-close>Close</button>
    </mat-dialog-actions>
    `
})
export class ProfilelinkDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ProfilelinkDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { tsProfileId: string },
        private logger: NGXLogger
    ) { }

    ngOnInit() {
        this.logger.debug('generate link with tsid', this.data?.tsProfileId);
    }

}
