import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-ts-import-dialog',
    template: `
    <h1 mat-dialog-title>Paste the TournamentSoftware Tournament ID and Ranking ID here</h1>
    <mat-dialog-content style="width: 500px;">
        See Example URL: <a href="https://ico.tournamentsoftware.com/tournament/EDB5ADCC-7FEE-41AD-8373-746027AD90F6" target="_new">https://ico.tournamentsoftware.com/tournament/EDB5ADCC-7FEE-41AD-8373-746027AD90F6</a><br/>
        <mat-form-field class="form-group">
            <input matInput #firstInput type="text" placeholder="E.g. EDB5ADCC-7FEE-41AD-8373-746027AD90F6" name="ts_tid1" [(ngModel)]="ts_tid1">
        </mat-form-field>
        See Example URL: <a href="https://ico.tournamentsoftware.com/ranking/tournament.aspx?id=25666&tournament=147470" target="_new">https://ico.tournamentsoftware.com/ranking/tournament.aspx?id=25666&tournament=147470</a><br/>
        <mat-form-field class="form-group">
            <input matInput type="text" placeholder="E.g. 147470" name="ts_tid2" [(ngModel)]="ts_tid2">
        </mat-form-field>
    </mat-dialog-content>
    <mat-dialog-actions>
        <button mat-raised-button type="button" mat-dialog-close><mat-icon>cancel_presentation</mat-icon></button>
        <button mat-raised-button (click)="onYesClick()">Import</button>
    </mat-dialog-actions>
    `
})
export class TSImportDialogComponent implements OnInit, AfterViewInit {

    constructor(
        public dialogRef: MatDialogRef<TSImportDialogComponent>,
    ) { }

    @ViewChild('firstInput') firstInput;

    ts_tid1: string;
    ts_tid2: string;

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.firstInput?.nativeElement?.focus(), 750);
    }

    onYesClick(): void {
        this.dialogRef.close({ ts_tid1: this.ts_tid1, ts_tid2: this.ts_tid2 });
    }
}
