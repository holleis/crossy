import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { saveAs } from 'file-saver';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class Utils {
    constructor(
        @Inject(LOCALE_ID) private locale: string,
    ) {
        this.locale = this.locale ? this.locale.substr(0, 2) : 'en';
        moment.locale(this.locale);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    saveToFileSystem(data: any, type: 'text', filename?: string) {
        let ttype = 'text/plain;charset=utf8';
        const blob = new Blob([data], { type: ttype });
        saveAs(blob, filename);

        // (async (text) => {
        //     const encoding = 'windows-1252'; // a.k.a ANSI

        //     // const utf8_blob = new Blob([text], { endings: "native" });
        //     // const utf_8_txt = await utf8_blob.text();
        //     const utf_8_txt = text;

        //     const TE = window.TextEncoder as any;
        //     const encoder = new TE(encoding, {
        //         NONSTANDARD_allowLegacyEncoding: true
        //     });
        //     const data = encoder.encode(utf_8_txt); // now `data` is an Uint8Array
        //     const encoded_as_ANSI = new Blob([data]);

        //     // const read_as_ANSI = await readAsText(encoded_as_ANSI, encoding)
        //     saveAs(encoded_as_ANSI, filename);
        // })(data);
    }

    saveBlobToFileSystem(blob: Blob, filename?: string) {
        saveAs(blob, filename);
    }

    getMonthStr(m: string | number): string {
        if (typeof m === 'string') {
            m = parseInt(m, 10);
        }
        if (m >= 0 && m < 12) {
            return moment.months()[m];
        } else {
            return '';
        }
    }

    isDefined(obj: unknown): boolean {
        return typeof obj !== 'undefined';
    }
}
