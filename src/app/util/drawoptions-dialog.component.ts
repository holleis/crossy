import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';

@Component({
    selector: 'app-drawoptions-dialog',
    templateUrl: './drawoptions-dialog.component.html'
})
export class DrawOptionsDialogComponent implements OnInit {

    constructor(
        private dialogRef: MatDialogRef<DrawOptionsDialogComponent>,
        private logger: NGXLogger,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    rulesFor2020 = new Date().getFullYear() >= 2020;

    ngOnInit() {
        this.logger.debug('in dialog:', this.data.cats);
    }

    onYesClick(): void {
        this.dialogRef.close({
            doGroupPhase: this.data.doGroupPhase,
            assignmentMode: this.data.assignmentMode,
            minPlayersPerCategory: this.data.minPlayersPerCategory
        });
    }
}
