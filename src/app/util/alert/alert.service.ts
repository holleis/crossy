import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { TranslatorService } from '../translate/translator.service';
import { Alert, AlertMessageInfo, AlertType } from './alert';

@Injectable({
    providedIn: 'root',
})
export class AlertService {
    private subject = new Subject<Alert>();
    private keepAfterRouteChange = false;

    constructor(
        router: Router,
        private tr: TranslatorService,
    ) {
        // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterRouteChange) {
                    // only keep for a single route change
                    this.keepAfterRouteChange = false;
                } else {
                    // clear alert messages
                    this.clear(true);
                }
            }
        });
    }

    getAlert(): Observable<Alert> {
        return this.subject.asObservable();
    }

    success(message: string, msgObj?: string | AlertMessageInfo, keepAfterRouteChange = false, clear = true): void {
        this.alert(AlertType.Success, message, msgObj, keepAfterRouteChange);
        if (clear) {
            setTimeout(() => {
                this.clear();
            }, 3000);
        }
    }

    error(message: string, msgObj?: string | AlertMessageInfo, keepAfterRouteChange = false, clear = true): void {
        this.alert(AlertType.Error, message, msgObj, keepAfterRouteChange);
        if (clear) {
            setTimeout(() => {
                this.clear();
            }, 10000);
        }
    }

    info(message: string, msgObj?: string | AlertMessageInfo, keepAfterRouteChange = false, clear = true): void {
        this.alert(AlertType.Info, message, msgObj, keepAfterRouteChange);
        if (clear) {
            setTimeout(() => {
                this.clear();
            }, 3000);
        }
    }

    warn(message: string, msgObj?: string | AlertMessageInfo, keepAfterRouteChange = false, clear = true): void {
        if (clear) {
            setTimeout(() => {
                this.clear();
            }, 8000);
        }
        this.alert(AlertType.Warning, message, msgObj, keepAfterRouteChange);
    }

    private alert(type: AlertType, alertMessage: string, msgObj?: string | AlertMessageInfo, keepAfterRouteChange = false): void {
        setTimeout(() => {
            this.clear();
            this.keepAfterRouteChange = keepAfterRouteChange;
            let link: string;
            let linkText: string;
            let msgText = '';
            if (typeof msgObj === 'string') {
                msgText = msgObj;
            } else {
                if (msgObj?.error) {
                    const obj = msgObj.error;
                    if (obj instanceof ProgressEvent) {
                        msgText = 'Could not reach server.';
                    } else if (typeof obj !== 'string' && obj?.error) {
                        msgText = obj.error;
                        if (msgText['message']) {
                            msgText = msgText['message'];
                        }
                    } else if (typeof obj !== 'string' && obj.message) {
                        msgText = obj.message;
                    }
                } else if (msgObj?.message && msgObj.link && msgObj.linkText) {
                    link = msgObj.link;
                    linkText = msgObj.linkText;
                    msgText = msgObj.message;
                } else if (msgObj?.message) {
                    msgText = msgObj.message;
                }
            }
            // msgText = this.tr.anslateMessage(msgText);
            msgText = this.tr.anslate(msgText);
            this.subject.next(<Alert>{ type, message: alertMessage + (msgText ? (': ' + msgText) : ''), link, linkText });
        });
    }

    clear(alsoErrors = false): void {
        // clear alerts
        this.subject.next(<Alert>{ type: alsoErrors ? AlertType.ClearAlsoErrors : AlertType.Clear });
    }
}
