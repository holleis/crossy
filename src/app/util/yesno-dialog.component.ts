import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';

@Component({
    selector: 'app-yesno-dialog',
    template: `
    <h1 mat-dialog-title i18n="dialog, are you sure header@@are you sure">Are you sure?</h1>
    <mat-dialog-content>
        {{data.text}}
    </mat-dialog-content>
    <mat-dialog-actions>
        <button mat-raised-button type="button" mat-dialog-close i18n="dialog, No button@@No">No</button>
        <button mat-raised-button (click)="onYesClick()" i18n="dialog, Yes button@@Yes">Yes</button>
    </mat-dialog-actions>
    `
})
export class YesNoDialogComponent implements OnInit {

    constructor(
        private dialogRef: MatDialogRef<YesNoDialogComponent>,
        private logger: NGXLogger,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
        this.logger.debug(this.data);
    }

    onYesClick(): void {
        this.dialogRef.close(true);
    }
}
