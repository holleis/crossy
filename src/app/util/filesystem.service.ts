// import { AlertService } from './alert/alert.service';
import { NGXLogger } from 'ngx-logger';
import { Injectable } from '@angular/core';
import { EnrollmentInfo } from '../tournament/cat-info';
import { Group } from '../tournament/group';
import { Ko } from '../tournament/ko';

enum FileErrorEnum {
    ENCODING_ERR = 5,
    // QUOTA_EXCEEDED_ERR = 10,
    // NOT_FOUND_ERR = 1,
    // SECURITY_ERR = 2,
    // INVALID_MODIFICATION_ERR = 9,
    // INVALID_STATE_ERR = 7,
    ABORT_ERR = 20,
    DATA_CLONE_ERR = 25,
    DOMSTRING_SIZE_ERR = 2,
    HIERARCHY_REQUEST_ERR = 3,
    INDEX_SIZE_ERR = 1,
    INUSE_ATTRIBUTE_ERR = 10,
    INVALID_ACCESS_ERR = 15,
    INVALID_CHARACTER_ERR = 5,
    INVALID_MODIFICATION_ERR = 13,
    INVALID_NODE_TYPE_ERR = 24,
    INVALID_STATE_ERR = 11,
    NAMESPACE_ERR = 14,
    NETWORK_ERR = 19,
    NOT_FOUND_ERR = 8,
    NOT_SUPPORTED_ERR = 9,
    NO_DATA_ALLOWED_ERR = 6,
    NO_MODIFICATION_ALLOWED_ERR = 7,
    QUOTA_EXCEEDED_ERR = 22,
    SECURITY_ERR = 18,
    SYNTAX_ERR = 12,
    TIMEOUT_ERR = 23,
    TYPE_MISMATCH_ERR = 17,
    URL_MISMATCH_ERR = 21,
    VALIDATION_ERR = 16,
    WRONG_DOCUMENT_ERR = 4,
}
interface FileError {
    code: FileErrorEnum;
}

interface TournamentData {
    players: EnrollmentInfo['players'];
    groups: Group[];
    ko: Ko;
    ko_b: Ko;
}


@Injectable({
    providedIn: 'root'
})
export class FilesystemService {

    constructor(
        private logger: NGXLogger,
        // private alertService: AlertService,
    ) { }

    // tournamentId: string;
    // categoryId: string;
    // tournamentData: TournamentData;
    fileSystem: any;
    havePersistentStorage = true;

    useFileStorage(tournamentId: string, categoryId: string, tournamentData: TournamentData) {
        if (!this.fileSystem) {
            this.requestFileStorage(tournamentId, categoryId, tournamentData);
        } else {
            this.storeToFileSystem(tournamentId, categoryId, tournamentData);
        }
    }

    requestFileStorage(tournamentId?: string, categoryId?: string, tournamentData?: TournamentData): Promise<{}> {
        return new Promise((resolve, reject) => {
            if (navigator.storage && navigator.storage.persist) {
                navigator.storage.persisted().then(granted => {
                    if (granted) {
                        this.logger.debug('have persistent storage!');
                        this.havePersistentStorage = true;
                        this.initFileStorage(tournamentId, categoryId, tournamentData).then(
                            () => resolve(true),
                            (err: any) => reject(err)
                        );
                    } else {
                        navigator.storage.persist().then(granted2 => {
                            if (granted2) {
                                this.logger.debug('have persistent storage');
                                this.havePersistentStorage = true;
                                this.initFileStorage(tournamentId, categoryId, tournamentData).then(
                                    () => resolve(true),
                                    (err: any) => reject(err)
                                );
                            } else {
                                this.logger.debug('Don\'t have persistent storage.');
                                this.havePersistentStorage = false;
                                this.initFileStorage(tournamentId, categoryId, tournamentData).then(
                                    () => resolve(true),
                                    (err: any) => reject(err)
                                );
                            }
                        });
                    }
                });
            } else {
                reject('not supported on this browser / platform');
            }
        });
    }

    initFileStorage(tournamentId: string, categoryId: string, tournamentData: TournamentData) {
        return new Promise((resolve, reject) => {
            if (this.havePersistentStorage) {
                navigator['webkitPersistentStorage'].queryUsageAndQuota(
                    (usedBytes: number, grantedBytes: number) => {
                        this.logger.debug('we are using ', usedBytes, ' of ', grantedBytes, 'bytes');
                        if (grantedBytes > 0 && usedBytes < grantedBytes) {
                            if (!this.fileSystem) {
                                this.requestFileSystem(tournamentId, categoryId, tournamentData).then(
                                    () => resolve(true),
                                    (err: any) => reject(err)
                                );
                            } else if (tournamentId && categoryId && tournamentData) {
                                resolve(true);
                                this.storeToFileSystem(tournamentId, categoryId, tournamentData);
                            }
                        } else {
                            this.requestFileSystem(tournamentId, categoryId, tournamentData).then(
                                () => resolve(true),
                                (err: any) => reject(err)
                            );
                        }
                    },
                    function(e: FileError) {
                        this.fileSystemErrorHandler(e).bind(this);
                        reject(e);
                    }
                );
            } else {
                navigator['webkitTemporaryStorage'].queryUsageAndQuota(
                    (usedBytes: number, grantedBytes: number) => {
                        this.logger.debug('we are using ', usedBytes, ' of ', grantedBytes, 'bytes');
                        if (grantedBytes > 0 && usedBytes < grantedBytes) {
                            if (!this.fileSystem) {
                                this.requestFileSystem(tournamentId, categoryId, tournamentData).then(
                                    () => resolve(true),
                                    (err: any) => reject(err)
                                );
                            } else if (tournamentId && categoryId && tournamentData) {
                                resolve(true);
                                this.storeToFileSystem(tournamentId, categoryId, tournamentData);
                            }
                        } else {
                            this.requestFileSystem(tournamentId, categoryId, tournamentData).then(
                                () => resolve(true),
                                (err: any) => reject(err)
                            );
                        }
                    },
                    function(e: FileError) {
                        this.fileSystemErrorHandler(e).bind(this);
                        reject(e);
                    }
                );
            }
        });
    }

    requestFileSystem(tournamentId: string, categoryId: string, tournamentData: TournamentData) {
        return new Promise((resolve, reject) => {
            if (this.havePersistentStorage) {
                navigator['webkitPersistentStorage'].requestQuota(1024 * 1024 * 15, (reallyGrantedBytes: number) => {
                    // actuall call to request file system
                    window['webkitRequestFileSystem'](window['PERSISTENT'], reallyGrantedBytes, this.onInitFileSystem.bind(this, tournamentId, categoryId, tournamentData, resolve), this.fileSystemErrorHandler.bind(this, reject));
                }, function (e: FileError) {
                    this.fileSystemErrorHandler(e).bind(this);
                    reject(e);
                });
            } else {
                window['webkitRequestFileSystem'](window['TEMPORARY'], 1024 * 1024 * 15, this.onInitFileSystem.bind(this, tournamentId, categoryId, tournamentData, resolve), this.fileSystemErrorHandler.bind(this, reject));
            }
        });
    }

    onInitFileSystem(tournamentId: string, categoryId: string, tournamentData: TournamentData, resolve: (value?: unknown) => void, fs: any) {
        this.logger.debug('Opened file system: ' + fs.name);
        this.fileSystem = fs;
        if (typeof resolve === 'function') {
            resolve();
        }
        if (tournamentId && categoryId && tournamentData) {
            this.storeToFileSystem(tournamentId, categoryId, tournamentData);
        }
    }

    storeToFileSystem(tournamentId: string, categoryId: string, tournamentData: TournamentData) {
        if (!this.fileSystem) {
            this.logger.error('filesystem undefined');
            return;
        }
        const filename = 'crossy_' + tournamentId + '_' + categoryId + '.json';
        this.fileSystem.root.getFile(filename, { create: true }, (fileEntryToRemove: any) => {
            fileEntryToRemove.remove(() => {

                this.fileSystem.root.getFile(filename, { create: true, exclusive: false }, (fileEntry) => {
                    fileEntry.createWriter((fileWriter: any) => {

                        fileWriter.onwriteend = (_: any) => {
                            this.logger.debug('Write completed (' + tournamentId + ', ' + categoryId + ')');
                        };

                        fileWriter.onerror = (e: any) => {
                            this.logger.debug('Write failed (' + tournamentId + ', ' + categoryId + '): ' + e.toString());
                        };

                        const blob = new Blob([JSON.stringify(tournamentData)], { type: 'text/plain' });
                        fileWriter.write(blob);

                    }, this.fileSystemErrorHandler.bind(this, undefined));
                }, this.fileSystemErrorHandler.bind(this, undefined));
            }, this.fileSystemErrorHandler.bind(this, undefined));
        }, this.fileSystemErrorHandler.bind(this, undefined));
    }

    deleteFile(tournamentId: string, categoryId: string) {
        const filename = 'crossy_' + tournamentId + '_' + categoryId + '.json';
        this.fileSystem.root.getFile(filename, { create: false }, (fileEntry: any) => {
            fileEntry.remove(() => {
                this.logger.debug('File removed (' + tournamentId + ', ' + categoryId + '): ' + filename);
            }, this.fileSystemErrorHandler.bind(this, undefined));
        }, this.fileSystemErrorHandler.bind(this, undefined));
    }

    fileSystemErrorHandler(reject: (reason?: any) => void, e: FileError) {
        if (typeof reject === 'function') {
            reject(e);
        }

        let msg = '';

        switch (e.code) {
            case FileErrorEnum.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileErrorEnum.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                this.logger.debug('file not found: ' + msg);
                return;
            case FileErrorEnum.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileErrorEnum.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileErrorEnum.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = 'Unknown Error ' + e.code;
                break;
        }

        this.logger.error('Error using file system: ' + msg);
    }


    checkLocalFilesystem(tournamentId: string, categoryId: string, cb: (td: TournamentData) => void) {
        if (!this.fileSystem) {
            this.requestFileStorage().then(
                () => this.doCheckLocalFileSystem(tournamentId, categoryId, cb),
                (err) => this.logger.error(err)
            );
        } else {
            this.doCheckLocalFileSystem(tournamentId, categoryId, cb);
        }

    }

    private doCheckLocalFileSystem(tournamentId: string, categoryId: string, cb: (td: TournamentData) => void) {
        const filename = 'crossy_' + tournamentId + '_' + categoryId + '.json';
        this.fileSystem.root.getFile(filename, { create: false, exclusive: false }, (fileEntry) => {
            fileEntry.file((file: any) => {
                const reader = new FileReader();

                reader.onload = (e) => {
                    const txt = e.target['result'];
                    if (txt) {
                        try {
                            const jsondata = JSON.parse(txt as string) as { players: EnrollmentInfo['players'], groups: Group[], ko: Ko, ko_b: Ko };
                            if (cb && jsondata) {
                                cb(jsondata);
                            }
                        } catch (err) {
                            this.logger.error('could not parse json from file system: ' + err.toString());
                        }
                    }
                };

                reader.readAsText(file);
            }, this.fileSystemErrorHandler.bind(this, undefined));

        }, this.fileSystemErrorHandler.bind(this, undefined));
    }

    // pushLocalFiles(entries) {
    //     entries.forEach((entry, i) => {
    //         const filename = entry.name; // 'crossy_' + this.tournamentId + '_' + this.categoryId + '.json';
    //         const fsplit = filename.split('_');
    //         if (fsplit.length !== 3) {
    //             return;
    //         }
    //         const tournamentId = fsplit[1];
    //         const categoryId = fsplit[2];
    //         this.fileSystem.root.getFile(filename, { create: true, exclusive: false }, (fileEntry) => {
    //             fileEntry.file((file: any) => {
    //                 const reader = new FileReader();

    //                 reader.onload = (e) => {
    //                     const txt = e.target['result'];
    //                     if (txt) {
    //                         try {
    //                             const jsondata = JSON.parse(txt as string) as {players: EnrollmentInfo['players'], groups: Group[], ko: Ko, ko_b: Ko};
    //                             if (jsondata) {
    //                                 // TODO this must be done with a callback!
    //                                 // store to DB all potentially locally stored categories
    //                                 this.matchDbService.storeTournamentData(tournamentId, categoryId, jsondata.players, jsondata.groups, jsondata.ko, jsondata.ko_b, '').subscribe(
    //                                     response => {
    //                                         if (response.success === true) {
    //                                             this.logger.debug('stored ok', tournamentId, categoryId);
    //                                             // fileEntry.remove(() => {
    //                                                 this.logger.debug('File removed:' + filename);
    //                                             // }, this.fileSystemErrorHandler.bind(this, undefined));
    //                                             this.alertService.success($localize`Matches successfully stored`);
    //                                         } else {
    //                                             this.alertService.warn($localize`Still offline! Stored locally.`);
    //                                         }
    //                                     },
    //                                     _error => {
    //                                         this.alertService.warn($localize`Still offline! Stored locally.`);
    //                                     }
    //                                 );
    //                             }
    //                         } catch (err) {
    //                             this.logger.error('could not parse json from file system: ' + err.toString());
    //                         }
    //                     }
    //                 };

    //                 reader.readAsText(file);
    //             }, this.fileSystemErrorHandler.bind(this, undefined));

    //         }, this.fileSystemErrorHandler.bind(this, undefined));
    //     });
    // }


    // private fileListToArray(list) {
    //     return Array.prototype.slice.call(list || [], 0);
    // }

            // if (this.fileSystem) {
        //     const dirReader = this.fileSystem.root.createReader();
        //     let entries = [];

        //     // Call the reader.readEntries() until no more results are returned.
        //     const readEntries = () => {
        //         dirReader.readEntries((results) => {
        //             if (!results.length) {
        //                 this.pushLocalFiles(entries);
        //             } else {
        //                 entries = entries.concat(this.fileListToArray(results));
        //                 readEntries();
        //             }
        //         }, this.fileSystemErrorHandler.bind(this, undefined));
        //     };

        //     readEntries();
        // }

}
