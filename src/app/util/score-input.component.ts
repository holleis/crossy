import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { NGXLogger } from 'ngx-logger';

@Component({
    selector: 'app-score-input',
    templateUrl: './score-input.component.html',
    styleUrls: ['./score-input.component.css']
})
export class ScoreInputComponent implements OnInit {

    constructor(
        private breakpointObserver: BreakpointObserver,
        // private logger: NGXLogger,
    ) { }

    MAX_POINTS = 29;
    
    @Input() partnerHas: number;
    allowedPoints = [];

    isLarge$: Observable<boolean> = this.breakpointObserver.observe('(min-width: 600px)')
        .pipe(map(result => result.matches));
    @Output() scored = new EventEmitter<number>();

    ngOnInit() {
        this.allowedPoints = [];
        if (this.partnerHas && this.partnerHas <= 14) {
            this.allowedPoints = [16];
        } else if (this.partnerHas === 15) {
            this.allowedPoints.push(17);
        } else if (this.partnerHas === 16) {
            for (let i = 0; i <= 14; i++) {
                this.allowedPoints.push(i);
            }
            this.allowedPoints.push(18);
        } else if (this.partnerHas >= 17) {
            this.allowedPoints.push(this.partnerHas - 2);
            this.allowedPoints.push(this.partnerHas + 2);
        } else {
            for (let i = 0; i <= this.MAX_POINTS; i++) {
                this.allowedPoints.push(i);
            }
        }
    }

    setPoints(p: number): void {
        this.scored.emit(p);
    }
}
