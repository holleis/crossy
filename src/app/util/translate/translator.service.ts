import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
// import { I18n } from '@ngx-translate/i18n-polyfill';

@Injectable({
    providedIn: 'root'
})
export class TranslatorService {

    constructor(
        // private i18n: I18n
        private logger: NGXLogger,
        @Inject(LOCALE_ID) public locale: string,
    ) { }

    // anslateMessage(msg: string): string {
    //     if (!msg) {
    //         return '';
    //     }
    //     if (!(typeof msg === 'string')) {
    //         msg = (msg as unknown).toString();
    //     }
    //     // switch (msg) {
    //     //     case 'value':
                
    //     //         break;
        
    //     //     default:
    //     // }

    //     let found: string[];
    //     if ((found = msg.match(/user with email (.*) already exists/)) && found[1]) {
    //         return $localize`:@@email exists:user with email ${found[1]}:email: already exists`;
    //     }
    //     if ((found = msg.match(/Player already exists - please login using email (.*)/)) && found[1]) {
    //         return $localize`:@@player exists:Player already exists - please login using email ${found[1]}:email:`;
    //     }
    //     // does not work since the template syntax does not recognize this correctly:
    //     // const trmsg = $localize`:@@${msg}:${msg}`;
    //     // // const trmsg = $localize`:@@Currently, only the DCV can make changes. We will probably enable this for next year!:Currently, only the DCV can make changes. We will probably enable this for next year!:`;
    //     // if (trmsg === `:@@${msg}:`) {
    //     //     this.logger.warn('no translation found for ' + msg);
    //     // } else {
    //     //     msg = trmsg;
    //     // }
    //     let mymsg = [`:@@${msg}:${msg}`];
    //     // anfang {{startIndex}} - {{endIndex}} of {{length}} ende
    //     // => :@@anfang {{startIndex}} - {{endIndex}} of {{length}} ende:anfang
    //     // => :INTERPOLATION: - 
    //     // => :INTERPOLATION_1: of
    //     // => :INTERPOLATION_2: ende
    //     if (mymsg.indexOf('{{') >= 0) {
    //         const msgInt = msg.replace(/\{\{.*?\}\}/g, '§§§:INTERPOLATION:');
    //         let cnt = 0;
    //         mymsg = msgInt.split('§§§').filter(m => m);
    //         for (let i = 0; i < mymsg.length; i++) {
    //             if (mymsg[i].indexOf('INTERPOLATION') >= 0) {
    //                 if (cnt > 0) {
    //                     mymsg[i] = mymsg[i].replace('INTERPOLATION', `INTERPOLATION_${cnt}`);
    //                 }
    //                 cnt += 1;
    //             }
    //         };
    //         if (mymsg[0].substring(0, ':INTERPOLATION:'.length) === ':INTERPOLATION:') {
    //             mymsg.unshift('');
    //         }
    //         mymsg[0] = `:@@${msg}:${msg[0]}`;
    //     }
    //     // eslint-disable-next-line @typescript-eslint/no-explicit-any
    //     const idarr = mymsg as any;
    //     idarr.raw = mymsg;
    //     // if (params) {
    //     //     // TODO! necessary only for the extract-i18n call
    //     //     // return id;
    //     //     return !mymsg ? '' : $localize(idarr as TemplateStringsArray, ...Object.values(params));
    //     // } else {
    //     //    // TODO! necessary only for the extract-i18n call
    //     //    // return id;
    //     return !mymsg ? '' : $localize(idarr as TemplateStringsArray, {});
    //     // }
    //     // return msg;
    // }

    /**
     * Returns the translation of the term with the given ID in the current language
     * @param id id of the translation
     * @param params key value pair of parameters that will be replaced in the translation
     */
    anslate(id: string, params?: { [key: string]: string | number }): string {
        if (!id) {
            return '';
        }
        if (!params) {
            if (!this.locale || this.locale.substring(0, 2) === 'en') {
                // shortcut for English without {{}}
                if (id.substring(0, 7) === 'plural_') {
                    // special treatment for 'plural_bag' etc.
                    if (id === 'plural_box') {
                        return 'boxes';
                    } else {
                        return id.substring(7) + 's';
                    }
                } else if (id.substring(0, 13) === '(every n>=2) ') {
                    // special treatment for '(every n>=2) days' etc.
                    return id.substring(13);
                }
                if (!this.locale || this.locale !== 'en-GB') {
                    // shortcut for default English without {{}}
                    return id;
                }
            }
        }

        let msg = [`:@@${id}:${id}`];
        // anfang {{startIndex}} - {{endIndex}} of {{length}} ende
        // => :@@anfang {{startIndex}} - {{endIndex}} of {{length}} ende:anfang
        // => :INTERPOLATION: - 
        // => :INTERPOLATION_1: of
        // => :INTERPOLATION_2: ende
        if (id.indexOf('{{') >= 0) {
            const msgInt = id.replace(/\{\{.*?\}\}/g, '§§§:INTERPOLATION:');
            let cnt = 0;
            msg = msgInt.split('§§§').filter(m => m);
            for (let i = 0; i < msg.length; i++) {
                if (msg[i].indexOf('INTERPOLATION') >= 0) {
                    if (cnt > 0) {
                        msg[i] = msg[i].replace('INTERPOLATION', `INTERPOLATION_${cnt}`);
                    }
                    cnt += 1;
                }
            };
            if (msg[0].substring(0, ':INTERPOLATION:'.length) === ':INTERPOLATION:') {
                msg.unshift('');
            }
            msg[0] = `:@@${id}:${msg[0]}`;
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const idarr = msg as any;
        idarr.raw = msg;
        if (params) {
            // TODO! necessary only for the extract-i18n call
            // return id;
            return !id ? '' : $localize(idarr as TemplateStringsArray, ...Object.values(params));
        } else {
            // TODO! necessary only for the extract-i18n call
            // return id;
            return !id ? '' : $localize(idarr as TemplateStringsArray, {});
        }
        // return id;
        // return !id ? '' : this.i18n({ id, value: id }, params);
    }

    anslateProperties(prop: string): string {
        if (!prop) {
            return '';
        }
        switch (prop) {
            case 'Title': return $localize`Title`;
            case 'Location': return $localize`Location`;
            case 'Organiser': return $localize`Organiser`;
            case 'Start Date': return $localize`Start Date`;
            case 'End Date': return $localize`End Date`;
            case 'Registration Deadline': return $localize`Registration Deadline`;
            case 'Nation': return $localize`Nation`;
            case 'Federation': return $localize`Federation`;
            case 'Tournament Type': return $localize`Tournament Type`;
            case 'Points': return $localize`Points`;
            case 'Categories': return $localize`Categories`;
            case 'Responsibles': return $localize`Responsibles`;
            
            case 'Group': return $localize`Group`;
            case 'KO': return $localize`KO`;

            case 'Ts Tid1':
            case 'Ts Tid2':
                return prop;
        
            default:
                this.logger.warn('no translation found for ' + prop);
                return prop;
        }
    }

/*
    // $localizeId(messageParts: TemplateStringsArray, ...expressions: any[]): any {
    //     // return $localize(['user with email ', ' already exists'] as any as TemplateStringsArray, expressions);
        
    //     // Create writeable copies
    //     const messagePartsCopy: any = [...messageParts];
    //     // const messagePartsRawCopy = [...messageParts.raw];

    //     // Strip trans-unit-id element
    //     const prefix = messagePartsCopy.shift();
    //     // const prefixRaw = messagePartsRawCopy.shift();
    //     const transUnitId = expressions.shift();

    //     // Re-add prefix and replace :TRANSUNITID: with transUnitId.
    //     messagePartsCopy[0] = prefix + messagePartsCopy[0].replace(':TRANSUNITID:', `:@@${transUnitId}:`);
    //     // messagePartsRawCopy[0] = prefixRaw + messagePartsRawCopy[0].replace(':TRANSUNITID:', `:${transUnitId}:`);

    //     // Create messageParts object
    //     // Object.defineProperty(messagePartsCopy, 'raw', { value: messagePartsRawCopy });

    //     const mp: string[] = [':@@email exists:user with email ', ':email: already exists'];
    //     // Call original localize function
    //     return $localize(mp, ...expressions);
    //     // return $localize([':@@email exists:user with email ', ':email: already exists'] as any as TemplateStringsArray, expressions);
    // }

    // msg: "user with email {{email}} already exists"
    // params: { email: "abc@def.com" }
    anslate(msg: string, params?: { [key: string]: any }): string {
        // TODO replace params
        // msg: "user with email ${email} already exists"
        // params: { email: "abc@def.com" }
        const email = 'myemail';
        // const transUnitId = 'Could not create user';
        return $localize`:@@email exists:user with email ${email}:email: already exists`;

        // return $localize(<any>{ '0': `:@@${transUnitId}:${transUnitId}`, 'raw': [':'] });
        // const mp = [':@@email exists:user with email ', ':email: already exists'];
        // return $localize(mp, ['myemail']);
        // return $localize`:@@email exists:user with email ${email}:email: already exists`;
        // return this.$localizeId`${transUnitId}:TRANSUNITID:user with email ${email}:email: already exists`;
        // return !msg ? '' : this.i18n({value: msg, id: msg}, params);
    }

    anslateM(msg: string, meaning: string, params?: { [key: string]: any }): string {
        // TODO
        return 'TODO translate';
        // return !msg ? '' : this.i18n({value: msg, id: msg, meaning: meaning}, params);
    }

    anslateWithId(msg: string, id: string, params?: { [key: string]: any }): string {
        // TODO
        return 'TODO translate';
        // return !msg || !id ? '' : this.i18n({value: msg, id: id}, params);
    }

    anslateFull(value: string, id: string, meaning?: string, description?: string, params?: { [key: string]: any }): string {
        // TODO
        return 'TODO translate';
        // return !value || !id ? '' : this.i18n({value: value, id: id, meaning: meaning, description: description}, params);
    }
*/
}
