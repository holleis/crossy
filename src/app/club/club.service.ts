import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { Club } from '../tournament/club';

@Injectable()
export class ClubService {

    constructor(
        private http: HttpClient,
    ) {}

    getClubNames(nation?: string) {
        if (nation) {
            const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/clubs/names/' + encodeURI(nation);
            return this.http.get<{success: boolean, result: {_id: string, name: string}[], error: string}>(url);
        } else {
            const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/clubs/names/' + 'ALL';
            return this.http.get<{success: boolean, result: {_id: string, name: string}[], error: string}>(url);
        }
    }

    getAllClubs() {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/clubs';
        return this.http.get<{success: boolean, result: Club[], error: string}>(url);
    }

    // getMainClubName(playerId: string) : Observable<string> {
    //     const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/clubs/player/name/';
    //     return this.http.get<any>(url + encodeURI(playerId));
    // }

    getMainClubIdForPlayer(playerId: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/clubs/player/' + encodeURI(playerId);
        return this.http.get<{success: boolean, result: string, error: string}>(url);
    }

    getClub(clubName: string) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/clubs/' + encodeURI(clubName);
        return this.http.get<{success: boolean, result: Club[], error: string}>(url);
    }

    sortClubs(clubs) {
        clubs.sort((c1: Club, c2: Club) => {
            if (c1.nation && c1.nation === 'GER') {
                if (c2.nation && c2.nation === 'GER') {
                    return c1.name.toUpperCase() < c2.name.toUpperCase() ? -1 :
                        (c1.name.toUpperCase() > c2.name.toUpperCase() ? 1 : 0);
                }
                return -1;
            }
            if (c2.nation && c2.nation === 'GER') {
                return 1;
            }
            return c1.name.toUpperCase() < c2.name.toUpperCase() ? -1 :
                (c1.name.toUpperCase() > c2.name.toUpperCase() ? 1 : 0);
        });
    }
}
