import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { Player } from './player';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { of } from 'rxjs';

@Injectable()
export class PlayerService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger
    ) { }

    createPlayer(player: Player) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/';
        this.logger.trace('call POST to create player', player, 'at', url);
        return this.http.post<{
            'success': boolean,
            'result': Player,
            'error': string
        }>(url, player);
    }

    // deletePlayer(player: Player) {
    //     const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/' + player._id;
    //     this.logger.trace('call POST to create player', player, 'at', url);
    //     return this.http.delete<{
    //         'success': boolean,
    //         'result': Player,
    //         'error': string
    //     }>(url);
    // }

    updatePlayer(playerId: string, playerInfo: any) {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/player/byid/' + encodeURI(playerId);
        this.logger.trace('call PUT to update player', playerId, 'at', url);
        return this.http.put<{
            'success': boolean,
            'result': Player,
            'error': string
        }>(url, playerInfo);
    }

    getPlayer(player_id: string) {
        return this.http.get<{
            'success': boolean,
            'result': Player,
            'error': string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/byid/' + encodeURI(player_id));
    }

    getPlayers(clubId: string) {
        return this.http.get<{
            'success': boolean,
            'result': Player[],
            'error': string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/clubs/' + encodeURI(clubId));
    }

    getPlayersPerNation(nation: string) {
        return this.http.get<{
            'success': boolean,
            'result': Player[],
            'error': string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/nation/' + encodeURI(nation));
    }

    getPlayersForClubsWithPoints(clubIds: string[], catId: string, germanOnly: boolean) {
        if (!clubIds || clubIds.length === 0) {
            clubIds = ['ALL'];
        }
        return this.http.get<{
            'success': boolean,
            'result': Player[],
            'error': string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/enroll/' + encodeURI(clubIds.join('&')) + '/' + encodeURI(catId) + '/' + (germanOnly ? 1 : 0));
    }

    getAllPlayers() {
        const url = Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/';

        this.logger.debug(url);
        return this.http.get<{success: boolean, result: Player[], error: string}>(url);
    }

    getBirthday(first_name: string, last_name: string) {
        const url = Globals.SCRIPTS_BASE_URL + Globals.GET_BIRTHDAY_PHP_URL;

        this.logger.debug('asking for birthday of', first_name, last_name);
        return this.http.get<any>(url + 'first_name=' + encodeURI(first_name) + '&last_name=' + encodeURI(last_name));
    }

    getGender(player_id: string) {
        const playersGenderUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/gender/';

        this.logger.debug('asking for gender of player with player_id', encodeURI(player_id));
        return this.http.get<{success: boolean, result: {gender: number}, error: string}>(playersGenderUrl + player_id);
    }

    getPlayersForClubs(clubIds: string[]) {
        if (!clubIds || clubIds.length === 0 || !clubIds[0]) {
            clubIds = ['ALL'];
        }
        return this.http.get<{
            'success': boolean,
            'result': Player[],
            'error': string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/clubs/' + encodeURI(clubIds.join('&')));
    }

    getPlayersForGermanClubs() {
        return this.http.get<{
            'success': boolean,
            'result': Player[],
            'error': string
        }>(Globals.BASE_API_URL + Globals.SUB_API_URL + '/players/germanclubs/');
    }

    playerFits(playerGender: number, playerAge: number, catGender: number, minimum_age: number, maximum_age: number): boolean {
        this.logger.trace('playerFits', playerGender, catGender, playerAge, minimum_age, maximum_age);
        if (!this.fitsGender(playerGender, catGender)) {
            return false;
        } else {
            // player birth year is 2018 if unknown, i.e. playerAge will be currentyear minus 2018
            return playerAge < 5 || (playerAge >= minimum_age && playerAge <= maximum_age);
        }
    }

    private fitsGender(playerGender: number, catGender: number): boolean {
        // TODO need partner gender
        // catGender: open=0, female=1, male=2, mixed=3
        // playerGender: female=1, male=2 (0 if unknown)

        if (catGender === 0 || catGender === 3 || playerGender === 0 ||
            typeof playerGender === 'undefined' || typeof catGender === 'undefined'
            || (playerGender === catGender)) {
            return true;
        }
        return false;
    }

    getMyAge(date_of_birth: string | Date, tournamentDate: string): number {
        if (!date_of_birth) {
            return 0;
        }
        let bdate;
        // deprecated:
        if (typeof date_of_birth === 'string') {
            if (!date_of_birth || date_of_birth === '') {
                return 0;
            }
            if (date_of_birth.length <= '11.11.2011'.length) {
                const dty = date_of_birth.split('.')[2]; // dd.MM.yyyy
                bdate = parseInt(dty, 10);
            } else {
                const date = new Date(date_of_birth);
                bdate = date.getFullYear();
            }
        } else {
            const date = new Date(date_of_birth);
            bdate = date.getFullYear();
        }
        // don't use 'now' but the date of the tournament
        const tourdty = tournamentDate.split('.')[2];
        const now = parseInt(tourdty, 10);
        const age = now - bdate;
        // this.logger.trace(dob, dt, dty, bdate, sd, now, age);
        return age;
    }

    getNations(): Observable<{success: boolean, error: string, result: {_id, name}[]}> {
        // parsed from https://en.wikipedia.org/wiki/List_of_IOC_country_codes
        return of({success: true, error: '', result: [
            {_id: 'GER', name: 'Germany'},
            {_id: 'AFG', name: 'Afghanistan'},
            {_id: 'ALB', name: 'Albania'},
            {_id: 'ALG', name: 'Algeria'},
            {_id: 'AND', name: 'Andorra'},
            {_id: 'ANG', name: 'Angola'},
            {_id: 'ANT', name: 'Antigua and Barbuda'},
            {_id: 'ARG', name: 'Argentina'},
            {_id: 'ARM', name: 'Armenia'},
            {_id: 'ARU', name: 'Aruba'},
            {_id: 'ASA', name: 'American Samoa'},
            {_id: 'AUS', name: 'Australia'},
            {_id: 'AUT', name: 'Austria'},
            {_id: 'AZE', name: 'Azerbaijan'},
            {_id: 'BAH', name: 'Bahamas'},
            {_id: 'BAN', name: 'Bangladesh'},
            {_id: 'BAR', name: 'Barbados'},
            {_id: 'BDI', name: 'Burundi'},
            {_id: 'BEL', name: 'Belgium'},
            {_id: 'BEN', name: 'Benin'},
            {_id: 'BER', name: 'Bermuda'},
            {_id: 'BHU', name: 'Bhutan'},
            {_id: 'BIH', name: 'Bosnia and Herzegovina'},
            {_id: 'BIZ', name: 'Belize'},
            {_id: 'BLR', name: 'Belarus'},
            {_id: 'BOL', name: 'Bolivia'},
            {_id: 'BOT', name: 'Botswana'},
            {_id: 'BRA', name: 'Brazil'},
            {_id: 'BRN', name: 'Bahrain'},
            {_id: 'BRU', name: 'Brunei'},
            {_id: 'BUL', name: 'Bulgaria'},
            {_id: 'BUR', name: 'Burkina Faso'},
            {_id: 'CAF', name: 'Central African Republic'},
            {_id: 'CAM', name: 'Cambodia'},
            {_id: 'CAN', name: 'Canada'},
            {_id: 'CAY', name: 'Cayman Islands'},
            {_id: 'CGO', name: 'Congo'},
            {_id: 'CHA', name: 'Chad'},
            {_id: 'CHI', name: 'Chile'},
            {_id: 'CHN', name: 'China'},
            {_id: 'CIV', name: 'Ivory Coast'},
            {_id: 'CMR', name: 'Cameroon'},
            {_id: 'COD', name: 'Democratic Republic of the Congo'},
            {_id: 'COK', name: 'Cook Islands'},
            {_id: 'COL', name: 'Colombia'},
            {_id: 'COM', name: 'Comoros'},
            {_id: 'CPV', name: 'Cape Verde'},
            {_id: 'CRC', name: 'Costa Rica'},
            {_id: 'CRO', name: 'Croatia'},
            {_id: 'CUB', name: 'Cuba'},
            {_id: 'CYP', name: 'Cyprus'},
            {_id: 'CZE', name: 'Czech Republic'},
            {_id: 'DEN', name: 'Denmark'},
            {_id: 'DJI', name: 'Djibouti'},
            {_id: 'DMA', name: 'Dominica'},
            {_id: 'DOM', name: 'Dominican Republic'},
            {_id: 'ECU', name: 'Ecuador'},
            {_id: 'EGY', name: 'Egypt'},
            {_id: 'ERI', name: 'Eritrea'},
            {_id: 'ESA', name: 'El Salvador'},
            {_id: 'ESP', name: 'Spain'},
            {_id: 'EST', name: 'Estonia'},
            {_id: 'ETH', name: 'Ethiopia'},
            {_id: 'FIJ', name: 'Fiji'},
            {_id: 'FIN', name: 'Finland'},
            {_id: 'FRA', name: 'France'},
            {_id: 'FSM', name: 'Federated States of Micronesia'},
            {_id: 'GAB', name: 'Gabon'},
            {_id: 'GAM', name: 'The Gambia'},
            {_id: 'GBR', name: 'Great Britain'},
            {_id: 'GBS', name: 'Guinea-Bissau'},
            {_id: 'GEO', name: 'Georgia'},
            {_id: 'GEQ', name: 'Equatorial Guinea'},
            {_id: 'GHA', name: 'Ghana'},
            {_id: 'GRE', name: 'Greece'},
            {_id: 'GRN', name: 'Grenada'},
            {_id: 'GUA', name: 'Guatemala'},
            {_id: 'GUI', name: 'Guinea'},
            {_id: 'GUM', name: 'Guam'},
            {_id: 'GUY', name: 'Guyana'},
            {_id: 'HAI', name: 'Haiti'},
            {_id: 'HKG', name: 'Hong Kong'},
            {_id: 'HON', name: 'Honduras'},
            {_id: 'HUN', name: 'Hungary'},
            {_id: 'INA', name: 'Indonesia'},
            {_id: 'IND', name: 'India'},
            {_id: 'IRI', name: 'Iran'},
            {_id: 'IRL', name: 'Ireland'},
            {_id: 'IRQ', name: 'Iraq'},
            {_id: 'ISL', name: 'Iceland'},
            {_id: 'ISR', name: 'Israel'},
            {_id: 'ISV', name: 'Virgin Islands'},
            {_id: 'ITA', name: 'Italy'},
            {_id: 'IVB', name: 'British Virgin Islands'},
            {_id: 'JAM', name: 'Jamaica'},
            {_id: 'JOR', name: 'Jordan'},
            {_id: 'JPN', name: 'Japan'},
            {_id: 'KAZ', name: 'Kazakhstan'},
            {_id: 'KEN', name: 'Kenya'},
            {_id: 'KGZ', name: 'Kyrgyzstan'},
            {_id: 'KIR', name: 'Kiribati'},
            {_id: 'KOR', name: 'South Korea'},
            {_id: 'KOS', name: 'Kosovo'},
            {_id: 'KSA', name: 'Saudi Arabia'},
            {_id: 'KUW', name: 'Kuwait'},
            {_id: 'LAO', name: 'Laos'},
            {_id: 'LAT', name: 'Latvia'},
            {_id: 'LBA', name: 'Libya'},
            {_id: 'LBN', name: 'Lebanon'},
            {_id: 'LBR', name: 'Liberia'},
            {_id: 'LCA', name: 'Saint Lucia'},
            {_id: 'LES', name: 'Lesotho'},
            {_id: 'LIE', name: 'Liechtenstein'},
            {_id: 'LTU', name: 'Lithuania'},
            {_id: 'LUX', name: 'Luxembourg'},
            {_id: 'MAD', name: 'Madagascar'},
            {_id: 'MAR', name: 'Morocco'},
            {_id: 'MAS', name: 'Malaysia'},
            {_id: 'MAW', name: 'Malawi'},
            {_id: 'MDA', name: 'Moldova'},
            {_id: 'MDV', name: 'Maldives'},
            {_id: 'MEX', name: 'Mexico'},
            {_id: 'MGL', name: 'Mongolia'},
            {_id: 'MHL', name: 'Marshall Islands'},
            {_id: 'MKD', name: 'Macedonia'},
            {_id: 'MLI', name: 'Mali'},
            {_id: 'MLT', name: 'Malta'},
            {_id: 'MNE', name: 'Montenegro'},
            {_id: 'MON', name: 'Monaco'},
            {_id: 'MOZ', name: 'Mozambique'},
            {_id: 'MRI', name: 'Mauritius'},
            {_id: 'MTN', name: 'Mauritania'},
            {_id: 'MYA', name: 'Myanmar'},
            {_id: 'NAM', name: 'Namibia'},
            {_id: 'NCA', name: 'Nicaragua'},
            {_id: 'NED', name: 'Netherlands'},
            {_id: 'NEP', name: 'Nepal'},
            {_id: 'NGR', name: 'Nigeria'},
            {_id: 'NIG', name: 'Niger'},
            {_id: 'NOR', name: 'Norway'},
            {_id: 'NRU', name: 'Nauru'},
            {_id: 'NZL', name: 'New Zealand'},
            {_id: 'OMA', name: 'Oman'},
            {_id: 'PAK', name: 'Pakistan'},
            {_id: 'PAN', name: 'Panama'},
            {_id: 'PAR', name: 'Paraguay'},
            {_id: 'PER', name: 'Peru'},
            {_id: 'PHI', name: 'Philippines'},
            {_id: 'PLE', name: 'Palestine'},
            {_id: 'PLW', name: 'Palau'},
            {_id: 'PNG', name: 'Papua New Guinea'},
            {_id: 'POL', name: 'Poland'},
            {_id: 'POR', name: 'Portugal'},
            {_id: 'PRK', name: 'North Korea'},
            {_id: 'PUR', name: 'Puerto Rico'},
            {_id: 'QAT', name: 'Qatar'},
            {_id: 'ROU', name: 'Romania'},
            {_id: 'RSA', name: 'South Africa'},
            {_id: 'RUS', name: 'Russia'},
            {_id: 'RWA', name: 'Rwanda'},
            {_id: 'SAM', name: 'Samoa'},
            {_id: 'SEN', name: 'Senegal'},
            {_id: 'SEY', name: 'Seychelles'},
            {_id: 'SGP', name: 'Singapore'},
            {_id: 'SKN', name: 'Saint Kitts and Nevis'},
            {_id: 'SLE', name: 'Sierra Leone'},
            {_id: 'SLO', name: 'Slovenia'},
            {_id: 'SMR', name: 'San Marino'},
            {_id: 'SOL', name: 'Solomon Islands'},
            {_id: 'SOM', name: 'Somalia'},
            {_id: 'SRB', name: 'Serbia'},
            {_id: 'SRI', name: 'Sri Lanka'},
            {_id: 'SSD', name: 'South Sudan'},
            {_id: 'STP', name: 'São Tomé and Príncipe'},
            {_id: 'SUD', name: 'Sudan'},
            {_id: 'SUI', name: 'Switzerland'},
            {_id: 'SUR', name: 'Suriname'},
            {_id: 'SVK', name: 'Slovakia'},
            {_id: 'SWE', name: 'Sweden'},
            {_id: 'SWZ', name: 'Swaziland'},
            {_id: 'SYR', name: 'Syria'},
            {_id: 'TAN', name: 'Tanzania'},
            {_id: 'TGA', name: 'Tonga'},
            {_id: 'THA', name: 'Thailand'},
            {_id: 'TJK', name: 'Tajikistan'},
            {_id: 'TKM', name: 'Turkmenistan'},
            {_id: 'TLS', name: 'East Timor'},
            {_id: 'TOG', name: 'Togo'},
            {_id: 'TPE', name: 'Chinese Taipei[5]'},
            {_id: 'TTO', name: 'Trinidad and Tobago'},
            {_id: 'TUN', name: 'Tunisia'},
            {_id: 'TUR', name: 'Turkey'},
            {_id: 'TUV', name: 'Tuvalu'},
            {_id: 'UAE', name: 'United Arab Emirates'},
            {_id: 'UGA', name: 'Uganda'},
            {_id: 'UKR', name: 'Ukraine'},
            {_id: 'URU', name: 'Uruguay'},
            {_id: 'USA', name: 'United States'},
            {_id: 'UZB', name: 'Uzbekistan'},
            {_id: 'VAN', name: 'Vanuatu'},
            {_id: 'VEN', name: 'Venezuela'},
            {_id: 'VIE', name: 'Vietnam'},
            {_id: 'VIN', name: 'Saint Vincent and the Grenadines'},
            {_id: 'YEM', name: 'Yemen'},
            {_id: 'ZAM', name: 'Zambia'},
            {_id: 'ZIM', name: 'Zimbabwe'}
        ]});
    }

    getFederations(): Observable<{success: boolean, error: string, result: {_id, name}[]}> {
        return of({success: true, error: '', result: [
            {_id: 'DCV', name: 'Deutscher Crossminton Verband e.V.'},
            {_id: 'ICO', name: 'International Crossminton Organization'},
            {_id: 'BRA', name: 'Brazil'},
            {_id: 'BUL', name: 'Bulgarian Crossminton Association'},
            {_id: 'CAF', name: 'Fédération Centrafricaine Crossminton'},
            {_id: 'SBC', name: 'Speed Badminton Canada'},
            {_id: 'CIV', name: 'Côte d’Ivoire Crossminton'},
            {_id: 'COL', name: 'Colombian Crossminton Association'},
            {_id: 'CSBA', name: 'Croatian Speed Badminton Association'},
            {_id: 'CAC', name: 'Česká Asociace Crossmintonu'},
            {_id: 'ESP', name: 'Spain'},
            {_id: 'FSS', name: 'Fédération Sportive de Speed Badminton'},
            {_id: 'HSBA', name: 'Hungarian Speed Badminton Association'},
            {_id: 'AICO', name: 'India Crossminton Association'},
            {_id: 'JCA', name: 'Japan Crossminton Association'},
            {_id: 'KCA', name: 'Korea Crossminton Association'},
            {_id: 'LCF', name: 'Latvian Crossminton Federation'},
            {_id: 'MAS', name: 'Malaysia'},
            {_id: 'MKD', name: 'Macedonia'},
            {_id: 'MSBF', name: 'Mauritius Crossminton Federation'},
            {_id: 'NSBF', name: 'Netherlands Speed Badminton Federation'},
            {_id: 'NBF', name: 'Norwegian Badminton Association'},
            {_id: 'PFC', name: 'Polska Federacja Crossmintona'},
            {_id: 'RPO', name: 'RPO Federation Crossminton'},
            {_id: 'SLO', name: 'Speed Badminton Zveza Slovenije'},
            {_id: 'SBSS', name: 'Speed Badminton Savez Serbije'},
            {_id: 'SUI', name: 'Switzerland'},
            {_id: 'CS', name: 'Crossminton Slovakia'},
            {_id: 'SWE', name: 'Svenska Speedbadmintonförbundet'},
            {_id: 'TUR', name: 'Turkish Crossminton Association'},
            {_id: 'USA', name: 'Crossminton USA'}
        ]});
    }
}
