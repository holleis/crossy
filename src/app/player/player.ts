import { Club } from '../tournament/club';

export class Player {
    _id: string;
    last_name?: string;
    first_name?: string;
    clubs?: Club[];
    mainclubindex? = 0;
    handedness?: number; // right: 1, left: 2
    picture?: string;
    language?: string;
    newsletter_acknowledgement?: boolean;
    data_protection_accepted?: boolean;
    nation?: string[];
    date_of_birth?: Date;
    gender?: number; // female: 1, male: 2
    address?: string;
    phone?: string;
    show_my_biography?: boolean;
    show_my_picture?: boolean;
    active?: boolean;
    comments?: string;
    // Ophardt ID
    opid?: string;
    // TournamentSoftware ID
    tsid?: string;
    tsProfileId?: string;
    tsLicence?: { from: Date, to: Date };
    // TODO add
    // licenses: License[];
    dcvBeitrag?: string[];
    details?: string;

    // only for internal purposes
    partner?: Player;

    // only for enrollment / tournament execution
    points?: number;
    rank?: any;

    // TODO maybe names etc. in non-latin letters extra
}
