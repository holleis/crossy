# Language translation based on $localize

- calls to this.alertService must pass translated string, e.g.
    - `this.alertService.error($localize`Error getting list of existing clubs`, error);`
    - name the placeholders: `this.alertService.error($localize`Error getting data for ${q.questions[i].label}:label:`, error);`
- this.tr.anslateMessage(msgText); can be used to translate dynamic text such as server errors but must list all messages since $localize can only take literal strings ("isLiteralArray")

This seems to work also ok (instead of the locl below):
- `ng extract-i18n --output-path src/app/locale/ --out-file messages.xlf.source`
- `xliffmerge -p xliffmergeoptions.json`

Using locl maybe retrives more?!
- use https://github.com/loclapp/locl/tree/master/libs/cli to extract all messages (generates 8746893242317 IDs)
  - `npx locl extract -s='dist/**/*.js' -f=xlf -o='src/app/locale/messages_extracted.xlf' --locale=de`
- convert this `messages_extracted.xlf` into the `messages.xlf.source` (remove all target tags)

- use `xliffmerge -p xliffmergeoptions.json` to merge those with the existing translations
- 

