# Questions

- duplicate players, e.g. Emilia CZEPIEC
- players with same name, e.g. Petr Makrlik, Philip Müller

- Brauche ich Ü40 Mixed, Ü50 Damendoppel etc. (bzw. was mach ich Ranglistentechnisch wenn diese zB auf der German Open gespielt werden?)

- *Frage*: DM Qualirangliste: Bis jetzt wurden keine Jugendlichen gelistet. Allerdings steht in §11(5)-5 "In den Kategorien U12, U14, U16 und U18 (jeweils weiblich und männlich) alle in der RL-Q geführten SpielerInnen." (ist jetzt in der Praxis ignoriert worden).
Sicherlich sinnvoll, dass nur Leute, die mind. 1 Turnier gespielt haben mitmachen dürfen. Das ist ja bei den Erwachsenen auch so. Die Frage ist nur, ob man sich da nicht viel Diskussionen einholt (siehe Diskussion bei der DV "ich würde gerne U12 Kategorie auch mit 2 Spielern durchführen").
Workaround wäre: man listet alle DCV Uxx Leute standardmäßig mit 0 Punkten (oder jeder kann beliegig Uxx Leute mit 0 Punkten eintragen lassen). Dann wären alle "in der RL-Q geführt".
Vermutlich müsste man das dann aber auch für die Erwachsenen zulassen, was in den Ü>50 Kategorien schon einen Unterschied macht.
=> *brauche ich einen Jugend QU-R?*

- Die 50er Turnierchen zählen nicht mehr zur Rangliste (weder Quali noch DCV)?


Punkte für die Ranglisten. Laut §8 (1) gibt es keinen Unterschied zw. DCV Rangliste und Qualirangliste (außer Verfalldatum 12 Monate bzw. Ende des Jahres).
Implizit sind in der Quali nur Deutsche ($11 (1)).
Nach $7(2) zählen in der Quali nur die 5 besten Ergebnisse.

Punkte kommen aus den DCV Qualifikationsturnieren
 - Ranglistenturniere (300er)
 - German Open
 - das 500er ICO Fun Turnier
 - 4x 250er ICO Turniere
 - alle 100er ICO Turniere
Alle nach der 300er Schlüssel.

Also _nicht_: die Deutschen Meisterschaften, Mannschaftswettbewerbe (Pokal, Liga, Nations Cup), 50er Turniere


# Mean stuff
Player	Result	Points	Matches
POL Mariusz SAWICKI	*1*
Player	Result	Points	Matches
POL Beata JAGIELLO	*1st*

In results https://ico.tournamentsoftware.com/ranking/tournament.aspx?id=25666&tournament=147535 Open Doubles, only 3 Teams appear although 5 Teams really played https://ico.tournamentsoftware.com/sport/draw.aspx?id=963A8E19-1E85-4C2D-B0AB-E825E618F22A&draw=21
=> Crossy assigns 0 points because <=3 players

In results https://ico.tournamentsoftware.com/ranking/tournament.aspx?id=25666&tournament=147536 e.g. Open only 13 players appear although 16 players really played https://ico.tournamentsoftware.com/sport/draws.aspx?id=F3375D1D-61A7-4988-826A-DE5CB9DB61E6
=> Crossy assigns wrong points because of #players
REASON: players without a flag do not appear in reults. Probably not known / found by TS while importing.


# TODOs

- getMyRankings etc. ARE WRONG
  - calculate the current rankings per category
  - every time the rankings are updated, re-calculate
  - new implementation of myrankingsPlayerId, rankings, rankingCatName

- add ICO categories to DCV ranking (esp. Oxx doubles)

- use the correct ranking when making a draw (time of registration deadline)

- tackle problem that KO is already populated although not clear which player of the group is 1st/2nd

- try to better identify players with names that contain non-ascii characters
(e.g. preprocess players, concatenating first and last name and removing diacritics)
- offer the user the chance to disambiguate if several players match a name

- check printing



# DONE

- Update to Angular 11
- adjust styles (color theme)
- import categories (events) and players from TS
- import tournament result https://ico.tournamentsoftware.com/ranking/tournament.aspx?id=25666&tournament=147470
- update documentation
- use correct tabIndex in groups and KO, probably need to calculate how many per category / group (=> can now use <ENTER> to move to next input)

- automatically "register" players that have not been found during the TS import process

- show future tournaments in DCV Ranking (as in DM Quali ranking)

- visualize that one can click on "future" and "past" tournaments in /home

- write / check ranking points in "Players" list of a draw (esp. doubles)

- continuously remove ranking entries older than 1 year (cron job or when retrieved)
  - NO: just query only the last 12 month (for DCV ranking)

