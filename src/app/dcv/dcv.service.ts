import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';
import { NGXLogger } from 'ngx-logger';
import { Player } from '../player/player';

@Injectable()
export class DCVService {

    constructor(
        private http: HttpClient,
        private logger: NGXLogger
    ) { }

    getAllDCVPlayers() {
        this.logger.debug('retrieving DCV players from TP');

        const rankingUrl = Globals.BASE_API_URL + Globals.SUB_API_URL + '/dcvplayers/';
        return this.http.get<{
            'success': boolean,
            'result': Player[],
            'error': string
        }>(rankingUrl);
    }
}
