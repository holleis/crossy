import { Component, OnInit } from "@angular/core";
import { DCVService } from "./dcv.service";
import { Player } from "../player/player";
import { NGXLogger } from "ngx-logger";
import { AlertService } from "../util/alert/alert.service";
import { User } from "../auth/user";
import moment from 'moment';

@Component({
    templateUrl: './dcv.component.html',
    styleUrls: ['./dcv.component.scss'],
})

export class DCVComponent implements OnInit {

    user: User = new User();
    players: Player[] = [];

    loading = false;

    moment = moment;

    constructor(
        private logger: NGXLogger,
        private alertService: AlertService,
        private dcvService: DCVService,
    ) { }

    ngOnInit() {
        moment.locale('de');

        this.loading = true;
        this.dcvService.getAllDCVPlayers().subscribe({
            next: response => {
                this.loading = false;
                if (response.success === true) {
                    this.players = response.result;
                    // first_name, last_name, clubs, tsid, nation, gender, date_of_birth, start_date, end_date, tsProfileId

                } else {
                    this.logger.error('Could not retrieve DCV players', response.error);
                    this.alertService.warn($localize`:@@Problems retrieving DCV players. Please try again later or contact Paul.:`);
                }
            },
            error: error => {
                this.loading = false;
                this.logger.error('Could not retrieve DCV players', error);
                this.alertService.warn($localize`:@@Problems retrieving DCV players. Please try again later or contact Paul.:`);
            }
        });
    }
}
