import { FilesystemService } from './util/filesystem.service';
import 'zone.js';
import 'reflect-metadata';

// import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserModule, Title } from '@angular/platform-browser';
import { MyMaterialModule } from './material.module';
// import { I18n, MISSING_TRANSLATION_STRATEGY } from '@ngx-translate/i18n-polyfill';
import { LOCALE_ID, MissingTranslationStrategy, NgModule/*, LOCALE_ID, TRANSLATIONS, TRANSLATIONS_FORMAT, MissingTranslationStrategy*/ } from '@angular/core';
// import { registerLocaleData } from '@angular/common';
// import localeDe from '@angular/common/locales/de';
// registerLocaleData(localeDe);
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PlayerComponent } from './player/player.component';
import { HomeComponent } from './user/home.component';
import { RankingComponent } from './ranking/ranking.component';
import { CalendarComponent } from './calendar/calendar.component';
import { PageNotFoundComponent } from './util/page-not-found.component';
import { OverviewComponent } from './overview/overview.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { RegisterComponent } from './auth/register/register.component';
import { JwtInterceptor } from './auth/_helpers/jwt.interceptor';
import { ClubComponent } from './club/club.component';
import { CreationComponent } from './tournament/creation/creation.component';
import { MytournamentsComponent } from './tournament/creation/mytournaments.component';
import { BracketComponent } from './tournament/execute/bracket.component';
import { AlertComponent } from './util/alert/alert.component';
import { GroupComponent } from './tournament/execute/group.component';
import { KoComponent } from './tournament/execute/ko.component';
import { MatchComponent } from './tournament/execute/match.component';
import { KopartComponent } from './tournament/execute/kopart.component';
import { YesNoDialogComponent } from './util/yesno-dialog.component';
import { TSImportDialogComponent } from './util/ts-import-dialog.component';
import { ProfilelinkDialogComponent } from './util/profilelink-dialog.component';

import { LoggerModule, NgxLoggerLevel, NGXLogger } from 'ngx-logger';
import { AlertService } from './util/alert/alert.service';
import { UserService } from './auth/_services/user.service';
import { EnrollComponent } from './tournament/enrollment/enroll.component';
import { TournamentsService } from './tournament/tournaments.service';
import { EnrollService } from './tournament/enrollment/enroll.service';
import { SocketService } from './util/websockets/socket.service';

import { QuillModule } from 'ngx-quill';
import { ShowComponent } from './tournament/show.component';
import { ChooseComponent } from './tournament/enrollment/choose.component';
import { TournamentComponent } from './tournament/execute/tournament.component';
import { ResetpasswordComponent } from './auth/login/resetpassword.component';
import { UsersComponent } from './user/users.component';
import { MunichcupComponent } from './tournament/execute/munichcup/munichcup.component';
import { MunichcupGroupsComponent } from './tournament/execute/munichcup-groups/munichcup-groups.component';
import { Register2Component } from './auth/register/register2.component';
import { ChoosePartnerComponent } from './tournament/enrollment/choose-partner.component';
import { PickPlayerComponent } from './tournament/execute/pickplayer.component';
import { PpolicyComponent } from './util/privacy/ppolicy.component';

import { FooterComponent } from './common/footer/footer.component';
import { ImprintComponent } from './util/privacy/imprint/imprint.component';
import { ListComponent } from './ranking/quali/list.component';
import { DrawOptionsDialogComponent } from './util/drawoptions-dialog.component';
import { MatchlistComponent } from './tournament/execute/matchlist.component';
import { TSMatchlistComponent } from './tournament/execute/tsmatchlist.component';
import { EnterScoresComponent } from './tournament/play/enter-scores.component';
import { AuthGuard } from './auth/_guards/auth.guard';
import { MessageDialogComponent } from './util/message-dialog.component';
import { ListSheetComponent } from './ranking/quali/list-sheet.component';
import { EditEntryComponent } from './ranking/quali/edit-entry.component';
import { GroupsChangedService } from './tournament/execute/groups-changed.service';
import { DocumentationComponent } from './doc/documentation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatchdbService } from './tournament/execute/matchdb.service';
import { RankingService } from './ranking/ranking.service';
import { ScoreInputComponent } from './util/score-input.component';
import { DCVService } from './dcv/dcv.service';
import { DCVComponent } from './dcv/dcv.component';

import { I18nModule } from './locale/i18n.module';
import { APP_BASE_HREF, PlatformLocation } from '@angular/common';

@NgModule({
    declarations: [
        AppComponent,
        PlayerComponent,
        HomeComponent,
        RankingComponent,
        CalendarComponent,
        PageNotFoundComponent,
        OverviewComponent,
        AlertComponent,
        LoginComponent,
        LogoutComponent,
        RegisterComponent,
        ClubComponent,
        CreationComponent,
        MytournamentsComponent,
        BracketComponent,
        GroupComponent,
        KoComponent,
        MatchComponent,
        KopartComponent,
        YesNoDialogComponent,
        MessageDialogComponent,
        DrawOptionsDialogComponent,
        TSImportDialogComponent,
        EnrollComponent,
        ShowComponent,
        ProfilelinkDialogComponent,
        ChooseComponent,
        TournamentComponent,
        ResetpasswordComponent,
        UsersComponent,
        MunichcupComponent,
        MunichcupGroupsComponent,
        Register2Component,
        ChoosePartnerComponent,
        PickPlayerComponent,
        PpolicyComponent,
        FooterComponent,
        ImprintComponent,
        ListComponent,
        ListSheetComponent,
        MatchlistComponent,
        TSMatchlistComponent,
        EnterScoresComponent,
        EditEntryComponent,
        DocumentationComponent,
        ScoreInputComponent,
        DCVComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        LoggerModule.forRoot({level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.OFF}),
        // environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : [],
        MyMaterialModule,
        // EditorModule
        QuillModule.forRoot(),
        BrowserAnimationsModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        {
            provide: APP_BASE_HREF,
            useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM(),
            deps: [PlatformLocation]
        },
        // { provide: LOCALE_ID, useValue: 'en' },
        // {
        //     provide: TRANSLATIONS,
        //     useFactory: (locale: string) => {
        //         if (locale && locale.substr && locale.substr(0, 2) !== 'en') {
        //             const myLocale = locale.substr(0, 2);
        //             return require(`raw-loader!./locale/messages.${myLocale}.xlf`).default;
        //         }
        //         return '';
        //     },
        //     deps: [LOCALE_ID],
        // },
        // { provide: TRANSLATIONS_FORMAT, useValue: 'xlf' },
        // // optional, defines how error will be handled
        // { provide: MISSING_TRANSLATION_STRATEGY, useValue: MissingTranslationStrategy.Warning },
        // I18n,
        {
            provide: MissingTranslationStrategy,
            // useValue: MissingTranslationStrategy.Warning,
            useFactory: (locale: string) => {
                if (!locale || locale.substring(0, 2) === 'en') {
                    return MissingTranslationStrategy.Ignore;
                }
                return MissingTranslationStrategy.Warning;
            },
            deps: [LOCALE_ID],
        },
        [I18nModule.setLocale(), I18nModule.setLocaleId()],
        NGXLogger,
        TournamentsService,
        MatchdbService,
        RankingService,
        EnrollService,
        AlertService,
        SocketService,
        UserService,
        Title,
        AuthGuard,
        GroupsChangedService,
        FilesystemService,
        DCVService,
    ],
    bootstrap: [AppComponent],
}
)


export class AppModule { }
