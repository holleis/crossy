import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RankingComponent } from './ranking/ranking.component';
import { CalendarComponent } from './calendar/calendar.component';
import { PageNotFoundComponent } from './util/page-not-found.component';
import { OverviewComponent } from './overview/overview.component';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './user/home.component';
import { UsersComponent } from './user/users.component';
import { CreationComponent } from './tournament/creation/creation.component';
import { MytournamentsComponent } from './tournament/creation/mytournaments.component';
import { AuthGuard } from './auth/_guards/auth.guard';
import { BracketComponent } from './tournament/execute/bracket.component';
import { EnrollComponent } from './tournament/enrollment/enroll.component';
import { ShowComponent } from './tournament/show.component';
import { TournamentComponent } from './tournament/execute/tournament.component';
import { ResetpasswordComponent } from './auth/login/resetpassword.component';
import { Register2Component } from './auth/register/register2.component';
import { PpolicyComponent } from './util/privacy/ppolicy.component';
import { ImprintComponent } from './util/privacy/imprint/imprint.component';
import { ListComponent } from './ranking/quali/list.component';
import { MatchlistComponent } from './tournament/execute/matchlist.component';
import { TSMatchlistComponent } from './tournament/execute/tsmatchlist.component';
import { EnterScoresComponent } from './tournament/play/enter-scores.component';
import { ListSheetComponent } from './ranking/quali/list-sheet.component';
import { DocumentationComponent } from './doc/documentation.component';
import { DCVComponent } from './dcv/dcv.component';

const routes: Routes = [
    {
        path: 'overview',
        component: OverviewComponent,
        children: [
            { path: '**', 
                component: OverviewComponent,
                children: [
                    { path: '**', component: OverviewComponent },
                ],
            },
        ],
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'resetPassword',
        component: ResetpasswordComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'secretregister',
        component: Register2Component
    },
    {
        path: 'ranking',
        component: RankingComponent,
        children: [
            { path: '', component: RankingComponent },
            {
                path: 'female',
                component: RankingComponent,
                children: [
                    { path: 'women', component: RankingComponent },
                    { path: 'u12', component: RankingComponent },
                    { path: 'u14', component: RankingComponent },
                    { path: 'u16', component: RankingComponent },
                    { path: 'u18', component: RankingComponent },
                    { path: 'ü40', component: RankingComponent },
                    { path: 'ü50', component: RankingComponent },
                    { path: 'ü60', component: RankingComponent },
                ]
            },
            {
                path: 'open',
                component: RankingComponent,
                children: [
                    { path: 'open', component: RankingComponent },
                    { path: 'u12', component: RankingComponent },
                    { path: 'u14', component: RankingComponent },
                    { path: 'u16', component: RankingComponent },
                    { path: 'u18', component: RankingComponent },
                    { path: 'ü40', component: RankingComponent },
                    { path: 'ü50', component: RankingComponent },
                    { path: 'ü60', component: RankingComponent },
                ]
            },
            {
                path: 'doubles',
                component: RankingComponent,
                children: [
                    { path: 'open', component: RankingComponent },
                    { path: 'women', component: RankingComponent },
                    {
                        path: 'mixed',
                        component: RankingComponent,
                        children: [
                            { path: 'ü40', component: RankingComponent },
                        ]
                    }
                ]
            },
        ],
    },
    {
        path: 'calendar',
        component: CalendarComponent
    },
    {
        path: 'showTournament',
        component: ShowComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'tournament',
        component: CreationComponent, canActivate: [AuthGuard]
    },
    {
        path: 'mytournaments',
        component: MytournamentsComponent, canActivate: [AuthGuard]
    },
    {
        path: 'enroll',
        component: EnrollComponent, canActivate: [AuthGuard]
    },
    {
        path: 'ko',
        component: TournamentComponent,
        runGuardsAndResolvers: 'always'
    }, // 'paramsOrQueryParamsChange'
    {
        path: 'ko/cat',
        component: BracketComponent
    },
    {
        path: 'ko/list',
        component: MatchlistComponent
    },
    {
        path: 'ko/tslist',
        component: TSMatchlistComponent
    },
    {
        path: 'users',
        component: UsersComponent, canActivate: [AuthGuard]
    },
    {
        path: 'dcv',
        component: DCVComponent, canActivate: [AuthGuard]
    },
    {
        path: 'dmquali',
        component: ListComponent,
        runGuardsAndResolvers: 'always'
    },
    {
        path: 'dmqualiforsheet',
        component: ListSheetComponent,
        runGuardsAndResolvers: 'always'
    },
    {
        path: 'scores',
        component: EnterScoresComponent
    },
    {
        path: 'doc',
        component: DocumentationComponent
    },
    {
        path: 'imprint',
        component: ImprintComponent
    },
    {
        path: 'privacy',
        component: PpolicyComponent
    },
    {
        path: '',
        redirectTo: '/overview',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })
    ],
    providers: [AuthGuard],
    exports: [RouterModule],
    declarations: []
})

export class AppRoutingModule { }
