/* eslint-disable no-console */

// script to convert messages.xy.xlf to xy.json
// XLFtoJSON.js [lang] - call by, e.g.:
// node XLFtoJSON.js de

import xliff12ToJs from 'xliff/xliff12ToJs';
import { readFile, writeFileSync } from 'fs';
import { join, dirname } from 'path';
import { fileURLToPath } from 'url';

const LANG = process.argv[2] || 'de';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const xlfFilePath = join(__dirname, `./messages.${LANG}.xlf`);
const jsonFilePath = join(__dirname, `${LANG}.json`);

// function replaceWithEquiv(target) {
//     // <x id="INTERPOLATION" equiv-text="{{user_nickname}}"/>
//     if (/equiv-text=/.test(target)) {
//         const regex = /<x id="INTERPOLATION_?[0-9]?" equiv-text="({{.*?}})"\/>/g;
//         return target.replace(regex, '$1');
//     } else {
//         return target;
//     }
// }

function findCorrespondingSource(source, tgtid) {
    if (typeof source === 'string') {
        return undefined;
    }
    let num = 0;
    for (let s = 0; s < source.length; s++) {
        const src = source[s];
        if (typeof src === 'string') {
            continue;
        }
        if (src.Standalone?.id) {
            if (src.Standalone.id === tgtid) {
                // return `${src.Standalone['equiv-text']}`;
                return `{$INTERPOLATION${num === 0 ? '' : `_${num}`}}`;
            }
            num += 1;
        }
    }
    return undefined;
}

function unpack(elem, source) {
    if (!elem) {
        return undefined;
    }
    if (typeof elem !== 'string' && elem.length > 0) {
        let str = '';
        for (let i = 0; i < elem.length; i++) {
            const el = elem[i];
            if (typeof el === 'string') {
                str += el;
            } else if (el.Standalone && (typeof el.Standalone.id === 'string') && el.Standalone.id.slice(0, 13) === 'INTERPOLATION') {
                if (el.Standalone['equiv-text']) {
                    // str += `<x id="${el.Standalone.id}" equiv-text="${el.Standalone['equiv-text']}"/>`;
                    str += `${el.Standalone['equiv-text']}`;
                } else {
                    const sstr = findCorrespondingSource(source, el.Standalone.id);
                    if (sstr) {
                        str += sstr;
                    } else {
                        str += '???';
                    }
                }
            }
        }
        return str;
    }
    return elem.toString();
}

// read xlf file and find all updated translations
readFile(xlfFilePath, { encoding: 'utf-8' }, (fserr, xlf) => {
    if (!fserr) {
        console.log('xlf file read');
        const allJsonTranslations = {};
        xliff12ToJs(xlf, (xlifferr, res) => {
            if (xlifferr) {
                console.log(`ERROR parsing XLIFF file: ${xlifferr}`);
                client.close();
            } else if (res) {
                console.log('xlf file parsed');
                try {
                    let changed = 0;
                    Object.entries(res.resources).forEach(([_file, fileDoc]) => {
                        Object.entries(fileDoc).forEach(([id, unit]) => {
                            let myid = id;
                            const target = unpack(unit.target, unit.source);
                            console.log('need to add ' + id + ' -> ' + target);
                            allJsonTranslations[myid] = target;
                            changed += 1;
                        });
                    });
                    if (changed) {
                        console.log(changed + ' entries changed');
                        writeFileSync(jsonFilePath, JSON.stringify(allJsonTranslations, null, '\t'));
                        console.log('json file saved');
                    } else {
                        console.log('no changes made');
                    }
                } catch (tryerr) {
                    console.log(tryerr);
                }
            }
        });
    } else {
        console.log(fserr);
    }
});
