from subprocess import call
import re
import shutil

LABEL_SEPARATOR = '#'

# ATOM regexp
# first add state="translated" to all, then remove those where source and target are the same
# <source>(.*)<\/source>\n(\s*)<target state="translated">\1<\/target>
# <source>$1</source>\n$2<target>$1</target>


languages = ['de']

dirpart = './'

outfilepart = 'messages.en.xlf'
outfile = dirpart + outfilepart

filepart = 'messages.xlf.source'
file = dirpart + filepart

knownElements = set()

def replaceXMLchars(strn):
    return strn.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;')


for lang in languages:
    try:
        filetobackup = outfile.replace('.en.', '.' + lang + '.')
        shutil.copyfile(filetobackup, filetobackup + '.bak')
        print('backed up ' + filetobackup + ' to ' + filetobackup + '.bak')
    except Exception as e:
        print('could not backup ' + filetobackup + ' to ' + filetobackup + '.bak:', str(e))

print('compiling app with ng xi18n, generating ' + file)
call(['ng', 'xi18n', '--outputPath', 'app/locale/', '--outFile', filepart, '--i18nFormat=xlf'], cwd='../../')

print('processing file ' + file)

with open(file, 'r') as source:
    with open(file + '.tmp', 'a') as outs:
        # copy everything up to the end tags
        for line in source:
            if (line.find('</body>') >= 0):
                # stop at end tags, will be added below
                break

            outs.write(line)


        additonalFile = 'additionalTranslationStrings.txt'
        print('adding fixed properties from ' + additonalFile + ' to ' + file)
        # add an entry for each insert
        with open(additonalFile, 'r') as ins:
            for line in ins:
                if (line[0] == '#' or len(line.strip()) == 0):
                    # ignore commented or empty lines
                    continue
                # id/label # description
                # Roast # transaction title
                strsplit = line.split(' # ')
                label = strsplit[0].strip()
                label = replaceXMLchars(label)
                desc = ''
                if (len(strsplit) > 1):
                    desc = strsplit[1].strip()

                if (label not in knownElements):
                    print('    ' + desc + '@' + label)

                    # 0 of {{length}}
                    # {{startIndex}} - {{endIndex}} of {{length}}
                    if (label.find('{{') >= 0):
                        slabel = label
                        outs.write('      <trans-unit id="' + label + '" datatype="html">\n')
                        outs.write('        <source>')
                        num = 0
                        while slabel.find('{{') >= 0:
                            matchObj = re.search(r'(.*?){{([^}]+)}}(.*)', slabel)
                            if (matchObj):
                                outs.write(matchObj.group(1))
                                if (num == 0):
                                    outs.write('<x id="INTERPOLATION" equiv-text="{{' + matchObj.group(2) + '}}"/>')
                                else:
                                    outs.write('<x id="INTERPOLATION_' + str(num) + '" equiv-text="{{' + matchObj.group(2) + '}}"/>')
                                num += 1
                                slabel = matchObj.group(3)
                            else:
                                slabel = ''
                        if (slabel):
                            if (num == 0):
                                outs.write('          ')
                            outs.write(slabel)
                        outs.write('</source>\n')
                        if (desc != ''):
                            desc = replaceXMLchars(desc)
                            outs.write('        <note priority="1" from="description">' + desc + '</note>\n')
                        outs.write('      </trans-unit>\n')
                        knownElements.add(label)
                    else:
                        outs.write('      <trans-unit id="' + label + '" datatype="html">\n')
                        outs.write('        <source>' + label + '</source>\n')
                        if (desc != ''):
                            desc = replaceXMLchars(desc)
                            outs.write('        <note priority="1" from="description">' + desc + '</note>\n')
                        outs.write('      </trans-unit>\n')
                        knownElements.add(label)

        outs.write('    </body>\n')
        outs.write('  </file>\n')
        outs.write('</xliff>\n')

shutil.move(file + '.tmp', file)

print('now executing xliffmerge to merge new words with existing translation')
call(['xliffmerge', '-v', '-p', 'xliffmergeoptions.json', 'de'])
# call(['ng run crossy:xliffmerge'], shell=True)

print('finished')
