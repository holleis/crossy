import { version } from './version';

export const environment = {
    production: true,
    latestWebVersion: version.latestWebVersion,
    buildNr: version.buildNr,
    RELOADTHROTTLE: 2000,
};
