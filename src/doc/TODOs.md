# Fragen

- Für die DCV Rangliste und DM Quali zählen die selben Turniere und nehmen denselben Punkteschlüssel? (§8 Spielordnung)?
  - D.h. die einzigen Unterschiede sind, bei Quali sind nur Deutsche und es zählen nur die besten 5 Ergebnisse?
  - Wahnwitzige Idee: zB German Open, bester Deutscher bekommt 300 etc. (Mindestanzahl Deutscher?, 100er ICO noch stärker, ...)

- Welche DCV Ranglisten gibt es? Wenn zB German Open mit O40 Mixed spielt, brauchen wir die auch?

- Wie kann ich die Spieler DB bei mir aktuell halten?

## TS

- Bartosz Intek: https://ico.tournamentsoftware.com/sport/player.aspx?id=963A8E19-1E85-4C2D-B0AB-E825E618F22A&player=46, hat keine Nationalität, taucht nicht in der Suche als Spieler oder in der Rangliste auf
- 

# IN PROGRESS

- delete all entries from non-DM Quali Rankings; implement that adding points to the DCV ranking also saves the tournament (as for DM Quali) to be able to show it in the ranking table

- import results of a TS tournament and calculate ranking / DM Quali ranking points



# TODOs

- enrollment: assignment of double players doesn't work

- make tabindex work consistently for brackets

- show current ranking points in "Players" tab

- check whether this works: "which player should get 0 points (not -100)"

- from tslint to eslint

- in mat-select dialog make the buttons (Select All etc.) sticky

- allow to delete the Quill text and then save

- disable "AKTUELLE Auslosung" in Organising if no draw exists

- update documentation

- import TS calendar (?)

- reset rankings

- nicer (home) "Meine letzten Turniere"

- show only a few recent tournaments in the organise section

- empty info after : in tournament bracket "Spieler" tab

- align explanation in "Aktuelle Gruppenrangliste" for better readability

- allow user to disambiguate if enrolling player name is not unique


# DONE

- enrollment: automatically create player if not found

- error ifenrolling player name is not unique

- show tournaments in DCV ranking (similar to DM Quali)

