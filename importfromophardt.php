<?php
/*
 * Retrieves (HTML) enrollment data from Ophardt and extracts the player
 * names for each category
 * To be called from Crossy
 */

$SHOWDEBUG = false;

#include <curl/curl.h>
$ch = curl_init();

ignore_user_abort(true);
set_time_limit(5000);

$ctx = stream_context_create(array('http'=>
    array(
        'timeout' => 120,  //120 Seconds is 2 Minutes
    )
));

// ICO Ophardt information
$category["ico/sen/o/c/i"] = "ICO Open Division";
$category["ico/sen/f/c/i"] = "ICO Female";
$category["ico/sen/f/c/d"] = "ICO Female Doubles";
$category["ico/sen/x/c/d"] = "ICO Mixed Doubles";
$category["ico/sen/o/c/d"] = "ICO Open Doubles";
$category["ico/o40/f/c/d"] = "ICO O40 Female Doubles";
$category["ico/o40/x/c/d"] = "ICO O40 Mixed Doubles";
$category["ico/o40/o/c/d"] = "ICO O40 Open Doubles";
$category["ico/o50/f/c/d"] = "ICO O50 Female Doubles";
$category["ico/o50/x/c/d"] = "ICO O50 Mixed Doubles";
$category["ico/o50/o/c/d"] = "ICO O50 Open Doubles";
$category["ico/o60/f/c/d"] = "ICO O60 Female Doubles";
$category["ico/o60/x/c/d"] = "ICO O60 Mixed Doubles";
$category["ico/o60/o/c/d"] = "ICO O60 Open Doubles";
$category["ico/o70/f/c/d"] = "ICO O70 Female Doubles";
$category["ico/o70/x/c/d"] = "ICO O70 Mixed Doubles";
$category["ico/o70/o/c/d"] = "ICO O70 Open Doubles";
$category["ico/u18/f/c/d"] = "ICO U18 Female Doubles";
$category["ico/u18/m/c/d"] = "ICO U18 Male Doubles";
$category["ico/u18/m/c/i"] = "ICO U18 Male";
$category["ico/u18/f/c/i"] = "ICO U18 Female";
$category["ico/u14/m/c/i"] = "ICO U14 Male";
$category["ico/u14/f/c/i"] = "ICO U14 Female";
$category["ico/u12/f/c/i"] = "ICO U12 Female";
$category["ico/u12/m/c/i"] = "ICO U12 Male";
$category["ico/o50/m/c/i"] = "ICO O50 Male";
$category["ico/o40/m/c/i"] = "ICO O40 Male";
$category["ico/o50/f/c/i"] = "ICO O50 Female";
$category["ico/o40/f/c/i"] = "ICO O40 Female";
$category["ico/o35/f/c/i"] = "ICO O35 Female";


// DCV Ophardt information

$category["ger/sen/o/c/i"] = "DCV Open";
$category["ger/sen/f/c/i"] = "DCV Damen";
$category["ger/u18/m/c/i"] = "DCV U18 Männlich";
$category["ger/u18/f/c/i"] = "DCV U18 Weiblich";
$category["ger/u16/m/c/i"] = "DCV U16 Männlich";
$category["ger/u16/f/c/i"] = "DCV U16 Weiblich";
$category["ger/u14/m/c/i"] = "DCV U14 Männlich";
$category["ger/u14/f/c/i"] = "DCV U14 Weiblich";
$category["ger/u12/m/c/i"] = "DCV U12 Männlich";
$category["ger/u12/f/c/i"] = "DCV U12 Weiblich";
$category["ger/o35/f/c/i"] = "DCV Ü35 Damen";
$category["ger/o40/m/c/i"] = "DCV Ü40 Herren";
$category["ger/o50/m/c/i"] = "DCV Ü50 Herren";
$category["ger/o40/f/c/i"] = "DCV Ü40 Damen";
$category["ger/o50/f/c/i"] = "DCV Ü50 Damen";
$category["ger/sen/o/c/d"] = "DCV Open Doppel";
$category["ger/sen/x/c/d"] = "DCV Mix Doppel";
$category["ger/sen/f/c/d"] = "DCV Damen Doppel";
$category["ger/u18/f/c/d"] = "DCV U18 Doppel Weiblich";
$category["ger/u18/m/c/d"] = "DCV U18 Doppel Männlich";

$errors = array();

$url = $_GET["url"];
$federation = $_GET["federation"];
if ($SHOWDEBUG) {
	print('looking at ' . $url . '\n');
}

function getEnrollment($url) {
	global $ch, $SHOWDEBUG;

	curl_setopt($ch, CURLOPT_POST, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_NOBODY, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
	curl_setopt($ch, CURLOPT_USERAGENT,
		"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
	curl_setopt($ch, CURLOPT_REFERER, 'https://crossminton.ophardt.online/en/calendar');

	curl_setopt($ch, CURLOPT_URL, $url);

	$answer = utf8_decode(curl_exec($ch));
	if (curl_error($ch)) {
		echo "error: ";
		echo curl_error($ch);
		return null;
	}
	//	print_r(curl_getinfo($ch));
	//echo "<html><head></head><body>";
	return $answer;
	//echo "</body>";
}

function translateCat($cat, $federation) {
    if ($federation == 'DCV') {
        switch($cat) {
    		case 'Crossminton female Adults Individual':
    			return 'DCV Damen';
    		case 'Crossminton male U14 Individual':
    		case 'Crossminton open U14 Individual':
    			return 'DCV U14 Männlich';
            case 'Crossminton male U12 Individual':
    		case 'Crossminton open U12 Individual':
    			return 'DCV U12 Männlich';
            case 'Crossminton male U16 Individual':
    		case 'Crossminton open U16 Individual':
    			return 'DCV U16 Männlich';
    		case 'Crossminton male U18 Individual':
            case 'Crossminton open U18 Individual':
    			return 'DCV U18 Männlich';
            case 'Crossminton female U12 Individual':
    			return 'DCV U12 Weiblich';
    		case 'Crossminton female U14 Individual':
    			return 'DCV U14 Weiblich';
            case 'Crossminton female U16 Individual':
    			return 'DCV U16 Weiblich';
    		case 'Crossminton female U18 Individual':
    			return 'DCV U18 Weiblich';
    		case 'Crossminton male O40 Individual':
    			return 'DCV Ü40 Herren';
    		case 'Crossminton male O50 Individual':
    			return 'DCV Ü50 Herren';
    		case 'Crossminton female O40 Individual':
    			return 'DCV Ü40 Damen';
    		case 'Crossminton female O50 Individual':
    			return 'DCV Ü50 Damen';
            case 'Crossminton open any Double':
    		case 'Crossminton open Adults Double':
    			return 'DCV Open Doppel';
    		case 'Crossminton open Adults Individual':
    			return 'DCV Open';
            case 'Crossminton mixed any Double':
    		case 'Crossminton mixed Adults Double':
    			return 'DCV Mix Doppel';
    		case 'Crossminton female Adults Double':
    			return 'DCV Damen Doppel';
			case 'Crossminton male O40 Double':
				return 'DCV Ü40 Open Doppel';
    		default:
    			return $cat;
    		// Crossminton female U18 Double
    		// Crossminton male U18 Double
    	}
    }

    // ICO
    switch($cat) {
		case 'Crossminton female Adults Individual':
			return 'ICO Women';
        case 'Crossminton male U12 Individual':
		case 'Crossminton open U12 Individual':
			return 'ICO U12 Male';
		case 'Crossminton male U14 Individual':
		case 'Crossminton open U14 Individual':
			return 'ICO U14 Male';
        case 'Crossminton male U16 Individual':
		case 'Crossminton open U16 Individual':
			return 'ICO U16 Male';
        case 'Crossminton open U18 Individual':
		case 'Crossminton male U18 Individual':
			return 'ICO U18 Male';
        case 'Crossminton female U12 Individual':
			return 'ICO U12 Female';
		case 'Crossminton female U14 Individual':
			return 'ICO U14 Female';
        case 'Crossminton female U16 Individual':
			return 'ICO U16 Female';
		case 'Crossminton female U18 Individual':
			return 'ICO U18 Female';
		case 'Crossminton male O40 Individual':
			return 'ICO O40 Men';
		case 'Crossminton male O50 Individual':
			return 'ICO O50 Men';
		case 'Crossminton female O40 Individual':
			return 'ICO O40 Women';
		case 'Crossminton female O50 Individual':
			return 'ICO O50 Women';
		case 'Crossminton open Adults Individual':
			return 'ICO Open';
		case 'Crossminton open Adults Double':
			return 'ICO Open Doubles';
		case 'Crossminton mixed Adults Double':
			return 'ICO Mixed Doubles';
		case 'Crossminton female Adults Double':
			return 'ICO Female Doubles';
		case 'Crossminton male O40 Double':
		case 'Crossminton open O40 Double':
			return 'ICO Male O40 Doubles';
		case 'Crossminton mixed O40 Double':
			return 'ICO O40 Mixed Doubles';
		case 'Crossminton female O40 Double':
			return 'ICO O40 Female Doubles';
		case 'Crossminton male O50 Double':
		case 'Crossminton open O50 Double':
			return 'ICO Male O50 Doubles';
		case 'Crossminton mixed O50 Double':
			return 'ICO O50 Mixed Doubles';
		case 'Crossminton female O50 Double':
			return 'ICO O50 Female Doubles';
		case 'Crossminton male O60 Double':
		case 'Crossminton open O60 Double':
			return 'ICO Male O60 Doubles';
		case 'Crossminton mixed O60 Double':
			return 'ICO O60 Mixed Doubles';
		case 'Crossminton female O60 Double':
			return 'ICO O70 Female Doubles';
		case 'Crossminton male O70 Double':
		case 'Crossminton open O70 Double':
			return 'ICO Male O70 Doubles';
		case 'Crossminton mixed O70 Double':
			return 'ICO O70 Mixed Doubles';
		case 'Crossminton female O70 Double':
			return 'ICO O70 Female Doubles';
		default:
			return $cat;
		// Crossminton female U18 Double
		// Crossminton male U18 Double
	}
}

header('Content-Type: application/json; charset=utf-8');
//print '<meta http-equiv="Content-Type" content="application/json; charset=utf-8"/>';

// get data from Ophardt
$html = getEnrollment($url);
if ($html == null) {
	echo "error retrieving data";
	curl_close($ch);
	return;
}

$result = array();

// parse data from HTML
if ($html) {
	$dom = new DOMDocument;

	// don't display warnings for malformed HTML
	$previous_value = libxml_use_internal_errors(TRUE);
	$dom->loadHTML($html);
	libxml_clear_errors();
	libxml_use_internal_errors($previous_value);
	$xpath = new DOMXpath($dom);

	$headers = $xpath->query("/html/body/div/h2");
	if ($SHOWDEBUG) print('found ' . $headers->length . 'categories<br/>');

	$tables = $xpath->query("/html/body/div/div/table");
	if ($SHOWDEBUG) print('found ' . $tables->length . 'tables<br/>');
	for ($tb = 0; $tb < $tables->length; $tb++) {
		if ($SHOWDEBUG) print('table nr ' . $tb . '<br/>');
		$catName = trim($headers->item($tb)->textContent);
		$catName = translateCat($catName, $federation);
		if ($SHOWDEBUG) print($catName . '<br/>:');
		$players = array();
		$table = $tables->item($tb);
		$rows = $xpath->query("tr/td[not(@style='width: 30%; text-decoration: line-through;')][1]", $table);
		if ($SHOWDEBUG) print('found ' . $rows->length . 'rows<br/>');
		for ($r = 0; $r < $rows->length; $r++) {
			$playerNames = trim($rows->item($r)->textContent);
			$playerNames = preg_replace ('#\n\s*\n\s*#', '#', $playerNames);
			$players[] = $playerNames;
			if ($SHOWDEBUG) print($rows->item($r)->textContent . '<br/>');
		}
        $result[] = array('category' => $catName, "players" => $players);
	}
}

// print result
print(json_encode($result));

// print collected errors if any
if (count($errors) > 0) {
	foreach ($errors as $error) {
		print "$error <br/>";
	}
	// print $totalcnt;
} else {
//	print "OK ($changedCnt/$lookedAtCnt)";
}

curl_close($ch);
