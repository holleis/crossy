<?php
require('./fpdf/fpdf.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Headers: Content-Type");
header("Content-Type: application/json; charset=utf-8'");
// Disable caching
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies

$tiny = false;
if (array_key_exists("tiny", $_GET)) {
    $tiny = $_GET["tiny"]; // 'true';
    $tiny = $tiny == 'true';
}
$tId = $_GET["tId"]; // '5acb697f079dc321a0435a9e';
$federation = $_GET["fed"]; // 'ICO';
$catId = $_GET["catId"]; // '5a82e8abf7b719719df7d8d0';

$matchesUrl = "https://crossy.paul-holleis.de/api/v1/tournaments/stateall/$tId/$catId";

// {"success":true,"result":{"ko":{"matches":[[{"matchNr":{"level":"0","num":0,"ko_type":"A"},"player1":{"koIdx":{"level":1,"num":0,"ko_type":"A"},"bye":false},"player2":{"koIdx":{"level":1,"num":1,"ko_type":"A"},"bye":false},"winner":{"bye":false},"_id":"5af5552916af0e282d289bbb","score":[{"_id":"5af5552916af0e282d289bbe","p1":0,"p2":0},{"_id":"5af5552916af0e282d289bbd","p1":0,"p2":0},{"_id":"5af5552916af0e282d289bbc","p1":0,"p2":0}],"tournament":"5acb697f079dc321a0435a9e","category":"5a82e8abf7b719719df7d8d0","updated":"2018-05-11T08:32:41.457Z","__v":0}],[...],...]},
//"groups":[{"matches":[{"matchNr":{"level":"A","num":0,"ko_type":"G"},"player1":{"bye":false,"playerListIdx":0},"player2":{"bye":false,"playerListIdx":12},"winner":{"bye":false},"_id":"5af5552916af0e282d289afa","score":[{"_id":"5af5552916af0e282d289afd","p1":16,"p2":1},{"_id":"5af5552916af0e282d289afc","p1":16,"p2":2},{"_id":"5af5552916af0e282d289afb","p1":0,"p2":0}],"tournament":"5acb697f079dc321a0435a9e","category":"5a82e8abf7b719719df7d8d0","updated":"2018-05-11T08:32:41.116Z","__v":0},{"matchNr":{"level":"A","num":1,"ko_type":"G"},"player1":{"bye":false,"playerListIdx":9},"player2":{"bye":false,"playerListIdx":12},"winner":{"bye":false},"_id":"5af5552916af0e282d289b02","score":[{"_id":"5af5552916af0e282d289b05","p1":16,"p2":3},{"_id":"5af5552916af0e282d289b04","p1":16,"p2":4},{"_id":"5af5552916af0e282d289b03","p1":0,"p2":0}],"tournament":"5acb697f079dc321a0435a9e","category":"5a82e8abf7b719719df7d8d0","updated":"2018-05-11T08:32:41.207Z","__v":0},{"matchNr":{"level":"A","num":2,"ko_type":"G"},"player1":{"bye":false,"playerListIdx":0},"player2":{"bye":false,"playerListIdx":9},"winner":{"bye":false},"_id":"5af5552916af0e282d289b0a","score":[{"_id":"5af5552916af0e282d289b0d","p1":16,"p2":5},{"_id":"5af5552916af0e282d289b0c","p1":16,"p2":6},{"_id":"5af5552916af0e282d289b0b","p1":0,"p2":0}],"tournament":"5acb697f079dc321a0435a9e","category":"5a82e8abf7b719719df7d8d0","updated":"2018-05-11T08:32:41.224Z","__v":0}],"players":[{"_id":"5af5552916af0e282d289c7e","bye":false,"calcPoints":-1e-8,"playerListIdx":0},{"_id":"5af5552916af0e282d289c7d","bye":false,"calcPoints":-1e-7,"playerListIdx":9},{"_id":"5af5552916af0e282d289c7c","bye":false,"calcPoints":-1.3e-7,"playerListIdx":12}],"winners":[{"_id":"5af5552916af0e282d289c81","bye":false,"calcPoints":0,"playerListIdx":0},{"_id":"5af5552916af0e282d289c80","bye":false,"calcPoints":0,"playerListIdx":9},{"_id":"5af5552916af0e282d289c7f","bye":false,"calcPoints":0,"playerListIdx":12}],"_id":"5af5552916af0e282d289c7b","name":"A","groupSize":3}, ...

$json = file_get_contents($matchesUrl);
$json = mb_convert_encoding($json, 'HTML-ENTITIES', "UTF-8");
$response = json_decode($json);

if ($tiny) {
    $pdf = new FPDF('L','mm','A4');
} else {
    $pdf = new FPDF('P','mm','A4');
}
$pdf->SetFont('Arial','B',16);

if ($response->{'success'} != true) {
	$pdf->AddPage();
	$pdf->SetXY(6, 59);
	$pdf->Cell(100,50, $response->{'error'}, 0, 'C');
	print('Error retrieving matches: ' . $response->{'error'});
	return;
}

$catName = html_entity_decode($response->{'result'}->{'category'}->{'name'});
$pdf->SetTitle(utf8_decode($catName) . ' - Scoresheets');

$totalCnt = 0;
$haveBko = false;


function convertLevel($lev) {
	switch ($lev) {
		case 1:
			return 'Final';
		case 2:
			return 'Semi-Final';
		case 3:
			return 'Quarter-Final';
		case 4:
			return 'Round of 16';
		case 5:
			return 'Round of 32';
		case 6:
			return 'Round of 64';
		default:
			return 'X';
	}
}

function printKoMatches($matches, $prefix, $fillInNames) {
	global $totalCnt, $pdf, $federation, $catName, $response, $tiny;
	$lev = 0;
	foreach ($matches as $level) {
		$lev++;
		$cnt = 1;
		foreach ($level as $match) {
			$p1Name = ' ';
			$p2Name = ' ';
			$p1Name2 = ' ';
			$p2Name2 = ' ';

			$p1 = $match->{'player1'};
			if (isset($p1->{'bye'}) && $p1->{'bye'} == true) {
				// ignore games with a (or more BYEs)
				continue;
			} else if ($fillInNames && isset($p1->{'playerListIdx'})) {
				$player1 = $response->{'result'}->{'players'}[$p1->{'playerListIdx'}];
				$p1Name = $player1->{'last_name'};
				$p1Name .= ' ' . $player1->{'first_name'};
				if (isset($player1->{'partner'})) {
					$p1Name2 = $player1->{'partner'}->{'last_name'};
					$p1Name2 .= ' ' . $player1->{'partner'}->{'first_name'};
				}
			} else if ($fillInNames && isset($p1->{'groupIdx'})) {
				$groupNr = $p1->{'groupIdx'}->{'groupNr'};
				$winnerNr = $p1->{'groupIdx'}->{'winnerNr'};
				$player1Idx = $response->{'result'}->{'groups'}[$groupNr]->{'winners'}[$winnerNr]->{'playerListIdx'};
				$player1 = $response->{'result'}->{'players'}[$player1Idx];
				$p1Name = $player1->{'last_name'};
				$p1Name .= ' ' . $player1->{'first_name'};
				if (isset($player1->{'partner'})) {
					$p1Name2 = $player1->{'partner'}->{'last_name'};
					$p1Name2 .= ' ' . $player1->{'partner'}->{'first_name'};
				}
			}
			$p2 = $match->{'player2'};
			if (isset($p2->{'bye'}) && $p2->{'bye'} == true) {
				// ignore games with a (or more BYEs)
				continue;
			} else if ($fillInNames && isset($p2->{'playerListIdx'})) {
				$player2 = $response->{'result'}->{'players'}[$p2->{'playerListIdx'}];
				$p2Name = $player2->{'last_name'};
				$p2Name .= ' ' . $player2->{'first_name'};
				if (isset($player2->{'partner'})) {
					$p2Name2 = $player2->{'partner'}->{'last_name'};
					$p2Name2 .= ' ' . $player2->{'partner'}->{'first_name'};
				}
			} else if ($fillInNames && isset($p2->{'groupIdx'})) {
				$groupNr = $p2->{'groupIdx'}->{'groupNr'};
				$winnerNr = $p2->{'groupIdx'}->{'winnerNr'};
				$player2Idx = $response->{'result'}->{'groups'}[$groupNr]->{'winners'}[$winnerNr]->{'playerListIdx'};
				$player2 = $response->{'result'}->{'players'}[$player2Idx];
				$p2Name = $player2->{'last_name'};
				$p2Name .= ' ' . $player2->{'first_name'};
				if (isset($player2->{'partner'})) {
					$p2Name2 = $player2->{'partner'}->{'last_name'};
					$p2Name2 .= ' ' . $player2->{'partner'}->{'first_name'};
				}
			}
			$pdf->AddPage();
	        	if ($tiny) {
        	        	fillInKOTiny($p1Name, $p1Name2, $p2Name, $p2Name2, $lev, $catName, $prefix);
            		} else {
	                	fillInKO($p1Name, $p1Name2, $p2Name, $p2Name2, $lev, $catName, $prefix);
        	    	}
			$totalCnt++;
		}
	}
}

function fillIn($p1Name, $p1Name2, $p2Name, $p2Name2, $groupName, $catName) {
    global $pdf, $federation, $cnt;
    $pdf->SetFont('Arial','B',16);
    $pdf->Image($federation . '_ss.png',4,6,200);
    if ($p1Name != 'X') {
        $pdf->SetXY(6, 59);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name)), 0, 0, 'C');
    }
    if ($p1Name2 != 'X') {
        $pdf->SetXY(6, 65);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name2)), 0, 0, 'C');
    }
    if ($p2Name != 'X') {
        $pdf->SetXY(110, 59);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name)), 0, 0, 'C');
    }
    if ($p2Name2 != 'X') {
        $pdf->SetXY(110, 65);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name2)), 0, 0, 'C');
    }
    $pdf->SetXY(125, 31.5);
    $pdf->Cell(40,20, utf8_decode(html_entity_decode($groupName)) . ' (' . $cnt++ . ')');
    $pdf->SetXY(125, 24);
    $pdf->Cell(140,20, utf8_decode(html_entity_decode($catName)));
}

function fillInTiny($p1Name, $p1Name2, $p2Name, $p2Name2, $groupName, $catName) {
    global $pdf, $federation, $cnt;
    $pdf->SetFont('Arial','B',16);
    $pdf->Image('Scoresheet_tiny.png',10,10,277);
    if ($p1Name != 'X') {
        $pdf->SetXY(20, 20);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name)), 0, 0, 'C');
    }
    if ($p1Name2 != 'X') {
        $pdf->SetXY(20, 26);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name2)), 0, 0, 'C');
    }
    if ($p2Name != 'X') {
        $pdf->SetXY(70, 20);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name)), 0, 0, 'C');
    }
    if ($p2Name2 != 'X') {
        $pdf->SetXY(70, 26);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name2)), 0, 0, 'C');
    }
    $pdf->SetXY(180, 20);
    $pdf->Cell(40,20, utf8_decode(html_entity_decode($groupName)) . ' (' . $cnt++ . ')');
    $pdf->SetXY(190, 20);
    $pdf->Cell(140,20, utf8_decode(html_entity_decode($catName)));
}

function fillInKO($p1Name, $p1Name2, $p2Name, $p2Name2, $lev, $catName, $prefix) {
    global $pdf, $federation, $cnt;
    $pdf->Image($federation . '_ss.png',4,6,200);
//$pdf->SetXY(6, 59);
//$pdf->Cell(1,1, 'idx: ' . json_encode($p1), 0, 0, 'L');
    if ($p1Name != 'X') {
        $pdf->SetXY(6, 59);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name)), 0, 0, 'C');
    }
    if ($p1Name2 != 'X') {
        $pdf->SetXY(6, 65);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name2)), 0, 0, 'C');
    }
    if ($p2Name != 'X') {
        $pdf->SetXY(110, 59);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name)), 0, 0, 'C');
    }
    if ($p2Name2 != 'X') {
        $pdf->SetXY(110, 65);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name2)), 0, 0, 'C');
    }

    $levName = convertLevel($lev);
    $pdf->SetXY(125, 31.5);
    $pdf->Cell(40,20, $prefix . $levName . (($lev > 1) ? (' / ' . $cnt++) : ''));

    $pdf->SetXY(125, 24);
    $pdf->Cell(140,20, $catName);
}

// TODO
function fillInKOTiny($p1Name, $p1Name2, $p2Name, $p2Name2, $lev, $catName, $prefix) {
    global $pdf, $federation, $cnt;
    $pdf->Image($federation . '_ss.png',4,6,200);
//$pdf->SetXY(6, 59);
//$pdf->Cell(1,1, 'idx: ' . json_encode($p1), 0, 0, 'L');
    if ($p1Name != 'X') {
        $pdf->SetXY(6, 59);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name)), 0, 0, 'C');
    }
    if ($p1Name2 != 'X') {
        $pdf->SetXY(6, 65);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p1Name2)), 0, 0, 'C');
    }
    if ($p2Name != 'X') {
        $pdf->SetXY(110, 59);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name)), 0, 0, 'C');
    }
    if ($p2Name2 != 'X') {
        $pdf->SetXY(110, 65);
        $pdf->Cell(100,20, utf8_decode(html_entity_decode($p2Name2)), 0, 0, 'C');
    }

    $levName = convertLevel($lev);
    $pdf->SetXY(125, 31.5);
    $pdf->Cell(40,20, $prefix . $levName . (($lev > 1) ? (' / ' . $cnt++) : ''));

    $pdf->SetXY(125, 24);
    $pdf->Cell(140,20, $catName);
}



$groups = $response->{'result'}->{'groups'};

/*
$pdf->AddPage();
$pdf->SetXY(6, 59);
$pdf->Cell(100,20, 'number of groups: ' . sizeof($groups), 0, 0, 'L');
$pdf->Write(5, $json);
*/

$haveGroupStage = false;
foreach ($groups as $group) {
	$groupName = $group->{'name'};
	$matches = $group->{'matches'};
	$cnt = 1;
	foreach ($matches as $match) {
		$haveGroupStage = true;
		$p1Name = 'X';
		$p2Name = 'X';
		$p1Name2 = 'X';
		$p2Name2 = 'X';
		$p1 = $match->{'player1'};
		if ($p1->{'bye'} == true) {
			// ignore games with a (or more BYEs)
			continue;
		} else if (isset($p1->{'playerListIdx'})) {
			$player1 = $response->{'result'}->{'players'}[$p1->{'playerListIdx'}];
			$p1Name = $player1->{'last_name'};
			$p1Name .= ' ' . $player1->{'first_name'};
			if (isset($player1->{'partner'})) {
				$p1Name2 = $player1->{'partner'}->{'last_name'};
				$p1Name2 .= ' ' . $player1->{'partner'}->{'first_name'};
			}
		}
		$p2 = $match->{'player2'};
		if ($p2->{'bye'} == true) {
			// ignore games with a (or more BYEs)
			continue;
		} else if (isset($p2->{'playerListIdx'})) {
			$player2 = $response->{'result'}->{'players'}[$p2->{'playerListIdx'}];
			$p2Name = $player2->{'last_name'};
			$p2Name .= ' ' . $player2->{'first_name'};
			if (isset($player2->{'partner'})) {
				$p2Name2 = $player2->{'partner'}->{'last_name'};
				$p2Name2 .= ' ' . $player2->{'partner'}->{'first_name'};
			}
		}
		$pdf->AddPage();
        	if ($tiny) {
            		fillInTiny($p1Name, $p1Name2, $p2Name, $p2Name2, $groupName, $catName);
        	} else {
            		fillIn($p1Name, $p1Name2, $p2Name, $p2Name2, $groupName, $catName);
        	}

		$totalCnt++;
	}
}

if (isset($response->{'result'}->{'ko'}) && (isset($response->{'result'}->{'ko'}->{'matches'}))) {
	$matches = $response->{'result'}->{'ko'}->{'matches'};
	if (isset($response->{'result'}->{'ko_b'}) && isset($response->{'result'}->{'ko_b'}->{'matches'}) && count($response->{'result'}->{'ko_b'}->{'matches'}) > 0) {
		$haveBko = true;
	}
	$cnt = 1;
	printKoMatches($matches, $haveBko ? 'A-' : '', !$haveGroupStage);
}

if (isset($response->{'result'}->{'ko_b'}) && isset($response->{'result'}->{'ko_b'}->{'matches'})) {
	$matches = $response->{'result'}->{'ko_b'}->{'matches'};
	$cnt = 1;
	printKoMatches($matches, 'B-', !$haveGroupStage);
}

$pdf->Output();
?>
