#!/bin/sh

# Clubs
# name: { type: String, required: true, default: 'Mooncrossers' },
# location: { type: String, required: true, default: 'Moon' },
# abbreviation: { type: String, required: false, default: 'Moonies' },
# nation: { type: String, required: true, default: 'GER' },
# region: { type: String, required: false, default: 'LS' },
# city: { type: String, required: false, default: 'Lunar' },
# established: { type: String, required: false, default: '1900-01-01' },
# responsible: { type: String, required: false, default: 'Man in the moon' },
# active: { type: Boolean, required: true, default: true },
# links: { type: [String], required: false, default: 'http://www.moon.de' },
# comments: { type: String, required: false, default: '' },
# logos: { type: [String], required: false },
# postal_address: { type: String, required: false, default: '3rd Crater right' },
# email: { type: String, required: true, default: 'contact@me.moon' },
# bank_account: { type: String, required: false, default: '' }

# ARRAY=('{"name":"TSV Poing Speedfires","location":"Poing","abbreviation":"Speedfires","nation":"GER","region":"LS","city":"Poing","established":"2013-01-11","responsible":"TSV Poing","links":["http://www.poingspeedfires.de/"],"logos":["http://www.poingspeedfires.de/wp-content/uploads/2015/12/cropped-header_img_big-1.jpg"],"postal_address":"Plieninger Str. 22, 85586 Poing","email":"crossminton@gmx.de"}')
ARRAY=('{"name":"*","location":"*","abbreviation":"","nation":"*","region":"","city":"","established":"","responsible":"","active":true,"links":[],"comments":"","logos":[],"postal_address":"","email":"*","bank_account":""}')

for i in "${ARRAY[@]}"; do
    echo "curl -H \"Content-Type: application/json\" -k --data '$i' https://localhost:62187/api/v1/clubs"

    curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YWQzNzRjOGU4NzY1OTQxMDg3NmRlYzYiLCJpYXQiOjE1MjM5NjUzMjh9.t1JOnfoW6GSrkCtUH0AtDzA_cdhUO2D15BGA5RA045Y" -k \
        -X POST "https://localhost:62187/api/v1/clubs" \
        --data @- <<END;
$i
END
    echo "\n"
done

#!/bin/sh
set -e
