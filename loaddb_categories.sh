#!/bin/sh

# Categories
# federation: { type: String, required: true, default: 'DCV' },
# name: { type: String, required: true, default: 'DCV Damen' },
# type: { type: String, required: true, default: 'singles' },
# gender: { type: number, required: true, default: 1 },
# minimum_age: { type: Number, required: true, default: 0 },
# maximum_age: { type: Number, required: true, default: 999 },

ARRAY=(
# '{"federation":"DCV","name":"DM Quali Damen","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":999}'
# '{"federation":"DCV","name":"DM Quali Open","cat_type":"singles","gender":0,"minimum_age":0,"maximum_age":999}'
# '{"federation":"DCV","name":"DM Quali Ü40 Damen","cat_type":"singles","gender":1,"minimum_age":40,"maximum_age":999}'
# '{"federation":"DCV","name":"DM Quali Ü50 Damen","cat_type":"singles","gender":1,"minimum_age":50,"maximum_age":999}'
# '{"federation":"DCV","name":"DM Quali Ü40 Herren","cat_type":"singles","gender":2,"minimum_age":40,"maximum_age":999}'
# '{"federation":"DCV","name":"DM Quali Ü50 Herren","cat_type":"singles","gender":2,"minimum_age":50,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Damen","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Open","cat_type":"singles","gender":0,"minimum_age":0,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Ü35 Damen","cat_type":"singles","gender":1,"minimum_age":35,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Ü40 Damen","cat_type":"singles","gender":1,"minimum_age":40,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Ü50 Damen","cat_type":"singles","gender":1,"minimum_age":50,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü60 Damen","cat_type":"singles","gender":1,"minimum_age":60,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü70 Damen","cat_type":"singles","gender":1,"minimum_age":70,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Ü40 Herren","cat_type":"singles","gender":2,"minimum_age":40,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Ü50 Herren","cat_type":"singles","gender":2,"minimum_age":50,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü60 Herren","cat_type":"singles","gender":2,"minimum_age":60,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü70 Herren","cat_type":"singles","gender":2,"minimum_age":70,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV U18 Männlich","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":18}'
# '{"federation":"DCV","name":"DCV U16 Männlich","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":16}'
# '{"federation":"DCV","name":"DCV U14 Männlich","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":14}'
# '{"federation":"DCV","name":"DCV U12 Männlich","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":12}'
# '{"federation":"DCV","name":"DCV U18 Weiblich","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":18}'
# '{"federation":"DCV","name":"DCV U16 Weiblich","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":16}'
# '{"federation":"DCV","name":"DCV U14 Weiblich","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":14}'
# '{"federation":"DCV","name":"DCV U12 Weiblich","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":12}'
# '{"federation":"DCV","name":"DCV Damen Doppel","cat_type":"doubles","gender":1,"minimum_age":0,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü40 Damen Doppel","cat_type":"doubles","gender":1,"minimum_age":40,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü50 Damen Doppel","cat_type":"doubles","gender":1,"minimum_age":50,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü60 Damen Doppel","cat_type":"doubles","gender":1,"minimum_age":60,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV Open Doppel","cat_type":"doubles","gender":0,"minimum_age":0,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü40 Open Doppel","cat_type":"doubles","gender":0,"minimum_age":40,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü50 Open Doppel","cat_type":"doubles","gender":0,"minimum_age":50,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü60 Open Doppel","cat_type":"doubles","gender":0,"minimum_age":60,"maximum_age":999}'
# '{"federation":"DCV","name":"DCV U18 Open Doppel","cat_type":"doubles","gender":0,"minimum_age":0,"maximum_age":18}'
# '{"federation":"DCV","name":"DCV U18 Weiblich Doppel","cat_type":"doubles","gender":1,"minimum_age":0,"maximum_age":18}'
# '{"federation":"DCV","name":"DCV Mixed Doppel","cat_type":"doubles","gender":3,"minimum_age":0,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü40 Mixed Doppel","cat_type":"doubles","gender":3,"minimum_age":40,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü50 Mixed Doppel","cat_type":"doubles","gender":3,"minimum_age":50,"maximum_age":999}'
'{"federation":"DCV","name":"DCV Ü60 Mixed Doppel","cat_type":"doubles","gender":3,"minimum_age":60,"maximum_age":999}'

# '{"federation":"ICO","name":"ICO Women","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO Open","cat_type":"singles","gender":0,"minimum_age":0,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO O35 Women","cat_type":"singles","gender":1,"minimum_age":35,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO O40 Women","cat_type":"singles","gender":1,"minimum_age":40,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO O50 Women","cat_type":"singles","gender":1,"minimum_age":50,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO O40 Men","cat_type":"singles","gender":2,"minimum_age":40,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO O50 Men","cat_type":"singles","gender":2,"minimum_age":50,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO U18 Male","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":18}'
# '{"federation":"ICO","name":"ICO U16 Male","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":16}'
# '{"federation":"ICO","name":"ICO U14 Male","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":14}'
# '{"federation":"ICO","name":"ICO U12 Male","cat_type":"singles","gender":2,"minimum_age":0,"maximum_age":12}'
# '{"federation":"ICO","name":"ICO U18 Female","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":18}'
# '{"federation":"ICO","name":"ICO U16 Female","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":16}'
# '{"federation":"ICO","name":"ICO U14 Female","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":14}'
# '{"federation":"ICO","name":"ICO U12 Female","cat_type":"singles","gender":1,"minimum_age":0,"maximum_age":12}'
# '{"federation":"ICO","name":"ICO Female Doubles","cat_type":"doubles","gender":1,"minimum_age":0,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO Open Doubles","cat_type":"doubles","gender":0,"minimum_age":0,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO Mixed Doubles","cat_type":"doubles","gender":3,"minimum_age":0,"maximum_age":999}'
# '{"federation":"ICO","name":"ICO U18 Open Doubles","cat_type":"doubles","gender":0,"minimum_age":0,"maximum_age":18}'
# '{"federation":"ICO","name":"ICO U18 Female Doubles","cat_type":"doubles","gender":1,"minimum_age":0,"maximum_age":18}'
)

for i in "${ARRAY[@]}"; do
    echo "curl -H \"Content-Type: application/json\" -k --data '$i' http://localhost:62187/api/v1/categories"

    curl -H "Content-Type: application/json" \
        -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YWQzNzRjOGU4NzY1OTQxMDg3NmRlYzYiLCJpYXQiOjE1MjQwNDE4MTV9.I9daOyIRHtYgL-67zrSK-3cVwJTZ1SV7HrrN3XZQlMQ" \
        -k \
        -X POST "http://localhost:62187/api/v1/categories" \
        --data @- <<END;
$i
END
    echo "\n"
done

# for i in "${ARRAY[@]}"; do
#     echo "curl -H \"Content-Type: application/json\" -k --data '$i' https://crossy.paul-holleis.de:62187/api/v1/categories"
#
#     curl -H "Content-Type: application/json" \
#         -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YWQzNzRjOGU4NzY1OTQxMDg3NmRlYzYiLCJpYXQiOjE1MjU3MjgwODR9.hhr0BqmdUI4QOevv94b5L5l2Xhm06EZWIYRnKNFLHHI" \
#         -k \
#         -X POST "https://crossy.paul-holleis.de:62187/api/v1/categories" \
#         --data @- <<END;
# $i
# END
#     echo "\n"
# done


#!/bin/sh
set -e
